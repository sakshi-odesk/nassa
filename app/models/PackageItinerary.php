<?php

class PackageItinerary extends Eloquent
{
    protected $fillable = array('package_id','day_num','destination','desc','image','weight','meal');
    
    public function package()
    {
        return $this->belongsTo('Package');
    }
    
    public function packageHotelDetail()
    {
        return $this->hasMany('PackageHotelDetail');
    }
}