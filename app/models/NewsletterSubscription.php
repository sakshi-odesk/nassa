<?php

class NewsletterSubscription extends Eloquent
{
    protected $fillable = array('email');
  
    protected $table = 'newsletter_subscription';
}