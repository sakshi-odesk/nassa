<?php

class PackagePriceSummary extends Eloquent
{
    protected $fillable = array('package_id','desc','cost','weight');
    
    public function package()
    {
        return $this->belongsTo('Package');
    }
}