<?php

class Page extends Eloquent
{
    protected $fillable = array('title','slug','page_image','body','user_id','published','seo_meta_desc','seo_meta_kw','seo_page_title');
    
    public static function PageChart()
    {
        $unpublished = self::where('published','=',null)->count();
        $published = self::whereNotNull('published')->count();
        
        $data = array();
        
        array_push($data,array('label'=>'Draft Pages','value'=>$unpublished));
        array_push($data,array('label'=>'Live Pages','value'=>$published));
        
        return json_encode($data);
    }
}