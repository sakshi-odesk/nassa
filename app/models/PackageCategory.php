<?php

class PackageCategory extends Eloquent
{
    protected $fillable = array('package_type_id','title','slug','main_image','body','seo_meta_desc','seo_meta_kw','seo_page_title','published');
    
    public function package_type()
    {
        return $this->belongsTo('PackageType');
    }
    
    public function package()
    {
        return $this->hasOne('Package');
    }
    
    public static function domesticPackages()
    {
       return DB::table('destinations')
                        ->join('packages','packages.id','=','destinations.min_package_id')
                        ->join('package_types','package_types.id','=','packages.package_type_id')
                        ->select('destinations.id','destinations.slug','destinations.label','packages.package_type_id',
                                 'packages.min_price','packages.main_image','packages.duration')
                        ->where('destinations.popular_destination','=',1)
                        ->where('package_types.id','=',1)
                        ->whereNotNull('packages.published')
                        ->whereNotNull('packages.min_price')
                        ->take(5)
                        ->get();
    }
    
    public static function internationalPackages()
    {
        return DB::table('destinations')
                        ->join('packages','packages.id','=','destinations.min_package_id')
                        ->join('package_types','package_types.id','=','packages.package_type_id')
                        ->select('destinations.id','destinations.slug','destinations.label','packages.package_type_id',
                                 'packages.min_price','packages.main_image','packages.duration')
                        ->where('destinations.popular_destination','=',1)
                        ->where('package_types.id','=',2)  
                        ->whereNotNull('packages.published')
                        ->whereNotNull('packages.min_price')
                        ->take(5)
                        ->get();
    }
}