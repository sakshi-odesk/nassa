<?php

class Blog extends Eloquent
{
    protected $fillable = array('blog_category_id','title','slug','body','blog_image','slider_title','slider_image','show_on_slider','published','user_id','seo_meta_desc',
                               'seo_meta_kw','seo_page_title');
    
    public static function BlogChart()
    {
        $unpublished = self::where('published','=',null)->count();
        $published = self::whereNotNull('published')->count();
        
        $data = array();
        
        array_push($data,array('label'=>'Draft Blogs','value'=>$unpublished));
        array_push($data,array('label'=>'Live Blogs','value'=>$published));
        
        return json_encode($data);
    }
}