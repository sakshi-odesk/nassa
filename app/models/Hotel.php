<?php

class Hotel extends Eloquent
{

    protected $fillable = array('title','slug','hotel_category','star_category','destination_id','user_id','location','price','main_image','special_offer','hotpick','recommended','body','seo_meta_desc','seo_meta_kw','seo_page_title');


    public static function HotelChart()
    {
        $unpublished = self::where('published','=',null)->count();
        $published = self::whereNotNull('published')->count();

        $data = array();

        array_push($data,array('label'=>'Draft Hotels','value'=>$unpublished));
        array_push($data,array('label'=>'Live Hotels','value'=>$published));

        return json_encode($data);   
    }

    public function hotel_images()
    {
        return $this->hasMany('HotelImage');
    }

    public static function gethotpick()
    {
        return DB::table('hotel_categories')
                     ->join('hotels','hotels.hotel_category','=','hotel_categories.id')
                     ->join('destinations','destinations.id','=','hotels.destination_id')
                     ->select('hotel_categories.title','hotels.title as hotel_title','hotel_categories.slug',
                              'hotel_categories.main_image','hotels.price','hotels.main_image',
                              'destinations.label')
                     ->where('hotels.hotpick','=',1)->whereNotNull('hotels.published')
                     ->get();

    }
}