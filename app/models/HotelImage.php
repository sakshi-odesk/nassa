<?php

class HotelImage extends Eloquent
{
    protected $fillable = array('hotel_id','path');
  
  
    public function hotel()
    {
       return $this->belongsTo('Hotel');
    }
}