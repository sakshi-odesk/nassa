<?php

class SlideShow extends Eloquent
{
    protected $fillable = array('title','published');
    
    public function slides()
    {
        return $this->hasMany('Slide');
    }
    
    public static function SlideShowChart()
    {
        $unpublished = self::where('published','=',null)->count();
        $published = self::whereNotNull('published')->count();
        
        $data = array();
        
        array_push($data,array('label'=>'Draft SlideShows','value'=>$unpublished));
        array_push($data,array('label'=>'Live SlideShows','value'=>$published));
        
        return json_encode($data);
    }
}