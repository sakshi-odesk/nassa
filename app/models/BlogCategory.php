<?php

class BlogCategory extends Eloquent
{
    protected $fillable = array('title','slug','desc');
}