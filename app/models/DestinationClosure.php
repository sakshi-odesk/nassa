<?php 

class destinationClosure extends \Franzose\ClosureTable\Models\ClosureTable implements destinationClosureInterface {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'destination_closure';
}