<?php

class Slide extends Eloquent
{
    protected $fillable = array('slide_show_id','title','url','main_image');
    
    public function slide_show()
    {
        return $this->belongsTo('SlideShow');
    }
}