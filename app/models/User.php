<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
  
    protected $fillable = array('email','password','role','active','packages_access','hotels_access', 'created_by');
  
    
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    /*
     * Static function for updating user last login details.
     * 
     * 
     */
    public static function updateLastLogin($email)
	{
		DB::table('users')
		->where('email', $email)
		->update(array('last_login' => DB::raw('now()')));		
	}
    
    public function pages()
	{
		return $this->hasMany('Page');
    }
    
    public function blogs()
	{
		return $this->hasMany('Blog');
	}
  
}
