<?php

class Facility extends Eloquent
{
    protected $fillable = array('title','main_image');
    
    public function package_facility()
    {
        return $this->hasOne('PackageFacility');
    }
}