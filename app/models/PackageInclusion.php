<?php

class PackageInclusion extends Eloquent
{
    protected $fillable = array('package_id','title','desc','weight');
    
    public function package()
    {
        return $this->belongsTo('Package');
    }
}