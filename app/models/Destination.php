<?php 

class destination extends \Franzose\ClosureTable\Models\Entity implements destinationInterface {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'destinations';
    
    public $timestamps = true;
    /**
     * ClosureTable model instance.
     *
     * @var destinationClosure
     */
    protected $closure = '\destinationClosure';
    
    /**
     * fillable fields
     * 
     * @var array
     */
    protected $fillable = array('label','slug','type','pin','city','state','popular_destination','country','num_children','seo_meta_desc','seo_meta_kw','seo_page_title','published');
 
    
    public static function getPopularDestinationIds() 
    {
        $destinations = DB::table('destinations')
                                ->where('popular_destination','=',1)->get();
        
        $destinations_ids = array();
        
         foreach ($destinations as $destination) {
             
                array_push($destinations_ids, $destination->id);

                $dest = Destination::find($destination->id);
             
                $descendants = $dest->getDescendants();

                if (isset($descendants)) {
                    
                    foreach ($descendants as $descendant) {
                        
                        array_push($destinations_ids, $descendant->id);
                        
                        $dest_child = Destination::find($destination->id);
                        
                        $childs = $dest_child->getDescendants();

                        if (isset($childs)) {
                            
                            foreach ($childs as $child) {
                                
                                array_push($destinations_ids, $child->id);
                        }
                    }
                }
            }
        }
        
        return array_unique($destinations_ids);
    }
    
    public static function getChild($destinations)
    {
       $destinations_ids = array();    
        
       array_push($destinations_ids, $destinations->id);     
        
       $descendants = $destinations->getDescendants();

        if (isset($descendants))
        {
            foreach ($descendants as $descendant) 
            {
                array_push($destinations_ids, $descendant->id);

                $dest_child = Destination::find($descendant->id);

                $childs = $dest_child->getDescendants();

                if (isset($childs)) 
                {
                    foreach ($childs as $child) 
                    {

                        array_push($destinations_ids, $child->id);
                    }
                }
            }
        }
        
        return array_unique($destinations_ids);
    }
    
    public static function updateMinPackage($destination)
    {
        $destination_ids = Destination::getChild($destination);
                
        $destination_packages = Package::join('package_itineraries','packages.id','=','package_itineraries.package_id')
                                        ->select('packages.id','packages.min_price')
                                        ->whereIn('package_itineraries.destination_id',$destination_ids)
                                        ->whereNotNull('packages.published')    
                                        ->whereNotNull('packages.min_price')    
                                        ->orderBy('min_price')
                                        ->take(1)
                                        ->get();
        
        $destination->min_package_id = $destination_packages[0]->id;
        $destination->save();
    }
    
}