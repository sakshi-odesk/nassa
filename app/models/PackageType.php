<?php

class PackageType extends Eloquent
{
    protected $fillable = array('title');
    
    public function packagecategory()
    {
        return $this->hasOne('PackageCategory');
    }
}