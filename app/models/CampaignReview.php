<?php

class CampaignReview extends Eloquent
{
    protected $fillable = array('campaign_id','desc','reviewer');
    
    public function campaign()
    {
        return $this->belongsTo('Campaign');
    }
}