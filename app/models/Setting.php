<?php

class Setting extends Eloquent
{
    protected $fillable = array('name','desc','value','type');
    
    /**
     * Function for serializing a setting value before creating and updating.
     * 
     * 
     */
    
    public static function boot()
    {
        parent::boot();

        Setting::creating(function($setting)
        {
            $setting->value = serialize($setting->value);

        });
        
        Setting::updating(function($setting)
        {
            $setting->value = serialize($setting->value);

        });
    }
    
    /**
     * Function for load settings.
     * take setting name as argument.
     * 
     * @return setting details.
     */
    public static function get($name)
    {
        $setting = Setting::where('name',$name)->first();
        
        $setting = unserialize($setting->value);   
        
        return $setting;
    }
}