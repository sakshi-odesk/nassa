<?php

class Feedback extends Eloquent
{
    protected $fillable = array('name','email','phone','message','ip','published');
}