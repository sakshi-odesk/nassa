<?php

class PackageHotelDetail extends Eloquent
{
    protected $fillable = array('package_id','destination','destination_id','package_itinerary_id','hotel_id','hotel','meal_plan','weight','start_date','end_date','category','price');
    
    public function package()
    {
        return $this->belongsTo('Package');
    }
    
    public function packageItinerary()
    {
        return $this->belongsTo('PackageItinerary');
    }
}