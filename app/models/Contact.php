<?php

class Contact extends Eloquent
{
    protected $fillable = array('name','email','phoneno','message');
}