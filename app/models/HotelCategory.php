<?php

class HotelCategory extends Eloquent
{
    protected $fillable = array('title','slug','main_image');
}