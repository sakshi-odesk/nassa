<?php

class Package extends Eloquent
{
    protected $fillable = array('package_category_id',
                                'package_type_id',
                                'destination_id',
                                'user_id',
                                'hotel_level',
                                'title',
                                'slug',
                                'price',
                                'min_price',
                                'default_price',
                                'default_discounted_price',
                                'desc',
                                'discounted_price',
                                'route_map',
                                'duration',
                                'special_offer',
                                'hotpick',
                                'terms_conditions',
                                'policy',
                                'faq',
                                'recommended',
                                'main_image',
                                'seo_meta_desc',
                                'seo_meta_kw',
                                'seo_page_title',
                                'published');
    
    public function package_itineraries()
    {
        return $this->hasMany('PackageItinerary');
    }
    
    public function package_inclusions()
    {
        return $this->hasMany('PackageInclusion');
    }
    
    public function package_price_summaries()
    {
        return $this->hasMany('PackagePriceSummary');
    }
    
    public function package_hotel_details()
    {
        return $this->hasMany('PackageHotelDetail');
    }
    
    public function package_facilities()
    {
        return $this->hasMany('PackageFacility');
    }
    
    public function package_category()
    {
        return $this->belongsTo('PackageCategory');
    }
    
    public static function PackageChart()
    {
        $unpublished = self::where('published','=',null)->count();
        $published = self::whereNotNull('published')->count();
        
        $data = array();
        
        array_push($data,array('label'=>'Draft Packages','value'=>$unpublished));
        array_push($data,array('label'=>'Live Packages','value'=>$published));
        
        return json_encode($data);
    }
  
  
  public static function getHotPick($packagetype)
  {
       return DB::table('packages')
         ->join('package_types','package_types.id','=','packages.package_type_id')
         ->select(array('packages.title','packages.slug','packages.main_image','package_types.title as package_type',
                        'packages.duration','packages.min_price'))
         ->where('packages.hotpick','=',1)
         ->where('package_types.id','=',$packagetype)
         ->whereNotNull('packages.published')
         ->get();
      

  }
    
  public static function  package_hotel_details_formatted($package_hotel_details)
  {
       $hotels_details = array();

      foreach($package_hotel_details as $hotels)
        {
            if(!isset($hotels_details[$hotels->destination_id]))
            {
                if(!isset($hotels_details[$hotels->destination_id]['destination']))
                {
                    $hotels_details[$hotels->destination_id]['destination'] = array();

                    $destination = Destination::find($hotels->destination_id);

                    array_push($hotels_details[$hotels->destination_id]['destination'],$destination);
                }

                if(!isset($hotels_details[$hotels->destination_id]['hotels']))
                {
                    $hotels_details[$hotels->destination_id]['hotels'] = array();

                    array_push($hotels_details[$hotels->destination_id]['hotels'],(array) $hotels);
                }
            }
            else
            {
               array_push($hotels_details[$hotels->destination_id]['hotels'],(array) $hotels);
            }
        }
      
        return $hotels_details;
  
  }
  
}