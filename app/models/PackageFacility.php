<?php

class PackageFacility extends Eloquent
{
    protected $fillable = array('package_id','facility_id','weight');
    
    public function package()
    {
        return $this->belongsTo('Package');
    }
    
    public function facility()
    {
        return $this->belongsTo('Facility');
    }
}