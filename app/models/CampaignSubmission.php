<?php

class CampaignSubmission extends Eloquent
{
    protected $fillable = array('campaign_id','name','phone_no','email','best_time_to_call','subscribe','referer','request','user_agent','ip');
    
    public function campaign()
    {
        return $this->belongsTo('Campaign');
    }
}