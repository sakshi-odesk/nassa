<?php

class Campaign extends Eloquent
{
    protected $fillable = array('title','slug','heading','sub_heading','desc','inclusion','background_image','redirect','published','slider');
    
    public function campaign_submissions()
    {
        return $this->hasMany('CampaignSubmission');
    }
    
    public function campaign_reviews()
    {
        return $this->hasMany('CampaignReview');
    }
    
    public static function CampaignChart()
    {
        $campaigns = Campaign::all();
        
        $data = array();
        
        foreach($campaigns as $campaign)
        {
           $count =  $campaign->campaign_submissions()->count();
            array_push($data,array('label'=>$campaign->title,'value'=>$count));
        }
        
        return json_encode($data);
        
    }
}