<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Start HomeController Routes */

Route::get('/',array('as' => 'home', 'uses' => 'HomeController@getIndex'));

Route::get('/flights' , array('as' => 'flights', 'uses' => 'HomeController@getFlights'));

Route::get('/hotelsearch' , array('as' => 'hotelsearch', 'uses' => 'HomeController@getHotelSearch'));

Route::get('/cars-volvos' , array('as' => 'cars-volvos', 'uses' => 'HomeController@getCarsVolvos'));

Route::get('/special-offers' , array('as' => 'special-offers', 'uses' => 'HomeController@getSpecialOffers'));

Route::get('/testimonials' , array('as' => 'testimonials', 'uses' => 'HomeController@getTestimonials'));

Route::get('/contact',array('as'=>'contactus','uses'=>'HomeController@getContact'));

Route::post('/contact',array('uses'=>'HomeController@postContact'));

Route::get('/aboutus' , array('as' => 'aboutus', 'uses' => 'HomeController@getAboutUs'));

Route::get('/blogs' , array('as' => 'blog', 'uses' => 'HomeController@getBlog'));

Route::get('/policy' , array('as' => 'policy', 'uses' => 'HomeController@getPolicy'));

Route::get('/term' , array('as' => 'term', 'uses' => 'HomeController@getTerm'));

Route::post('/feedback-enquiry',array('uses'=>'HomeController@postFeedbackEnquiry'));


Route::get('/flight-search',array('as'=>'flight-search','uses'=>'HomeController@postFlightSearch'));

Route::post('/transport-enquiry',array('as'=>'transport-enquiry','uses'=>'HomeController@postTransportSearch'));

Route::post('newslettersubscription',array('uses'=>'HomeController@postNewsletterSubscription'));

Route::resource('blog','BlogController');

Route::get('/blog/v/{slug}',array('as' => 'blog-view','uses' => 'BlogController@getBlogView'));


/* End HomeController Routes */


/* Start PackageCategoryController Routes */

Route::get('/domestic-packages' , array('as' => 'domestic', 'uses' => 'PackageCategoryController@getDomestic'));

Route::get('/international-packages' , array('as' => 'international', 'uses' => 'PackageCategoryController@getInternational'));

Route::get('/holidays/destination/{destination_slug}',
           array('as' => 'destinations-packages', 'uses' => 'PackageCategoryController@getDestinationsPackages'));

Route::get('packages/search',array('as' => 'packages-search','uses' => 'PackageCategoryController@getSearchPackage'));

Route::get('/get/destination',array('as' => 'destination-label','uses' => 'PackageCategoryController@getDestination'));

Route::get('/get/hotel',array('as' => 'hotel-label','uses' => 'PackageCategoryController@getHotel'));

Route::get('package-listing/search/price',array('as' => 'search-listing-price','uses' =>'PackageCategoryController@getPriceSearch'));

Route::get('package-listing/search/category',array('as' => 'search-listing-category','uses' =>'PackageCategoryController@getPackageCategorySearch'));



/* End PackageCategoryController Routes */




/* Start PackageController Routes */

Route::get('/packages/{pacakge_slug}' , 
           array('as' => 'packages-details', 'uses' => 'PackageController@getPackageDetails'));

//Route::get('/packages/{package_slug}/{package_title}' , 
//           array('as' => 'international-packages-details', 'uses' => 'PackageController@getInternationalPackageDetails'));

//Route::post('booknow',array('uses'=>'PackageController@postBookNow'));

Route::get('package/hotel/search',array('as' => 'hotel-search','uses' => 'PackageController@PackageHotelSearch'));

Route::get('package/category/search',array('as' => 'category-search','uses' => 'PackageController@getPackageCategorySearch'));

Route::get('package/book/{id}',array('as'=>'package-book','uses'=>'PackageController@getBookNow'));

Route::post('package/book/post',array('as'=>'package-book-post','uses'=>'PackageController@postBookNow'));


Route::get('package/add/hotpicks/{id}',array('as' => 'package-add-hotpicks','uses' => 'PackageController@getPackageAddHotpicks'));

Route::get('package/remove/hotpicks/{id}',array('as' => 'package-remove-hotpicks','uses' => 'PackageController@getPackageRemoveHotpicks'));


Route::get('hotel/add/hotpicks/{id}',array('as' => 'hotel-add-hotpicks','uses' => 'HotelCategoryController@getHotelAddHotpicks'));

Route::get('hotel/remove/hotpicks/{id}',array('as' => 'hotel-remove-hotpicks','uses' => 'HotelCategoryController@getHotelRemoveHotpicks'));


/* End PackageCategoryController Routes */



/*  Start User Login and Logout  */

Route::get('user/login', 
    array('as' => 'user.login', 'uses' => 'UserController@getLogin'));

Route::post('user/login', array('uses' => 'UserController@postLogin'));

Route::get('user/logout', 
    array('as' => 'user.logout', 'uses' => 'UserController@getLogout'));

Route::controller('password', 'RemindersController');

/*  End User Login and Logout  */


/* Start Dashboard Routes */

Route::get('dashboard', 
    array('as' => 'dashboard', 'uses' => 'DashboardController@getIndex'));

Route::get('dashboard/blog-category/list', 
    array('as' => 'dashboard.blog-category.list', 'uses' => 'DashboardController@getBlogCategoryList'));

Route::get('dashboard/blog-category/form', 
    array('as' => 'dashboard.blog-category.form', 'uses' => 'DashboardController@getBlogCategoryAdd'));

Route::post('dashboard/blog-category/form', array('uses' => 'DashboardController@postBlogCategoryAdd'));

Route::get('dashboard/blog-category/edit/{id}', 
    array('as' => 'dashboard.blog-category.edit', 'uses' => 'DashboardController@getBlogCategoryEdit'));

Route::post('dashboard/blog-category/edit', array('uses' => 'DashboardController@postBlogCategoryEdit'));

Route::get('dashboard/blog-category/delete/{id}', 
    array('as' => 'dashboard.blog-category.delete', 'uses' => 'DashboardController@getBlogCategoryDelete'));

Route::post('dashboard/blog-category/delete', array('uses' => 'DashboardController@postBlogCategoryDelete'));

Route::get('dashboard/blog-category/cloning/{id}', 
    array('as' => 'dashboard.blog-category.cloning', 'uses' => 'DashboardController@getBlogCategoryCloning'));

Route::post('dashboard/blog-category/cloning', array('uses' => 'DashboardController@postBlogCategoryCloning'));


Route::get('dashboard/page/list', 
    array('as' => 'dashboard.page.list', 'uses' => 'DashboardController@getPageList'));

Route::get('dashboard/page/form', 
    array('as' => 'dashboard.page.form', 'uses' => 'DashboardController@getPageAdd'));

Route::post('dashboard/page/form', array('uses' => 'DashboardController@postPageAdd'));

Route::get('dashboard/page/edit/{id}', 
    array('as' => 'dashboard.page.edit', 'uses' => 'DashboardController@getPageEdit'));

Route::post('dashboard/page/edit', array('uses' => 'DashboardController@postPageEdit'));

Route::get('dashboard/page/delete/{id}', 
    array('as' => 'dashboard.page.delete', 'uses' => 'DashboardController@getPageDelete'));

Route::post('dashboard/page/delete', array('uses' => 'DashboardController@postPageDelete'));

Route::get('dashboard/page/cloning/{id}', 
    array('as' => 'dashboard.page.cloning', 'uses' => 'DashboardController@getPageCloning'));

Route::post('dashboard/page/cloning', array('uses' => 'DashboardController@postPageCloning'));



Route::get('dashboard/blog/list', 
    array('as' => 'dashboard.blog.list', 'uses' => 'DashboardController@getBlogList'));

Route::get('dashboard/blog/form', 
    array('as' => 'dashboard.blog.form', 'uses' => 'DashboardController@getBlogAdd'));

Route::post('dashboard/blog/form', array('uses' => 'DashboardController@postBlogAdd'));

Route::get('dashboard/blog/edit/{id}', 
    array('as' => 'dashboard.blog.edit', 'uses' => 'DashboardController@getBlogEdit'));

Route::post('dashboard/blog/edit', array('uses' => 'DashboardController@postBlogEdit'));

Route::get('dashboard/blog/delete/{id}', 
    array('as' => 'dashboard.blog.delete', 'uses' => 'DashboardController@getBlogDelete'));

Route::post('dashboard/blog/delete', array('uses' => 'DashboardController@postBlogDelete'));

Route::get('dashboard/blog/cloning/{id}', 
    array('as' => 'dashboard.blog.cloning', 'uses' => 'DashboardController@getBlogCloning'));

Route::post('dashboard/blog/cloning', array('uses' => 'DashboardController@postBlogCloning'));


Route::get('dashboard/hotel-category/list', 
    array('as' => 'dashboard.hotel-category.list', 'uses' => 'DashboardController@getHotelCategoryList'));

Route::get('dashboard/hotel-category/form', 
    array('as' => 'dashboard.hotel-category.form', 'uses' => 'DashboardController@getHotelCategoryAdd'));

Route::post('dashboard/hotel-category/form', array('uses' => 'DashboardController@postHotelCategoryAdd'));

Route::get('dashboard/hotel-category/edit/{id}', 
    array('as' => 'dashboard.hotel-category.edit', 'uses' => 'DashboardController@getHotelCategoryEdit'));

Route::post('dashboard/hotel-category/edit', array('uses' => 'DashboardController@postHotelCategoryEdit'));

Route::get('dashboard/hotel-category/delete/{id}', 
    array('as' => 'dashboard.hotel-category.delete', 'uses' => 'DashboardController@getHotelCategoryDelete'));

Route::post('dashboard/hotel-category/delete', array('uses' => 'DashboardController@postHotelCategoryDelete'));

Route::get('dashboard/hotel-category/cloning/{id}', 
    array('as' => 'dashboard.hotel-category.cloning', 'uses' => 'DashboardController@getHotelCategoryCloning'));

Route::post('dashboard/hotel-category/cloning', array('uses' => 'DashboardController@postHotelCategoryCloning'));


Route::get('dashboard/hotel/list', 
    array('as' => 'dashboard.hotel.list', 'uses' => 'DashboardController@getHotelList'));

Route::get('dashboard/hotel/form', 
    array('as' => 'dashboard.hotel.form', 'uses' => 'DashboardController@getHotelAdd'));

Route::post('dashboard/hotel/form', array('uses' => 'DashboardController@postHotelAdd'));

Route::get('dashboard/hotel/edit/{id}', 
    array('as' => 'dashboard.hotel.edit', 'uses' => 'DashboardController@getHotelEdit'));

Route::post('dashboard/hotel/edit', array('uses' => 'DashboardController@postHotelEdit'));

Route::get('dashboard/hotel/delete/{id}', 
    array('as' => 'dashboard.hotel.delete', 'uses' => 'DashboardController@getHotelDelete'));

Route::post('dashboard/hotel/delete', array('uses' => 'DashboardController@postHotelDelete'));

Route::get('dashboard/hotel/cloning/{id}', 
    array('as' => 'dashboard.hotel.cloning', 'uses' => 'DashboardController@getHotelCloning'));

Route::post('dashboard/hotel/cloning', array('uses' => 'DashboardController@postHotelCloning'));

Route::get('dashboard/hotel-image/delete/{id}', 
    array('as' => 'dashboard.hotel-image.delete', 'uses' => 'DashboardController@getHotelImageDelete'));


Route::get('dashboard/package-category/list', 
    array('as' => 'dashboard.package-category.list', 'uses' => 'DashboardController@getPackageCategoryList'));

Route::get('dashboard/package-category/form', 
    array('as' => 'dashboard.package-category.form', 'uses' => 'DashboardController@getPackageCategoryAdd'));

Route::post('dashboard/package-category/form', array('uses' => 'DashboardController@postPackageCategoryAdd'));

Route::get('dashboard/package-category/edit/{id}', 
    array('as' => 'dashboard.package-category.edit', 'uses' => 'DashboardController@getPackageCategoryEdit'));

Route::post('dashboard/package-category/edit', array('uses' => 'DashboardController@postPackageCategoryEdit'));

Route::get('dashboard/package-category/delete/{id}', 
    array('as' => 'dashboard.package-category.delete', 'uses' => 'DashboardController@getPackageCategoryDelete'));

Route::post('dashboard/package-category/delete', array('uses' => 'DashboardController@postPackageCategoryDelete'));

Route::get('dashboard/package-category/cloning/{id}', 
    array('as' => 'dashboard.package-category.cloning', 'uses' => 'DashboardController@getPackageCategoryCloning'));

Route::post('dashboard/package-category/cloning', array('uses' => 'DashboardController@postPackageCategoryCloning'));


Route::get('dashboard/package/list', 
    array('as' => 'dashboard.package.list', 'uses' => 'DashboardController@getPackageList'));

Route::get('dashboard/package/form', 
    array('as' => 'dashboard.package.form', 'uses' => 'DashboardController@getPackageAdd'));

Route::post('dashboard/package/form', array('uses' => 'DashboardController@postPackageAdd'));

Route::get('dashboard/package/edit/{id}', 
    array('as' => 'dashboard.package.edit', 'uses' => 'DashboardController@getPackageEdit'));

Route::post('dashboard/package/edit', array('uses' => 'DashboardController@postPackageEdit'));

Route::get('dashboard/package/delete/{id}', 
    array('as' => 'dashboard.package.delete', 'uses' => 'DashboardController@getPackageDelete'));

Route::post('dashboard/package/delete', array('uses' => 'DashboardController@postPackageDelete'));

Route::get('dashboard/package/cloning/{id}', 
    array('as' => 'dashboard.package.cloning', 'uses' => 'DashboardController@getPackageCloning'));

Route::post('dashboard/package/cloning', array('uses' => 'DashboardController@postPackageCloning'));


Route::get('dashboard/slide-show/list', 
    array('as' => 'dashboard.slide-show.list', 'uses' => 'DashboardController@getSlideShowList'));

Route::get('dashboard/slide-show/form', 
    array('as' => 'dashboard.slide-show.form', 'uses' => 'DashboardController@getSlideShowAdd'));

Route::post('dashboard/slide-show/form', array('uses' => 'DashboardController@postSlideShowAdd'));

Route::get('dashboard/slide-show/edit/{id}', 
    array('as' => 'dashboard.slide-show.edit', 'uses' => 'DashboardController@getSlideShowEdit'));

Route::post('dashboard/slide-show/edit', array('uses' => 'DashboardController@postSlideShowEdit'));

Route::get('dashboard/slide-show/delete/{id}', 
    array('as' => 'dashboard.slide-show.delete', 'uses' => 'DashboardController@getSlideShowDelete'));

Route::post('dashboard/slide-show/delete', array('uses' => 'DashboardController@postSlideShowDelete'));

Route::get('dashboard/slide-show/cloning/{id}', 
    array('as' => 'dashboard.slide-show.cloning', 'uses' => 'DashboardController@getSlideShowCloning'));

Route::post('dashboard/slide-show/cloning', array('uses' => 'DashboardController@postSlideShowCloning'));


Route::get('dashboard/campaign/list',
    array('as' => 'dashboard.campaign.list', 'uses' => 'DashboardController@getCampaignList'));

Route::get('dashboard/campaign/form', 
    array('as' => 'dashboard.campaign.form', 'uses' => 'DashboardController@getCampaignAdd'));

Route::post('dashboard/campaign/form', array('uses' => 'DashboardController@postCampaignAdd'));

Route::get('dashboard/campaign/edit/{id}', 
    array('as' => 'dashboard.campaign.edit', 'uses' => 'DashboardController@getCampaignEdit'));

Route::post('dashboard/campaign/edit', array('uses' => 'DashboardController@postCampaignEdit'));

Route::get('dashboard/campaign/delete/{id}', 
    array('as' => 'dashboard.campaign.delete', 'uses' => 'DashboardController@getCampaignDelete'));

Route::post('dashboard/campaign/delete', array('uses' => 'DashboardController@postCampaignDelete'));

Route::get('dashboard/campaign/cloning/{id}', 
    array('as' => 'dashboard.campaign.cloning', 'uses' => 'DashboardController@getCampaignCloning'));

Route::post('dashboard/campaign/cloning', array('uses' => 'DashboardController@postCampaignCloning'));

Route::get('dashboard/campaign/submission/list',
    array('as' => 'dashboard.campaign.submission-list', 'uses' => 'DashboardController@getCampaignSubmissionList'));

Route::get('dashboard/campaign/submission/{id}',
    array('as' => 'dashboard.campaign.submission', 'uses' => 'DashboardController@getCampaignSubmission'));

Route::get('dashboard/campaign/submission/export/{id}',
    array('as' => 'dashboard.campaign.submission-export', 'uses' => 'DashboardController@getCampaignSubmissionExport'));

Route::get('dashboard/setting/list',
    array('as' => 'dashboard.setting.list', 'uses' => 'DashboardController@getSettingList'));

Route::get('dashboard/setting/form', 
    array('as' => 'dashboard.setting.form', 'uses' => 'DashboardController@getSettingAdd'));

Route::post('dashboard/setting/form', array('uses' => 'DashboardController@postSettingAdd'));

Route::get('dashboard/setting/edit/{id}', 
    array('as' => 'dashboard.setting.edit', 'uses' => 'DashboardController@getSettingEdit'));

Route::post('dashboard/setting/edit', array('uses' => 'DashboardController@postSettingEdit'));

Route::get('dashboard/setting/cloning/{id}', 
    array('as' => 'dashboard.setting.cloning', 'uses' => 'DashboardController@getSettingCloning'));

Route::post('dashboard/setting/cloning', array('uses' => 'DashboardController@postSettingCloning'));

Route::get('dashboard/setting/delete/{id}', 
    array('as' => 'dashboard.setting.delete', 'uses' => 'DashboardController@getSettingDelete'));

Route::post('dashboard/setting/delete', array('uses' => 'DashboardController@postSettingDelete'));



//admin listing

Route::get('dashboard/admin/form', array('as' => 'dashboard.admin.form', 'uses' => 'DashboardController@getAdminFormAdd'));

Route::post('dashboard/admin/form', array('uses' => 'DashboardController@postAdminFormAdd'));

Route::get('dashboard/admin/list', array('as' => 'dashboard.admin.list', 'uses' => 'DashboardController@getAdminList'));

Route::get('dashboard/users/admin', array('as'=>'dashboard.users.admin', 'uses' => 'DashboardController@getAdminAdd'));

Route::get('dashboard/users/admin/{id}/edit', array('as' => 'dashboard.admin.edit', 'uses' => 'DashboardController@getAdminEdit'));

Route::post('dashboard/users/admin/edit', array('uses' => 'DashboardController@postAdminEdit'));

Route::get('dashboard/users/admin/{id}/delete', array('as' => 'dashboard.admin.delete', 'uses' => 'DashboardController@getAdminDelete'));

Route::post('dashboard/users/admin/delete', array('uses' => 'DashboardController@postAdminDelete'));

Route::get('dashboard/users/admin/{id}/access/profile', array('as' => 'dashboard.admin.profile', 'uses' => 'DashboardController@getAdminAccess'));

Route::post('dashboard/users/admin/access/profile', array('uses' => 'DashboardController@postAdminAccess'));



//Supplier listing

Route::get('dashboard/supplier/form', array('as' => 'dashboard.supplier.form', 'uses' => 'DashboardController@getSupplierFormAdd'));

Route::post('dashboard/supplier/form', array('uses' => 'DashboardController@postSupplierFormAdd'));


Route::get('dashboard/supplier/list', array('as' => 'dashboard.supplier.list', 'uses' => 'DashboardController@getSupplierList'));

Route::get('dashboard/users/supplier', array('as'=>'dashboard.users.supplier', 'uses' => 'DashboardController@getSupplierAdd'));

Route::get('dashboard/users/supplier/{id}/edit', array('as' => 'dashboard.supplier.edit', 'uses' => 'DashboardController@getSupplierEdit'));

Route::post('dashboard/users/supplier/edit', array('uses' => 'DashboardController@postSupplierEdit'));

Route::get('dashboard/users/supplier/{id}/delete', array('as' => 'dashboard.supplier.delete', 'uses' => 'DashboardController@getSupplierDelete'));

Route::post('dashboard/users/supplier/delete', array('uses' => 'DashboardController@postSupplierDelete'));

Route::get('dashboard/users/supplier/{id}/access/profile', array('as' => 'dashboard.supplier.profile', 'uses' => 'DashboardController@getSupplierAccess'));

Route::post('dashboard/users/supplier/access/profile', array('uses' => 'DashboardController@postSupplierAccess'));



// All users listing
Route::get('dashboard/allusers/form', array('as' => 'dashboard.allusers.form', 'uses' => 'DashboardController@getAllusersFormAdd'));

Route::post('dashboard/allusers/form', array('uses' => 'DashboardController@postAllusersFormAdd'));


Route::get('dashboard/allusers/list', array('as' => 'dashboard.allusers.list', 'uses' => 'DashboardController@getAllUsersList'));

Route::get('dashboard/users/allusers', array('as'=>'dashboard.users.allusers', 'uses' => 'DashboardController@getAllUsersAdd'));

Route::get('dashboard/users/allusers/{id}/edit', array('as' => 'dashboard.allusers.edit', 'uses' => 'DashboardController@getAllUsersEdit'));

Route::post('dashboard/users/allusers/edit', array('uses' => 'DashboardController@postAllUsersEdit'));

Route::get('dashboard/users/allusers/{id}/delete', array('as' => 'dashboard.allusers.delete', 'uses' => 'DashboardController@getAllUsersDelete'));

Route::post('dashboard/users/allusers/delete', array('uses' => 'DashboardController@postAllusersDelete'));

Route::get('dashboard/users/allusers/{id}/access/profile', array('as' => 'dashboard.allusers.profile', 'uses' => 'DashboardController@getAllusersAccess'));

Route::post('dashboard/users/allusers/access/profile', array('uses' => 'DashboardController@postAllusersAccess'));





Route::get('dashboard/destinations', 
    array('as' => 'dashboard.destination.view', 'uses' => 'DashboardController@getDestinationView'));

Route::get('dashboard/destinations/add-child/{id}', 
    array('as' => 'dashboard.destination.add-child', 'uses' => 'DashboardController@getDestinationAddChild'));

Route::post('dashboard/destinations/add-child', array('uses' => 'DashboardController@postDestinationAddChild'));

Route::get('dashboard/destinations/add-sibling/{id}', 
    array('as' => 'dashboard.destination.add-sibling', 'uses' => 'DashboardController@getDestinationAddSibling'));

Route::post('dashboard/destinations/add-sibling', array('uses' => 'DashboardController@postDestinationAddSibling'));

Route::get('dashboard/destinations/edit/{id}', 
    array('as' => 'dashboard.destination.edit', 'uses' => 'DashboardController@getDestinationEdit'));

Route::post('dashboard/destinations/edit', array('uses' => 'DashboardController@postDestinationEdit'));

Route::get('dashboard/destinations/delete/{id}', 
    array('as' => 'dashboard.destination.delete', 'uses' => 'DashboardController@getDestinationDelete'));

Route::get('dashboard/destinations/tree', 
    array('as' => 'dashboard.destination.tree', 'uses' => 'DashboardController@getDestinationTree'));

Route::get('dashboard/facility/list',
    array('as' => 'dashboard.facility.list', 'uses' => 'DashboardController@getFacilityList'));

Route::get('dashboard/facility/form', 
    array('as' => 'dashboard.facility.form', 'uses' => 'DashboardController@getFacilityAdd'));

Route::post('dashboard/facility/form', array('uses' => 'DashboardController@postFacilityAdd'));

Route::get('dashboard/facility/edit/{id}', 
    array('as' => 'dashboard.facility.edit', 'uses' => 'DashboardController@getFacilityEdit'));

Route::post('dashboard/facility/edit', array('uses' => 'DashboardController@postFacilityEdit'));

Route::get('dashboard/facility/delete/{id}', 
    array('as' => 'dashboard.facility.delete', 'uses' => 'DashboardController@getFacilityDelete'));

Route::post('dashboard/facility/delete', array('uses' => 'DashboardController@postFacilityDelete'));

Route::get('dashboard/facility/cloning/{id}', 
    array('as' => 'dashboard.facility.cloning', 'uses' => 'DashboardController@getFacilityCloning'));

Route::post('dashboard/facility/cloning', array('uses' => 'DashboardController@postFacilityCloning'));

Route::get('dashboard/feedback/list',
    array('as' => 'dashboard.feedback.list', 'uses' => 'DashboardController@getFeedbackList'));

Route::get('dashboard/feedback/form', 
    array('as' => 'dashboard.feedback.form', 'uses' => 'DashboardController@getFeedbackAdd'));

Route::post('dashboard/feedback/form', array('uses' => 'DashboardController@postFeedbackAdd'));

Route::get('dashboard/feedback/edit/{id}', 
    array('as' => 'dashboard.feedback.edit', 'uses' => 'DashboardController@getFeedbackEdit'));

Route::post('dashboard/feedback/edit', array('uses' => 'DashboardController@postFeedbackEdit'));

Route::get('dashboard/feedback/delete/{id}', 
    array('as' => 'dashboard.feedback.delete', 'uses' => 'DashboardController@getFeedbackDelete'));

Route::post('dashboard/feedback/delete', array('uses' => 'DashboardController@postFeedbackDelete'));


Route::post('dashboard/package/itinerary',array('uses'=> 'DashboardController@postPackageItineraryAdd'));

Route::get('dashboard/package-itinerary/edit/{id}',
          array('as' => 'dashboard.package-itinerary.edit', 'uses' => 'DashboardController@getPackageItineraryEdit'));
    
Route::post('dashboard/package-itinerary/edit',array('uses'=> 'DashboardController@postPackageItineraryEdit'));

Route::get('dashboard/package-itinerary/delete/{id}',
          array('as' => 'dashboard.package-itinerary.delete', 'uses' => 'DashboardController@getPackageItineraryDelete'));
    
Route::post('dashboard/package-itinerary/delete',array('uses'=> 'DashboardController@postPackageItineraryDelete'));



Route::post('dashboard/package/price-summary',array('uses'=> 'DashboardController@postPackagePriceSummaryAdd'));

Route::get('dashboard/package-pricesummary/edit/{id}',
          array('as' => 'dashboard.package-pricesummary.edit', 'uses' => 'DashboardController@getPackagePriceSummaryEdit'));
    
Route::post('dashboard/package-pricesummary/edit',array('uses'=> 'DashboardController@postPackagePriceSummaryEdit'));

Route::get('dashboard/package-pricesummary/delete/{id}',
          array('as' => 'dashboard.package-pricesummary.delete', 'uses' => 'DashboardController@getPackagePriceSummaryDelete'));
    
Route::post('dashboard/package-pricesummary/delete',array('uses'=> 'DashboardController@postPackagePriceSummaryDelete'));



Route::post('dashboard/package/inclusion',array('uses'=> 'DashboardController@postPackageInclusionAdd'));

Route::get('dashboard/package-inclusion/edit/{id}',
          array('as' => 'dashboard.package-inclusion.edit', 'uses' => 'DashboardController@getPackageInclusionEdit'));
    
Route::post('dashboard/package-inclusion/edit',array('uses'=> 'DashboardController@postPackageInclusionEdit'));

Route::get('dashboard/package-inclusion/delete/{id}',
          array('as' => 'dashboard.package-inclusion.delete', 'uses' => 'DashboardController@getPackageInclusionDelete'));
    
Route::post('dashboard/package-inclusion/delete',array('uses'=> 'DashboardController@postPackageInclusionDelete'));


Route::post('dashboard/package/hotel-detail',array('uses'=> 'DashboardController@postPackageHotelDetailAdd'));

Route::get('dashboard/package-hoteldetail/edit/{id}',
          array('as' => 'dashboard.package-hoteldetail.edit', 'uses' => 'DashboardController@getPackageHotelDetailEdit'));
    
Route::post('dashboard/package-hoteldetail/edit',array('uses'=> 'DashboardController@postPackageHotelDetailEdit'));

Route::get('dashboard/package-hoteldetail/delete/{id}',
          array('as' => 'dashboard.package-hoteldetail.delete', 'uses' => 'DashboardController@getPackageHotelDetailDelete'));
    
Route::post('dashboard/package-hoteldetail/delete',array('uses'=> 'DashboardController@postPackageHotelDetailDelete'));

Route::get('dashboard/package-category/{id}', 
    array('as' => 'dashboard.package-category', 'uses' => 'DashboardController@getPackageCategory'));





Route::get('dashboard/package/form/step2/{id}',array('as' => 'package-step2','uses' => 'DashboardController@getPackageStep2'));
Route::post('dashboard/package/form/step2',array('uses' => 'DashboardController@postPackageStep2'));

Route::get('dashboard/package/form/step3/{id}',array('as' => 'package-step3','uses' => 'DashboardController@getPackageStep3'));
Route::post('dashboard/package/form/step3',array('uses' => 'DashboardController@postPackageStep3'));

Route::get('dashboard/package/form/step4/{id}',array('as' => 'package-step4','uses' => 'DashboardController@getPackageStep4'));
Route::post('dashboard/package/form/step4',array('uses' => 'DashboardController@postPackageStep4'));



//New Package hotel details routes...

Route::post('dashboard/package/new-hotel-detail',array('uses' => 'DashboardController@postNewPackageHotelDetailAdd'));

Route::post('dashboard/package/new-hotel-detail/edit',array('uses'=> 'DashboardController@postNewPackageHotelDetailEdit'));

Route::get('dashboard/campaign/slider/delete',array('as' => 'campaign-slider-delete','uses' => 'DashboardController@getDeleteCampaignSlider'));

Route::get('/dashboard/package/published/{package_id}',array('as' => 'package-published','uses' => 'DashboardController@getPackagePublished'));

Route::get('/dashboard/package/unpublished/{package_id}',array('as' => 'package-unpublished','uses' => 'DashboardController@getPackageUnpublished'));





// Start Routes for Operator Management


Route::get('dashboard/operator/list',array('as' => 'dashboard.operator.list','uses' => 'DashboardController@getOperatorList'));

Route::get('dashboard/operator/create',array('as' => 'dashboard.operator.create','uses' => 'DashboardController@getOperatorCreate'));

Route::post('dashboard/operator/create',array('as' => 'dashboard.operator.store','uses' => 'DashboardController@postOperatorStore'));

Route::get('dashboard/operator/edit/{id}',array('as' => 'dashboard.operator.edit','uses' => 'DashboardController@getOperatorEdit'));

Route::post('dashboard/operator/update/{id}',array('as' => 'dashboard.operator.update','uses' => 'DashboardController@postOperatorUpdate'));

Route::get('dashboard/operator/delete/{id}',array('as' => 'dashboard.operator.delete','uses' => 'DashboardController@getOperatorDelete'));

Route::post('dashboard/operator/delete/{id}',array('as' => 'dashboard.operator.destroy','uses' => 'DashboardController@postOperatorDestroy'));



// End Routes for Operator Management






/* End Dashboard Routes */



/* Start HotelCategoryController Routes */

Route::get('/hotels' , array('as' => 'hotels', 'uses' => 'HotelCategoryController@getHotels'));

Route::get('/hotels/{title}' , array('as' => 'hotel-details', 'uses' => 'HotelCategoryController@getHotelDetails'));

Route::post('/booknow',array('uses'=>'HotelCategoryController@postBookNow'));

Route::get('hotels.json', 
    array('as' => 'hotels-json', 'uses' => 'HotelCategoryController@getHotelJson'));

/* End HotelCategoryController Routes */



/* Start DestinationController Routes */

Route::get('destinations.json', 
    array('as' => 'destinations-json', 'uses' => 'DestinationController@getDestinationJson'));

Route::get('popular/destinations',array('as' => 'popular-destinations','uses' => 'DestinationController@getPopularDestinations'));

Route::get('popular/destinations/remove/{id}',array('as' => 'popular-destination-remove','uses' => 'DestinationController@getPopularDestinationRemove'));


/* End DestinationController Routes */

Route::get('inclusion.json',
          array('as' => 'inclusion.json','uses' => 'DashboardController@getPackageInclusions'));

/* Start CampaignController Routes */

Route::get('campaign/{slug}', 
    array('as' => 'campaign.view', 'uses' => 'CampaignController@getView'));

Route::post('campaign/{slug}', array('as' => 'campaign.post','uses' => 'CampaignController@postCampaignSubmissionAdd'));
Route::post('ajax/campaign/submit', array('uses' => 'CampaignController@postAjaxCampaignSubmit'));

/* End CampaignController Routes */

Route::post('summernote/upload','SummernoteController@postSummernoteImage');

Route::resource('flight','FlightController');

Route::resource('car-volvo','CarVolvoController');

//-------------------------------------------------------------------------------------------------------

Route::get('test',function(){

     Config::set('database.connections.olddata', array(
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'nassa',
        'username'  => 'root',
        'password'  => 'root',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
        ));
    
    $old_data =  DB::connection('olddata')->table('tbl_category')->select('category','image')->get();
    
    foreach($old_data as $data)
    {
        $new_data = PackageCategory::where('title','LIKE',$data->category)->first();
        
        if(empty($data->image))
        {
            echo 'No Image for'.$data->category.'<br/>';
        }
        else{
            $source_file = '/home/praveen/Downloads/upload/'.$data->image;

            $ext = pathinfo($data->image, PATHINFO_EXTENSION);

            $dest_filename = Util::rand_filename($ext);

            $dest_path = 'upload'.DIRECTORY_SEPARATOR.date('Y').DIRECTORY_SEPARATOR.date('m').DIRECTORY_SEPARATOR.date('d');
            
            if(!File::exists($dest_path))
            {
                File::makeDirectory($dest_path, 0755, true, true);
            }
            $upload_success = File::copy($source_file,public_path().DIRECTORY_SEPARATOR.$dest_path.DIRECTORY_SEPARATOR.$dest_filename);    
            
            $new_data->main_image = $dest_path.DIRECTORY_SEPARATOR.$dest_filename;
            $new_data->save();
            echo $new_data->main_image .' for '. $data->category.'<br/>';
        }
    }
    
});
