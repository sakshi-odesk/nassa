<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class dbRecords extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'dbRecords:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get old data from database  and create new database';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->getRecords();
        
	}
    
    public function getRecords()
    {
        $hostname = $this->option('hostname');
		$dbname = $this->option('dbname');
		$username = $this->option('username');
		$password = $this->option('password');
        
        Config::set('database.connections.olddata', array(
        'driver'    => 'mysql',
        'host'      => $hostname,
        'database'  => $dbname,
        'username'  => $username,
        'password'  => $password,
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
        ));
        
        
        
        //get all old PackageCategory data.
        $package_categories = DB::connection('olddata')->table('tbl_category')->get();
        
        foreach($package_categories as $package_category)
        {
            $new_package_category = array('package_type_id'=>$package_category->menu,
                                          'title'=>$package_category->category,
                                          'slug'=>Util::uniqueSlug($package_category->category,'Package'),
                                          'body'=>$package_category->des);
             
            PackageCategory::create($new_package_category);

        }
        
        // get all old feedback data.
        $feedbacks = DB::connection('olddata')->table('tbl_feedback')->where('status',0)->get();
    
        foreach($feedbacks as $feedback)
        {
            $new_feedback = array('name'=>$feedback->name,
                                 'email'=>$feedback->email,
                                 'phone'=>$feedback->mobile,
                                 'message'=>$feedback->feedback,
                                 'ip'=>$feedback->ip,
                                 'published'=>1);

            Feedback::create($new_feedback);
        }
        
        //get all old Package data.
        $packages = DB::connection('olddata')->table('tbl_packages')->get();
        
        foreach($packages as $package)
        {
            $facilities = DB::connection('olddata')->table('tbl_facility_details')->where('pid',$package->id)->get();
            $itineraries = DB::connection('olddata')->table('tbl_itinerary')->where('pid',$package->id)->get();
            $inclusions = DB::connection('olddata')->table('tbl_inclusion')->where('pid',$package->id)->get();
            $hotel_details = DB::connection('olddata')->table('tbl_hotel_details')->where('pid',$package->id)->get();
            $price_summaries = DB::connection('olddata')->table('tbl_price_summary')->where('pid',$package->id)->get();
            $hotel_levels = DB::connection('olddata')->table('tbl_hotel_category')->where('id',$package->hotel_category)->get();
            $package_categories = DB::connection('olddata')->table('tbl_category')->where('id',$package->destination)->get();
            
            $new_package = array('title'=>$package->title,
                             'slug'=>Util::uniqueSlug($package->title,'Package'),
                             'destination_id' => 1,
                             'price'=>$package->price,
                             'route_map'=>$package->rout,
                             'duration'=>$package->duration,
                             'special_offer'=>$package->spl,
                             'published'=>$package->status);
            
            if($hotel_levels)
            {
                foreach($hotel_levels as $hotel_level)
                {
                    $new_package['hotel_level'] = $hotel_level->category;
                }
            }
            
            if($package_categories)
            {
                foreach($package_categories as $package_category)
                {
                  $new_package_category = PackageCategory::where('title',$package_category->category)->first();
                    
                  $new_package['package_category_id'] = $new_package_category->id;
                }
            }
            
            $new_package = Package::create($new_package);
            
            if($facilities)
            {
                foreach($facilities as $facility)
                {
                    $new_facility = array('facility_id'=>$facility->fid);

                    $new_facility = new PackageFacility($new_facility);

                    $new_package->package_facilities()->save($new_facility);
                }
            }

            if($itineraries)
            {
                foreach($itineraries as $itinerary)
                {
                    $new_itinerary = array('day_num'=>$itinerary->day,
                                           'destination'=>$itinerary->destination,
                                           'desc'=>$itinerary->detail);

                    $new_itinerary = new PackageItinerary($new_itinerary);

                    $new_package->package_itineraries()->save($new_itinerary);
                }
            }

            if($inclusions)
            {
                foreach($inclusions as $inclusion)
                {
                    $new_inclusion = array('title'=>$inclusion->incl);

                    $new_inclusion = new PackageInclusion($new_inclusion);

                    $new_package->package_inclusions()->save($new_inclusion); 
                }
            }

            if($hotel_details)
            {
                foreach($hotel_details as $hotel_detail)
                {
                    $new_hotel_detail = array('destination'=>$hotel_detail->destination,
                                              'hotel'=>$hotel_detail->hotel,
                                              'meal_plan'=>$hotel_detail->meal);

                    $new_hotel_detail = new PackageHotelDetail($new_hotel_detail);

                    $new_package->package_hotel_details()->save($new_hotel_detail);
                }
            }

            if($price_summaries)
            {
                foreach($price_summaries as $price_summary)
                {
                    $new_price_summary = array('desc'=>$price_summary->des,
                                               'cost'=>$price_summary->cost);

                    $new_price_summary = new PackagePriceSummary($new_price_summary);

                    $new_package->package_price_summaries()->save($new_price_summary);
                }
            }

        }
        
        //get all old Package seo data
//        $seo_package_details = DB::connection('olddata')->table('tbl_seo')->where('action',3)->get();
//    
//        foreach($seo_package_details as $seo_package_detail)
//        {
//            $old_package = DB::connection('olddata')->table('tbl_packages')->where('id',$seo_package_detail->page)->first();
//
//            $new_package = Package::where('title',$old_package->title)->first();
//
//            $new_package->seo_meta_desc = $seo_package_detail->des;
//            $new_package->seo_meta_kw = $seo_package_detail->keyword;
//            $new_package->seo_page_title = $seo_package_detail->title;
//
//            $new_package->save();
//        }
        
        
        //get all old PackageCategory seo data.
//        
//        $seo_package_details = DB::connection('olddata')->table('tbl_seo')->where('action',2)->get();
//    
//        foreach($seo_package_details as $seo_package_detail)
//        {
//            $old_package_category = DB::connection('olddata')->table('tbl_category')->where('id',$seo_package_detail->page)->first();
//
//            $new_package_category = PackageCategory::where('title',$old_package_category->category)->first();
//            
//            $new_package_category->seo_meta_desc = $seo_package_detail->des;
//            $new_package_category->seo_meta_kw = $seo_package_detail->keyword;
//            $new_package_category->seo_page_title = $seo_package_detail->title;
//
//            $new_package_category->save();
//        }
//        
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('hostname', null, InputOption::VALUE_REQUIRED, 'Database host name', null),
			array('dbname', null, InputOption::VALUE_REQUIRED, 'Database name', null),
			array('username', null, InputOption::VALUE_REQUIRED, 'Database username', null),
			array('password', null, InputOption::VALUE_REQUIRED, 'Database password', null),
		);
	}

}
