@extends('layout.frontend')

@section('css')
<style>

.san
{
  border: 1px solid #CCCCCC;
    color: #666666;
    margin: 0px 10px 0;
    padding: 10px;
    width: 275px;
	float:right;
}
.san1
{
  border: 1px solid #CCCCCC;
    color: #666666;
    margin: 8px 10px 0;
    padding: 10px;
    width: 275px;
	height:60px;
	float:right;
}
mid
{
height:400px!important;
}

.clear
{
clear:both;
}
.but
{
width:150px;
}
.abttitle
{
color:#666666!important;
margin:10px 10px 0;
font-size:15px;
}
.sss
{
 height:30px;
}
</style>
@stop

@section('content')

<div class="mid" style="height:400px;">

<div class="left">

<div class="search" style="border:none;">


<div>

</div>

<div style="margin-top:30px; width:550px;">
<div class="sss">
<strong>Reach us </strong> :-
</div>
<div class="sss">
<strong>Head Office </strong> :- &nbsp; 706 Jaina Tower-1, Janakpuri District Center, Janakpuri, New Delhi, Delhi 110058
</div>
<div class="sss">
<strong>Office No </strong> :- &nbsp; 011 - 4173 1234,
  <p style="margin-left:74px;">  011 - 6608 1234</p>
</div>
<div class="sss" style="height:80px;">
<strong>Mobile No </strong> :- &nbsp; 8588 872 474,
<p style="margin-left:78px;"> 7838 779 981,</p>
<p style="margin-left:78px;"> 8800 799 881,</p>
<p style="margin-left:78px;"> 9250 079 981</p>
</div>

<div class="sss">
<strong>Email </strong> :- &nbsp; paras@nassatravels.com
<p style="margin-left:52px;"> magicofkerala@gmail.com</p>
</div>
<div class="sss">
  <strong>Web </strong> :- &nbsp; <a href="http://www.nassatravels.com" target="_blank">www.nassatravels.com</a>
</div>
</div>
</div>

</div>

<div class="right" style="width:450px; height:400px;line-height:1.7;color:#333333;">
<div class="search">
<div class="tag1">Enquiry Form</div>
    
    {{ Form::open(array( 'action' => 'HomeController@postContact', 'files' => true,'role' => 'form')) }} 
  
    <div class="container-fluid">
          @foreach($errors->all(':message') as $message) 
              <div class="row">
                  <div class="col-md-12">
                      <div class="alert alert-danger" role="alert">
                          {{ $message }} 
                      </div>
                  </div>
              </div>
          @endforeach
        <div class="box-body">
            <input type="hidden" name="heading" value="Enquiry">
            
            <div class="form-group abttitle">
                <label>Full Name :</label>
                <input type="text" class="form-control input-sm san" name='name' >
            </div>
            <div class="clear"></div>
            <div class="form-group abttitle">
                <label>Email Address :</label>
                <input type="text" class="form-control input-sm san" name='email'>
            </div>
            <div class="clear"></div>
            <div class="form-group abttitle">
                <label>Mobile No. :</label>
                <input type="text" class="form-control input-sm san" name='phoneno'>
            </div>
            <div class="clear"></div>
            <div class="form-group abttitle">
                <label>Message :</label>
                <textarea name="message" class="form-control input-sm san" rows="3"></textarea>
            </div>
            <div class="clear"></div>       
            <div align="right" style="padding:15px;line-height:1.7;color:#333333;">
            <input class="but" type="submit"  name="submit" value="Submit"/>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.Container-fluid -->
    {{ Form::close() }}
</div>
</div>
</div>
@stop