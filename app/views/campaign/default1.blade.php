<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NassaTravels.com | {{$campaign->heading}} best packages deals</title>
    
    <!-- Bootstrap Core CSS -->
    {{HTML::style('theme/front/css/bootstrap.min.css')}}

    <!-- Custom CSS -->
    {{HTML::style('theme/front/css/campaign.css')}}
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    
<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="{{URL::route('home')}}" target="_blank">
                    <img src="{{asset('theme/front/images/NassaTravels_logo.png')}}" />
                </a>
            </div>
            <div class='head-phone pull-right' style="font-weight:bold;padding-top:25px">
	        	<span>011 - 4173 1234, &nbsp;&nbsp; +91 - 8588 872 474</span>
	            <br />
	        	<span>+91 - 8800 799 881, &nbsp;&nbsp; +91 - 7838 779 981</span>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
    <!--Banner-->
    <div class="banner">
    	<div class="banner-img" style="background:url({{asset($campaign->background_image)}}) no-repeat center center / cover"></div>
        <div class="title-overlay"></div>
    </div>
     <!--Banner End-->

    <!-- Page Content -->
    <div class="container">
        <!-- Row One -->
        <div class="row">
            <div class="col-md-6 page-content">
            	<h1 class="page-header">{{$campaign->heading}}
                    <small>{{$campaign->sub_heading}}</small>
                </h1>
                <br />
               
                <p>{{$campaign->desc}}</p>

                <h3 class="why-us">Why Us?</h3>

                <ul class="why-us">
                	<li>
                    	<img src="{{asset('theme/front/images/expert-advice.png')}}" />
                    	<h5>Get Expert Advice</h5>
                    </li>
                    <li>
                    	<img src="{{asset('theme/front/images/24hr.png')}}" />
                    	<h5>24 / 7 Services</h5>
                    </li>
                    <li>
                    	<img src="{{asset('theme/front/images/best_price.png')}}" />
                    	<h5>Best Price Guarantee</h5>
                    </li>
                    <li>
                    	<img src="{{asset('theme/front/images/customise_solution.png')}}" />
                    	<h5>Get Customize Solutions</h5>
                    </li>
                    <li>
                    	<img src="{{asset('theme/front/images/save_money.png')}}" />
                    	<h5>Save Money</h5>
                    </li>
                    <li>
                    	<img src="{{asset('theme/front/images/we_value_your_time.png')}}" />
                    	<h5>We Value Your Time</h5>
                    </li>
                </ul>
                <div class="clearfix"></div>
                <div class="review-container">
                	<h3>Traveler's Review</h3>
                    
                    @foreach($reviews as $review)
                    <div class="reviews">
                    	<p>"{{$review->desc}}"<figcaption>- {{$review->reviewer}}</figcaption></p>
        			<hr>
                    </div>
                    @endforeach
                </div> 
            </div>
            
            <div class="col-md-6">
            	<div class="enquiry-form">
            	   <h3 class="form-head">Get Best Travel Deals.</h3>
                     @foreach($errors->all(':message') as $message) 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                  <b>{{ $message }} </b>
                                </div>
                            </div>
                        </div>
                    @endforeach
                       <div class="form-container">
                            <h3 class="form-big">Enter your details to get customised travel deals</h3>
                            {{ Form::open(array( 'route'=>array('campaign.view',$campaign->slug),'method'=>'POST', 'files' => true,'role' => 'form','class'=>'campaign-submission')) }}
                                <input type="hidden" name="id" {{isset($campaign) ? 'value="'.$campaign['id']. '"' : ''}}>
                                <input type="hidden" name="referer" value="{{Request::header('referer')}}" >
                                <input type="text" name="name" class="inputbox" placeholder="Enter your name"/>
                                <input type="number" name="phone_no" class="inputbox" placeholder="Enter your phone number" />
                                <input type="email" name="email" class="inputbox" placeholder="Enter your email id" />

                                <br />
                           
                                <label>Best time to call</label>
                                <div class="styled-select">
                                  <select name="best_time_to_call" class="selectbox styled-select">
                                      <option value="Anytime">Anytime</option>
                                      <option value="Morning">Morning</option>
                                      <option value="Afternoon">Afternoon</option>
                                       <option value="Evening">Evening</option>
                                  </select>
                                </div>  

                                <br />
                                <br class="group" />
                                <input type="checkbox" name="subscribe" class="checkbox" value="1" checked>
                                <span class="small">By submitting this form, I authorize nassatravels.com to send me information related to this travel enquiry via email and/or phone.</span>
                                <button name="submit" onclick="submit(); return false;" class="submit-enquiry">Show Me the Best Deal</button>
                            </select>
                            {{Form::close()}}
                        </div>
                </div><!--enquiry form-->
                <!--Need help-->
                <br />
                	<div class="contact-info">
                    	<div class="contact-info-inner">
                        	<div class="need-help"><h3 style="margin:0;">Need help?</h3></div>
                        	<p><strong>Not sure where to start? </strong>Our travel expert's team is  just one call/email away from assisting you with all your travel related queries.</p>
                            
                            <ul class="ctc-info">
                            	<li class="phone">
                                	<span>011 - 4173 1234, &nbsp;&nbsp; +91 - 8588 872 474</span>
                                    <br />
                                	<span>+91 - 8800 799 881, &nbsp;&nbsp; +91 - 7838 779 981</span>
                                </li>
                                
                                <li class="mail">
                                	<span>paras@nassatravels.com</span>
                                    <br />
                                	<span>magicofkerala@gmail.com</span>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                <!--Need help end-->
                <br />
            
                <!--About Nassa travels-->
                	<div class="about-us">
                    	<p>Nassa Travels  is one of the fastest growing travel company in India with a young and enthusiastic team working 24 *7 to provide the best travel services. It is a single platform providing all travel related services including destination knowledge, guidance, pricing and booking facility for domestic hotel bookings, holiday packages, and car rentals. We not just to sell we make your travel simple,comfortable and memorable.</p>
                        <a class="read-more" href="{{URL::route('home')}}" target="_blank">Read more...</a>
                    </div>
                <!--About Nassa travels-->
                
            </div><!---->
        </div>
        <!-- /.row -->
        <hr>
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; NassaTravels.com 2014</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>
    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
    function submit()
    {
        alert('Thankyou for submitting');
        $('form.campaign-submission').first().submit();
    } 
    </script>

</body>

</html>