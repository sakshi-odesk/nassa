<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>NassaTravels.com | {{$campaign->heading}} best packages deals</title>

<!-- Bootstrap Core CSS -->
{{HTML::style('theme/front/css/bootstrap.min.css')}}
{{HTML::style('theme/front/css/style.css')}}
{{HTML::style('theme/front/css/font-awesome.min.css')}}
{{HTML::style('theme/front/css/carousel.css')}}
  
<!--
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/carousel.css" rel="stylesheet">
-->
  
{{-- HTML::style('theme/front/css/campaign.css') --}}  

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  
<style>  
  span.red {
    color:#fe4b28;
  }  
</style>

</head>

<body>
<div class="fixed-bottom show-xs">
  <div class="container">
    <div class="row">
      <div class="col-xs-6">
          <a class="btn btn-call btn-block" href="tel:8588872474" data-role="button" rel="external">
              <i class="fa fa-phone" style="font-size: 21px; vertical-align:middle;"></i> &nbsp;Call Now
          </a>
      </div>
      <div class="col-xs-6"> 
        <a id="enqry" class="btn btn-outline btn-block" href="#">
          <i class=" fa fa-envelope" style="font-size: 21px; vertical-align:middle;"></i> &nbsp;Enquiry
        </a> 
      </div>
    </div>
  </div>
</div>

<!-- Navigation -->
<nav role="navigation" class="navbar">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle hidden collapsed" type="button" aria-expanded="false"> 
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a href="{{URL::route('home')}}" class="navbar-brand"><img alt="NassaTravels.com" src="{{asset('theme/front/images/NassaTravels_logo.png')}}"></a> </div>
    
    <div class="nav-top col-md-6 col-sm-7 col-xs-12 pull-right text-right hidden-xs">
      <ul class="list-inline list-unstyled">
        <li class="header-ctc"><i style="font-size:22px; vertical-align:middle; margin-right:7px;" class="fa fa-phone"></i>
            011-66081234, 8588872474
        </li>
        <!-- <li><i class="fa fa-envelope" style="font-size:20px; vertical-align:middle; margin-right:7px;"></i>sales@nassatravels.com</li>-->
        <li><a href="#"><i style="font-size: 20px; color: #7d7d7d;" class="fa fa-facebook"></i></a></li>
<!--
        <li><a href="#"><i style="font-size: 20px; color: #7d7d7d;" class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i style="font-size: 20px; color: #7d7d7d;" class="fa fa-linkedin"></i></a></li>
        <li><a href="#"><i style="font-size: 20px; color: #7d7d7d;" class="fa fa-google-plus"></i></a></li>
-->
      </ul>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
      
      <ul class="nav navbar-nav hidden">
        <li> <a class="active" href="#">Hotels</a> </li>
        <li> <a href="#">Flights</a> </li>
        <li> <a href="#">Hotels + Flight</a> </li>
        <li> <a href="package_landing.html">Holidays</a> </li>
        <li> <a href="#">Train</a> </li>
        <li> <a href="#">Bus</a> </li>
        <li> <a href="#">Car/Volvo</a> </li>
        <li class="dropdown"> <a data-toggle="dropdown" href="#"> Latest Deals <i class="fa fa-caret-down"></i> </a>
          <ul role="menu" class="dropdown-menu">
            <a data-toggle="dropdown" href="#"> </a>
            <li><a data-toggle="dropdown" href="#"></a><a data-toggle="tab" href="#tab4default">The Chill Out  Summer Deal</a></li>
            <li><a data-toggle="tab" href="#tab5default">Honeymoon Specials</a></li>
          </ul>
        </li>
      </ul>
      
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container --> 
</nav>

<!-- Page Content -->
<div class="page-wrapper clearfix">
  <div class="banner-container top-widget-area push-bottom-20" style="background:url({{asset($campaign->background_image)}}) no-repeat center center / cover">
    <div class="container">
      <div class="row">
        {{ Form::open(array( 'route'=>array('campaign.view',$campaign->slug),'method'=>'POST', 'files' => true,'role' => 'form','class'=>'campaign-submission','id'=>'app_form')) }}
        <div class="col-lg-5 col-xs-12 form-widget-wrapper img-rounded">
          <h2 class="text-white">Plan Your Travel</h2>
          <div class="form-widget-inner img-rounded push-bottom-20">
            <div class="enquiry-form-wrapper clearfix"> 
              
              <!--From / To Options-->
               
                <input type="hidden" name="id" {{isset($campaign) ? 'value="'.$campaign['id']. '"' : ''}}>
                  <input type="hidden" name="referer" value="{{Request::header('referer')}}" >
              <div class="clearfix panel-body">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 push-bottom-10">
                  <input id="form-field" class="inputbox input-color input-block autowidth" name="name" type="text" placeholder="Name" required>
                  <span></span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 push-bottom-10">
                  <input class="inputbox input-color input-block autowidth" name="phone_no" type="tel" placeholder="Phone Number" required>
                  <span class="red"></span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 push-bottom-10">
                  <input class="inputbox input-color input-block autowidth" name="email" type="email" placeholder="Email Id" required>
                  <span class="red"></span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <input class="inputbox input-color input-block autowidth" type="text" placeholder="Other Destination Interested In (Optional)">
                </div>
              </div>
               
              <!--/From / To Options-->
              
              <div class="col-lg-12">
                <div class="" style="margin-top:0;">
                  <label class="checkbox-row">
                    <input class="pull-left" name="subscribe" type="checkbox" value="1" checked>
                    <span class="text-11 pull-left">By submitting this form, I authorize nassatravels.com to send me information related to this travel enquiry via email and/or phone.</span> </label>
                </div>
              </div>
              <div class="col-lg-12 col-md-3 col-sm-4 col-xs-12"> 
                <button name="submit" class="submit-enquiry search nt-btn btn-block btn-orange">Show Me the Best Deal</button>
              </div>
            </div>
          </div>
        </div>
        {{Form::close()}}
        <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 banner-area">
          <div class="dest-banner-wrapper"> 
            <!--<div class="dest-banner"><img src="images/dest01.jpg" /></div>-->
            <div class="pkg-name">
              <h1>{{ $campaign->heading }} - 
                  <span class="small">{{ $campaign->sub_heading }}</span>
              </h1>
            </div>
          </div>
          <div class="enq-pkg-desc panel-body"> 
            <!--<h1>Goa Special Package <span class="small fade-color">starting @ <i class="fa fa-inr"></i>16,999/- onwards</span></h1>-->
            <P>{{$campaign->desc}}</P>
   
             @if(strlen(strip_tags(trim($campaign->inclusion))) > 0)  
                <p class="text-18">Package Includes :</p>
                <p class="text-18">{{ $campaign->inclusion }}</p>
            @endif  
<!--
            <ul class="list-bullet">
              <li>Pickup / Drop from Airport.</li>
              <li>Siteseeing on persoan coach</li>
              <li>Pickup / Drop from Airport.</li>
              <li>Siteseeing on persoan coach</li>
              <li>Pickup / Drop from Airport.</li>
            </ul>
-->
<!--            <a class="pull-right" href="{{-- URL::route('home') --}}">Show More...</a> -->
            </div>
        </div>
      </div>
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
  </div>
  <!--/.top-widget-area-->
  
  <div class="body-container clearfix">
    <div class="container container-fluid">
    
      @if(!empty($campaign->slider))    
      <div class="row">
        <div class="recmd-hotels col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h2 class="section-title">Other Holiday Deals</h2>
          <div class="carousel carousel-showsixmoveone slide" id="carousel123">
            <div class="carousel-inner">
                
                <?php $count = 0; ?>
              @foreach(json_decode($campaign->slider) as $slide) 
            
              <div class="item @if($count == 0) active @endif">
                <div class="col-xs-12 col-sm-4 col-md-4">
                 <a href="{{ $slide->Url }}">
                  <div class="thumbnail panel box-rd"> <img src="{{ asset($slide->Image) }}" alt="">
                    <div class="deal-info">
                      <h4 class="section-title">{{ $slide->Title }}</h4>
                      <div class="deal-price clearfix"> 
                        <!--<p class="text-11 fade-color">Visit Kerala, Gods own country. Explore the best of nature and Visit Kerala, Gods own country. Explore the best of nature and</p>-->
                        <div class="pull-left"> <small>Starting from</small> <span class="main-price text-18"><i class="fa fa-inr"></i> {{ $slide->Starting_from }}/-</span> </div>
                      </div>
                    </div>
                  </div>
                  </a></div>
              </div> 
                <?php  $count++; ?>
              @endforeach
                
            </div>
            <a class="left carousel-control" href="#carousel123" data-slide="prev">‹</a> <a class="right carousel-control" href="#carousel123" data-slide="next">›</a> </div>
        </div>
      </div>
      <!--/.row-->
      @endif
      <div class="clearfix"></div>
      <div class="row"> 
        <!--Testimonial-->
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <h2 class="section-title">Our Customer's Say</h2>
          <div class="carousel slide panel" data-ride="carousel" id="quote-carousel"> 
            
            <!-- Carousel Slides / Quotes -->
            <div class="carousel-inner text-center1 panel-body"> 
              <?php $count = 1; ?>
               @foreach($reviews as $review)
              
              <!-- Quote 1 -->
              <div class="item @if($count == 1) active @endif">
                <blockquote>
                  <div class="row">
                    <div class="col-sm-12">
                      <h5>Excellent service, Manali package was awesome.</h5>
                      <p class="text-14 fade-color"> {{$review->desc}}</p>
                      <small> {{$review->reviewer}} </small> </div>
                  </div>
                </blockquote>
              </div>
              <?php $count = $count + 1; ?>
              @endforeach
            
            </div>
            
            <!-- Carousel Buttons Next/Prev --> 
            <a data-slide="prev" href="#quote-carousel" class="left carousel-control2"><i class="fa fa-chevron-left"></i></a> <a data-slide="next" href="#quote-carousel" class="right carousel-control2"><i class="fa fa-chevron-right"></i></a> </div>
          <h2 class="section-title hidden">Connect With Us.</h2>
          <div class="panel box-rd hidden">
            <div class="panel-body"> 
              <!-- Facebook -->
              <div class="col-md-6 col-xs-12"> <a href="https://www.facebook.com/sharer/sharer.php?u=" title="Share on Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook" style="font-size: 35px;"></i> Share on Facebook</a> </div>
            </div>
          </div>
        </div>
        <!--/testimonial-->
        
        <div style="overflow:hidden;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <h2 class="section-title">Why US?</h2>
          <div class="panel box-rd">
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img src="{{asset('theme/front/images/expert-advice.png')}}" />
                  <p>Get Expert Advice</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img src="{{asset('theme/front/images/24hr.png')}}" />
                  <p>24 / 7 Services</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i style="font-size: 60px; color: #B5BDC9;" class="fa fa-thumbs-up"></i>
                  <p>Best Price Guarantee</p>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i style="font-size: 60px; color: #B5BDC9;" class="fa fa-magic"></i>
                  <p>Get Customize Solutions</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img src="{{asset('theme/front/images/save_money.png')}}" />
                  <p>Save Money</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i style="font-size: 60px; color: #B5BDC9;" class="fa fa-smile-o"></i>
                  <p>We Value Your Time</p>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <p class="fade-color text-12">Nassa Travels is one of the fastest growing travel company in India with a young and enthusiastic team working 24 *7 to provide the best travel services. It is a single platform providing all travel related services including destination knowledge, guidance, pricing and booking facility for domestic hotel bookings, holiday packages, and car rentals. We not just to sell we make your travel simple,comfortable and memorable.</p>
<!--            <a href="{{--URL::route('home')--}}">Read More...</a> -->
            </div>
        </div>
      </div>
      <hr />
      <!--/Bottom Links-->
      <div class="row push-bottom-20 bottom-links hidden-xs">
        <div class="col-lg-12 col-md-12 col-sm-12 text-12">
          <h4>India Holidays</h4>
          <p><a href="http://nassatravels.com/domestic-packages/kerala">Kerala Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/himachal">Himachal Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/south-india">South India Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/rajasthan">Rajasthan Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/kathmandu">Kathmandu Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/uttarakhand">Uttarakhand Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/goa">Goa Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/golden-triangle">Golden Triangle Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/kashmir">Kashmir Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/andaman-nicobar">Andaman Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/leh-and-ladakh">Leh Ladakh Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/north-east-package">North East Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/manali-volvo-packages">Manali Volvo Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/kashmir-package-with-flights">Kashmir Holiday Packages with Flights</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/manali-group-tour">Manali Group Tour</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/goawithflights">Goa Holiday Packages with Flights</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/dharamshalavolvopackage">Dharamshala Holiday Packages</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/keralawithflights">Kerala Holiday Packages with Flights</a> &nbsp; <a href="http://nassatravels.com/domestic-packages/north-east-packages-with-flight-1">Norht East Holiday Packages</a> &nbsp; </p>
        </div>
      </div>
      <!--/Bottom Links--> 
      
    </div>
  </div>
  <!--/.body-container--> 
  
  <!--Footer-->
  <footer class="">
    <div class="footer-bottom">
      <div class="container container-fluid"> <small class="fade-color pull-left">nassatravels.com | &copy; 2015 | All Rights Reserve</small>
        <div class="pull-right">
          <ul class="list-unstyled list-inline" style="margin-bottom:0;">
            <li><a href="#"><i class="fa fa-facebook" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
<!--
            <li><a href="#"><i class="fa fa-twitter" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
-->
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!--/.Footer--> 
  
</div>
<!--/ .page-wrapper--> 

<!-- jQuery Version 1.11.1 --> 
<!--<script src="js/jquery-min.js"></script> -->
{{HTML::script('theme/front/js/jquery-1.11.0.js')}}
<!-- Bootstrap Core JavaScript --> 
<!--<script src="js/bootstrap.min.js"></script> -->
{{HTML::script('theme/front/js/bootstrap.min.js')}}  
{{HTML::script('theme/front/js/moment.js')}}    

{{HTML::script('theme/front/js/jquery.validate.min.js')}}
{{HTML::script('theme/nassatravels/assets/js/jquery-ui-1.9.2.custom.min.js')}}
{{HTML::script('theme/nassatravels/assets/js/jquery.ui.touch-punch.min.js')}}  
<!--<script type="text/javascript" src="js/snipper.js"></script> --> 
<!--<script type="text/javascript" src="js/carousel.js"></script>--> 

<script>
	$('#enqry').click(function(){
	  $('.inputbox').addClass('highlight');
	});
</script> 
<script>
	(function() {
  // setup your carousels as you normally would using JS
  // or via data attributes according to the documentation
  // http://getbootstrap.com/javascript/#carousel
  $('#carousel123').carousel({
    interval: 2000
  });
  $('#carouselABC').carousel({
    interval: 3600
  });
}());

(function() {
  $('.carousel-showsixmoveone .item').each(function() {
    var itemToClone = $(this);

    for (var i = 1; i < 3; i++) {
      itemToClone = itemToClone.next();

      // wrap around if at end of item collection
      if (!itemToClone.length) {
        itemToClone = $(this).siblings(':first');
      }

      // grab item, clone, add marker class, add to collection
      itemToClone.children(':first-child').clone()
        .addClass("cloneditem-" + (i))
        .appendTo($(this));
    }
  });
}());
</script>
  
 <script type="text/javascript">
//  function submit()
//  {
//      alert('Thankyou for submitting');
//      $('form.campaign-submission').first().submit();
//  }
   
  $(document).ready(function () {

      $.validator.addMethod("validatephonenumber",function(value,element){

            value = value.replace(/[^0-9]/g,'').length;
            if(value == 10)
            {
                return true;
            }else{
                return false;
            }
         },'Please enter full phone number.'); 


      $("#app_form").validate({

          errorPlacement: function (error, element) {
              error.appendTo(element.next());
          },

          rules: {
                phone_no: {
                    number: true,
                    validatephonenumber:true,
                },
                email: {
                    email: true
                },

            },
      });
      
    $('.submit-enquiry').click(function () {
        
        var $valid = $("#app_form").valid();
        if ($valid) {
           $('#app_form').submit();
        }

    });
      
  }); 
   
  </script>  
</body>
</html>