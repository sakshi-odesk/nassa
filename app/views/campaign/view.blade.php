@extends('layout.default')

@section('content')

<div class="well">
    <legend>Campaign View</legend>
    @foreach($errors->all(':message') as $message) 
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    {{ $message }} 
                </div>
            </div>
        </div>
    @endforeach
    @if(isset($campaigns))
        @foreach($campaigns as $campaign)
        <div class="container-fluid">
               <div class="form-group">
                    <label class="control-label">Title : {{$campaign->title}}</label>
               </div>

                <div class="form-group">
                    <label class="control-label">Heading : {{$campaign->heading}}</label>
                </div>

                <div class="form-group">
                    <label class="control-label">Sub Heading : {{$campaign->sub_heading}}</label>
                </div>

                <div class="form-group">
                    <label class="control-label">Description : {{$campaign->desc}}</label>
                </div>

                <div class="form-group">
                    <label class="control-label">Backgroung Image :</label><br/>
                     <img class="img-responsive" src='{{asset($campaign->background_image)}}' height="100px" width="100px"></img>
                </div>
        </div>
        @endforeach
    @endif
</div>

<div class="well">
<!--    <legend></legend>-->
    <div class="container-fluid">
        <div class="box-body">
            {{ Form::open(array( 'route'=>array('campaign.view',$campaign->slug),'method'=>'POST', 'files' => true,'role' => 'form','class'=>'campaign-submission')) }}
             <input type="hidden" name="id" {{isset($campaign) ? 'value="'.$campaign['id']. '"' : ''}}>
             <input type="hidden" name="referer" value="{{Request::header('referer')}}" >
            <div class="form-group">
                <label>Name</label>
                <input type="text" placeholder="Name" class="form-control input-sm" name='name' >
            </div>
                
            <div class="form-group">
                <label>Phone no.</label>
                <input type="text" placeholder="Phone No." class="form-control input-sm" name='phone_no'>
            </div>
            
            <div class="form-group">
                <label>Email</label>
                <input type="text" placeholder="Email" class="form-control input-sm" name='email'>
            </div>

            <div class="form-group">
                <label>Best time to Call</label>
                <select class="form-control input-sm" name="best_time_to_call">
                    <option value="">Please Select...</option>
                    <option value="Morning">Morning</option>
                    <option value="Afternoon">Afternoon</option>
                    <option value="Evening">Evening</option>
                    <option value="Anytime">Anytime</option>
                </select>
            </div>
           <div class="form-group">
                <div class="checkbox">
                <label>
                   <input name='subscribe' type="checkbox" value="1"> Subscribed
                </label>
                </div>
            </div>
             {{ Form::close()}}
            
            <div class="login-footer">
               <button onclick="submit(); return false;" class="btn btn-primary btn-sm pull-right">Submit</button>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.Container-fluid -->
   

</div>
      
@stop

@section('css')
@stop


@section('js')
<script type="text/javascript">
     
    function submit()
    {
        alert('Thankyou for submitting');
        $('form.campaign-submission').first().submit();
    }     
</script>
@stop
