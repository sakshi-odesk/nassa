@extends('layout.login')

@section('css')
@stop

@section('content')
<div id="login-page">
    <div class="container">

        {{ Form::open(array('action' => 'RemindersController@postReset','files' => true,'role' => 'form','class'=>'form-login')) }}
        <h2 class="form-login-heading">Reset Password</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" name="email" placeholder="Email ID">
            <br>

            <input type="password" class="form-control" name="password" placeholder="Password">
            <br>

            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">

            <input type="hidden" value="{{$token}}"  name="token">
            <br>
            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> Reset Password</button>

        </div>
        
    </div>
</div>
@stop
