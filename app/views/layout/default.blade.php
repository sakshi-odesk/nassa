<!DOCTYPE html>
<html lang="en">
  <head>
      @section('meta')
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
      @show
      @section('title')
        <title>Nassa Travels</title>
      @show

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/ionicons.min.css" rel="stylesheet">
    <link href="/css/jquery-ui.css" rel="stylesheet">
    @section('css')
    @show
      
  </head>
  <body>
    @section('content')
    @show
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    {{HTML::script('theme/nassatravels/assets/js/jquery-ui-1.9.2.custom.min.js')}}
    {{HTML::script('theme/nassatravels/assets/js/jquery.ui.touch-punch.min.js')}}
    @section('js')
    @show
  </body>
</html>