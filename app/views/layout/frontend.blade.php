<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>NassaTravels</title>
        
         <!-- Start Meta tags -->  
            <meta name="description" content="">
            <meta name="keywords" content="">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="robots" content="FOLLOW,INDEX">
            <meta name="googlebot" content="INDEX, FOLLOW">
            <meta name="YahooSeeker" content="INDEX, FOLLOW">
            <meta name="allow-search" content="yes">
            <meta name="distribution" content="global">
            <meta name="revisit-after" content="1 days">
            <meta name="language" content="en-gb">
            <meta name="rating" content="General">
            <meta name="audience" content="All">
            <meta name="coverage" content="Worldwide">
            <meta name="document-rating" content="Safe for Kids">
            <meta name="resource-type" content="Web Page">
            <meta name="google-site-verification" content="d286aefkrdWHzyJ7Yyk7BzquwtnNX_slTFySOgZCBNI">
         <!-- End Meta tags -->
        
            @section('meta-tags')
            @show
        
         <!-- Start css -->
        
            {{ HTML::style('theme/front/css/css.css')}}
            {{ HTML::style('theme/front/css/csss.css')}}
            {{ HTML::style('theme/front/css/styles.css')}}
            {{ HTML::style('theme/front/css/jquery.css')}}
            {{ HTML::style('theme/front/css/jsDatePick_ltr.css')}}
            {{ HTML::style('theme/front/css/custom.css')}}
        
         <!-- End css -->  
        
            @section('css')
            @show
    </head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MNVZTD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MNVZTD');</script>
<!-- End Google Tag Manager -->


   
    <center>
        <div class="main">

            <!-- Start Header -->  
                @include('widget.frontend-header')
            <!-- End Header-->  

            <!-- Start Menu-->
                @include('widget.frontend-menu')
            <!-- End Menu-->
            
            <!-- Start Mid-->
             @section('content')
            
            @show
            <!-- End Mid-->
            
            <!-- Start Line -->
            <div class="line"></div>
            <!-- End Line -->
            
            <!-- Start Footer -->
                @include('widget.frontend-footer')
            <!-- End footer -->
            
            <div class="copy">© copyright all right reserved <a href="">Nassa Travels</a></div>

            
            <!-- Start slide-main1 -->
            <div class="slide-main1"> 
                <img src="{{asset('theme/front/images/feedback.png')}}">
                <div class="slide-container1">
                    <div class="fonts">
                        <h2 style="color:#E4AA2C; padding:0px; margin:0px; margin-top:10px; font-size:25px; font-weight:bold;">Feedback</h2>
                        <div class="sign-in-wrap" style=" margin-left:20px;">

                            {{Form::open(array('action'=>'HomeController@postFeedbackEnquiry','role'=>'form','files'=>'true'))}}
                            <div class="error"></div>
                            <input type="hidden" name="heading" value="Feedback">
                            <div class="input1">
                                <input name="name" id="name" placeholder="Name*" type="text">
                            </div>
                            <div class="input1">
                                <input name="email" id="email" placeholder="Email*" type="text">
                            </div>
                            <div class="input1">
                                <input name="phoneno" id="mobile" placeholder="Mobile*" type="text">
                            </div>

                            <div class="input1">
                                <textarea name="message" id="feedback" style="height:100px;" placeholder="Enter your feedback"></textarea>
                            </div>
                                <input class="button" name="homefeedback" value="Submit" style="margin-top:97px; margin-left:0px; background:#E4AA2C;" type="submit">
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- End slide-main1 -->
            
            
            <!-- Start slide-main2 -->
            <div class="slide-main2"> 
                <img src="{{asset('theme/front/images/enform.png')}}">
                <div class="slide-container2">
                    <div class="fonts">
                      <h2 style="color:#E4AA2C; padding:0px; margin:0px; margin-top:10px; font-size:25px; font-weight:bold;">Quick Enquiry</h2>
                      <div class="sign-in-wrap" style=" margin-left:20px;">

                          {{Form::open(array('action'=>'HomeController@postFeedbackEnquiry','role'=>'form','files'=>'true'))}}
                          <div class="error"></div>
                          <input type="hidden" name="heading" value="Enquiry">
                          <div class="input1">
                            <input name="name" id="ename" placeholder="Name*" type="text">
                          </div>
                          <div class="input1">
                            <input name="email" id="eemail" placeholder="Email*" type="text">
                          </div>
                          <div class="input1">
                            <input name="phoneno" id="emobile" placeholder="Mobile*" type="text">
                          </div>

                          <div class="input1">
                            <textarea name="message" id="emessage" style="height:100px;" placeholder="Enter your message"></textarea>
                          </div>
                          <input class="button" name="homeen" value="Submit" style="margin-top:97px; margin-left:0px; background:#E4AA2C;" type="submit">
                          {{Form::close()}}

                      </div>
                    </div>
                </div>
            </div>
            <!-- End slide-main2 -->
        </div>
    </center>

    <!-- Start script -->
    
    
    {{ HTML::script('theme/front/js/jquery.js')}}
    {{ HTML::script('theme/front/js/jsDatePick.js')}}
    {{ HTML::script('theme/front/js/custom.js?5457')}}
  
<script type="text/javascript" src="https://nassa.deskero.com/widget.js"></script><script>widget.init("0f60ca65a0f3d9f691dd2e3fc559685b","5506c3ffe4b09d8d4c11d746");widget.load();</script>    
   <!-- End Script --> 
    
        @section('js')
        @show
    
</body>
</html>
