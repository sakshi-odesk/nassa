<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    @section('title')  
    <title>Nassa Travels</title>
    @show
      
    <!-- Bootstrap core CSS -->
    {{ HTML::style('theme/nassatravels/assets/css/bootstrap.css')}}


    <!--external css-->
    {{ HTML::style('theme/nassatravels/assets/font-awesome/css/font-awesome.css')}}
        
    <!-- Custom styles for this template -->
      
     {{ HTML::style('theme/nassatravels/assets/css/style.css')}}
     {{ HTML::style('theme/nassatravels/assets/css/style-responsive.css')}}  
     {{ HTML::style('theme/nassatravels/assets/css/jqtree.css')}}
     {{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
     {{ HTML::style('theme/nassatravels/assets/css/bootstrap-rating.css')}}
    @section('css')
    @show
      
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body ng-app="NassaTravels">

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      @include('widget.header')
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
        @include('widget.sidebar')
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <!-- /MAIN CONTENT -->
      @section('content')
      @show
      <!--main content end-->
      <!--footer start-->
      @include('widget.footer')
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    {{HTML::script('theme/nassatravels/assets/js/jquery.js')}}  
    {{HTML::script('theme/nassatravels/assets/js/jquery.validate.min.js')}}  
    {{HTML::script('theme/nassatravels/assets/js/jquery-ui-1.9.2.custom.min.js')}}
    {{HTML::script('theme/nassatravels/assets/js/jquery.ui.touch-punch.min.js')}}
    {{HTML::script('theme/nassatravels/assets/js/jquery.dcjqaccordion.2.7.js')}}
    {{HTML::script('theme/nassatravels/assets/js/jquery.scrollTo.min.js')}}
    {{HTML::script('theme/nassatravels/assets/js/jquery.nicescroll.js')}}
    {{HTML::script('theme/nassatravels/assets/js/bootstrap.min.js')}} 
    {{HTML::script('theme/nassatravels/assets/js/bootstrap-rating.min.js')}}
    {{HTML::script('theme/nassatravels/assets/js/tree.jquery.js')}}  
    {{HTML::script('theme/nassatravels/assets/js/angular.min.js')}}
    
      
    <script type="text/javascript" src="{{ asset('theme/front-v3/js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('theme/front-v3/js/daterangepicker.js') }}"></script>  
      
     <!--common script for all pages-->  
    {{HTML::script('theme/nassatravels/assets/js/common-scripts.js')}}
      
    {{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js') }}  
      
    <!--script for this page-->
      <script type="text/javascript">
          
        $("[data-toggle='tooltip']").tooltip();
        
        var app = angular.module('NassaTravels',[]);   
          
      </script>
      
      @section('js')
      @show
  </body>
</html>
