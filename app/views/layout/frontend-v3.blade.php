<!DOCTYPE html>
<html lang="en">

<head>
     <!-- Start Meta tags -->  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
     @section('meta-tags')
     @show
    
    <!-- End Meta tags -->  
    <title>NassaTravels</title>
     <!-- Start css -->
    
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('theme/front-v4/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/front-v4/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/front-v4/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/front-v4/css/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/front-v4/css/jBox.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/front-v4/css/vertical_tabs.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/front-v4/css/bootstrap-rating.css') }}" rel="stylesheet">
   

    <!-- Custom CSS -->
    <style>
    body {
        /*padding-top: 70px;*/
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }

    button.nt-btn {
        padding: 7px 50px;
        text-align: center;
        font-size:16px;
        border-radius: 4px;
        display:block;
        margin: 10px 0;
    }  
    button.btn-orange {
        background: #e76601;
        border-bottom: 2px solid #ad6800;
        color: #ffffff;
        }    
    .panel-body.hot-picks{
    height:200px;
    overflow-y:scroll;
    }
	.jBox-container .enquiry-form-popup-wrapper {
		margin-top:0;
	}
    a.color:focus, a:hover {
        color:#444444;
        text-decoration: none;
    }   
		
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @section('css')
    @show
    
    
     <!-- Start css -->
</head>

<body>
	
    <!--Top bar-->
    <!--<div class="top-bar">
    </div>-->
    
     <!--Quick Enquiry-->
    	<div class="quick-enquiry-wrapper hidden-xs">
        	<div class="quick-enquiry">
            	<a href="#" class="enquiry-btn">
                    <img src="{{ asset('theme/front-v4/images/icons/quick_enquiry.png') }}" width="42" height="172" alt="Quick Enquiry" />
                </a>
            </div>
        </div>
    <!--/Quick Enquiry-->

    @section('header')
    <!-- Navigation -->
    <nav class="navbar" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
           
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
        <span class="sr-only">Toggle navigation</span> 
        <span class="icon-bar"></span> 
        <span class="icon-bar"></span> 
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="{{ URL::route('home',array('redirect'=>'holidays')) }}"><img src="{{ asset('theme/front-v4/images/logo.png') }}" alt="NassaTravels.com" /></a> 
    </div>
            
            <div class="nav-top col-md-64 col-sm-4 col-xs-12 pull-right hidden-xs">
              <ul class="list-inline list-unstyled">
                <li><i class="fa fa-phone" style="font-size:20px; vertical-align:middle; margin-right:7px;"></i>011- 66081234, 8800799881</li>
                <li><a href="#"><i class="fa fa-facebook" style="font-size: 20px; color: #444;"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" style="font-size: 20px; color: #444;"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" style="font-size: 20px; color: #444;"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" style="font-size: 20px; color: #444;"></i></a></li>
              </ul>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
          
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    @if(Request::is('/'))
                    <li> <a href="#tab3default" data-toggle="tab" onclick="tabs_new_active('holidays');">Holidays</a> </li>
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'holidays'))}}">Holidays</a> </li>
                    @endif
                    
                    @if(Request::is('/'))
                    <li> <a href="#tab1default"  data-toggle="tab" onclick="tabs_new_active('flights');">Flights</a> </li>  
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'flights'))}}">Flights</a> </li> 
                    @endif
                           
                    @if(Request::is('/'))
                    <li> <a href="#tab2default" data-toggle="tab" onclick="tabs_new_active('hotels');">Hotels</a> </li>
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'hotels'))}}">Hotels</a> </li>
                    @endif
                    
                    @if(Request::is('/'))
                    <li> <a href="#tab2default" data-toggle="tab" onclick="tabs_new_active('hotels_flights');">Hotels + Flights</a> </li>
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'hotels'))}}">Hotels + Flights</a> </li>
                    @endif
                    
                    @if(Request::is('/'))
                    <li> <a href="#tab4default" data-toggle="tab" onclick="tabs_new_active('train');">Train</a> </li>
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'transport'))}}">Train</a> </li>
                    @endif
                    
                    @if(Request::is('/'))
                    <li> <a href="#tab4default" data-toggle="tab" onclick="tabs_new_active('bus');">Bus</a> </li>
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'transport'))}}">Bus</a> </li>
                    @endif
                    
                    @if(Request::is('/'))
                    <li> <a href="#tab4default" data-toggle="tab" onclick="tabs_new_active('car_volvo');">Car/Volvo</a> </li>
                    @else
                    <li> <a href="{{ URL::route('home', array('redirect'=>'transport'))}}">Car/Volvo</a> </li>
                    @endif
                    
                    <li class="dropdown"> 
                      <a href="#" data-toggle="dropdown"> Latest Deals <i class="fa fa-caret-down"></i>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#tab4default" data-toggle="tab">The Chill Out  Summer Deal</a></li>
                        <li><a href="#tab5default" data-toggle="tab">Honeymoon Specials</a></li>
                      </ul>
                      </a> </li>
                  </ul>
            </div>
          
            
                    
          
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    @show
    
    <div class="page-wrapper clearfix">
    
<!--        <div id="loader"></div>-->
        
      @section('content')  
      <div class="banner-container top-widget-area push-bottom-20">
          <div class="container">
                   <div class="row">
                  <div class="col-lg-8 col-xs-12 form-widget-wrapper img-rounded">
                  	<h2 class="text-white">Plan Your Travel</h2>
                      <div class="form-widget-inner img-rounded push-bottom-20">
                      
                        <!--Tabs Start-->
            <div class="form-nav-tabs">
              <div class="panel-heading">
                <ul class="nav nav-tabs nt-std-tab search-form-tab">
     
        <li class="col-xs-3 text-center text-12 no-padding nav_new active" id="holidays"><a href="#tab3default" data-toggle="tab"><i class="fa fa-tree"></i>Holidays</a></li>      
        <li class="col-xs-3 text-center text-12 no-padding nav_new"   id="flights"><a href="#tab1default" data-toggle="tab"><i class="fa fa-plane"></i>Flights</a></li>       
        <li class="col-xs-3 text-center text-12 no-padding nav_new" id="hotels"><a href="#tab2default" data-toggle="tab"><i class="fa fa-hotel"></i>Hotels</a></li>       
        <li class="col-xs-3 text-center text-12 no-padding nav_new" id="transport"><a href="#tab4default" data-toggle="tab"><i class="fa fa-car"></i>Transport</a></li>
      
                </ul>
              </div>
              <div class="main-form-wrapper clearfix">
                <div class="tab-content clearfix">
                  <div class="tab-pane fade in" id="tab1default">
                    <div class="col-md-12 col-sm-12 col-xs-12 clearfix"> 
                      <!--One/Round Way Options-->
                      <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                        <div class="hidden-xs"> <a id="one-way-option-main" class="" tabindex="1">
                          <div class="radio">
                            <label style="font-size: 1em; padding:0;">
                              <input type="radio" name="o5" value="">
                              One Way <span class="cr"><i class="cr-icon fa fa-circle"></i></span> </label>
                          </div>
                          </a> </div>
                        <!--/.hidden-xs--> 
                      </div>
                      <!--/.one-way-box-->
                      
                      <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                        <div class="hidden-xs"> <a id="one-way-option-main" class="" tabindex="1">
                          <div class="radio">
                            <label style="font-size: 1em; padding:0;">
                              <input type="radio" name="o5" value="">
                              Round Trip <span class="cr"><i class="cr-icon fa fa-circle"></i></span> </label>
                          </div>
                          </a> </div>
                        <!--/.hidden-xs--> 
                      </div>
                      <!--/.one-way-box--> 
                      
                    </div>
                    <!--/One/Round Way Options--> 
                    
                    <!--From / To Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix">
                      <div class="col-md-5 col-sm-5">
                        <label>From</label>
                        <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">
                      </div>
                      <div class="col-md-1 col-sm-1 hidden-xs" style="margin-top:30px;  max-width:20px; padding:0; text-align:center;"><i class="fa fa-exchange" style="vertical-align:middle; font-size:20px; color:#94a2c4;"></i></div>
                      <div class="col-md-5 col-sm-5">
                        <label>To</label>
                        <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">
                      </div>
                    </div>
                    <!--/From / To Options--> 
                    
                    <!--Date Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix">
                      <div class="col-md-3 col-sm-3">
                        <label>Departure </label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2013" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
							 $(document).ready(function() {
								$('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
								  console.log(start.toISOString(), end.toISOString(), label);
								});
							 });
						  </script> 
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <label>Arrival </label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2013" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
							 $(document).ready(function() {
								$('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
								  console.log(start.toISOString(), end.toISOString(), label);
								});
							 });
						  </script> 
                        </div>
                      </div>
                    </div>
                    <!--Date Options--> 
                    
                    <!--People Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix">
                      <div class="col-xs-3">
                        <label class="text-12">Adult (12+ yrs) </label>
                        <div class="input-group number-spinner"> <span class="input-group-btn data-dwn">
                          <button class="btn btn-default" data-dir="dwn"><span class="fa fa-minus"></span></button>
                          </span>
                          <input type="text" class="form-control text-center input-color" value="1" min="1" max="7">
                          <span class="input-group-btn data-up">
                          <button class="btn btn-default" data-dir="up"><span class="fa fa-plus"></span></button>
                          </span> </div>
                      </div>
                      <div class="col-xs-3">
                        <label class="text-12">Children (0-12 yrs) </label>
                        <div class="input-group number-spinner"> <span class="input-group-btn data-dwn">
                          <button class="btn btn-default" data-dir="dwn"><span class="fa fa-minus"></span></button>
                          </span>
                          <input type="text" class="form-control text-center input-color" value="1" min="1" max="7">
                          <span class="input-group-btn data-up">
                          <button class="btn btn-default" data-dir="up"><span class="fa fa-plus"></span></button>
                          </span> </div>
                      </div>
                      <div class="col-xs-3">
                        <label class="text-12">Infant (0-2 yrs) </label>
                        <div class="input-group number-spinner"> <span class="input-group-btn data-dwn">
                          <button class="btn btn-default" data-dir="dwn"><span class="fa fa-minus"></span></button>
                          </span>
                          <input type="text" class="form-control text-center input-color" value="1" min="1" max="7">
                          <span class="input-group-btn data-up">
                          <button class="btn btn-default" data-dir="up"><span class="fa fa-plus"></span></button>
                          </span> </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-xs-12">
                        <label class="text-12">Class </label>
                        <div class="styled-select">
                          <select class="selectbox styled-select">
                            <option>Economy</option>
                            <option>Business</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <!--People Options-->
                    
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"> <a class="search nt-btn btn-orange" href="#">Search</a> </div>
                  </div>
                  <!--/.tab-pane--> 
                  
                  <!--HOTELS FORM SECTION-->
                  
                
                  <div class="tab-pane fade in" id="tab2default"> 
                                 
                    <!--GOING TO OPTION-->
                    <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix">
                      <div class="col-md-9 col-sm-8 col-xs-12">
                        <label>Going To</label>
                        <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">
                      </div>
                    </div>
                    <!--/From / To Options--> 
                    
                    <!--Date Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix">
                      <div class="col-md-3 col-sm-3">
                        <label>Check in </label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2013" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
							 $(document).ready(function() {
								$('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
								  console.log(start.toISOString(), end.toISOString(), label);
								});
							 });
						</script> 
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <label>Check out</label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2013" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
							 $(document).ready(function() {
								$('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
								  console.log(start.toISOString(), end.toISOString(), label);
								});
							 });
						  </script> 
                        </div>
                      </div>
                    </div>
                    <!--Date Options--> 
                    
                    <!--People Options-->
                    <div class="col-md-12 col-sm-12 clearfix room-info box-divider">
                      <div class="col-md-1 col-sm-1 room-count">
                        <div class="row room-title"> Room &nbsp;<span>1</span> </div>
                      </div>
                      <div class="col-xs-3">
                        <label class="text-12">Adult (12+ yrs) </label>
                        <div class="input-group number-spinner"> <span class="input-group-btn data-dwn">
                          <button class="btn btn-default" data-dir="dwn"><span class="fa fa-minus"></span></button>
                          </span>
                          <input type="text" class="form-control text-center input-color" value="1" min="1" max="7">
                          <span class="input-group-btn data-up">
                          <button class="btn btn-default" data-dir="up"><span class="fa fa-plus"></span></button>
                          </span> </div>
                      </div>
                      <div class="col-xs-3">
                        <label class="text-12">Children (0-12 yrs) </label>
                        <div class="input-group number-spinner"> <span class="input-group-btn data-dwn">
                          <button class="btn btn-default" data-dir="dwn"><span class="fa fa-minus"></span></button>
                          </span>
                          <input type="text" class="form-control text-center input-color" value="1" min="1" max="7">
                          <span class="input-group-btn data-up">
                          <button class="btn btn-default" data-dir="up"><span class="fa fa-plus"></span></button>
                          </span> </div>
                      </div>
                    </div>
                    <!--People Options-->
                    
                    <div class="col-md-12 col-sm-12 clearfix">
                      <div class="col-md-3 col-sm-3"> <a href="#" class="add-room btn nt-btn btn-default">Add Room <i class="fa fa-plus"></i></a> </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"> <a class="search nt-btn btn-orange" href="#">Search</a> </div>
                  </div>
                  
                  <!--HOLIDAY FORM SECTION-->
                   
                  <div class="tab-pane fade in" id="tab3default"> 
                   
                    
                    <!--GOING TO OPTION-->
                    <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix">
                      <div class="col-md-9 col-sm-8 col-xs-12">
                        <label>Going To</label>
                        <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">
                      </div>
                    </div>
                    <!--/From / To Options--> 
                    
                    <!--Date Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix">
                      <div class="col-xs-4">
                        <label>In the Month of</label>
                        <div class="styled-select">
                          <select class="selectbox styled-select">
                            <option>May 2015</option>
                            <option>June 2015</option>
                            <option>July 2015</option>
                            <option>August 2015</option>
                            <option>September 2015</option>
                            <option>October 2015</option>
                            <option>November 2015</option>
                            <option>December 2015</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <label>Fixed Date</label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2015" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
							 $(document).ready(function() {
								$('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
								  console.log(start.toISOString(), end.toISOString(), label);
								});
							 });
						  </script> 
                        </div>
                      </div>
                    </div>
                    <!--Date Options-->
                    
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"> <a class="search nt-btn btn-orange" href="#">Search</a> </div>
                    <div class=" divider clearfix"></div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right text-center">
                      <p>Don't have a plan yet?</p>
                      <a class="nt-btn btn-green btn-block" href="theme_packages_landing.html">I Wish to Explore all Deals</a> </div>
                  </div>
                  
                  <!--Car/Bus/Volvo FORM SECTION-->
                 
                  <div class="tab-pane fade in" id="tab4default">
                  
                    <div class="col-md-12 col-sm-12 col-xs-12 clearfix"> 
                      <!--One/Round Way Options-->
                      <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                        <div class="hidden-xs"> <a id="one-way-option-main" class="" tabindex="1">
                          <div class="radio">
                            <label style="font-size: 1em; padding:0;">
                              <input type="radio" name="o5" value="">
                              One Way <span class="cr"><i class="cr-icon fa fa-circle"></i></span> </label>
                          </div>
                          </a> </div>
                        <!--/.hidden-xs--> 
                      </div>
                      <!--/.one-way-box-->
                      
                      <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                        <div class="hidden-xs"> <a id="one-way-option-main" class="" tabindex="1">
                          <div class="radio">
                            <label style="font-size: 1em; padding:0;">
                              <input type="radio" name="o5" value="">
                              Round Trip <span class="cr"><i class="cr-icon fa fa-circle"></i></span> </label>
                          </div>
                          </a> </div>
                        <!--/.hidden-xs--> 
                      </div>
                      <!--/.one-way-box--> 
                      
                    </div>
                    <!--/One/Round Way Options--> 
                    
                    <!--From / To Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix">
                      <div class="col-md-5 col-sm-5">
                        <label>Picking Up</label>
                        <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">
                      </div>
                      <div class="col-md-1 col-sm-1 hidden-xs" style="margin-top:30px;  max-width:20px; padding:0; text-align:center;"><i class="fa fa-exchange" style="vertical-align:middle; font-size:20px; color:#94a2c4;"></i></div>
                      <div class="col-md-5 col-sm-5">
                        <label>Dropping Off</label>
                        <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">
                      </div>
                    </div>
                    <!--/From / To Options--> 
                    
                    <!--Date Options-->
                    <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix">
                      <div class="col-md-3 col-sm-3">
                        <label>Departure</label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2013" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
                                                     $(document).ready(function() {
                                                        $('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
                                                          console.log(start.toISOString(), end.toISOString(), label);
                                                        });
                                                     });
                                                     </script> 
                        </div>
                      </div>
                      <div class="col-md-3 col-sm-3">
                        <label>Arrival </label>
                        <div class="">
                          <form class="form-horizontal">
                            <fieldset>
                              <div class="control-group">
                                <div class="controls">
                                  <div class="input-prepend input-group"> 
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i>                                                                              </span>
                                    <input type="text" name="birthday" id="birthday" class="form-control input-color" value="03/18/2013" />
                                  </div>
                                </div>
                              </div>
                            </fieldset>
                          </form>
                          <script type="text/javascript">
							 $(document).ready(function() {
								$('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
								  console.log(start.toISOString(), end.toISOString(), label);
								});
							 });
							 </script> 
                        </div>
                      </div>
                    </div>
                    <!--Date Options-->
                    
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12"> <a class="search nt-btn btn-orange" href="#">Send Enquiry</a> </div>
                  </div>
                </div>
              </div>
            </div>
                          
                        <!--Tabs Ends-->
                        
                    </div>
                  </div>
                  
                              
                 <div class="top-widget-banner-right col-lg-4 hidden-md hidden-sm hidden-xs">
                  <div class="top-widget-banner-1 push-bottom-20"> 
                    <img src="{{ asset('theme/front-v4/images/widget_sliders/top_widget_slider/top_widget_slide_01.jpg') }}"/>                       
                  </div>
                    <div class="top-widget-banner-2"> 
                    <img src="{{ asset('theme/front-v4/images/widget_sliders/top_widget_banner_2/top-widget-banner-2.jpg') }}"/> 
                   </div>
                </div>
                  
              </div>
              <!-- /.row -->
      
          </div>
          <!-- /.container -->
            
      </div><!--/.top-widget-area-->
      
      <div class="body-container clearfix">
      	<div class="container container-fluid">
        	<div class="row">
            	<div class="col-lg-4 col-md-6 col-sm-6">
                	<h2 class="section-title">Season's Special</h2>
                    <div class="box-rd">
                    	<img src="{{ asset('theme/front-v4/images/widget_sliders/season_spl_widget/season-spl-01.jpg') }}" class="img-responsive" />
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                	<h2 class="section-title">Hot Picks!</h2>
                	<div class="list-display hot-picks panel box-rd">
                    	
                        <div class="with-nav-tabs">
                          <div class=" panel-heading">
                            <ul class="nav nav-tabs nt-std-tab">
                                <li class="active col-xs-4 text-center no-padding"><a class="nt-tab" href="#hotpickdomestic" data-toggle="tab">Domestic</a></li>
                                <li class="col-xs-4 text-center no-padding"><a class="nt-tab" href="#hotpickint" data-toggle="tab">Internatinal</a></li>
                                <li class="col-xs-4 text-center no-padding"><a class="nt-tab" href="#hotpickflthotel" data-toggle="tab">Flight + Hotel</a></li>
                            </ul>
                          </div>
                          <div class="panel-body">
                              <div class="tab-content">
                                  <div class="tab-pane fade in active" id="hotpickdomestic">
                                    <div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                        	<img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <p class="text-16">Exotic Kerala</p>
                                          <p class="text-12" style="margin-bottom:5px;">5 Nights / 6 Days</p>
                                          <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>10,999/- onwards</p>
                                      </div>
                                    </div>
                                    
                                    <div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                        	<img class="img-responsive" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}" />
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <p class="text-16">Karismatic Kashmir</p>
                                          <p class="text-12" style="margin-bottom:5px;">4 Nights / 5 Days</p>
                                          <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                                      </div>
                                    </div>
                                  </div><!--/tab1-->
                                  
                                  <div class="tab-pane fade" id="hotpickint">
                                  	<div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                        	<img class="img-responsive" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}" />
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <p class="text-16">Exotic Kerala</p>
                                          <p class="text-12" style="margin-bottom:5px;">5 Nights / 6 Days</p>
                                          <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>10,999/- onwards</p>
                                      </div>
                                    </div>
                                    
                                    <div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                        	<img class="img-responsive" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}" />
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <p class="text-16">Karismatic Kashmir</p>
                                          <p class="text-12" style="margin-bottom:5px;">4 Nights / 5 Days</p>
                                          <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                                      </div>
                                    </div>
                                  </div><!--/tab2-->
                                  
                                  <div class="tab-pane fade" id="hotpickflthotel">
                                  	<div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                        	<img class="img-responsive" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}" />
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <p class="text-16">Exotic Kerala</p>
                                          <p class="text-12" style="margin-bottom:5px;">5 Nights / 6 Days</p>
                                          <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>10,999/- onwards</p>
                                      </div>
                                    </div>
                                    
                                    <div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                        	<img class="img-responsive" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}" />
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <p class="text-16">Karismatic Kashmir</p>
                                          <p class="text-12" style="margin-bottom:5px;">4 Nights / 5 Days</p>
                                          <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                                      </div>
                                    </div>
                                  </div><!--/tab3-->
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                	<h2 class="section-title">Travel Guide</h2>
                    <div class="box-rd">
                		<img class="img-responsive" src="{{ asset('theme/front-v4/images/widget_sliders/travel_guide/travel_guide_01.jpg') }}" />
                    </div>
                </div>
                
            </div>
            
            
            <div class="row push-bottom-20">
            	<div class="recmd-hotels col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h2 class="section-title">Recommended Hotels</h2>
          <div class="panel">
            <div class="carousel slide" id="Carousel">
              <ol class="carousel-indicators">
                <li class="" data-slide-to="0" data-target="#Carousel"></li>
                <li data-slide-to="1" data-target="#Carousel" class=""></li>
                <li data-slide-to="2" data-target="#Carousel" class="active"></li>
              </ol>
              
              <!-- Carousel items -->
              <div class="carousel-inner">
                <div class="item">
                  <div class="row"> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel01.jpg') }}"> </div>   
                      
                                       
                     <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> 
                        <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel02.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> 
                        <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span>
                      </div>
                    </div>
                    </a> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel03.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> </div>
                  <!--.row--> 
                </div>
                <!--.item-->
                
                <div class="item">
                  <div class="row"> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel04.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel05.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel06.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> </div>
                  <!--.row--> 
                </div>
                <!--.item-->
                
                <div class="item active">
                  <div class="row"> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel01.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel02.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> <a href="#">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <div class="thumbnail"> <img style="max-width:100%;" alt="Image" src="{{ asset('theme/front-v4/images/hotels/hotel03.jpg') }}"> </div>
                      <div class="text-16">Leela The Grand <span class="text-14 fade-color"><i class="fa fa-map-marker "></i> Goa</span></div>
                      <span class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </span><!--/.hotel-star-->
                      <div class="text-12 fade-color">Starting from <span class="text-highlight"><i class="fa fa-inr"></i>4999/-</span></div>
                    </div>
                    </a> </div>
                  <!--.row--> 
                </div>
                <!--.item--> 
                
              </div>
              <!--.carousel-inner--> 
              <a class="left carousel-control" href="#Carousel" data-slide="prev">‹</a> <a class="right carousel-control" href="#Carousel" data-slide="next">›</a> 
            </div>
          </div>
        </div>
                 <div class="pop-dest pull-right col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <h2 class="section-title">Popular Destinations</h2>
                      <div class="list-display panel box-rd">
                        <div class="with-nav-tabs">
                          <div class="panel-heading">
                            <ul class="nav nav-tabs nt-std-tab">
                              <li class="active col-xs-6 text-center no-padding"><a class="nt-tab" href="#pop-dest-tab1default" data-toggle="tab">Domestic</a></li>
                              <li class="col-xs-6 text-center no-padding"><a class="nt-tab" href="#pop-dest-tab2default" data-toggle="tab">Internatinal</a></li>
                            </ul>
                          </div>
                          <div class="panel-body1">
                            <div class="tab-content">
                              <div class="tab-pane fade in active" id="pop-dest-tab1default">
                                <div class="clearfix list-group-item">
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                                  </div>
                                  <div class="pull-left">
                                    <p class="text-16">Manali</p>
                                    <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>10,999/- onwards</p>
                                  </div>
                                </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Shimla</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Dalhousie</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Munnar</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Jaipur</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                  </div>
                  <!--/tab1-->
                  
                  <div class="tab-pane fade" id="pop-dest-tab2default">
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Thaliland</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>10,999/- onwards</p>
                      </div>
                    </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Dubai</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Paris</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                    <div class="clearfix list-group-item">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="img-box-sml1"> <img class="img-responsive" src="images/img-placeholder.jpg" /> </div>
                      </div>
                      <div class="pull-left">
                        <p class="text-16">Singapore</p>
                        <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i>16,999/- onwards</p>
                      </div>
                    </div>
                  </div>
                  <!--/tab2--> 
                </div>
              </div>
            </div>
          </div>
        </div><!--/.pop-dest-->
                
                 <div class="col-lg-8 col-md-8 col-sm-12" style="overflow:hidden;">
          <h2 class="section-title">Why US?</h2>
          <div class="panel box-rd">
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img  src="{{ asset('theme/front-v4/images/expert-advice.png') }}" />
                  <p class="text-14">Get Expert Advice</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img  src="{{ asset('theme/front-v4/images/24hr.png') }}" />
                 
                  <p class="text-14">24 / 7 Services</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i class="fa fa-thumbs-up" style="font-size: 60px; color: #B5BDC9;"></i>
                  <p class="text-14">Best Price Guarantee</p>
                </div>
              </div>
              <div class="row push-top-20">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i class="fa fa-magic" style="font-size: 60px; color: #B5BDC9;"></i>
                  <p class="text-14">Customize Solutions</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"><img  src="{{ asset('theme/front-v4/images/save_money.png') }}" /> 
                  <p class="text-14">Save Money</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i class="fa fa-smile-o" style="font-size: 60px; color: #B5BDC9;"></i>
                  <p class="text-14">We Value Your Time</p>
                </div>
              </div>
            </div>
          </div>
        </div>
                
                
                
             <div class="col-lg-8 col-md-12 col-sm-12" style="overflow:hidden;">
          <h2 class="section-title">Subscribe to Our Newsletter</h2>
          <div class="panel box-rd">
            <div class="panel-body subscribe-box">
              <h4>Get Latest Deals In Your Inbox</h4>
              <p>Sign up to get the latest deals and offers directly into your inbox!</p>
              <div id="mc_embed_signup" class="email-signup-wrapper">
                <form id="mc-embedded-subscribe-form" class="validate" novalidate target="_blank" name="mc-embedded-subscribe-form" method="post" action="">
                  <div class="input-group">
                    <input id="mce-EMAIL" class="required email form-control inputbox autowidth newsletter-input" type="email" name="EMAIL" placeholder="E-mail address" value="">
                    <span class="input-group-btn"> <a href="#"><i class="fa fa-paper-plane"></i></a> </span> </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12" style="overflow:hidden;">
          <h2 class="section-title">We Are Social</h2>
          <div class="panel box-rd">
            <div class="panel-body"> 
              
              <!-- Facebook -->
              <div class="col-md-3 col-xs-12"> <a href="https://www.facebook.com/sharer/sharer.php?u=" title="Share on Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook" style="font-size: 35px;"></i> Share on Facebook</a> </div>
              <!-- Google+ -->
              <div class="col-md-3 col-xs-12"> <a href="https://plus.google.com/share?url=" title="Share on Google+" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus" style="font-size: 35px;"></i> Share on Google+</a> </div>
              <!-- LinkedIn -->
              <div class="col-md-3 col-xs-12"> <a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=" title="Share on LinkedIn" target="_blank" class="btn btn-linkedin"><i class="fa fa-linkedin" style="font-size: 35px;"></i> Share on LinkedIn</a> </div>
              <!-- Twitter -->
              <div class="col-md-3 col-xs-12"> <a href="http://twitter.com/home?status=" title="Share on Twitter" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter" style="font-size: 35px;"></i> Share on Twitter</a> </div>
            </div>
          </div>
        </div>
       </div>
          
           
      <!--/Bottom Links-->
      <div class="row push-bottom-20">
        <div class="col-lg-10 col-md-12 col-sm-12 text-12">
          <h4>India Holidays</h4>
          <p>
          <a href="{{ URL::route('home') }}">Kerala Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Himachal Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">South India Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Rajasthan Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Kathmandu Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Uttarakhand Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Goa Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Golden Triangle Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Kashmir Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Andaman Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Leh Ladakh Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">North East Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Manali Volvo Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Kashmir Holiday Packages with Flights</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Manali Group Tour</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Goa Holiday Packages with Flights</a> &nbsp;           
          <a href="{{ URL::route('home') }}">Dharamshala Holiday Packages</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Kerala Holiday Packages with Flights</a> &nbsp; 
          <a href="{{ URL::route('home') }}">Norht East Holiday Packages</a> &nbsp;</p>
        </div>
      </div>
      <!--/Bottom Links--> 
                        
      </div>
    </div> <!--/.body-container-->
      @show
    
         
        
      @section('footer')  
      <!--Footer-->
      <footer class="hidden-xs">
        <div class="container container-fluid">
          <div class="row push-bottom-20">
            <div class="col-lg-3 col-md-3 col-sm-3">
              <ul class="footer-list list-unstyled text-12">
                <h4>NassaTravels</h4>
                <li><a href="{{ URL::route('home') }}">About Us</a></li>
                <li><a href="{{ URL::route('home') }}">Blog</a></li>
                <li><a href="{{ URL::route('home') }}">Testimonial</a></li>
                <li><a href="{{ URL::route('home') }}">Deals & Offers</a></li>
                <li><a href="{{ URL::route('home') }}">Careers</a></li>
                <li><a href="{{ URL::route('home') }}">Holidays in India</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <ul class="footer-list list-unstyled text-12">
                <h4>Services</h4>
                <li><a href="{{ URL::route('home') }}">Flights</a></li>
                <li><a href="{{ URL::route('home') }}">Hotels</a></li>
                <li><a href="{{ URL::route('home') }}">International Hotels</a></li>
                <li><a href="{{ URL::route('home') }}">Holidays in India</a></li>
              </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <ul class="footer-list list-unstyled text-12">
                <h4>Other Links</h4>
                <li><a href="{{ URL::route('home') }}">FAQs</a></li>
                <li><a href="{{ URL::route('home') }}">Sitemap</a></li>
                <li><a href="{{ URL::route('home') }}">Feedback</a></li>
                <li><a href="{{ URL::route('home') }}">Contact Us</a></li>
                <li><a href="{{ URL::route('home') }}">Terms & Conditions</a></li>
                <li><a href="{{ URL::route('home') }}">Privacy Poilicy</a></li>
                <li><div class="pm-button"><a href="https://www.payumoney.com/paybypayumoney/#/109789"><img src="https://www.payumoney.com//media/images/payby_payumoney/buttons/212.png" /></a></div></li>  
              </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <ul class="footer-list list-unstyled text-12">
                <h4>Best Selling Packages</h4>
                <li><a href="{{ URL::route('home') }}">Five Gems Of Kerala</a></li>
                <li><a href="{{ URL::route('home') }}">Goa Package</a></li>
                <li><a href="{{ URL::route('home') }}">Kashmir Package</a></li>
                <li><a href="{{ URL::route('home') }}">Manali Package</a></li>
                <li><a href="{{ URL::route('home') }}">Singapore Package</a></li>
                <li><a href="{{ URL::route('home') }}">Dubai Package</a></li>
              </ul>
            </div>
          </div>
          <!--/row--> 
        </div>
        <div class="footer-bottom">
          <div class="container container-fluid"> <small class="fade-color pull-left">nassatravels.com | &copy; 2015 | All Rights Reserve</small>
            <div class="pull-right">
              <ul class="list-unstyled list-inline" style="margin-bottom:0;">
                <li><a href="#"><i class="fa fa-facebook" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus" style="font-size: 20px; color: #7d7d7d;"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      <!--/.Footer--> 
      @show
    </div>
    <!--/ .page-wrapper-->
    
    <!-- jQuery Version 1.11.1 -->
    <script src="{{ asset('theme/front-v4/js/jquery.js') }}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    {{ HTML::script('theme/nassatravels/assets/js/jquery-ui-1.9.2.custom.min.js') }}
    {{ HTML::script('theme/nassatravels/assets/js/jquery.ui.touch-punch.min.js') }}
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('theme/front-v4/js/bootstrap.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('theme/front-v4/js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('theme/front-v4/js/daterangepicker.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('theme/front-v4/js/snipper.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('theme/front-v4/js/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('theme/front-v4/js/URI.min.js') }}"></script>
    {{ HTML::script('theme/nassatravels/assets/js/angular.min.js') }} 
    <script type="text/javascript" src="{{ asset('theme/front-v4/js/jBox.min.js') }}"></script>
    
    {{HTML::script('theme/nassatravels/assets/js/bootstrap-rating.min.js')}}
 
    
    <script>
		$(document).ready(function() {

			$('a.xhr-modal.fresh').each(function(k,v){
				new jBox('Modal', {
					attach:$(v),
					ajax: {
						url: $(v).data('url'),
						reload: true,
						spinner:true, 
					},
					animation: 'pulse',
					maxWidth:720,
					closeButton: 'box'
				}); 
				$(v).removeClass('fresh');
			});
		});
      
 function tabs_new_active(data) 
      {             
            $('.nav_new').removeClass('active');
   
            if(data =='holidays')
                    {
                      $('#holidays').addClass('active');
                    }
           
            if(data =='flights')                                                     
                    { 
                      $('#flights').addClass('active');               
                    }
           
            if(data =='hotels')
                    {                    
                      $('#hotels').addClass('active');
                    }
           
            if(data =='hotels_flights')
                    {                    
                      $('#hotels').addClass('active');
                    }
          
            if(data =='train')
                    {                    
                      $('#transport').addClass('active');
                    }
          
            if(data =='bus')
                    {                    
                      $('#transport').addClass('active');
                    }
          
            if(data =='car_volvo')
                    {
                      $('#transport').addClass('active');
                    }          
        }
   
  </script>
    
    @section('js')
    @show

</body>

</html>
