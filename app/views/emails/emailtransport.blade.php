Transport Booking Details<br/>

Trip Type : @if(isset($input['radioway'])) {{$input['radioway']}} @else @endif<br/>

Picking up : @if(isset($input['picking'])) {{ $input['picking']}} @else @endif<br/>

Dropping off :@if(isset($input['dropping'])) {{ $input['dropping']}} @else @endif<br/>

Departure Date : @if(isset($input['dept'])) {{$input['dept']}} @else @endif<br/>

Arrival Date : @if(isset($input['arrival'])) {{$input['arrival']}} @else @endif<br/>
