<p>To reset your password, complete this form:</p> 
<p><a href="{{ URL::to('password/reset', array($token)) }}">{{ URL::to('password/reset', array($token)) }}</a></p>
<p>This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.</p>
