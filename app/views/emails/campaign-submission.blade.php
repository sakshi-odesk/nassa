<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h4>Following details were provided</h4>

        <div>
            <p>Campaign : {{$title}}</p>
            <p>----------------------------</p>
            <p>Name : {{$input['name']}}</p>
            <p>Phone : {{$input['phone_no']}}</p>
            <p>Email : {{$input['email']}}</p>
            <p>Best time to call : {{$input['best_time_to_call']}}</p>
        </div>
    </body>
</html>