<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h4>Following details were provided</h4>

        <div>
            <p>New {{$input['heading']}} has been received successfully!</p>
            <p>Name : {{$input['name']}}</p>
            <p>Phone : {{$input['phoneno']}}</p>
            <p>Email : {{$input['email']}}</p>
            <p>Message : {{$input['message']}}</p>
        </div>
    </body>
</html>