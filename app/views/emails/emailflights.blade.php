Flight Booking Details<br/>

From : @if(isset($input['fcity'])) {{$input['fcity']}} @else @endif <br/>

To : @if(isset($input['tcity'])) {{$input['tcity']}} @else @endif<br/>

Trip Type :@if(isset($input['type'])) {{$input['type']}} @else @endif <br/>

Departure Date : @if(isset($input['dept'])) {{$input['dept']}} @else @endif<br/>

Arrival Date : @if(isset($input['arrival'])) {{$input['arrival']}} @else @endif<br/>

Adult : @if(isset($input['adult'])) {{$input['adult']}} @else @endif<br/>

Children : @if(isset($input['children']))  {{$input['children']}} @else @endif<br/>

Infant : @if(isset($input['infant'])) {{$input['infant']}} @else @endif<br/>

class : @if(isset($input['class'])) {{$input['class']}} @else @endif<br/>