Hotel Booking Details<br/>

Going To : @if(isset($input['gcity'])) {{$input['gcity']}} @else @endif<br/>

Check in Date:   @if(isset($input['checkin'])) {{$input['checkin']}} @else @endif<br/>

Check out Date : @if(isset($input['checkout'])) {{$input['checkout']}} @else @endif<br/>

Room 1 Adult : @if(isset($input['adult1'])) {{$input['adult1']}} @else @endif<br/>

Room 1 Children : @if(isset($input['children1'])) {{$input['children1']}} @else @endif<br/>
