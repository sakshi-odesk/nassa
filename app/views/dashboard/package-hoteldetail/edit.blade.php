@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
          <h3><i class="fa fa-angle-right"></i>Edit Package Hotel Detail</h3>
         <div class="row mt">
            {{ Form::open(array('action' => array('DashboardController@postPackageHotelDetailEdit'),'files'=> true, 'role' => 'form'))}}
             
            <div class="container-fluid">
                <input type="hidden" name="id" {{isset($hotel_detail) ? 'value="'.$hotel_detail['id']. '"' : ''}} >
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Destination :</label>
                                <input type="text" name="destination" class="form-control input-sm" required
                                       placeholder="Destination..." {{isset($hotel_detail) ? 'value="'.$hotel_detail['destination']. '"' : ''}} />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Hotel :</label>
                                <input type="text" name="hotel" class="form-control input-sm" required
                                       placeholder="Hotel..." {{isset($hotel_detail) ? 'value="'.$hotel_detail['hotel']. '"' : ''}} />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Meal Plan:</label>
                                <input type="text" name="meal_plan" class="form-control input-sm" required
                                       placeholder="Meal Plan..." {{isset($hotel_detail) ? 'value="'.$hotel_detail['meal_plan']. '"' : ''}} />
                            </div>
                        </div>
                    </div>
                    <div class="login-footer">
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop