@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($setting))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Setting from <i>"{{$setting->name}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Setting <i>"{{$setting->name}}"</i>
                <a href="{{URL::route('dashboard.setting.cloning',array($setting->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
        <h3><i class="fa fa-angle-right"></i>Add New Setting</h3>
        @endif
        <div class="row mt">
            @if(isset($setting))
                @if(isset($clone))
                    {{ Form::open(array( 'action' => 'DashboardController@postSettingCloning','files' => true,'role' => 'form')) }}
                @else
                    {{ Form::open(array( 'action' => 'DashboardController@postSettingEdit','files' => true,'role' => 'form')) }}
                @endif
            @else
                {{ Form::open(array( 'action' => 'DashboardController@postSettingAdd','files' => true,'role' => 'form')) }}
            @endif

            <input type="hidden" name="id" @if((isset($setting)) && (!isset($clone))) value="{{$setting['id']}}" @endif>

            <div class="container-fluid">
                <div class="form-group">
                             <label class="control-label">Name :</label>
                            <input type="text" class="form-control input-sm" name="name" placeholder="Enter Name" {{isset($setting) ? 'value="'.$setting['name']. '"' : ''}}>
                        </div>

                <div class="form-group">
                            <label class="control-label">Description :</label>
                            <input type="text" class="form-control input-sm" name="desc" placeholder="Description" {{isset($setting) ? 'value="'.$setting['desc']. '"' : ''}}>
                        </div>

                @if(!isset($setting))
                <div class="form-group">
                    <select id="type" class="form-control input-sm" name="type">
                        <option value="">Select Type...</option>
                        <option value="text">Text Field</option>
                        <option value="textarea">Textarea</option>
                        <option value="file">File</option>
                    </select>
                </div>
                @endif

                @if(isset($setting))
                    <input type="hidden" id="input_hidden" name="type" value="{{$setting['type']}}">
                @endif

                <div class="form-group">
                            <input id="input_text" type="text" class="form-control input-sm" placeholder="value" {{isset($setting) ? 'value="'.unserialize($setting['value']). '"' : ''}}>
                        </div>

                <div class="form-group">
                            <input id="input_file" type="file"><br>
                            @if(isset($setting))
                                @if(!isset($clone))
                                    @if($setting['type'] == 'file')
                                    <a class="input_file_logo" href="{{URL::asset(unserialize($setting['value']))}}" target="_blank">Your uploaded file</a>
                                    @endif
                                @endif
                            @endif
                        </div>

                <div class="form-group">
                            @if(!isset($setting))
                            <textarea id="input_textarea" class="form-control input-sm" placeholder="Text" rows="4"></textarea>
                            @else
                            <textarea id="input_textarea" class="form-control input-sm" rows="4">{{isset($setting) ? unserialize($setting['value']):''}}</textarea>
                            @endif
                        </div>

                <div class="login-footer">
                             @if(isset($setting))
                                @if(isset($clone))
                                    <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Clone</button>
                                @else
                                    <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                                @endif
                            @else
                                <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @endif
                        </div>
                {{ Form::close() }}
            </div>
        </div>
         
    </section><! --/wrapper -->
</section>
@stop

@section('js')
<script type="text/javascript">
$(document).ready(function () { 
    
    if ($('#input_hidden').val() == 'text') 
    {
        $('#input_text').show();
        $('#input_text').attr('name','value');
        $('#input_textarea').hide();
        $('#input_file').hide();
     }

    if ($('#input_hidden').val() == 'textarea') {
        $('#input_textarea').show();
        $('#input_textarea').attr('name','value');
        $('#input_text').hide();
        $('#input_file').hide();
     }

    if ($('#input_hidden').val() == 'file') {
        $('#input_file').show();
        $('#input_file').attr('name','value');
        $('#input_text').hide();
        $('#input_textarea').hide();
     }
    
    $('#type').bind('change keyup', function () {
        
        if ($(this).val() == 'text') {
            $('#input_text').show();
            $('#input_text').attr('name','value');
            $('#input_textarea').hide();
            $('#input_file').hide();
        }
        
        if ($(this).val() == 'textarea') {           
            $('#input_textarea').show();
            $('#input_textarea').attr('name','value');
            $('#input_text').hide();
            $('#input_file').hide();
        }

        if ($(this).val() == 'file') {
            $('#input_file').show();
            $('#input_file').attr('name','value');
            $('#input_text').hide();
            $('#input_textarea').hide();
        }
    });
    
});
</script>
@stop