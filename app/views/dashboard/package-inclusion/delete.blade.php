@extends('layout.dashboard')

@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>Delete Inclusion "{{$inclusion->title}}"</h3>
        <div class="row mt">
            {{ Form::open(array( 'action' => 'DashboardController@postPackageInclusionDelete', 'files' => true,'role' => 'form')) }} 
            <div class="container-fluid">
                
                <div class="box-body">
                <p>This action is irreversible. 
                    Do you wish to continue ?</p>
                    {{ Form::hidden('id', $inclusion->id) }}
                </div>
                <div class="box-footer">
                    <a class="btn btn-secondary btn-success" href="{{URL::route('dashboard.package.edit',array($inclusion->package_id))}}">Back to package</a>
                    <button class="btn btn-primary btn-danger pull-right" type="submit">Yes, Delete this Inclusion</button>
                </div>
            
            </div>
                {{ Form::close()}}
            </div>
    </section><! --/wrapper -->
</section>
@stop