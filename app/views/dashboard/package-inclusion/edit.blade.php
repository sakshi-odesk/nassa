@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
       <h3><i class="fa fa-angle-right"></i>Edit Package Inclusion</h3>
        <div class="row mt">
           
            {{ Form::open(array('action' => array('DashboardController@postPackageInclusionEdit'),'files'=> true, 'role' => 'form'))}}
           
            <input type="hidden" name="id" {{isset($inclusion) ? 'value="'.$inclusion['id']. '"' : ''}} >
            <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Inclusion Title :</label>
                                <input type="text" name="title" class="form-control input-sm" required
                                       placeholder="Inclusion Title..." {{isset($inclusion) ? 'value="'.$inclusion['title']. '"' : ''}} />
                            </div>
                        </div>
                    </div>
                    <div class="login-footer">
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop