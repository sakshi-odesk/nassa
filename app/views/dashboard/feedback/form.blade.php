@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($feedback))
        <h3><i class="fa fa-angle-right"></i>Edit Feedback from <i>"{{$feedback->name}}"</i></h3>
        @else
         <h3><i class="fa fa-angle-right"></i>Add New Feedback</h3>
        @endif
        
         <div class="row mt">
            @if(isset($feedback))
                {{ Form::open(array('action' => array('DashboardController@postFeedbackEdit'), 'role' => 'form'))}}
            @else
                {{ Form::open(array('action' => array('DashboardController@postFeedbackAdd'), 'role' => 'form'))}}
            @endif

            <input type="hidden" name="id" @if(isset($feedback)) value="{{$feedback['id']}}" @endif>
             
            <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Name :</label>
                                <input type="text" name="name" class="form-control input-sm"
                                       placeholder="Name" {{isset($feedback) ? 'value="'.$feedback['name']. '"' : ''}} />
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Email :</label>
                                <input type="text" name="email" class="form-control input-sm"
                                       placeholder="Email" {{isset($feedback) ? 'value="'.$feedback['email']. '"' : ''}} />
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Phone :</label>
                                <input type="text" name="phone" class="form-control input-sm"
                                       placeholder="Phone" {{isset($feedback) ? 'value="'.$feedback['phone']. '"' : ''}} />
                            </div> 
                        </div>
                
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Message :</label>
                                <textarea name="message" class="form-control input-sm" rows="4" placeholder="Message">{{isset($feedback) ? $feedback['message']: ''}}</textarea>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        @if(!isset($feedback))
                                        <input id='published' name='published' type="checkbox" value="1"> Publish this Feedback
                                        @else
                                            @if($feedback['published'] == 0)
                                            <input id='published' name='published' type="checkbox" value="1"> Publish this Feedback
                                            @else
                                            <input id='published' name='published' type="checkbox" value="1" checked> Publish this Feedback
                                            @endif
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="login-footer">
                        @if(isset($feedback))
                            <button type="submit" name="edit" class="btn btn-primary btn-sm pull-left">Submit</button>
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                        @endif
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop