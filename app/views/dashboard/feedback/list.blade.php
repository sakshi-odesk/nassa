@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                              <h4>
                                  <i class="fa fa-angle-right"></i> Feedback
                                  <a href="{{URL::route('dashboard.feedback.form')}}" class="btn btn-info btn-sm pull-right">
                                  <i class="fa fa-building-o"></i>&nbsp; Add New Feedback</a>
                              </h4>
                              <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Name</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Email</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Phone</th>
                                  <th><i class=" fa fa-bookmark"></i> Status</th>
                                  <th><i class=" fa fa-edit"></i> Action</th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($feedbacks as $feedback)
                                <tr>
                                    <td>{{ $feedback->created_at }}</td>
                                    <td>{{ $feedback->name }}</td>
                                    <td>{{ $feedback->email }}</td>
                                    <td>{{ $feedback->phone }}</td>
                                    <td>
                                        @if($feedback->published)
                                        <span class="label label-success">Published</span>
                                        @else
                                        <span class="label label-danger">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{URL::route('dashboard.feedback.edit',array($feedback->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;
                                        <a href="{{URL::route('dashboard.feedback.delete',array($feedback->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                   </td>
                                </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
              {{$feedbacks->links()}}
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

