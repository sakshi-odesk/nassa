@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
          <h3><i class="fa fa-angle-right"></i>Edit Package PriceSummary</h3>
         <div class="row mt">
            {{ Form::open(array('action' => array('DashboardController@postPackagePriceSummaryEdit'),'files'=> true, 'role' => 'form'))}}
             
             <input type="hidden" name="id" {{isset($price_summary) ? 'value="'.$price_summary['id']. '"' : ''}} >
            <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Description :</label>
                                <input type="text" name="desc" class="form-control input-sm" required
                                       placeholder="Description..." {{isset($price_summary) ? 'value="'.$price_summary['desc']. '"' : ''}} />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Cost :</label>
                                <input type="text" name="cost" class="form-control input-sm" required
                                       placeholder="Cost..." {{isset($price_summary) ? 'value="'.$price_summary['cost']. '"' : ''}} />
                            </div>
                        </div>
                    </div>
                <div class="login-footer">
                    <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                </div>        
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop