@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                              <h4>
                                    <i class="fa fa-angle-right"></i>Hotel Categories
                                    <a href="{{URL::route('dashboard.hotel-category.form')}}" class="btn btn-info btn-sm pull-right">
                                    <i class="fa fa-building-o"></i>&nbsp; Add New Hotel Category</a>
                              </h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Title</th>
                                  <th><i class=" fa fa-edit"></i> Action</th>
                              </tr>
                              </thead>
                              <tbody>
                                @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->created_at }}</td>
                                    <td>{{ $category->title }}</td>
                                    <td>
                                        <a href="{{URL::route('dashboard.hotel-category.edit',array($category->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;
                                        <a href="{{URL::route('dashboard.hotel-category.delete',array($category->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                        &nbsp;
                                        <a href="{{URL::route('dashboard.hotel-category.cloning',array($category->id))}}" class="btn btn-primary btn-xs">
                                        <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                        </a>
                                   </td>
                                </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
              {{$categories->links()}}
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

