@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($hotel_category))
            @if((isset($clone)))
            <h3><i class="fa fa-angle-right"></i>Clone Hotel Category from <i>"{{$hotel_category->title}}"</i></h3>
            @else
            <h3><i class="fa fa-angle-right"></i>Edit Hotel Category <i>"{{$hotel_category->title}}"</i>
            <a href="{{URL::route('dashboard.hotel-category.cloning',array($hotel_category->id))}}" class="btn btn-info btn-sm pull-right">
            <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
         <h3><i class="fa fa-angle-right"></i>Add New Hotel Category </h3>
        @endif
         <div class="row mt">
            
            @if(isset($hotel_category))
                @if(isset($clone))
                {{ Form::open(array('action' => array('DashboardController@postHotelCategoryCloning'),'files'=>true, 'role' => 'form'))}}
                @else
                {{ Form::open(array('action' => array('DashboardController@postHotelCategoryEdit'),'files'=>true, 'role' => 'form'))}}
                @endif
            @else
             {{ Form::open(array('action' => array('DashboardController@postHotelCategoryAdd'),'files' => true,'role' => 'form'))}}
            @endif

            <input type="hidden" name="id" @if((isset($hotel_category)) && (!isset($clone))) value="{{$hotel_category['id']}}" @endif>
             
             <div class="container-fluid">
                    
                    <div class="form-group">
                        <label for="title" class="control-label">Hotel Category Title :</label>
                        <input type="text" name="title" id="title" class="form-control input-sm" 
                               placeholder="Category Title..."
                               @if(isset($hotel_category)) 
                                    value="{{$hotel_category['title']}}"
                               @else
                               @endif/>
                    </div>

                    <div class="form-group">
                        <label for="main_image" class="control-label">Hotel Image :</label>
                        @if(isset($hotel_category))
                            @if(isset($clone))
                                <input type="file" id="main_image" name="main_image"></input><br/>
                            @else
                                <input type="file" id="main_image" name="main_image"></input><br/>
                                <div class="row">
                                    <div class="col-md-3">
                                        @if(!is_null($hotel_category->main_image))
                                        <div class="thumbnail">
                                            <img class="img-responsive" src='{{asset($hotel_category->main_image)}}' height="100px"></img>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @else
                            <input type="file" id="main_image" name="main_image"></input>
                        @endif
                    </div>

                    <div class="login-footer">
                        @if(isset($hotel_category))
                            @if(isset($clone))
                            <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @else
                            <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @endif
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                        @endif
                    </div>
            </div>
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop