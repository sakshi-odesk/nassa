@extends('layout.dashboard')

@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>Delete Hotel Category "{{$hotel_category->title}}"</h3>
        <div class="row mt">
            {{ Form::open(array( 'action' => 'DashboardController@postHotelCategoryDelete', 'files' => true, 
                                                                    'role' => 'form')) }} 
                <div class="container-fluid">
                    <div class="box-body">
                    <p>This action is irreversible. 
                        Do you wish to continue ?</p>
                        {{ Form::hidden('id', $hotel_category->id) }}
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-secondary btn-success" href="{{URL::route('dashboard.hotel-category.list')}}">Back to list</a>
                        <button class="btn btn-primary btn-danger pull-right" type="submit">Yes, Delete this Hotel Category</button>
                    </div>
                </div>
            {{ Form::close() }}
            </div>
    </section><! --/wrapper -->
</section>
@stop