@extends('layout.dashboard')

@section('content')

<section id="main-content">
    
    <section class="wrapper site-min-height">
        <div class="container-fluid">
            <div class="row mt">
                <h4>
                    <i class="fa fa-angle-right"></i>Destinations
                </h4>
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                <div id="tree1"></div>
            </div>
        </div>
    </section><! --/wrapper -->
</section>


@stop

@section('css')
{{-- HTML::style('theme/nassatravels/assets/css/jquery-ui.css') --}}
@stop

@section('js')
{{-- HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js') --}}
<script type="text/javascript">
    
    $(document).ready(function() {
        
        $.getJSON('{{ URL::to("dashboard/destinations/tree") }}',
        function(data) {
         
            $('#tree1').tree({
                data: data,
                autoOpen: false,
                dragAndDrop: false,
                closedIcon: $('<i class="glyphicon glyphicon-plus"></i>'),
                openedIcon: $('<i class="glyphicon glyphicon-minus"></i>'),
                onCreateLi: function(node, $li) {
                   if(node.id)
                    {   
                        $li.data('id',node.id);
                        $li.data('label',node.label);
                        $li.data('type',node.type);
                        $li.data('pin',node.pin);
                        $li.data('city',node.city);
                        $li.data('state',node.state);
                        $li.data('country',node.country);
                    }
                }
        });
            
                  
        $('#tree1').bind(
            'tree.click',
                function(event) {
                
                $('span.destination-action').remove();
                
                var actions = '<span class="destination-action pull-right">';    
                
                if(event.node.type != 'pin')
                {   
                    actions = actions + '<a href="/dashboard/destinations/add-child/'+event.node.id+'" '+
                                       'class="destinations-actions label label-info" '+
                                       'data-node-id="'+event.node.id +'">Add Child</a>';
                }
                    
                if(event.node.type != null)
                {
                    actions = actions + '<a href="/dashboard/destinations/add-sibling/'+event.node.id+'" '+
                                       'class="destinations-actions label label-primary" data-node-id="'+event.node.id +'">Add Sibling</a>';
                }
                    
                if(event.node.num_children == 0)
                {
                    actions = actions + '<a href="/dashboard/destinations/delete/'+event.node.id+'" '+
                                        'class="destinations-actions label label-danger" data-node-id="'+event.node.id +'">Delete</a>';
                }
                
                actions = actions + '<a href="/dashboard/destinations/edit/'+event.node.id+'" '+
                                       'class="destinations-actions label label-warning" data-node-id="'+event.node.id +'">Edit</a>';
                    
                actions = actions + '</span>';
                $(event.node.element).children().first().append(actions);
                
            });
        });
        
    });
    
</script>
@stop
