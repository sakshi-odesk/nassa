@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
                              <h4><i class="fa fa-angle-right"></i>Popular Destinations</h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Title</th>
                                  <th><i class=" fa fa-edit"></i> Action</th>
                              </tr>
                              </thead>
                              <tbody>
                               @foreach($popular_destinations as $destination)
                                  <tr>
                                    <td>{{ $destination->label }}</td>
                                    <td>
                                        <a href="{{URL::route('popular-destination-remove',array($destination->id))}}" class="btn btn-danger btn-xs">
                                             <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Click here to remove popular destination"></span>
                                        </a>
                                    </td>
                                  </tr>
                               @endforeach  
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
              {{$popular_destinations->links()}}
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

