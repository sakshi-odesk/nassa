@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($destination))
        <h3><i class="fa fa-angle-right"></i>Add Sibling for <i>"{{$destination->label}}"</i></h3>
        @endif
         <div class="row mt">
            {{ Form::open(array('action' => array('DashboardController@postDestinationAddSibling'), 'role' => 'form'))}}

             <input type="hidden" name="id" {{isset($destination) ? 'value="'.$destination['id']. '"' : ''}}>
             <div class="container-fluid">
                    <div class="row">
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Label :</label>
                                <input type="text" 
                                       name="label" 
                                       class="form-control input-sm" 
                                       placeholder="Label"
                                       />
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <input type="text" name="country" class="form-control input-sm" required 
                                       placeholder="Counrty"
                                       @if(isset($destination)) @if(!($destination['type'] == 'country')) value="{{$destination['country']}}" {{'readonly'}} @endif @endif
                                        />
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">State :</label>
                                <input type="text" 
                                       name="state" 
                                       class="form-control input-sm" 
                                       placeholder="State"
                                       @if(isset($destination)) @if(!($destination['type'] == 'state')) value="{{$destination['state']}}" {{'readonly'}} @endif @endif/>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">City :</label>
                                <input type="text" 
                                       name="city" 
                                       class="form-control input-sm" 
                                       placeholder="City"
                                       @if(isset($destination)) @if(!($destination['type'] == 'city')) value="{{$destination['city']}}" {{'readonly'}} @endif @endif/>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Type :</label>
                                <input type="text" 
                                       name="type" 
                                       class="form-control input-sm" 
                                       placeholder="Type"
                                       @if(isset($destination)) @if(!is_null($destination['type'])) {{'readonly'}} @endif @endif
                                       {{isset($destination) ? 'value="'.$destination['type']. '"' : ''}}/>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Pincode :</label>
                                <input type="text" 
                                       name="pin" 
                                       class="form-control input-sm" 
                                       placeholder="Pincode"
                                       @if(isset($destination)) @if(!($destination['type'] == 'pin')) value="{{$destination['pin']}}" {{'readonly'}} @endif @endif/>
                            </div>
                        </div>
                    </div>
                 
                    <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="seo_page_title">Seo Page Title :</label>
                                            <input type="text" 
                                               placeholder="Seo page title" 
                                               id="seo_page_title" 
                                               name='seo_page_title' 
                                               class="form-control input-sm" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_desc">Seo Meta Desc :</label>
                                        <textarea id="seo_meta_desc" name="seo_meta_desc" class="form-control input-sm" required placeholder="SEO meta description  ..." rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_kw">Seo Meta Keyword :</label>
                                        <textarea id="seo_meta_kw" name="seo_meta_kw" class="form-control input-sm" required placeholder="SEO meta Keyword..." rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                              <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        @if(!isset($destination))
                                        <input name='published' type="checkbox" value="1"> Popular Destination
                                        @else
                                            @if($destination['popular_destination'] == 0)
                                            <input name='popular_destination' type="checkbox" value="1"> Popular Destination
                                            @else
                                            <input name='popular_destination' type="checkbox" value="1" checked> Popular Destination
                                            @endif
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                              <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        @if(!isset($destination))
                                        <input name='published' type="checkbox" value="1"> Publish this Destination
                                        @else
                                            @if($destination['published'] == 0)
                                            <input name='published' type="checkbox" value="1"> Publish this Destination
                                            @else
                                            <input name='published' type="checkbox" value="1" checked> Publish this Destination
                                            @endif
                                        @endif
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                    <div class="login-footer">
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop