@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table id="pgrid" class="table table-striped table-advance table-hover">
	                  	  	  <h4>
                                  <i class="fa fa-angle-right"></i>All Users
                                @if(Auth::user()->role=='admin')
                                  <a href="{{URL::route('dashboard.allusers.form')}}" class="btn btn-info btn-sm pull-right">
                                  <i class="fa fa-building-o"></i>&nbsp; Add New User</a>
                               @endif
                              </h4>
                             <hr>
	                  	  	 
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Email</th>
                                  <th><i class="fa fa-user"></i>Role</th>
                                  <th></th>
                              </tr>
                              </thead>
                              <tfoot>
                              <tr>
                                  <th>Date</th>
                                  <th>Email</th>
                                  <th>Role</th>
                                  <th></th>
                              </tr>
                              </tfoot>
                              <tbody>
                               @foreach($allusers as $alluser)
                                
                                <tr>
                                    <td>{{ $alluser->created_at }}</td>
                                    <td>{{ $alluser->email }}</td>
                                    <td>{{ $alluser->role}}</td>
                                                                    
                                    <td>                                      
                                     
                                        <a href="{{URL::route('dashboard.allusers.edit',array($alluser->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;
                                      @if(Auth::user()->id != $alluser->id)   
                                      @if(Auth::user()->role=='admin')    
                                        <a href="{{URL::route('dashboard.allusers.delete',array($alluser->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                        &nbsp;
                                      @endif
                                      @endif
                                      
<!--
                                                                           
                                       <a href="{{URL::route('dashboard.allusers.profile',array($alluser->id))}}" class="btn btn-primary btn-xs">
                                       <span class="glyphicon glyphicon-user" data-toggle="tooltip" data-original-title="Profile"></span>
                                       </a>
                                      &nbsp;
-->
                                      
                                       
                                   </td>
                                </tr>
                               
                                @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

@section('css')
{{HTML::style('theme/nassatravels/assets/css/dataTables.bootstrap.css')}}
@stop
@section('js')
{{ HTML::script('theme/nassatravels/assets/js/jquery.dataTables.js') }}
{{ HTML::script('theme/nassatravels/assets/js/dataTables.bootstrap.js') }}

<script type="text/javascript">

$(document).ready(function(){

    // Setup - add a text input to each footer cell
    $('#pgrid tfoot th').each( function () {

        if ($.inArray($(this).index(), [0,3]) === -1) {
            var title = $('#pgrid thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } else {
            $(this).html('');
        }
    } );

    // DataTable
    var table = $('#pgrid').DataTable({
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [1,2],
        }],
        initComplete: function (){
          var r = $('#pgrid tfoot tr');
          r.find('th').each(function(){
            $(this).css('padding', 8);
          });
          $('#pgrid thead').append(r);
          $('#search_0').css('text-align', 'center');
        },
    });

    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    });
});
</script>

@stop


