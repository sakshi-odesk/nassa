@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($page))
            @if(isset($clone))
        <h3><i class="fa fa-angle-right"></i>Clone Page from <i>"{{$page->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Page <i>"{{$page->title}}"</i>
                <a href="{{URL::route('dashboard.page.cloning',array($page->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
        <h3><i class="fa fa-angle-right"></i>Add New Page</h3>
        @endif
         <div class="row mt">
            @if(isset($page))
                @if(isset($clone))
                    {{ Form::open(array( 'action' => 'DashboardController@postPageCloning', 'files' => true,'role' => 'form')) }}    
                @else
                    {{ Form::open(array( 'action' => 'DashboardController@postPageEdit', 'files' => true,'role' => 'form')) }} 
                @endif
            @else
             {{ Form::open(array( 'action' => 'DashboardController@postPageAdd', 'files' => true,'role' => 'form')) }} 
            @endif

            <input type="hidden" name="id" @if((isset($page)) && (!isset($clone))) value="{{$page['id']}}" @endif>
             <div class="container-fluid">
                    
                 <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title" class="control-label">Title :</label>
                                        <input type="text" 
                                        placeholder="Page's title goes here.." 
                                        id="title" 
                                        name='title'
                                        required
                                        {{isset($page) ? 'value="'.$page['title']. '"' : ''}} class="form-control input-sm" required>
                                        <small class="help-block"><em>The page title would be used to generate SEO friendly URLS</em></small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="page_image">Page Image :</label>
                                        @if(isset($page))
                                            @if(isset($clone))
                                                <input multiple="0" type="file" id="page_image" name="page_image"><br/>
                                            @else
                                                <input multiple="0" type="file" id="page_image" name="page_image"><br/>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        @if(!is_null($page->page_image))
                                                        <div class="thumbnail">
                                                           <img class="img-responsive" src='{{asset($page->page_image)}}' height="200px"></img>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            <input multiple="0" type="file" id="page_image" name="page_image" required>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($page))
                                                <input id='published' name='published' type="checkbox" value="1"> Publish this page
                                                @else
                                                    @if($page['published'] == null)
                                                    <input id='published' name='published' type="checkbox" value="1"> Publish this page
                                                    @else
                                                    <input id='published' name='published' type="checkbox" value="1" checked> Publish this page
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Page Text :</label>
                                <textarea id="body" name="body" class="form-control" placeholder="Content for Page's body text ..." rows="6">{{isset($page) ? $page['body']: ''}}</textarea>
                            </div>
                        </div>
                    </div>
                  
                 <br/>
                 
                 <fieldset>
                        <legend>Seo Options</legend>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="seo_page_title">Seo Page Title :</label>
                                    <input type="text" 
                                    placeholder="Seo page title" 
                                    id="seo_page_title" 
                                    name='seo_page_title'
                                    {{isset($page) ? 'value="'.$page['seo_page_title']. '"' : ''}} class="form-control input-sm" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo_meta_kw">Seo Meta Keyword :</label>
                                    <textarea id="seo_meta_kw" name="seo_meta_kw" required  class="form-control input-sm" placeholder="SEO meta Keyword..." rows="3">{{isset($page) ? $page['seo_meta_kw']: ''}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="seo_meta_desc">Seo Meta Desc :</label>
                                    <textarea id="seo_meta_desc" name="seo_meta_desc" required class="form-control input-sm" placeholder="SEO meta description  ..." rows="3">{{isset($page) ? $page['seo_meta_desc']: ''}}</textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                 
                 <div class="login-footer">
                        @if(isset($page))
                            @if(isset($clone))
                                <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Clone</button>
                            @else
                                <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @endif
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                        @endif
                    </div>
             
             </div>
                {{ Form::close() }}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 

 <script type="text/javascript">
    $(document).ready(function() {
        
      $('#body').summernote({ 
         height:200,
        onImageUpload:function(files,editor,welEditable){
            sendFile(files[0],editor,welEditable);
        }
      });
        
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: '{{URL::to("summernote/upload")}}',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
    });
    </script>
@stop