@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($blog))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Blog from <i>"{{$blog->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Blog <i>"{{$blog->title}}"</i>
                <a href="{{URL::route('dashboard.blog.cloning',array($blog->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
        <h3><i class="fa fa-angle-right"></i>Add New Blog</h3>
        @endif
         <div class="row mt">
                @if(isset($blog))
                    @if(isset($clone))
                        {{ Form::open(array( 'action' => 'DashboardController@postBlogCloning', 'files' => true,'role' => 'form')) }}
                    @else
                        {{ Form::open(array( 'action' => 'DashboardController@postBlogEdit', 'files' => true,'role' => 'form')) }}
                    @endif
                @else
                {{ Form::open(array( 'action' => 'DashboardController@postBlogAdd', 'files' => true,'role' => 'form')) }} 
                @endif

                <input type="hidden" name="id" @if((isset($blog)) && (!isset($clone))) value="{{$blog['id']}}" @endif>
                <div class="container-fluid">
                   
                        <div class="row">
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="title" class="control-label">Title :</label>
                                            <input type="text" 
                                               placeholder="Blog's title goes here.." 
                                               id="title" 
                                               name='title'
                                               required
                                               {{isset($blog) ? 'value="'.$blog['title']. '"' : ''}} class="form-control input-sm" required>
                                            <small class="help-block"><em>The Blog title would be used to generate SEO friendly URLS</em></small>
                                        </div>
                                    </div>        
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="blog_category_id" class="control-label">Blog Category :</label>
                                            <select id="blog_category_id" class="form-control input-sm" name="blog_category_id" required>
                                                <option value="">Please Select...</option>
                                                @if(isset($blog_categories))
                                                    @foreach($blog_categories as $category)
                                                <option value="{{ $category->id }}" @if(isset($blog) && ($blog['blog_category_id'] == $category->id)) {{'selected'}} @endif>{{ $category->title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>        
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="blog_image" class="control-label">Blog Image :</label>
                                            @if(isset($blog))
                                                @if(isset($clone))
                                                    <input multiple="0" type="file" id="blog_image" name="blog_image"><br/>
                                                @else
                                                    <input multiple="0" type="file" id="blog_image" name="blog_image"><br/>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            @if(!is_null($blog->blog_image))
                                                            <div class="thumbnail">
                                                                <img class="img-responsive" src='{{asset($blog->blog_image)}}' height="100px"></img>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @else
                                                <input multiple="0" type="file" id="blog_image" name="blog_image" required>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    @if(!isset($blog))
                                                    <input id='show_on_slider' name='show_on_slider' type="checkbox" value="1"> Show on Slider 
                                                    @else
                                                        @if($blog['show_on_slider'] == 0))
                                                        <input id='show_on_slider' name='show_on_slider' type="checkbox" value="1"> Show on Slider
                                                        @else
                                                        <input id='show_on_slider' name='show_on_slider' type="checkbox" value="1" checked> Show on Slider
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col-md-12">
                                         <div id="show_slider_options">
                                                <div class="form-group">
                                                    <label for="slider_title" class="control-label">Slider Title :</label>
                                                    <input type="text" 
                                                       placeholder="Slider title goes here.." 
                                                       id="slider_title" 
                                                       name='slider_title'
                                                       required
                                                       {{isset($blog) ? 'value="'.$blog['slider_title']. '"' : ''}} class="form-control input-sm" required>
                                                </div>

                                                <div class="form-group">
                                                    <label for="slider_image" class="control-label">Slider Image :</label>
                                                     @if(isset($blog))
                                                        @if(isset($clone))
                                                           <input multiple="0" type="file" id="slider_image" name="slider_image"><br/>
                                                        @else
                                                            <input multiple="0" type="file" id="slider_image" name="slider_image"><br/>
                                                             <div class="row">
                                                                <div class="col-md-6">
                                                                    @if(!is_null($blog->slider_image))
                                                                    <div class="thumbnail">
                                                                        <img class="img-responsive" src='{{asset($blog->slider_image)}}' height="100px"></img>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        @endif
                                                     @else
                                                        <input multiple="0" type="file" id="slider_image" name="slider_image" required>
                                                    @endif
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="checkbox">
                                            <label>
                                                @if(!isset($blog))
                                                <input id='published' name='published' type="checkbox" value="1"> Publish this Blog
                                                @else
                                                    @if($blog['published'] == null)
                                                    <input id='published' name='published' type="checkbox" value="1"> Publish this Blog
                                                    @else
                                                    <input id='published' name='published' type="checkbox" value="1" checked> Publish this Blog
                                                    @endif
                                                @endif
                                            </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Blog Text :</label>
                                    <textarea id="body" name="body" class="form-control" placeholder="Content for Blog's body text ..." rows="8">{{isset($blog) ? $blog['body']: ''}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <br/>
                        
                        <fieldset>
                            <legend>Seo Options</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_page_title" class="control-label">Seo Page Title :</label>
                                        <input type="text" 
                                           placeholder="Seo page title" 
                                           id="seo_page_title" 
                                           name='seo_page_title' required
                                           {{isset($blog) ? 'value="'.$blog['seo_page_title']. '"' : ''}} class="form-control input-sm" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_desc" class="control-label">Seo Meta Desc :</label>
                                        <textarea id="seo_meta_desc" required name="seo_meta_desc" class="form-control input-sm" placeholder="SEO meta description  ..." rows="3">{{isset($blog) ? $blog['seo_meta_desc']: ''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_kw" class="control-label">Seo Meta Keyword :</label>
                                        <textarea id="seo_meta_kw" name="seo_meta_kw" required class="form-control input-sm" placeholder="SEO meta Keyword..." rows="3">{{isset($blog) ? $blog['seo_meta_kw']: ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
             
                        <div class="login-footer">
                            @if(isset($blog))
                                @if(isset($clone))
                                    <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Clone</button>
                                @else
                                    <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                                @endif
                            @else
                            <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @endif
                        </div>
                   
                </div><!-- /.Container-fluid -->
                {{ Form::close() }}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 

 <script type="text/javascript">
    $(document).ready(function() {
        
      $('#body').summernote({ 
         height:200,
        onImageUpload:function(files,editor,welEditable){
            sendFile(files[0],editor,welEditable);
        }
      });
        
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: '{{URL::to("summernote/upload")}}',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
        
        if ($('#show_on_slider').is(':checked'))
        {
            $('#show_slider_options').show();
        } 
        else
        {
            $('#show_slider_options').hide();
        }
        
        $('#show_on_slider').change(function () {

        if (this.checked == true) {
           $('#show_slider_options').show();

        } else {
            $('#show_slider_options').hide();
        }
    });
        
    });
     
    </script>
@stop