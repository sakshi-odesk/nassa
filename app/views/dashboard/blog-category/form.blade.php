@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($blog_category))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Blog Category from <i>"{{$blog_category->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Blog Category <i>"{{$blog_category->title}}"</i>
                <a href="{{URL::route('dashboard.blog-category.cloning',array($blog_category->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
            <h3><i class="fa fa-angle-right"></i>Add New Blog Category</h3>
        @endif
         <div class="row mt">
            @if(isset($blog_category))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postBlogCategoryCloning'), 'role' => 'form'))}}
                @else
                    {{ Form::open(array('action' => array('DashboardController@postBlogCategoryEdit'), 'role' => 'form'))}}
                @endif
            @else 
                {{ Form::open(array('action' => array('DashboardController@postBlogCategoryAdd'), 'role' => 'form'))}}
            @endif

            <input type="hidden" name="id"  @if((isset($blog_category)) && (!isset($clone))) value="{{$blog_category['id']}}" @endif>
            <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Category Title :</label>
                                <input type="text" name="title" class="form-control input-sm" required
                                       placeholder="Category Title..." 
                                       {{isset($blog_category) ? 'value="'.$blog_category['title']. '"' : ''}}/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Description :</label>
                                <input type="text" 
                                       name="desc" 
                                       class="form-control input-sm" 
                                       placeholder=" Description..."
                                       {{isset($blog_category) ? 'value="'.$blog_category['desc']. '"' : ''}}/>
                            </div>
                        </div>
                    </div>            
                    <div class="login-footer">
                        @if(isset($blog_category))
                            @if(isset($clone))
                                <button type="submit" name="clone" class="btn btn-primary btn-sm pull-left">Clone</button>
                            @else
                                <button type="submit" name="edit" class="btn btn-primary btn-sm pull-left">Submit</button>
                            @endif
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                        @endif
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop