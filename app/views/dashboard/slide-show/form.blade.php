@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($slide_show))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone SlideShow from <i>"{{$slide_show->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit SlideShow <i>"{{$slide_show->title}}"</i>
                <a href="{{URL::route('dashboard.slide-show.cloning',array($slide_show->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
         <h3><i class="fa fa-angle-right"></i>Add New SlideShow</h3>
        @endif
         <div class="row mt">
            @if(isset($slide_show))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postSlideShowCloning'),'files'=>true, 'role' => 'form'))}}
                @else
                    {{ Form::open(array('action' => array('DashboardController@postSlideShowEdit'),'files'=>true, 'role' => 'form'))}}
                @endif
            @else
             {{ Form::open(array('action' => array('DashboardController@postSlideShowAdd'),'files'=> true, 'role' => 'form'))}}
            @endif
            <input type="hidden" name="id" @if((isset($slide_show)) && (!isset($clone))) value="{{$slide_show['id']}}" @endif>
            
             <div class="container-fluid">
                 
                 <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                        <label for="title" class="control-label">Title :</label>
                        <input type="text" name="title" id="title" class="form-control input-sm" required 
                               placeholder="SlideShow Title..." {{isset($slide_show) ? 'value="'.$slide_show['title']. '"' : ''}} />
                </div>
                     </div>     
                 </div>
                 <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                    <div class="checkbox">
                        <label>
                            @if(!isset($slide_show))
                            <input id='published' name='published' type="checkbox" value="1"> Publish this SlideShow
                            @else
                                @if($slide_show['published'] == null)
                                <input id='published' name='published' type="checkbox" value="1"> Publish this SlideShow
                                @else
                                <input id='published' name='published' type="checkbox" value="1" checked> Publish this SlideShow
                                @endif
                            @endif
                        </label>
                    </div>
                </div> 
                     </div>     
                 </div>
                 
                
                <div class="slides_wrapper">
                        <div class="slides">
                            @if(isset($slide_show))
                                <?php if(isset($slide_show)){ $slides = $slide_show->slides()->getResults();} $i = 0;?>
                                @foreach($slides as $slide)
                                    <div class="slide well">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="Slide[{{$i}}][id]" value="{{$slide->id}}">
                                                        <div class="form-group">
                                                            <label>Image</label>
                                                            <input type="file" name="Slide[{{$i}}][main_image]"></input>
                                                        </div>
                                                </div>
                                            </div>
                                            @if(!isset($clone))
                                            <div class="row">
                                                <div class="col-md-2">
                                                    @if(!is_null($slide->main_image))
                                                    <div class="thumbnail">
                                                        <div class="images">
                                                            <div class="image-wrapper">
                                                                 <img class="img-responsive" src='{{asset($slide->main_image)}}'></img>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">Title :</label>
                                                            <input type="text" name="Slide[{{$i}}][title]"  value="{{$slide->title}}" required
                                                                   class="form-control input-sm" placeholder="Slide Title..." /> 
                                                        </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">URL :</label>
                                                            <input type="text" name="Slide[{{$i}}][url]" value="{{$slide->url}}" required
                                                                   class="form-control input-sm" placeholder="Slide Url..." />
                                                        </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                <?php $i++; ?>
                                @endforeach
                            @endif
                        </div>
                        <a href="#" id="add_new_slide">Add New Slides</a>
                    </div>
                

                 <div class="login-footer">
                        @if(isset($slide_show))
                            @if(isset($clone))
                                <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Clone</button>
                            @else
                                <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @endif
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                        @endif
                    </div>
            </div>
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop

@section('js')
<script type="text/javascript">
$(document).ready(function() {
        
    $('#add_new_slide').click(function (e) {
            e.preventDefault();
            
            
            $('<div class="container-fluid">'+
                '<div class="slide well">'+
                    '<div class="row">'+
                        '<div class="col-md-12">'+
                            '<input type="hidden" name="Slide['+$('.slides .slide').length+'][id]" value="">'+
                            '<div class="form-group">'+
                                '<label>Image</label>'+
                                '<input type="file" name="Slide['+$('.slides .slide').length+'][main_image]" required></input>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row">'+
                        '<div class="col-md-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Title :</label>'+
                                '<input type="text" name="Slide['+$('.slides .slide').length+'][title]" required class="form-control input-sm" placeholder="Slide Title..." /> '+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">URL :</label>'+
                                '<input type="text" name="Slide['+$('.slides .slide').length+'][url]" required class="form-control input-sm" placeholder="Slide Url..." />'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
              '</div>')
              .appendTo('.slides');
            
        });
});
</script>
@stop