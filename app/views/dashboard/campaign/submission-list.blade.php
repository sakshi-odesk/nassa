@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 <div class="col-md-12">
                      <div class="content-panel">
                           <h4 style="padding-right: 53px;"><i class="fa fa-angle-right"></i>Campaign Submissions
                               @if(isset($campaign_id))
                                    <a href="{{URL::route('dashboard.campaign.submission-export',array($campaign_id))}}" class="btn btn-info btn-sm pull-right">
                                        <span class="glyphicon glyphicon-download-alt" data-toggle="tooltip" data-original-title="Download"></span>    
                                    </a>
                               @elseif(Input::get('search'))
                                   <a href="{{URL::route('dashboard.campaign.submission-export',array(Input::get('search')))}}" class="btn btn-info btn-sm pull-right">
                                        <span class="glyphicon glyphicon-download-alt" data-toggle="tooltip" data-original-title="Download"></span>    
                                   </a>
                               @else    
                                   <a href="{{URL::route('dashboard.campaign.submission-export',array(0))}}" class="btn btn-info btn-sm pull-right">
                                     <span class="glyphicon glyphicon-download-alt" data-toggle="tooltip" data-original-title="Download"></span>    
                                   </a>
                               @endif
                           </h4>
                           <hr>
                          {{ Form::open(array( 'action' => 'DashboardController@getCampaignSubmissionList','files' => true,
                                               'role' => 'form','method'=>'get','class'=>'form-inline pull-right')) }}
                              <div class="container-fluid">
                                    <div class="row">
                                         <div class="form-group">
                                                <label>Search</label>
                                                <select class="form-control input-sm" name="search" required>
                                                    <option value="">Select Campaign...</option>
                                                    @if(isset($campaigns))
                                                        @foreach($campaigns as $campaign)
                                                    <option value="{{ $campaign->id }}" 
                                                            @if(isset($campaign_submissions)) @if(Input::get('search') == $campaign->id) {{'selected'}} @else @endif @endif>
                                                            {{ $campaign->title }}
                                                    </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                         </div>
                                         <button type="submit" class="btn btn-primary btn-sm">Filter</button>
                                    </div>
                              </div>
                            {{Form::close()}}
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Name</th>
                                  <th><i class="fa fa-mobile"></i> Phone No.</th>
                                  <th><i class="fa fa-bookmark"></i> Email</th>
                                  <th><i class="fa fa-bookmark"></i> IP Address</th>
                                  <th><i class="fa fa-bookmark"></i> Best time to call</th>
                                  <th><i class="fa fa-bookmark"></i> Subscribed</th>
                              </tr>
                              </thead>
                              <tbody>
                               @foreach($campaign_submissions as $campaign_submission)
                                <tr>
                                    <td>{{$campaign_submission->created_at}}</td>
                                    <td>{{$campaign_submission->name}}</td>
                                    <td>{{$campaign_submission->phone_no}}</td>
                                    <td>{{$campaign_submission->email}}</td>
                                    <td>{{$campaign_submission->ip}}</td>
                                    <td>{{$campaign_submission->best_time_to_call}}</td>
                                    <td>
                                        @if($campaign_submission->subscribe == 1)
                                            {{'Yes'}}
                                        @else
                                            {{'No'}}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
               </div><!-- /row -->
              {{$campaign_submissions->links()}}
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

