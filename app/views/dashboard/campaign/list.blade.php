@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table class="table table-striped table-advance table-hover">
	                  	  	  <h4>
                                  <i class="fa fa-angle-right"></i>Campaigns
                                  <a href="{{URL::route('dashboard.campaign.form')}}" class="btn btn-info btn-sm pull-right">
                                  <i class="fa fa-building-o"></i>&nbsp; Add New Campaign</a>
                              </h4>
	                  	  	  <hr>
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Title</th>
                                  <th><i class="fa fa-bookmark"></i> Heading</th>
                                  <th><i class="fa fa-bookmark"></i> Status</th>
                                  <th><i class=" fa fa-edit"></i> Action</th>
                              </tr>
                              </thead>
                              <tbody>
                               @foreach($campaigns as $campaign)
                                <tr>
                                    <td>{{ $campaign->created_at }}</td>
                                    <td>{{ $campaign->title }}</td>
                                    <td>{{ $campaign->heading }}</td>
                                    <td>
                                        @if($campaign->published)
                                        <span class="label label-success">Published</span>
                                        @else
                                        <span class="label label-danger">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{URL::route('campaign.view',array($campaign->slug))}}" class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-placement="top" data-original-title="View Campaign Page"></span>
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{URL::route('dashboard.campaign.submission',array($campaign->id))}}" class="btn btn-warning btn-xs">
                                        <span class="glyphicon glyphicon-list" data-toggle="tooltip" data-placement="top" data-original-title="View Campaign Submissions"></span>
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{URL::route('dashboard.campaign.edit',array($campaign->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="top" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{URL::route('dashboard.campaign.cloning',array($campaign->id))}}" class="btn btn-primary btn-xs">
                                        <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{URL::route('dashboard.campaign.delete',array($campaign->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" data-original-title="Delete"></span>
                                        </a>
                                        
                                   </td>
                                </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
              {{$campaigns->links()}}
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

