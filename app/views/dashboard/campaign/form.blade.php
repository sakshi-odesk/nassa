@extends('layout.dashboard')

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
@stop


@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($campaign))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Campaign from <i>"{{$campaign->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Campaign <i>"{{$campaign->title}}"</i>
                <a href="{{URL::route('dashboard.campaign.cloning',array($campaign->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
         <h3><i class="fa fa-angle-right"></i>Add New Campaign</h3>
        @endif
        
         <div class="row mt">
            @if(isset($campaign))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postCampaignCloning'),'files'=>true, 'role' => 'form'))}}
                @else
                    {{ Form::open(array('action' => array('DashboardController@postCampaignEdit'),'files'=>true, 'role' => 'form'))}}
                @endif
            @else
            {{ Form::open(array('action' => array('DashboardController@postCampaignAdd'),'files'=> true, 'role' => 'form'))}}
            @endif
             
            <input type="hidden" name="id" {{isset($campaign) ? 'value="'.$campaign['id']. '"' : ''}}>
             <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="container-fluid">
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Title :</label>
                                        <input type="text" name="title" class="form-control input-sm" required
                                               placeholder="Campaign Title..." {{isset($campaign) ? 'value="'.$campaign['title']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label class="control-label">Heading :</label>
                                        <input type="text" name="heading" class="form-control input-sm" required
                                               placeholder="Campaign Heading..." {{isset($campaign) ? 'value="'.$campaign['heading']. '"' : ''}} />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Sub Title :</label>
                                        <input type="text" name="sub_heading" class="form-control input-sm" required
                                               placeholder="Campaign Sub Title..." {{isset($campaign) ? 'value="'.$campaign['sub_heading']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description :</label>
                                            <textarea id="desc" name="desc" class="form-control" required placeholder="Campaign Description..." rows="3">{{isset($campaign) ? $campaign['desc']: ''}}</textarea>
                                        </div>

                                    </div>  
                                </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Inclusion :</label>
                                            <textarea id="body" name="inclusion" class="form-control" placeholder="Content for inclusion text ..." rows="6">{{isset($campaign) ? $campaign['inclusion']: ''}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Image :</label>
                                            @if(isset($campaign))
                                                @if(isset($clone))
                                                    <input multiple="0" type="file" name="background_image"/><br/>
                                                @else
                                                    <input multiple="0" type="file" name="background_image"/><br/>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            @if(!is_null($campaign->background_image))
                                                            <div class="thumbnail">
                                                                <img class="img-responsive" src='{{asset($campaign->background_image)}}' height="100px"></img>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @else
                                            <input multiple="0" type="file" name="background_image" required/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Redirection Url :</label>
                                            <input type="text" name="redirect" class="form-control input-sm" required
                                                    placeholder="Redirection Url.." {{isset($campaign) ? 'value="'.$campaign['redirect']. '"' : ''}}>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($campaign))
                                                <input id='published' name='published' type="checkbox" value="1"> Publish this Campaign
                                                @else
                                                    @if($campaign['published'] == 0)
                                                    <input id='published' name='published' type="checkbox" value="1"> Publish this Campaign
                                                    @else
                                                    <input id='published' name='published' type="checkbox" value="1" checked> Publish this Campaign
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6" ng-controller="CampaignSliderController">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <div class="campaign_reviewers_wrapper">
                                                <div class="campaign_reviewers">
                                                    @if(isset($campaign))
                                                        <?php if(isset($campaign)){ $campaign_records = $campaign->campaign_reviews()->getResults();}  $i = 0;?>
                                                        @foreach($campaign_records as $campaign_record)
                                                            <div class="campaign_reviewer">
                                                                <div class="form-group">
                                                                        <label class="control-label">Description :</label>
                                                                            <textarea name="CampaignReview[{{$i}}][desc]" required class="form-control" placeholder="Review Description..." rows="3">{{$campaign_record->desc}}</textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label">Reviewer :</label>
                                                                            <input type="text" name="CampaignReview[{{$i}}][reviewer]" value="{{$campaign_record->reviewer}}" required
                                                                                    class="form-control input-sm" placeholder="Reviewer..."/>
                                                                    </div>
                                                            </div>

                                                        <?php $i++; ?>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <a href="#" id="add_new_reviews">Add New Review</a>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                
                                <div class="well" ng-show="sliders.length > 0" ng-repeat="(key,value) in sliders track by $index">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="campaign_slider_wrapper">
                                                <div class="form-group">
                                                    <label class="control-label">Image : </label>
                                                    <input type="file" name="Sliders[<% key %>][Image]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="campaign_slider_wrapper">
                                                <div class="form-group">
                                                    <label class="control-label">Title : </label>
                                                    <input type="text" name="Sliders[<% key %>][Title]" class="form-control input-sm" value="<% value.Title %>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="campaign_slider_wrapper">
                                                <div class="form-group">
                                                    <label class="control-label">Starting From : </label>
                                                    <input type="text" name="Sliders[<% key %>][Starting_from]" value="<% value.Starting_from %>" class="form-control input-sm">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="campaign_slider_wrapper">
                                                <div class="form-group">
                                                    <label class="control-label">Url : </label>
                                                    <input type="text" name="Sliders[<% key %>][Url]" value="<% value.Url %>" class="form-control input-sm">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(isset($campaign) && !isset($clone))
                                            <a href="javascript:;" ng-click="delete_slider(e,key,{{ $campaign->id }})" class="btn btn-sm btn-danger pull-right">
                                            <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" data-original-title="Delete"></span>
                                            </a>
                                            @else
                                                
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                                <a href="javascript:;" ng-click="add_slider($event)">Add Slider Details</a>
                                <div class="login-footer">
                                    @if(isset($campaign))
                                        @if(isset($clone))
                                            <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Clone</button>
                                        @else
                                            <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                                        @endif
                                    @else
                                    <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
             </div><!-- /.Container-fluid -->
            {{Form::close()}} 
        </div>
         
    </section><! --/wrapper -->
</section>

@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 

<script type="text/javascript">
$(document).ready(function() {
        
        
        $('#body').summernote({ 
            height:200,
            onImageUpload:function(files,editor,welEditable){
                sendFile(files[0],editor,welEditable);
            }
        });

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: '{{URL::to("summernote/upload")}}',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
        
        $('#add_new_reviews').click(function (e) {
            e.preventDefault();
        
            $('<div class="campaign_reviewer">'+
                '<div class="form-group">'+
                        '<label class="control-label">Description :</label>'+
                            '<textarea name="CampaignReview['+$('.campaign_reviewers .campaign_reviewer').length+'][desc]" required class="form-control" placeholder="Review Description..." rows="3"></textarea>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="control-label">Reviewer :</label>'+
                            '<input type="text" name="CampaignReview['+$('.campaign_reviewers .campaign_reviewer').length+'][reviewer]" required class="form-control input-sm" placeholder="Reviewer..."/>'+
                    '</div>'+
            '</div>').appendTo('.campaign_reviewers');
                    
        });
        
});
</script>


<script>
  app
  .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
  })
  .controller('CampaignSliderController', function($scope,$http,$rootScope,$element){  
  
  $scope.sliders = (JSON.parse(decodeURIComponent('{{rawurlencode($slider)}}')));
              
  $scope.add_slider = function(e)
  {
      $scope.sliders.push({
                          Image:null,
                          Title:null,
                          Starting_from:null,
                          Url:null
                          });
  }
  
  $scope.delete_slider = function(e,k,campaign_id){
      
      $http({
            url:'/dashboard/campaign/slider/delete',
            method: "GET",
            params:{
                    'id':campaign_id,
                    'index' : k
                   },
            }).success(function(data) 
            { 
                $scope.sliders = data;
            });
      
  }
      
  
  });    
    
</script>
@stop