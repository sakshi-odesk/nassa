@extends('layout.dashboard')

@section('css')
{{HTML::style('theme/nassatravels/assets/css/morris-0.4.3.min.css')}}
@stop
@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Dashboard</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <! -- 1st ROW OF PANELS -->
             
					<div class="row">
                        
						<div class="col-lg-4 col-md-4 col-sm-4 mb">
							<!-- INSTAGRAM PANEL -->
                                 
							<div class="content-panel pn">
								<div class="widget">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p><i class="fa fa-th"></i></p>
                                            </div>
                                         
                                            <div class="col-sm-6 col-xs-6 goright">
                                                <p class="small">Campaigns</p>
                                            </div>
                                        
									    </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="chart-wrapper">
                                          
                                            <div id="CampaignChart" style=" max-height: 200px;"></div>
                                        </div>
                                    </div>
									<div class="widget-footer" style="bottom:0%;">
                                        Total Enquiries : {{DB::table('campaign_submissions')->count()}}<br/>
									</div>
								</div>
							</div>
                         
                            
						</div><!-- /col-md-4 -->
                        
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 mb">
							<!-- INSTAGRAM PANEL -->
                            <div class="content-panel pn">
                              
								<div class="widget">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p><i class="fa fa-building"></i></p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 goright">
                                                <p class="small">Hotels</p>
                                            </div>
									    </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="chart-wrapper">
                                         
                                            <div id="HotelChart" style=" max-height: 200px;"></div>
                                    
                                        </div>
                                    </div>
									<div class="widget-footer" style="bottom:0%;">
                                        Total Hotels : {{DB::table('hotels')->count()}}<br/>
									</div>
								</div>
							</div>
						</div><!-- /col-md-4 -->
                        
                        <div class="col-lg-4 col-md-4 col-sm-4 mb">
							<!-- INSTAGRAM PANEL -->
                            <div class="content-panel pn">
								<div class="widget">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p><i class="fa fa-plane"></i></p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 goright">
                                                <p class="small">Packages</p>
                                            </div>
									    </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="chart-wrapper">
                                            <div id="PackageChart" style=" max-height: 200px;"></div>
                                        </div>
                                    </div>
									<div class="widget-footer" style="bottom:0%;">
                                        Total Packages : {{DB::table('packages')->count()}}<br/>
									</div>
								</div>
							</div>
						</div><!-- /col-md-4 -->
					</div>
                   
              
                  
              
              <! --/END 1ST ROW OF PANELS -->
                
                <! -- 2ND ROW OF PANELS -->
                <div class="row">
                     <div class="col-lg-4 col-md-4 col-sm-4 mb">
							<!-- INSTAGRAM PANEL -->
                            <div class="content-panel pn">
								<div class="widget">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p><i class="fa fa-list"></i></p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 goright">
                                                <p class="small">Blogs</p>
                                            </div>
									    </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="chart-wrapper">
                                            <div id="BlogChart" style=" max-height: 200px;"></div>
                                        </div>
                                    </div>
									<div class="widget-footer" style="bottom:0%;">
                                        Total Blogs : {{DB::table('blogs')->count()}}<br/>
									</div>
								</div>
							</div>
				    </div><!-- /col-md-4 -->
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 mb">
							<!-- INSTAGRAM PANEL -->
<!--
							<div class="instagram-panel pn">
								<i class="fa fa-pencil fa-4x"></i>
								<p>Pages<br/><br/><br/>
                                    <a href="{{URL::route('dashboard.page.list')}}">{{DB::table('pages')->count()}} Pages</a>
								</p>
							</div>
-->
                            <div class="content-panel pn">
								<div class="widget">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p><i class="fa fa-pencil"></i></p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 goright">
                                                <p class="small">Pages</p>
                                            </div>
									    </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="chart-wrapper">
                                            <div id="PageChart" style=" max-height: 200px;"></div>
                                        </div>
                                    </div>
									<div class="widget-footer" style="bottom:0%;">
                                        Total Pages : {{DB::table('pages')->count()}}<br/>
									</div>
								</div>
							</div>
				    </div><!-- /col-md-4 -->
                    
                    <div class="col-lg-4 col-md-4 col-sm-4 mb">
							<!-- INSTAGRAM PANEL -->
                            <div class="content-panel pn">
								<div class="widget">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <p><i class="fa fa-bar-chart-o"></i></p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6 goright">
                                                <p class="small">SlideShows</p>
                                            </div>
									    </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="chart-wrapper">
                                            <div id="SlideShowChart" style=" max-height: 200px;"></div>
                                        </div>
                                    </div>
									<div class="widget-footer" style="bottom:0%;">
                                        Total SlideShows : {{DB::table('slide_shows')->count()}}<br/>
									</div>
								</div>
							</div>
				    </div><!-- /col-md-4 -->
                </div><! --/END 2ND ROW OF PANELS -->
      
            </div>
        </div>

    </section><! --/wrapper -->
</section>
@stop

@section('js')
{{HTML::script('theme/nassatravels/assets/js/plugins/chartjs/Chart.min.js')}}
{{HTML::script('theme/nassatravels/assets/js/raphael-min.js')}}
{{HTML::script('theme/nassatravels/assets/js/morris-0.4.3.min.js')}}

<script type="text/javascript">

$(document).ready(function(){
  
    var campaigns = {{$campaigns}};
    var hotels = {{$hotels}};
    var blogs = {{$blogs}};
    var pages = {{$pages}};
    var packages = {{$packages}};
    var slide_shows = {{$slide_shows}};
 
    Morris.Donut({
      element: 'CampaignChart',
      data: campaigns
    });
      Morris.Donut({
      element: 'BlogChart',
      data: blogs
    });
    Morris.Donut({
      element: 'PageChart',
      data: pages
    });
    Morris.Donut({
      element: 'HotelChart',
      data: hotels
    });
    Morris.Donut({
      element: 'PackageChart',
      data: packages
    });
    Morris.Donut({
      element: 'SlideShowChart',
      data: slide_shows
    });
    
});

</script>
@stop