@extends('layout.dashboard')

@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>Delete Package Category "{{$package_category->title}}"</h3>
        <div class="row mt">
            
            <?php $packages = Package::where('package_category_id','=',$package_category->id)->get();
                 
            ?>
            
            @if($packages->count() == 0)
            {{ Form::open(array( 'action' => 'DashboardController@postPackageCategoryDelete', 'files' => true, 
                                                                    'role' => 'form')) }} 
            <div class="container-fluid">
                <div class="box-body">
                <p>This action is irreversible. 
                    Do you wish to continue ?</p>
                    {{ Form::hidden('id', $package_category->id) }}
                </div>
                <div class="box-footer">
                    <a class="btn btn-secondary btn-success" href="{{URL::route('dashboard.package-category.list')}}">Back to list</a>
                    <button class="btn btn-primary btn-danger pull-right" type="submit">Yes, Delete this Package Category</button>
                </div>
            </div>
            {{ Form::close() }}
            @else
            <div class="container-fluid">
                <div class="box-body">
                <p>This category has packages. it can not be deleted</p>
                </div>
                <div class="box-footer">
                    <a class="btn btn-secondary btn-success" href="{{URL::route('dashboard.package-category.list')}}">Back to list</a>
                </div>
            </div>
            
            @endif
            
            </div>
    </section><! --/wrapper -->
</section>
@stop