@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($package_category))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Package Category from <i>"{{$package_category->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Package Category <i>"{{$package_category->title}}"</i>
                <a href="{{URL::route('dashboard.package-category.cloning',array($package_category->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
            <h3><i class="fa fa-angle-right"></i>Add New Package Category</h3>
        @endif
        
         <div class="row mt">
            @if(isset($package_category))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postPackageCategoryCloning'),'files'=>true, 'role' => 'form'))}}
                @else
                    {{ Form::open(array('action' => array('DashboardController@postPackageCategoryEdit'),'files'=>true, 'role' => 'form'))}}
                @endif
            @else
             {{ Form::open(array('action' => array('DashboardController@postPackageCategoryAdd'),'files'=> true, 'role' => 'form'))}}
            @endif
            <input type="hidden" name="id" @if((isset($package_category)) && (!isset($clone))) value="{{$package_category['id']}}" @endif>
             
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-4">
                                   <div class="form-group">
                                        <label for="package_type_id">Type :</label>
                                        <select id="package_type_id" class="form-control input-sm" name="package_type_id" required>
                                            <option value="">Please Select...</option>
                                            @if(isset($package_types))
                                                @foreach($package_types as $type)
                                            <option value="{{ $type->id }}" @if(isset($package_category) && ($package_category['package_type_id'] == $type->id)) {{'selected'}} @endif>{{ $type->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="title" class="control-label">Package Category Title :</label>
                                        <input type="text" name="title" id="title" class="form-control input-sm"  required
                                               placeholder="Category Title..." {{isset($package_category) ? 'value="'.$package_category['title']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="main_image">Package Category Image :</label>
                                        @if(isset($package_category))
                                            @if(isset($clone))
                                                <input type="file" id="main_image" name="main_image"></input><br/>
                                            @else
                                                <input type="file" id="main_image" name="main_image"></input><br/>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        @if(!is_null($package_category->main_image))
                                                        <div class="thumbnail">
                                                            <img class="img-responsive" src='{{asset($package_category->main_image)}}' height="100px"></img>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            <input type="file" id="main_image" name="main_image" required></input>
                                        @endif
                                    </div>
                                </div>
                            </div>
        
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="seo_page_title">Seo Page Title :</label>
                                            <input type="text" 
                                               placeholder="Seo page title" 
                                               id="seo_page_title" 
                                               name='seo_page_title' 
                                               {{isset($package_category) ? 'value="'.$package_category['seo_page_title']. '"' : ''}} class="form-control input-sm">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="seo_meta_desc">Seo Meta Desc :</label>
                                            <textarea id="seo_meta_desc" name="seo_meta_desc" class="form-control input-sm"  placeholder="SEO meta description  ..." rows="3">{{isset($package_category) ? $package_category['seo_meta_desc']: ''}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="seo_meta_kw">Seo Meta Keyword :</label>
                                            <textarea id="seo_meta_kw" name="seo_meta_kw" class="form-control input-sm"  placeholder="SEO meta Keyword..." rows="3">{{isset($package_category) ? $package_category['seo_meta_kw']: ''}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
        
                            <div class="row">
                                <div class="col-md-12">
                                      <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package_category))
                                                <input name='published' type="checkbox" value="1"> Publish this PackageCategory
                                                @else
                                                    @if(strtotime($package_category['published']) == 0)
                                                    <input name='published' type="checkbox" value="1"> Publish this PackageCategory
                                                    @else
                                                    <input name='published' type="checkbox" value="1" checked> Publish this PackageCategory
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>

                    <div class="col-md-7">
                        <div class="form-group">
                            <label>Package Category Text :</label>
                            <textarea id="body" name="body" class="form-control" placeholder="Content for Blog's body text ..." rows="6">{{isset($package_category) ? $package_category['body']: ''}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="login-footer">
                        @if(isset($package_category))
                            @if(isset($$clone))
                                <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right">Clone</button>
                            @else
                                <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right">Submit</button>
                            @endif
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                        @endif
                    </div>
            </div>
            {{Form::close()}} 
        </div>
         
    </section><! --/wrapper -->
</section>
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 
 <script type="text/javascript">
    $(document).ready(function() {
        
      $('#body').summernote({ 
         height:200,
        onImageUpload:function(files,editor,welEditable){
            sendFile(files[0],editor,welEditable);
        }
      });
        
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: '{{URL::to("summernote/upload")}}',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
    });
    </script>
@stop