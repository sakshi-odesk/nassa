@extends('layout.dashboard')

@section('content')


<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($hotel))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Hotel from <i>"{{$hotel->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Hotel <i>"{{$hotel->title}}"</i>
                <a href="{{URL::route('dashboard.hotel.cloning',array($hotel->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
         
        @else
         <h3><i class="fa fa-angle-right"></i>Add New Hotel</h3>
        @endif
         <div class="row mt">
                @if(isset($hotel))
                    @if(isset($clone))
                        {{ Form::open(array( 'action' => 'DashboardController@postHotelCloning', 'files' => true,'role' => 'form', 'id'=>'hotel_form_clone')) }} 
                    @else
                        {{ Form::open(array( 'action' => 'DashboardController@postHotelEdit', 'files' => true,'role' => 'form', 'id'=>'hotel_form_edit')) }} 
                    @endif
                @else
                {{ Form::open(array( 'action' => 'DashboardController@postHotelAdd', 'files' => true,'role' => 'form' ,'id'=>'hotel_form')) }} 
                @endif

                <input type="hidden" name="id" {{isset($hotel) ? 'value="'.$hotel['id']. '"' : ''}}>
                <div class="container-fluid">
                    <div class="box-body">
                        
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Title :</label>
                                    <input type="text" 
                                       placeholder="Hotel's title goes here.." 
                                       name='title'
                                       class="form-control input-sm"
                                       {{isset($hotel) ? 'value="'.$hotel['title']. '"' : ''}}  required>
                                    <small class="help-block"><em>The Hotel title would be used to generate SEO friendly URLS</em></small>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Hotel Category :</label>
                                    <select class="form-control input-sm" name="hotel_category" required>
                                        <option value="">Please Select...</option>
                                        @if(isset($hotel_categories))
                                            @foreach($hotel_categories as $category)
                                        <option value="{{ $category->id }}" @if(isset($hotel) && ($hotel['hotel_category'] == $category->id)) {{'selected'}} @endif>{{ $category->title }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                              
                              <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Star Category :</label> <br/>
                                     <input type="hidden" name="star_category" class="rating" data-step="0.1" data-min="1"
                                            data-max="5" {{isset($hotel) ? 'value="'.$hotel['star_category']. '"' : ''}} />
                                </div>
                              </div>
                          
                            <?php if(isset($hotel)) { $destination = Destination::find($hotel->destination_id); }?>
                            <div class="col-md-2">
                                <div class="form-group" id="destination_input" >
                                    <label class="control-label">Destinations :</label>
                                    @if(isset($hotel))
                                        <input id="destination_title" type="hidden" name="destination_id" value="{{$destination->id}}" >
                                    @else
                                        <input id="destination_title" type="hidden" name="destination_id" value="" >
                                    @endif
                                    <input id="topic_title" 
                                           type="text" 
                                           placeholder="Search Destinations" 
                                           class="form-control input-sm" 
                                           onclick="change_val(); return false;"
                                          {{isset($hotel) ? 'value="'.$destination['label']. '"' : ''}} >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Location :</label>
                                    <input type="text" 
                                       placeholder="Hotel Location.." 
                                       name='location'
                                       class="form-control input-sm"
                                       {{isset($hotel) ? 'value="'.$hotel['location']. '"' : ''}}>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="control-label">Price :</label>
                                    <input type="text" 
                                       placeholder="Hotel Price.." 
                                       name='price'
                                       class="form-control input-sm"
                                       {{isset($hotel) ? 'value="'.$hotel['price']. '"' : ''}} required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Hotel Text :</label>
                                    <textarea id="body" name="body" class="form-control" placeholder="Content for Hotel's body text ..." rows="6">{{isset($hotel) ? $hotel['body']: ''}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="hotel_images">
                                    <label>Hotel Image :</label>
                                    <input  type="file" id="main_image" name="image"/>        
                                </div>
                                <a href="#" id="add_more_image">Add More Images</a><br>
                            </div>
                        </div>    
                            @if(isset($hotel_images))
                                @if(!isset($clone))
                                    @foreach($hotel_images as $hotel_image)
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="thumbnail">
                                                <div class="images">
                                                    <div class="image-wrapper">
                                                         <img class="img-responsive" src='{{asset($hotel_image->path)}}'></img><br/>
                                                         <a href="{{URL::route('dashboard.hotel-image.delete',array($hotel_image->id))}}" class="label label-primary">Delete</a>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                            @endif
                  
              
                  
                  <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    @if(!isset($hotel))
                                    <input id='special_offer' name='special_offer' type="checkbox" value="1"> Special Offer
                                    @else
                                        @if($hotel['special_offer'] == 0)
                                        <input id='special_offer' name='special_offer' type="checkbox" value="1"> Special Offer
                                        @else
                                        <input id='special_offer' name='special_offer' type="checkbox" value="1" checked> Special Offer
                                        @endif
                                    @endif
                                </label>
                            </div>
                        </div>
                  
                      
                      @if(Auth::user()->role == 'admin') 
                      <div class="form-group">
                            <div class="checkbox">
                            <label>
                                @if(!isset($hotel))
                                <input id='published' name='published' type="checkbox" value="1"> Publish this Hotel
                                @else
                                    @if($hotel['published'] == null)
                                    <input id='published' name='published' type="checkbox" value="1"> Publish this Hotel
                                    @else
                                    <input id='published' name='published' type="checkbox" value="1" checked> Publish this Hotel
                                    @endif
                                @endif
                            </label>
                            </div>
                        </div>
                    @endif
                  
                   @if(Auth::user()->role == 'supplier') 
                      @if(in_array('publish',$hotel_access))
                                 
                        <div class="form-group">
                            <div class="checkbox">
                            <label>
                                @if(!isset($hotel))
                                <input id='published' name='published' type="checkbox" value="1"> Publish this Hotel
                                @else
                                    @if($hotel['published'] == 0)
                                    <input id='published' name='published' type="checkbox" value="1"> Publish this Hotel
                                    @else
                                    <input id='published' name='published' type="checkbox" value="1" checked> Publish this Hotel
                                    @endif
                                @endif
                            </label>
                            </div>
                        </div>
                    @endif
                    @endif
                  
                      @if(Auth::user()->role == 'allusers') 
                      @if(in_array('publish',$hotel_access))
                                 
                        <div class="form-group">
                            <div class="checkbox">
                            <label>
                                @if(!isset($hotel))
                                <input id='published' name='published' type="checkbox" value="1"> Publish this Hotel
                                @else
                                    @if($hotel['published'] == null)
                                    <input id='published' name='published' type="checkbox" value="1"> Publish this Hotel
                                    @else
                                    <input id='published' name='published' type="checkbox" value="1" checked> Publish this Hotel
                                    @endif
                                @endif
                            </label>
                            </div>
                        </div>
                    @endif
                    @endif
                  
                  <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    @if(!isset($hotel))
                                    <input id='hotpick' name='hotpick' type="checkbox" value="1"> Hot Pick
                                    @else
                                        @if($hotel['hotpick'] == 0)
                                        <input id='hotpick' name='hotpick' type="checkbox" value="1"> Hot Pick
                                        @else
                                        <input id='hotpick' name='hotpick' type="checkbox" value="1" checked> Hot Pick
                                        @endif
                                    @endif
                                </label>
                            </div>
                        </div>
                  
                 <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    @if(!isset($hotel))
                                    <input id='recommended' name='recommended' type="checkbox" value="1"> Recommended
                                    @else
                                        @if($hotel['recommended'] == 0)
                                        <input id='recommended' name='recommended' type="checkbox" value="1"> Recommended
                                        @else
                                        <input id='recommended' name='recommended' type="checkbox" value="1" checked> Recommended
                                        @endif
                                    @endif
                                </label>
                            </div>
                        </div>
                  
                        <br/>
                        <fieldset>
                            <legend>Seo Options</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="seo_page_title">Seo Page Title :</label>
                                        <input type="text" 
                                           placeholder="Seo page title" 
                                           id="seo_page_title" 
                                           name='seo_page_title' 
                                           {{isset($hotel) ? 'value="'.$hotel['seo_page_title']. '"' : ''}} class="form-control input-sm" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_desc">Seo Meta Desc :</label>
                                        <textarea id="seo_meta_desc" name="seo_meta_desc" class="form-control input-sm"  placeholder="SEO meta description  ..." rows="3">{{isset($hotel) ? $hotel['seo_meta_desc']: ''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_kw">Seo Meta Keyword :</label>
                                        <textarea id="seo_meta_kw" name="seo_meta_kw" class="form-control input-sm" placeholder="SEO meta Keyword..." rows="3">{{isset($hotel) ? $hotel['seo_meta_kw']: ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
             
                         <div class="login-footer">
                                    
                            @if(isset($hotel))
                                @if(isset($clone))
                                    <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right" onclick="valid_destination('clone'); return false;">Edit</button>
                                @else
                                    <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right" onclick="valid_destination('edit'); return false;">Edit</button>
                                @endif
                            @else
                            <button type="submit" name="save" class="btn btn-primary btn-sm pull-right"  onclick="valid_destination('add'); return false;">Submit</button>
                            @endif
                        </div>
             
                    </div><!-- /.box-body -->
                </div><!-- /.Container-fluid -->
                
                {{ Form::close() }}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js')}}

<script type="text/javascript">
    $(document).ready(function() {
        
        $('#body').summernote({ 
            height:200,
            onImageUpload:function(files,editor,welEditable){
                sendFile(files[0],editor,welEditable);
            }
        });

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: '{{URL::to("summernote/upload")}}',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
        
        $('.images .del').click(function(e){
            
            var self = this;
            image_url = this.href;
            e.preventDefault();
                        
            $.ajax({
                type: 'get',
                url: image_url,
                success:function(data){
                    if(data)
                    { 
                        $(self).parent('.image-wrapper').remove();
                    }
                }
            });
            
        });
        
        $('#add_more_image').click(function (e) {
            e.preventDefault();
            $('<input type="file" name="main_image[]">').appendTo('#hotel_images');
           
        });
    });
  
  
 
        
  function change_val()
  {
        $('input[name="destination_id"]').attr('value','');
      
//       if(change_des)
//        {
            $(function() {

                $("#topic_title").autocomplete({
                    source: '{{URL::to("destinations.json")}}',
                    minLength: 1,

                      select: function(event, ui) {

//                        Prevent value from being put in the input:
                            this.value = ui.item.id;

                            $('input[name="destination_id"]').attr('value',this.value);
                    },

                    html: true, // optional (jquery.ui.autocomplete.html.js required)

                    // optional (if other layers overlap autocomplete list)
                    open: function(event, ui) {
                        $(".ui-autocomplete").css("z-index", 1000);
                    }
                  });

              }); 
//        }

    }


  
//  console.log(input[name="destination_id"]);
  
function valid_destination(value)
  {
      var valid_des = $("#destination_title").val();
    
         if(!valid_des)
          {                
            alert ("Please enter valid destination");
          }
          else
            {
              if(value == 'add')
              {
                  $( "#hotel_form" ).submit();
              }
              
              if(value == 'edit')
              {
                  $( "#hotel_form_edit" ).submit();
              }
              
              if(value == 'clone')
              {
                  $( "#hotel_form_clone" ).submit();
              }
                        
            }
      }

</script>
@stop