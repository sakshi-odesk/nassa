@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table id="pgrid" class="table table-striped table-advance table-hover">
	                  	  	  <h4>
                                  @if(Auth::user()->role == 'admin') 
                                    <i class="fa fa-angle-right"></i>Hotels
                                     <a href="{{URL::route('dashboard.hotel.form')}}" class="btn btn-info btn-sm pull-right">
                                    <i class="fa fa-building-o"></i>&nbsp; Add New Hotel</a>
                                  @endif
                                
                                  @if(Auth::user()->role == 'supplier') 
                                  @if(in_array('create',$hotel_access))
                                    <i class="fa fa-angle-right"></i>Hotels
                                     <a href="{{URL::route('dashboard.hotel.form')}}" class="btn btn-info btn-sm pull-right">
                                    <i class="fa fa-building-o"></i>&nbsp; Add New Hotel</a>
                                  @endif
                                  @endif
                                
                                
                                  @if(Auth::user()->role == 'allusers') 
                                  @if(in_array('create',$hotel_access))
                                    <i class="fa fa-angle-right"></i>Hotels
                                     <a href="{{URL::route('dashboard.hotel.form')}}" class="btn btn-info btn-sm pull-right">
                                    <i class="fa fa-building-o"></i>&nbsp; Add New Hotel</a>
                                  @endif
                                  @endif
                              </h4>
                            <hr>
	                  	  	  
                              <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Title</th>
                                  <th><i class="fa fa-location-arrow"></i> Location</th>
                                  <th><i class="fa fa-inr"></i> Price</th>
                                  <th><i class="fa fa-bookmark"></i> Hot Picks</th>
                                  <th><i class="fa fa-bookmark"></i> Status</th>
                                  <th><i class=" fa fa-edit"></i> Action</th>
                              </tr>
                              </thead>
                              <tfoot>
                              <tr>
                                  <th></th>
                                  <th>Title</th>
                                  <th>Location</th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                              </tr>
                              </tfoot>
                              <tbody>
                                        
                               @foreach($hotels as $hotel)
                                  @if(Auth::user()->role != 'admin')
                                  @if(Auth::user()->id == $hotel->user_id)
                                <tr>                                   
                                    <td>{{ $hotel->created_at }}</td>
                                    <td>{{ $hotel->title }}</td>
                                    
                                    <td>{{ $hotel->location }}</td>
                                    <td>{{ $hotel->price }}</td>
                                    
                                     @if($hotel->hotpick == 1)
                                    <td>
                                        <a href="javascript:;" class="btn btn-success btn-xs">
                                        <span class="label label-success" data-toggle="tooltip" data-original-title="Click here to remove hotel from hot picks section">Yes</span>
                                        </a>
                                    </td>
                                    @else
                                    <td>
                                        <a href="javascript:;" class="btn btn-danger btn-xs">
                                        <span class="label label-danger" data-toggle="tooltip" data-original-title="Click here to add hotel on hot picks section">No</span>
                                        </a>
                                    </td>
                                    @endif
                                    <td>
                                        @if($hotel->published)
                                        <span class="label label-success">Published</span>
                                        @else
                                        <span class="label label-danger">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{URL::route('dashboard.hotel.edit',array($hotel->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;
                                      
                                      @if(Auth::user()->role == 'admin') 
                                   
                                        <a href="{{URL::route('dashboard.hotel.delete',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                    
                                      @endif
                                      
                                      @if(Auth::user()->role == 'supplier') 
                                      @if(in_array('delete',$hotel_access))
                                        <a href="{{URL::route('dashboard.hotel.delete',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                      @endif
                                      @endif
                                      
                                      @if(Auth::user()->role == 'user') 
                                      @if(in_array('delete',$hotel_access))
                                        <a href="{{URL::route('dashboard.hotel.delete',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                      @endif
                                      @endif
                                        &nbsp;
                                       @if(Auth::user()->role == 'admin') 
                                       @if(Auth::user()->id == $hotel->user_id)                                                 
                                        <a href="{{URL::route('dashboard.hotel.cloning',array($hotel->id))}}" class="btn btn-primary btn-xs">
                                        <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                        </a>
                                       @endif
                                      @endif
                                      
                                      @if(Auth::user()->role == 'supplier') 
                                       @if(Auth::user()->id == $hotel->user_id)                                                                                   
                                        <a href="{{URL::route('dashboard.hotel.cloning',array($hotel->id))}}" class="btn btn-primary btn-xs">
                                        <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                        </a>
                                       @endif
                                      @endif
                                   </td>
                                </tr>
                                @endif
                                
                                @else
                                <tr>                                   
                                    <td>{{ $hotel->created_at }}</td>
                                    <td>{{ $hotel->title }}</td>
                                    <td>{{ $hotel->location }}</td>
                                    <td>{{ $hotel->price }}</td>
                                    
                                     @if($hotel->hotpick == 1)
                                    <td>
                                        <a href="{{URL::route('hotel-remove-hotpicks',array($hotel->id))}}" class="btn btn-success btn-xs">
                                        <span class="label label-success" data-toggle="tooltip" data-original-title="Click here to remove hotel from hot picks section">Yes</span>
                                        </a>
                                    </td>
                                    @else
                                    <td>
                                        <a href="{{URL::route('hotel-add-hotpicks',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="label label-danger" data-toggle="tooltip" data-original-title="Click here to add hotel on hot picks section">No</span>
                                        </a>
                                    </td>
                                    @endif
                                    <td>
                                        @if($hotel->published)
                                        <span class="label label-success">Published</span>
                                        @else
                                        <span class="label label-danger">Unpublished</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{URL::route('dashboard.hotel.edit',array($hotel->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;
                                      
                                      @if(Auth::user()->role == 'admin') 
                                      @if(in_array('delete',$hotel_access))
                                        <a href="{{URL::route('dashboard.hotel.delete',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                      @endif
                                      @endif
                                      
                                      @if(Auth::user()->role == 'supplier') 
                                      @if(in_array('delete',$hotel_access))
                                        <a href="{{URL::route('dashboard.hotel.delete',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                      @endif
                                      @endif
                                      
                                      @if(Auth::user()->role == 'allusers') 
                                      @if(in_array('delete',$hotel_access))
                                        <a href="{{URL::route('dashboard.hotel.delete',array($hotel->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                      @endif
                                      @endif
                                        &nbsp;
                                       @if(Auth::user()->role == 'admin')                                                                              
                                        <a href="{{URL::route('dashboard.hotel.cloning',array($hotel->id))}}" class="btn btn-primary btn-xs">
                                        <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                        </a>
                                       @endif
                                      
                                       @if(Auth::user()->role == 'supplier') 
                                       @if(Auth::user()->id == $hotel->user_id)                                                                                   
                                        <a href="{{URL::route('dashboard.hotel.cloning',array($hotel->id))}}" class="btn btn-primary btn-xs">
                                        <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                        </a>
                                       @endif
                                       @endif                               
                                   </td>
                                </tr>
                                @endif
                                @endforeach
                               
                                
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

@section('css')
{{HTML::style('theme/nassatravels/assets/css/dataTables.bootstrap.css')}}
@stop
@section('js')
{{ HTML::script('theme/nassatravels/assets/js/jquery.dataTables.js') }}
{{ HTML::script('theme/nassatravels/assets/js/dataTables.bootstrap.js') }}

<script type="text/javascript">

$(document).ready(function(){

    // Setup - add a text input to each footer cell
    $('#pgrid tfoot th').each( function () {

        if ($.inArray($(this).index(), [0,3,4,5,6]) === -1) {
            var title = $('#pgrid thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } else {
            $(this).html('');
        }
    } );

    // DataTable
    var table = $('#pgrid').DataTable({
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [1,2,3,4],
        }],
        initComplete: function (){
          var r = $('#pgrid tfoot tr');
          r.find('th').each(function(){
            $(this).css('padding', 8);
          });
          $('#pgrid thead').append(r);
          $('#search_0').css('text-align', 'center');
        },
    });

    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    });
});
</script>

@stop


