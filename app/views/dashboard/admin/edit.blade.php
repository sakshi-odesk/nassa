@extends('layout.dashboard')

@section('content')

   

<section id="main-content">
    <section class="wrapper site-min-height">
     <div class= "container">
       
     
              {{ Form::open(array('action' => array('DashboardController@postAdminEdit'), 'files' => true, 'role' => 'form'))}} 
    
           
         <div class ="form-group">
              <label for="inputname" class ="col-sm-2 control-lebel"> Role:</label>
                <div class ="col-sm-10">
                  <input style ="width:200px" type ="text" name ="role" id ="role" placeholder="enter role" required @if(isset($admin))  value="{{ $admin->role}}" @endif readonly><br><br>
                </div>
              <label for="inputname" class ="col-sm-2 control-lebel"> Enter Email:</label>
                <div class ="col-sm-10">
                  <input style ="width:200px" type ="text" name ="email" id ="email" placeholder="enter user" required @if(isset($admin))  value="{{ $admin->email}}" @endif><br><br>
                </div>
         
                  <input style ="width:200px" type ="hidden" name ="id" value="{{$admin->id}}"> 
              
              <label for="inputname" class="col-sm-2 control-lebel"> Enter password:</label>
                <div class="col-sm-10">
                  <input type="password" name="password" id="password" style="width:200px" placeholder="password" required><br><br>
                </div>
             <label for="inputname" class="col-sm-2 control-lebel"> Reconfirm password:</label>
                <div class="col-sm-10">
                  <input type="password" name="password_confirmation" id="password" style="width:200px" placeholder="reconfirm password"><br><br>
                </div>
              </div>
       
              <div class="form-group">
                  <div class="sol-sm-offset-2 col-sm-10">
       
                     @if(count($errors) > 0)
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li style="color:red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
         
           
            <div class="form-group">
                  <div class="sol-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </div>
      {{ Form::close() }}
        </div>
      </section><! --/wrapper -->
</section>

       
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js')}}


@stop