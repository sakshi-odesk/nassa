@extends('layout.dashboard')

@section('content')

   

<section id="main-content">
    <section class="wrapper site-min-height">
      <h3><i class="fa fa-angle-right"></i>Access Rights</h3>
        <div class="container">
       
     
              {{ Form::open(array('action' => array('DashboardController@postAdminAccess'), 'files' => true,'role' => 'form'))}} 
         
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label"> Packages Access:</label>
                     
                   <select id="packages_access" class="form-control input-sm" name="packages_access" >
                    <option value="none" selected>None</option>
                    <option value="create">Create</option>
                    <option value="create/publish">Create/Publish</option>
                    <option value="create/publish/delete">Create/Publish/Delete</option>
                   </select>                                     
              </div>
          </div>
          <br><br><br><br><br><br>
             
           <div class="col-md-4">
            <div class="form-group">
              <label class="control-label"> Hotels Access:</label>
                     
                   <select id="hotels_access" class="form-control input-sm" name="hotels_access" >
                    <option value="none" selected>None</option>
                    <option value="create">Create</option>
                    <option value="create/publish">Create/Publish</option>
                    <option value="create/publish/delete">Create/Publish/Delete</option>
                   </select>                                     
              </div>
          </div>   
          <br><br><br><br><br><br>
          
          <input style ="width:200px" type ="hidden" name ="id" value="{{$admin->id}}"> 
          
           
              <div class="form-group">
                  <div class="sol-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary"> Update Profile</button>
                  </div>
              </div>
              
               
      {{ Form::close() }}-
        </div>
      </section><! --/wrapper -->
</section>

       
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js')}}


@stop