@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>Add New Operator</h3>
         <div class="row mt">
             
                {{ Form::open(array( 'action' => array('DashboardController@postOperatorUpdate',$user->id), 'files' => true,'role' => 'form')) }} 
                
                <div class="container-fluid">
                   
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="title" class="control-label">Email :</label>
                                    <input type="text" class="form-control"
                                       placeholder="Enter Email" 
                                       name='email'
                                       @if(isset($user)) value="{{ $user->email }}" @else @endif>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Password :</label>
                                    <input type="password" class="form-control"
                                       placeholder="Enter Password" 
                                       name='password'>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Confirmation Password :</label>
                                    <input type="password" class="form-control"
                                       placeholder="Confirmation Password" 
                                       name='password_confirmation'>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="login-footer">
                            <button type="submit" name="save" class="btn btn-primary btn-sm pull-right">Submit</button>
                        </div>
                   
                </div><!-- /.Container-fluid -->
                {{ Form::close() }}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop