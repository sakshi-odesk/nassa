@extends('layout.dashboard')

@section('content')
<section id="main-content">
          <section class="wrapper">
             <div class="row mt">
                 @if(Session::has('success'))
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{Session::get('success')}}
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
                  <div class="col-md-12">
                      <div class="content-panel">
                          <table id="pgrid" class="table table-striped table-advance table-hover">
	                  	  	  <h4>
                                  <i class="fa fa-angle-right"></i>Operator
                                  <a href="{{URL::route('dashboard.operator.create')}}" class="btn btn-info btn-sm pull-right">
                                  <i class="fa fa-building-o"></i>&nbsp; Add New Operator</a>
                              </h4>
	                  	  	  <hr>
                            
                             <thead>
                              <tr>
                                  <th><i class="fa fa-bullhorn"></i> Date</th>
                                  <th class="hidden-phone"><i class="fa fa-question-circle"></i> Email</th>
                                  <th><i class="fa fa-user"></i>Role</th>
                                  <th><i class="fa fa-edit"></i>Action</th>
                              </tr>
                              </thead>
                              <tfoot>
                              <tr>
                                  <th>Date</th>
                                  <th>Email</th>
                                  <th>Role</th>
                              </tr>
                              </tfoot>
                              <tbody>
                             
                             
                               @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role}}</td>
                                 
                                                                    
                                    <td>
                                        <a href="{{URL::route('dashboard.operator.edit',array($user->id))}}" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                        </a>
                                        &nbsp;
                                      
                                        <a href="{{URL::route('dashboard.operator.delete',array($user->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-user" data-toggle="tooltip" data-original-title="Delete"></span>
                                        </a>
                                        &nbsp;
                                   </td>
                                </tr>
                                @endforeach
                             
                              </tbody>
                          </table>
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
              </div><!-- /row -->
          </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

@section('css')
{{HTML::style('theme/nassatravels/assets/css/dataTables.bootstrap.css')}}
@stop
@section('js')
{{ HTML::script('theme/nassatravels/assets/js/jquery.dataTables.js') }}
{{ HTML::script('theme/nassatravels/assets/js/dataTables.bootstrap.js') }}

<script type="text/javascript">

$(document).ready(function(){

    // Setup - add a text input to each footer cell
    $('#pgrid tfoot th').each( function () {

        if ($.inArray($(this).index(), [0,3]) === -1) {
            var title = $('#pgrid thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } else {
            $(this).html('');
        }
    } );

    // DataTable
    var table = $('#pgrid').DataTable({
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [1,2],
        }],
        initComplete: function (){
          var r = $('#pgrid tfoot tr');
          r.find('th').each(function(){
            $(this).css('padding', 8);
          });
          $('#pgrid thead').append(r);
          $('#search_0').css('text-align', 'center');
        },
    });

    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    });
});
</script>

@stop


