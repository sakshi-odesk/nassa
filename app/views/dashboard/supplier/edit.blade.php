@extends('layout.dashboard')

@section('content')

  

<section id="main-content">
    <section class="wrapper site-min-height">
     <div class= "container">
         {{ Form::open(array( 'action' => 'DashboardController@postSupplierEdit', 'files' => true,'role' => 'form')) }} 
           <div class ="form-group">
              <label for="inputname" class ="col-sm-2 control-lebel"> Role:</label>
                <div class ="col-sm-10">
                  <input style ="width:200px" type ="text" name ="role" id ="role" placeholder="enter role" required @if(isset($supplier))  value="{{ $supplier->role}}" @endif readonly><br><br>
                </div>
              <label for="inputname" class ="col-sm-2 control-lebel"> Enter Email:</label>
                <div class ="col-sm-10">
                  <input style ="width:200px" type ="text" name ="email" id ="email" placeholder="enter user" required @if(isset($supplier))  value="{{ $supplier->email}}" @endif><br><br>
                </div>
         
                  <input style ="width:200px" type ="hidden" name ="id" value="{{$supplier->id}}"> 
              
              <label for="inputname" class="col-sm-2 control-lebel"> Enter password:</label>
                <div class="col-sm-10">
                  <input type="password" name="password" id="password" style="width:200px" placeholder="password" required><br><br>
                </div>
             <label for="inputname" class="col-sm-2 control-lebel"> Reconfirm password:</label>
                <div class="col-sm-10">
                  <input type="password" name="password_confirmation" id="password" style="width:200px" placeholder="reconfirm password"><br><br>
                </div>
              </div>
            <div>
      </div>
      
       <div class="form-group">
                  <div class="sol-sm-offset-2 col-sm-10">
       
                     @if(count($errors) > 0)
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li style="color:red">{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
           
                

            <div class="form-group">
            <div class="sol-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </div>
          {{ Form::close() }}
        </div>
      </section><! --/wrapper -->
</section>

        
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js')}}

<script type="text/javascript">
    $(document).ready(function() {
        
        $('#body').summernote({ 
            height:200,
            onImageUpload:function(files,editor,welEditable){
                sendFile(files[0],editor,welEditable);
            }
        });

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: '{{URL::to("summernote/upload")}}',
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    editor.insertImage(welEditable, url);
                }
            });
        }
        
        $('.images .del').click(function(e){
            
            var self = this;
            image_url = this.href;
            e.preventDefault();
                        
            $.ajax({
                type: 'get',
                url: image_url,
                success:function(data){
                    if(data)
                    { 
                        $(self).parent('.image-wrapper').remove();
                    }
                }
            });
            
        });
        
        $('#add_more_image').click(function (e) {
            e.preventDefault();
            $('<input type="file" name="main_image[]">').appendTo('#hotel_images');
           
        });
    });
    
    $(function() {
 
        $("#topic_title").autocomplete({
            source:'{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                $('<input type="hidden" value="'+this.value+'" name="destination_id" >').appendTo('#destination_input');
                
            },
         
            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
               $(".ui-autocomplete").css("z-index", 1000);
            }
        });
 
    });
    </script>
@stop