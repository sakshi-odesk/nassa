@extends('layout.dashboard')

@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i>Delete User "{{$supplier->email}}"</h3>
        <div class="row mt">
            {{ Form::open(array( 'action' => 'DashboardController@postSupplierDelete', 'files' => true, 'role' => 'form')) }} 
                <div class="container-fluid">
                    <div class="box-body">
                    <p>This action is irreversible. 
                        Do you wish to continue ?</p>
                        {{ Form::hidden('id', $supplier->id) }}
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-secondary btn-success" href="{{URL::route('dashboard.supplier.list')}}">Back to list</a>
                        <button class="btn btn-primary btn-danger pull-right" type="submit">Yes, Delete this User</button>
                     
                    </div>
                </div>
          
            <div>
            @if (Session::has('message'))
              <div class="alert alert-info" style="width:350px;margin-left:200px; color:red">{{ Session::get('message') }}</div>
            @endif
            </div>
            {{ Form::close()}}
            </div>
    </section><! --/wrapper -->
</section>
@stop