@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($facility))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Facility from <i>"{{$facility->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Facility <i>"{{$facility->title}}"</i>
                <a href="{{URL::route('dashboard.facility.cloning',array($facility->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
         <h3><i class="fa fa-angle-right"></i>Add New Facility</h3>
        @endif
         <div class="row mt">
            @if(isset($facility))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postFacilityCloning'),'files'=>true, 'role' => 'form'))}}
                 @else
                    {{ Form::open(array('action' => array('DashboardController@postFacilityEdit'),'files'=>true, 'role' => 'form'))}}
                 @endif
            
            @else
            {{ Form::open(array('action' => array('DashboardController@postFacilityAdd'),'files'=> true, 'role' => 'form'))}}
            @endif

            <input type="hidden" name="id" @if((isset($facility)) && (!isset($clone))) value="{{$facility['id']}}" @endif>
            <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Facility Title :</label>
                                <input type="text" name="title" class="form-control input-sm" required
                                       placeholder="Facility Title..." {{isset($facility) ? 'value="'.$facility['title']. '"' : ''}} />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="main_image" class="control-label">Image :</label>
                        @if(isset($facility))
                            @if(isset($clone))
                                <input type="file" id="main_image" name="main_image"></input><br/>
                            @else
                                <input type="file" id="main_image" name="main_image"></input><br/>
                                <div class="row">
                                    <div class="col-md-3">
                                        @if(!is_null($facility->main_image))
                                        <div class="thumbnail">
                                            <img class="img-responsive" src='{{asset($facility->main_image)}}' height="100px"></img>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @else
                            <input type="file" id="main_image" name="main_image" required></input>
                        @endif
                    </div>
                    <div class="login-footer">
                        @if(isset($facility))
                            @if(isset($clone))
                                <button type="submit" name="clone" class="btn btn-primary btn-sm pull-left">Clone</button>
                            @else
                                <button type="submit" name="edit" class="btn btn-primary btn-sm pull-left">Submit</button>
                            @endif`
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                        @endif
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop