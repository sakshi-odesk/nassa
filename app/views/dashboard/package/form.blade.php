@extends('layout.dashboard')

@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
        @if(isset($package))
            @if(isset($clone))
                <h3><i class="fa fa-angle-right"></i>Clone Package from <i>"{{$package->title}}"</i></h3>
            @else
                <h3><i class="fa fa-angle-right"></i>Edit Package <i>"{{$package->title}}"</i>
                <a href="{{URL::route('dashboard.package.cloning',array($package->id))}}" class="btn btn-info btn-sm pull-right">
                <i class="fa fa-files-o"></i>&nbsp; Clone</a></h3>
            @endif
        @else
         <h3><i class="fa fa-angle-right"></i>Add New Package</h3>
        @endif
        
        <div class="row mt">
            @if(isset($package))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postPackageCloning'),'files'=>true, 'role' => 'form'))}}
                @else
                    {{ Form::open(array('action' => array('DashboardController@postPackageEdit'),'files'=>true, 'role' => 'form'))}}
                @endif
            @else
            {{ Form::open(array('action' => array('DashboardController@postPackageAdd'),'files'=> true, 'role' => 'form','id'=>'package_form'))}}
            @endif
            
            <input type="hidden" name="id" @if((isset($package)) && (!isset($clone))) value="{{$package['id']}}" @endif>
             <div class="container-fluid">
              
                    <?php if(isset($package)){ 
                            $package_category = $package->package_category()->getResults();
    
                             if($package_category)
                             {
                                $pack_type = $package_category->package_type()->getResults();
                             }
                          }
                    ?>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                       <div class="form-group">
                                            <label class="control-label">Menu :</label>
                                            <select id="package_type_id" class="form-control input-sm" name="package_type_id" required>
                                                <option value="">Please Select...</option>
                                                @if(!isset($package))
                                                    @if(isset($package_types))
                                                        @foreach($package_types as $type)
                                                            <option value="{{ $type->id }}">
                                                            {{ $type->title }}</option>
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @if(isset($package_types))
                                                        @foreach($package_types as $type)
                                                            <option value="{{ $type->id }}" @if(isset($package) && isset($pack_type) && ($pack_type['id'] == $type->id)) {{'selected'}} @endif>
                                                            {{ $type->title }}</option>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </select>
                                        </div>
                                </div>

                                <div class="col-md-4">
                                    <?php if(isset($package)){ $package_category = $package->package_category()->getResults(); }?>
                                    <div class="form-group">
                                        <label class="control-label">Package Category :</label>
                                        <select id="package_category_id" class="form-control input-sm" name="package_category_id" required>
                                            <option value="">Please Select...</option>
                                            @if(isset($package) && isset($package_category))
                                                <option value="{{$package_category->id}}" selected>{{$package_category->title}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                
                             <?php if(isset($package)) { $destination = Destination::find($package->destination_id); }?>
                                <div class="col-md-4">
                                    <div class="form-group" id="destination_input" >
                                        <label class="control-label">Destinations :</label>
                                        @if(isset($package))
                                            <input type="hidden" name="destination_id" value="{{$destination['id']}}">
                                        @endif
                                        <input id="topic_title" 
                                              type="text" 
                                              placeholder="Search Destinations" 
                                              class="form-control input-sm"
                                              {{isset($package) ? 'value="'.$destination['label']. '"' : ''}}  required>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="control-label">Title :</label>
                                        <input type="text" name="title" class="form-control input-sm" required
                                               placeholder="Package Title..." {{isset($package) ? 'value="'.$package['title']. '"' : ''}} />
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Hotel Level :</label>
                                        <select id="hotel_level" class="form-control input-sm" name="hotel_level" required>
                                            <option value="">Please Select...</option>
                                            <option value="budget" @if(isset($package) && ($package['hotel_level'] == 'budget')) selected  @endif>Budget</option>
                                            <option value="standard" @if(isset($package) && ($package['hotel_level'] == 'standard')) selected  @endif>Standard</option>
                                            <option value="deluxe" @if(isset($package) && ($package['hotel_level'] == 'deluxe')) selected  @endif>Deluxe</option>
                                            <option value="luxury" @if(isset($package) && ($package['hotel_level'] == 'luxury')) selected  @endif>Luxury</option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Price :</label>
                                        <input type="text" name="price" class="form-control input-sm" required
                                               placeholder="Package price..." {{isset($package) ? 'value="'.$package['price']. '"' : ''}} />
                                    </div>
                                </div>
                              
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label  class="control-label">Discounted Price :</label>
                                        <input type="text" name="discounted_price" class="form-control input-sm"
                                               placeholder="Discounted Price..." {{isset($package) ? 'value="'.$package['discounted_price']. '"' : ''}} />
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                   <div class="form-group">
                                        <label  class="control-label">Duration :</label>
                                        <input type="text" name="duration" class="form-control input-sm" required
                                               placeholder="Package Duration..." {{isset($package) ? 'value="'.$package['duration']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                 <div class="col-md-12">
                                   <div class="form-group">
                                     <label class="control-label"> Description :</label>
                                       <textarea name="desc" class="form-control" placeholder="Description  ..." rows="3">@if(isset($package)) {{ $package->desc }} @endif</textarea>
                                   </div>
                                 </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                     <div class="form-group">
                                        <label class="control-label">Route Map :</label>
                                        <input type="text" name="route_map" class="form-control input-sm" required
                                               placeholder="Package Route Map..." {{isset($package) ? 'value="'.$package['route_map']. '"' : ''}} />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Image :</label>
                                        @if(isset($package))
                                            @if(isset($clone))
                                                <input type="file" name="main_image"></input><br/>
                                            @else
                                                <input type="file" name="main_image"></input><br/>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        @if(!is_null($package->main_image))
                                                        <div class="thumbnail">
                                                            <img class="img-responsive" src='{{asset($package->main_image)}}' height="100px"></img>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>    
                                            @endif
                                        @else
                                            <input type="file" name="main_image" required></input>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            	  <!-- insert images after route map image section !!-->
                                  
                             <div class="row">
									  <?php if(isset($package))
											{
											$package_image = PackageImage::where('package_id','=',$package->id)->get();
											}
										?>
									   
                                  <div class="col-md-4">
                                      <div class="form-group">
                                          <label class="control-label">Image :</label>
											@if(isset($package))
											@if(isset($clone))
                                           <input type="file" name="PackageImage[]" ></input><br/>
										  @else
										   <input type="file" name="PackageImage[]" ></input><br/>
								
                                 	<div class="row">
                                      <div class="col-md-6">
									@if(isset($package_image[0]))
                                       @if(!is_null($package_image[0]->image))
										  <div class="thumbnail">
											  <img class="img-responsive" src='{{ asset( $package_image[0]->image ) }}' height="100px">
                                       		</div>
										  @endif
										  @endif
                                      </div> 
									</div>
									   @endif
									   @else
									   		<input type="file" name="PackageImage[]" >
									   	@endif
									  
                                    </div> 
								</div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                          <label class="control-label">Image :</label>
											@if(isset($package))
											@if(isset($clone))
                                          <input type="file" name="PackageImage[]" /><br/>
										  @else
										  <input type="file" name="PackageImage[]"/><br/>
                                        
                                         
                                        <div class="row">
                                            <div class="col-md-6">
                                             	@if(isset($package_image[1]))
                                       @if(!is_null($package_image[1]->image))
										  <div class="thumbnail">
											  <img class="img-responsive" src='{{ asset( $package_image[1]->image ) }}' height="150px">
                                            </div>
												@endif
												@endif
                                          </div>    
                                        </div>
												@endif
													@else
													<input type="file" name="PackageImage[]" >
												@endif
											</div> 
									</div>
                                    
                                     <div class="col-md-4">
                                        <div class="form-group">
                                          <label class="control-label">Image :</label>
											@if(isset($package))
											@if(isset($clone))
                                           <input type="file" name="PackageImage[]"><br/>
										  @else
                                           <input type="file" name="PackageImage[]"><br/>
                                         
                                         
                                        <div class="row">
                                          <div class="col-md-6">
                                           @if(isset($package_image[2]))
                                       @if(!is_null($package_image[2]->image))
										  <div class="thumbnail">
											  <img class="img-responsive" src='{{ asset( $package_image[2]->image ) }}' height="100px">
                                            </div>
												@endif
												@endif
                                          </div>
                                         </div> 
											@endif
													@else
													<input type="file" name="PackageImage[]" >
												@endif
                                      		</div>
                                    	</div>
								</div>

<!--                                end of multiple images-->
                                  
                            <div class="row">
                                <div class="col-md-12">
                                    <?php if(isset($package))
                                          {  
                                                $package_facilities = $package->package_facilities()->getResults();  
                                                $arr = $package_facilities->fetch('facility_id');
                                                $arr = $arr->toArray();
                                          }  
                                    ?>
                                  
                    
                                    <div class="form-group">
                                        <label class="control-label">Facility :</label>
                                        <select id="PackageFacility" multiple="multiple" class="form-control input-sm" size="7" name="PackageFacility[]" required>
                                            @if(!isset($package))
                                                @if(isset($facilities))
                                                        @foreach($facilities as $facility)
                                                        <option value="{{ $facility->id}}">{{$facility->title}}</option>
                                                        @endforeach
                                                @endif
                                            @else
                                                @if(isset($facilities))
                                                    @foreach($facilities as $facility)
                                                    <option value="{{ $facility->id}}" @if(isset($package) && (in_array($facility->id,$arr))) {{'selected'}} @endif >
                                                        {{$facility->title}}</option>
                                                   @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='special_offer' type="checkbox" value="1" > Special Offer
                                                @else
                                                    @if($package['special_offer'] == 0)
                                                    <input  name='special_offer' type="checkbox" value="1" > Special Offer
                                                    @else
                                                    <input  name='special_offer' type="checkbox" value="1" checked > Special Offer
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
      
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='hotpick' type="checkbox" value="1" > Hot Pick
                                                @else
                                                    @if($package['hotpick'] == 0)
                                                    <input  name='hotpick' type="checkbox" value="1" > Hot Pick
                                                    @else
                                                    <input  name='hotpick' type="checkbox" value="1" checked > Hot Pick
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>

      
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='recommended' type="checkbox" value="1" > Recommended
                                                @else
                                                    @if($package['recommended'] == 0)
                                                    <input  name='recommended' type="checkbox" value="1" > Recommended
                                                    @else
                                                    <input  name='recommended' type="checkbox" value="1" checked > Recommended
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
      
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="seo_page_title">Seo Page Title :</label>
                                            <input type="text" 
                                               placeholder="Seo page title" 
                                               id="seo_page_title" 
                                               name='seo_page_title' 
                                               {{isset($package) ? 'value="'.$package['seo_page_title']. '"' : ''}} 
                                                   class="form-control input-sm">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_desc">Seo Meta Desc :</label>
                                        <textarea id="seo_meta_desc" name="seo_meta_desc" class="form-control input-sm"  placeholder="SEO meta description  ..." rows="3">{{isset($package) ? $package['seo_meta_desc']: ''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_kw">Seo Meta Keyword :</label>
                                        <textarea id="seo_meta_kw" name="seo_meta_kw" class="form-control input-sm"  placeholder="SEO meta Keyword..." rows="3">{{isset($package) ? $package['seo_meta_kw']: ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                            </div>
        
                            <div class="row">
                                <div class="col-md-12">
                                      <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='published' type="checkbox" value="1"> Publish this Package
                                                @else
                                                    @if(strtotime($package['published']) == 0)
                                                    <input name='published' type="checkbox" value="1"> Publish this Package
                                                    @else
                                                    <input name='published' type="checkbox" value="1" checked> Publish this Package
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Inclusions</legend>
                                            <div id="inclusion_add">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                         <div class="form-group">
                                                            <input type="text" id="inclusion_title" name="inclusion_title" class="form-control input-sm" placeholder="Inclusion Title"/>
                                                         </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button name="add" onclick="submit_inclusion(); return false;" class="btn btn-primary btn-sm pull-right">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="package_inclusion_wrapper">
                                                <div class="inclusions" >
                                                    <ul class="sortable">
                                                        @if(isset($package))
                                                            <?php if(isset($package)){ $inclusions = $package->package_inclusions()->getResults();} $i = 0;?>
                                                            @foreach($inclusions as $inclusion)
                                                                <li>
                                                                    <div class="inclusion sortable_handle">
                                                                        <div class="row">
                                                                            <div class="col-md-10"><span class="glyphicon glyphicon-move"></span>
                                                                                <div class="form-group">
                                                                                    <input type="hidden" name="PackageInclusion[{{$i}}][id]" value="{{$inclusion->id}}">
                                                                                    <input class="weight" type="hidden" name="PackageInclusion[{{$i}}][weight]" value="{{$inclusion->weight}}">
                                                                                    <span class="text-title">{{$inclusion->title}}</span>
                                                                                </div>        
                                                                            </div>
                                                                            <div class="col-md-2">
                                                                                <div class="action">
                                                                                    <a href="#" onclick="inclusion_modal_show(event,this); return false;"  data-id="{{$inclusion->id}}" data-title="{{$inclusion->title}}" class="edit-action">
                                                                                    <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                                                    </a><a href="{{URL::route('dashboard.package-inclusion.delete',array($inclusion->id))}}" class="delete-action">
                                                                                    <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php $i++; ?>
                                                            @endforeach
                                                        @endif
                                                        </ul>
                                                    </div>
                                            </div>
                                    </fieldset>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                       <fieldset>
                                            <legend>Hotel Details</legend>
                                           <div id="hotel_detail_add">
                                               <div class="row">

                                                   <div class="col-md-4">
                                                        <input type="text" name="hotel_detail_destination" class="form-control input-sm" placeholder="Destination..."/>
                                                   </div>
                                                   
                                                   <div class="col-md-4">
                                                        <input type="text" name="hotel_detail_hotel" class="form-control input-sm" placeholder="Hotel..."/>
                                                   </div>
                                                   
                                                   <div class="col-md-2">
                                                        <input type="text" name="hotel_detail_meal_plan" class="form-control input-sm" placeholder="Meal Plan..."/>
                                                   </div>
                                                   
                                                   <div class="col-md-2">
                                                        <button name="add" onclick="submit_hotel_detail(); return false;" class="btn btn-primary btn-sm pull-right">Add</button>
                                                   </div>
                                                   
                                               </div>
                                           </div>
                                            <div class="package_hotel_details_wrapper">
                                                <div class="hotel_details">
                                                    <ul class="sortable">
                                                    @if(isset($package))
                                                        <?php if(isset($package)){ $hotel_details = $package->package_hotel_details()->getResults();} $i = 0;?>
                                                        @foreach($hotel_details as $hotel_detail)
                                                        <li>
                                                            <div class="hotel_detail sortable_handle">
                                                                <div class="row">
                                                                    <div class="col-md-4"><span class="glyphicon glyphicon-move"></span>
                                                                        <div class="form-group">
                                                                            <input type="hidden" name="PackageHotelDetail[{{$i}}][id]" value="{{$hotel_detail->id}}">
                                                                            <input class="weight" type="hidden" name="PackageHotelDetail[{{$i}}][weight]" value="{{$hotel_detail->weight}}">
                                                                            <span class="text-destination">{{$hotel_detail->destination}}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <span class="text-hotel">{{$hotel_detail->hotel}}</span>  
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                           <span class="text-meal_plan">{{$hotel_detail->meal_plan}}</span>    
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="action">
                                                                            <a href="#" onclick="hotel_detail_modal_show(event,this)" data-id="{{$hotel_detail->id}}" data-destination="{{$hotel_detail->destination}}" data-hotel="{{$hotel_detail->hotel}}" data-meal_plan="{{$hotel_detail->meal_plan}}" class="edit-action">
                                                                            <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                                            </a><a href="{{URL::route('dashboard.package-hoteldetail.delete',array($hotel_detail->id))}}" class="delete-action">
                                                                            <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php $i++; ?>
                                                        @endforeach
                                                    @endif
                                                    </ul>
                                                </div>
                                            </div>
                                       </fieldset>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                        <fieldset>
                                            <legend>Price Summaries</legend>
                                            <div id="price_summary_add">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <input type="text" name="price_summary_desc" class="form-control input-sm" placeholder="Description..."/>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" name="price_summary_cost" class="form-control input-sm" placeholder="Cost..."/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button name="add" onclick="submit_price_summary(); return false;" class="btn btn-primary btn-sm pull-right">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="package_price_summary_wrapper">
                                                <div class="price_summaries">
                                                    <ul class="sortable">
                                                    @if(isset($package))
                                                        <?php if(isset($package)){ $price_summaries = $package->package_price_summaries()->getResults();} $i = 0;?>
                                                        @foreach($price_summaries as $price_summary)
                                                        <li>
                                                            <div class="price_summary sortable_handle">
                                                                <div class="row">
                                                                    <div class="col-md-5"><span class="glyphicon glyphicon-move"></span>
                                                                        <div class="form-group">
                                                                            <input type="hidden" name="PackagePriceSummary[{{$i}}][id]" value="{{$price_summary->id}}">
                                                                            <input class="weight" type="hidden" name="PackagePriceSummary[{{$i}}][weight]" value="{{$price_summary->weight}}">
                                                                            <span class="text-desc">{{$price_summary->desc}}</span>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <div class="form-group">
                                                                            <span class="text-cost">{{$price_summary->cost}}</span>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="action">
                                                                           <a href="#" onclick="price_summary_modal_show(event,this)" data-id="{{$price_summary->id}}" data-desc="{{$price_summary->desc}}" data-cost="{{$price_summary->cost}}" class="edit-action">
                                                                           <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                                           </a><a href="{{URL::route('dashboard.package-pricesummary.delete',array($price_summary->id))}}" class="delete-action">
                                                                            <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php $i++; ?>
                                                        @endforeach
                                                    @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" ng-controller="ItinerariesController" ng-cloak>
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Itineraries</legend>
                                <div class="package_itinerary_wrapper">
                                    <div class="itineraries">
                                        <ul class="sortable">
                                            
                                            <li ng-repeat="(key,value) in itineraries">
                                                <div class="itinerary sortable_handle">
                                                    <div class="row">
                                                        <div class="col-md-6"><span class="glyphicon glyphicon-move"></span>
                                                            <div class="form-group days">
                                                                <input type="hidden" id="PackageItineraryHotelDetails" 
                                                                       name="PackageItineraryHotelDetails" 
                                                                       value="<% itineraries %>">
                                                                
                                                                 <input type="hidden" name="PackageItinerary[<% key %>][id]" value="<% value.data.id %>">
                                                                <input class="weight" type="hidden" name="PackageItinerary[<% key %>][weight]" value="<% key %>">
                                                                <span class="text-day_num"><% value.data.day_num %></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <span class="text-destination"><% value.data.destination %></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                              <span class="text-desc"><% value.data.desc %></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                            
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                          <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                              Add Hotel Details
                                                            </a>
                                                          </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                          <div class="panel-body">
                                                                <div id="package_hotel_detail_show" ng-if="value.hotel.length > 0">
                                                                    <ul>
                                                                         <li>
                                                                            <div class="package_hotel_detail">
                                                                                <div class="row">
                                                                                   <div class="col-md-2"></span>
                                                                                        <div class="form-group days">
                                                                                            <span class="text-day_num">Name</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination">Category</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination">Start Date</span>
                                                                                        </div>
                                                                                    </div> 
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination">End Date</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination">Price</span>
                                                                                        </div>
                                                                                    </div> 
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination">Action</span>
                                                                                        </div>
                                                                                    </div> 
                                                                                </div> 
                                                                            </div>
                                                                        </li>
                                                                        
                                                                         <li ng-repeat="(k,v) in value.hotel" >
                                                                            <div class="package_hotel_detail sortable_handle">
                                                                                <input type="hidden" name="NewPackageHotelDetail[<% k %>][id]" value="<% v.id %>">
                                                                                <input type="hidden" name="NewPackageHotelDetail[<% k %>][package_itinerary_id]" value="<% value.data.id %>">
                                                                                <input class="weight" type="hidden" name="NewPackageHotelDetail[<% k %>][weight]" value="<% k %>">
                                                                                
                                                                                <div class="row">
                                                                                   <div class="col-md-2"><span class="glyphicon glyphicon-move"></span>
                                                                                        <div class="form-group days">
                                                                                            <span class="text-day_num"><% v.hotel %></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination"><% v.category %></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination"><% v.start_date %></span>
                                                                                        </div>
                                                                                    </div> 
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination"><% v.end_date %></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="form-group">
                                                                                            <span class="text-destination"><% v.price %></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-2">
                                                                                        <div class="action">
                                                                                            <a href="javascript:;" ng-click="package_hotel_detail_modal_show($event,k,v,key)" class="edit-action-hotel" style="float:left">
                                                                                                <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                                                            </a>
                                                                                            <a href="javascript:;" ng-click="package_delete_hotel_detail($event,key,k,value.hotel)" class="delete-action-hotel">
                                                                                                <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>    
                                                                                </div> 
                                                                            </div>
                                                                                        
                                                                            <!-- Package Hotel Details Modal -->
                                                                            <div class="modal fade" edit-modal id="HotelDetailmodal<%key%><%k%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                                              <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                  <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel">Edit Detail</h4>
                                                                                  </div>
                                                                                  <div class="modal-body">
                                                                                      <div class="container-fluid hotel-detail-modal">
                                                                                          <form id="hotel_detail_form" action="">
                                                                                            <input type="hidden" id="edit_hotel_id<%key%><%k%>" ng-model="edit_hotel_detail_id" name="edit_hotel_id" value="<% v.id %>" >
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label">Hotels :</label>
                                                                                                            <input type="text" edit-hotel id="edit_hotel_id" name="edit_hotel_title_label" ng-model="edit_hotel_label" class="form-control input-sm" 
                                                                                                                   placeholder="Hotels..." value="" />
                                                                                                            <input type="hidden" id="edit_package_hotel_title<%key%><%k%>" name="edit_hotel_title">
                                                                                                            <input type="hidden" id="edit_package_hotel_id<%key%><%k%>" name="edit_hotel_id">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-md-4">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label">Category :</label>
                                                                                                            <select name="package_hotel_detail_category" 
                                                                                                                    id="edit_package_hotel_detail_category<%key%><%k%>"
                                                                                                                    ng-model="edit_hotel_category" class="form-control package_category">
                                                                                                                    <option value="">Select Category</option>
                                                                                                                    <option value="Standard">Standard</option>
                                                                                                                    <option value="Delux">Delux</option>
                                                                                                                    <option value="Luxuary">Luxuary</option>
                                                                                                                    <option value="Premium">Premium</option>
                                                                                                           </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-md-4">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label">Price:</label>
                                                                                                            <input type="text" id="edit_package_hotel_detail_price<%key%><%k%>" 
                                                                                                                   ng-model="edit_hotel_price"
                                                                                                                   name="hotel_price" class="form-control input-sm" 
                                                                                                                  placeholder="Price..."/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label">Start Date:</label>
                                                                                                            <input type="text" id="edit_package_hotel_detail_start_date<%key%><%k%>" 
                                                                                                                   ng-model="edit_hotel_start_date" name="start_date" class="form-control input-sm" 
                                                                                                                  placeholder="Start Date..." value="v.start_date" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label">End Date:</label>
                                                                                                            <input type="text" id="edit_package_hotel_detail_end_date<%key%><%k%>" ng-model="edit_hotel_end_date" name="end_date" class="form-control input-sm" 
                                                                                                                  placeholder="End Date..." value="<% v.end_date %>"/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>    
                                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                                <button type="submit" ng-click="edit_hotel_detail_form($event,key,k);" class="btn btn-primary" data-dismiss="modal">Submit</button>
                                                                                            </form>
                                                                                        </div>
                                                                                  </div>
                                                                                </div>
                                                                              </div>
                                                                            </div>                        

                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                              
                                                                <div id="package_hotel_detail_add">
                                                                    
                                                                   <input type="hidden" id="hotel_detail_destination_id<%key%>" name="package_hotel_detail_destination_id" ng-model="package_hotel_detail_destination_id" value="<% value.data.destination_id %>">
                                                                   
                                                                   <input type="hidden" id="hotel_detail_itinerary_id<%key%>" name="package_hotel_detail_itinerary_id" ng-model="package_hotel_detail_itinerary_id" value="<% value.data.id %>">
                                                                   
                                                                   <div class="row">
                                                                       <div class="col-md-2">
                                                                            <div class="form-group" id="hotels_input" >
                                                                               <input id="hotel_title" modal
                                                                                      type="text" 
                                                                                      placeholder="Hotels.." 
                                                                                      class="form-control input-sm">
                                                                                <input type="hidden" id="hotel_id<%key%>" name="package_hotel_detail_hotel" ng-model="package_hotel_detail_hotel">
                                                                                <input type="hidden" id="hotel_name<%key%>" name="package_hotel_detail_hotel_title" ng-model="package_hotel_detail_hotel">
                                                                            </div>
                                                                       </div>
                                                                       <div class="col-md-2">
                                                                            <select name="package_hotel_detail_category" 
                                                                                    id="package_hotel_detail_category<%key%>"
                                                                                    ng-model="package_hotel_detail_category" class="form-control package_category">
                                                                                    <option value="">Select Category</option>
                                                                                    <option value="Standard">Standard</option>
                                                                                    <option value="Delux">Delux</option>
                                                                                    <option value="Luxuary">Luxuary</option>
                                                                                    <option value="Premium">Premium</option>
                                                                           </select>
                                                                       </div>

                                                                       <div class="col-md-2">
                                                                            <input type="text" start-date name="package_hotel_detail_startdate" ng-model="package_hotel_detail_startdate"
                                                                                   id="start_date<%key%>" class="form-control input-sm" placeholder="Start Date.."/>
                                                                       </div>

                                                                       <div class="col-md-2">
                                                                            <input type="text" end-date name="package_hotel_detail_enddate" ng-model="package_hotel_detail_enddate"
                                                                                   id="end_date<%key%>" class="form-control input-sm" placeholder="End Date..."/>
                                                                       </div>

                                                                       <div class="col-md-2">
                                                                            <input type="text" id="hotel_price<%key%>" name="package_hotel_detail_price" ng-model="package_hotel_detail_price" class="form-control input-sm" placeholder="Price..."/>
                                                                       </div>

                                                                       <div class="col-md-2">
                                                                            <button name="add" ng-click="submit_package_hotel_detail($event,key);" class="btn btn-primary btn-sm pull-right">Add</button>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </div>
                                                            
                                                <div class="row">
                                                    <div class="col-md-2 pull-right">
                                                        <div class="action">
                                                            <a href="javascript:;" ng-click="itinerary_modal_show($event,key,value.data)" class="edit-action" style="float:left">
                                                                <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                            </a>
                                                            <a href="javascript:;" ng-click="delete_itinerary($event,key,value.data)" class="delete-action">
                                                                <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                        
                                                 <!-- Itinerary Modal -->
                                                <div class="modal fade" id="Itinerarymodal<%key%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Edit Detail</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                          <div class="container-fluid itinerary-modal">
                                                                <form id="itinerary_form" action="">
                                                                    <input type="hidden" id="edit_itinerary_id<%key%>" name="id" ng-model="edit_itinerary_id" ng-value="<% value.data.id %>" >
                                                                    
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label">No. of Days :</label>
                                                                                <input type="text" id="edit_day_num<%key%>"  name="edit_itinerary_day_num" class="form-control input-sm" 
                                                                                       ng-model="edit_itinerary_day_num" value="<% value.data.day_num %>" 
                                                                                       placeholder="day_num..." />
                                                                            </div>
                                                                         </div>

                                                                         <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label">Destination :</label>
                                                                                <input type="text" edit-destination id="edit_destination_id" ng-model="destination_label" class="form-control input-sm" 
                                                                                       placeholder="Destination..." />
                                                                                <input type="hidden" name="edit_itinerary_destination_id" 
                                                                                       id="edit_itinerary_destination_id<%key%>" ng-model="edit_itinerary_destination_id">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label">Destination Description</label>
                                                                                <input type="text" id="edit_destination<%key%>" name="edit_itinerary_destination" class="form-control input-sm" ng-model="edit_itinerary_destination" value="<% value.data.destination %>"
                                                                                       placeholder="Destination Description.." />
                                                                            </div>
                                                                         </div>

                                                                         <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label">Image :</label>
                                                                                <input type="file" name="edit_itinerary_image" 
                                                                                       onchange="angular.element(this).scope().ItineraryImage(this.files)"
                                                                                       />
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group">
                                                                                <label class="control-label">Description:</label>
                                                                                <textarea name="edit_itinerary_desc" id="edit_desc<%key%>" class="form-control input-sm" ng-model="edit_itinerary_desc" placeholder="Description..." rows="4"><% value.data.desc %></textarea>
                                                                           </div>
                                                                       </div>
                                                                    </div>

                                                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                   <button type="submit" ng-click="edit_itinerary_form($event,key);"  class="btn btn-primary" data-dismiss="modal">Submit</button>
                                                                </form>
                                                            </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>         
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div id="itinerary_add">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" ng-model="itinerary_day_num" name="itinerary_day_num"
                                                       class="form-control input-sm" placeholder="No. of days..."/>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" id="destination_id" class="form-control input-sm" placeholder="Destination..."/>
                                                <input type="hidden" id="dest_id" name="itinerary_destination_id" ng-model="itinerary_destination_id">
                                            </div>  
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" name="itinerary_destination" ng-model="itinerary_destination" class="form-control input-sm" placeholder="Destination Desc..."/>
                                            </div> 
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="file" id="itinerary_image" name="itinerary_image"
                                                       onchange="angular.element(this).scope().ItineraryImage(this.files)"
                                                       ng-model="itinerary_image"/>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <textarea name="itinerary_desc" ng-model="itinerary_desc" class="form-control" placeholder="Description  ..." rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="margin-top: 60px;">
                                            <button name="add" ng-click="submit_itinerary($event);" class="btn btn-primary btn-sm pull-right">Add</button>        
                                        </div>
                                    </div>
                                </div>
                                  
                            </fieldset>   
                        </div>
                    </div>
        
                    <div class="login-footer">
                        @if(isset($package))
                            @if(isset($clone))
                        <button type="submit" name="clone" class="btn btn-primary btn-sm pull-right"><b>Save the Package</b></button>
                            @else
                        <button type="submit" name="edit" class="btn btn-primary btn-sm pull-right"><b>Save the Package</b></button>
                            @endif
                        @else
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-right"><b>Save the package</b></button>
                        @endif
                   </div>
        
            </div><!-- /.Container-fluid -->
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>

<!-- Inclusion Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Detail</h4>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
@stop

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
<style>
    .sortable { list-style-type: none; padding: 0; width: 100%; }
    .sortable li  { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1em; height: auto;}
    .sortable li span.glyphicon-move { position: absolute; margin-left: -1.3em;  padding-top:1px; cursor:default; color:#aaaaaa; font-weight:normal;}
    .sortable .form-group { margin-bottom:15px;}
  
    label.error -{
      color: #f56954;
  }
</style>
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js') }}

{{-- HTML::script('/js/setting.js') --}}

<script type="text/javascript">
    $(document).ready(function() {
        
    $('#inclusion_title').val('');
        
    $(this).find('.delete-action').hide();
        
    $(this).find('.edit-action').hide();
        
//        $(this).find('.delete-action-hotel').hide();
//        
//        $(this).find('.edit-action-hotel').hide();
        
    var count = $('.itinerary').length + 1;
    
    $('#itinerary_day_num').attr('value','Day ' + count);
        
    $('#package_type_id').bind('change keyup', function (e) {
            
            $package_id = $(this).val();
            e.preventDefault();
            $.ajax({
            type: 'get',
            url: '/dashboard/package-category/'+$package_id,
            success:function(data){
                if(data)
                {
                    $('#package_category_id').empty();
                    $('#package_category_id').append('<option value="">Please Select...</option>');
                    $.each(data, function(){

                        $('#package_category_id')
                        .append('<option value="'+this.id+'">'+this.title+'</option>');
                    });
                }
            }
            });
            
        });
    
    if ($('#package_type_id').val() == 2 ) {
            
            $package_id = $('#package_type_id').val();
            $selected = $('#package_category_id :selected').val();

            $.ajax({
            type: 'get',
            url: '/dashboard/package-category/'+$package_id,
            success:function(data){
                if(data)
                {
                    $.each(data, function(){
                        if($selected != this.id)
                        {
                            $('<option value="'+this.id+'">'+this.title+'</option>').appendTo('#package_category_id');
                        }
                    });
                }
            }
            });
        }
        
    if ($('#package_type_id').val() == 1 ) {
            
                $package_id = $('#package_type_id').val();
                $selected = $('#package_category_id :selected').val();
                $.ajax({
                type: 'get',
                url: '/dashboard/package-category/'+$package_id,
                success:function(data){
                    if(data)
                    {
                        $.each(data, function(){
                            if($selected != this.id)
                            {
                                $('<option value="'+this.id+'">'+this.title+'</option>').appendTo('#package_category_id');
                            }
                           
                        });
                    }
                }
                });
        }
              
    });
     
    $(function() {
 
        $("#topic_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                 $('<input type="hidden" value="'+this.value+'" name="destination_id" >').appendTo('#destination_input');
                
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
     
    $(function() {
 
        $("#destination_id").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                $('input[name="itinerary_destination_id"]').attr('value',this.value);
                
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }   
        });
    });
     
    $(function() {
        $( ".sortable" ).sortable({
            handle:'.sortable_handle',
            stop: function(event, ui) {
                //add input weight hidden value according to position.
                $('.inclusions .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.price_summaries .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.hotel_details .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.package_hotel_details .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.itineraries .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
            }
        });
    });
     
     function submit_inclusion()
     {
         $.ajax({
         url: '/dashboard/package/inclusion',
         type:'post',
         data: {'title':$('#inclusion_add').find('input[name="inclusion_title"]').val()},
         success : function (data){

             $('<li><div class="inclusion sortable_handle">'+
                '<div class="row">'+
                    '<div class="col-md-10"><span class="glyphicon glyphicon-move"></span>'+
                        '<div class="form-group">'+
                            '<input type="hidden" name="PackageInclusion['+$('.inclusions .inclusion').length+'][id]" value="'+data.id+'">'+
                            '<input class="weight" type="hidden" name="PackageInclusion['+$('.inclusions .inclusion').length+'][weight]" value="'+$('.inclusions .inclusion').length+'">'+
                            '<span class="text-title">'+data.title+'</span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-2">'+
                        '<div class="action">'+
                                '<a href="#" onclick="inclusion_modal_show(event,this); return false;"  data-id="'+data.id+'" data-title="'+data.title+'"  class="edit-action">'+
                                '<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span></a>'+
                                '<a href="#" onclick="delete_inclusion(event,this); return false;" data-id="'+data.id+'"  class="delete-action">'+
                                '<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
              '</div></li>').appendTo('.inclusions .sortable');
             
            }
       });
    }
     
     function inclusion_modal_show(event,element)
     {  
         event.preventDefault();
         
         $('.modal-body').html('<div class="container-fluid inclusion-modal">'+
                '<form id="inclusion_form" action="">'+
                '<input type="hidden" name="id" value="'+$(element).data('id')+'">'+
                '<div class="row">'+
                    '<div class="col-md-6">'+
                        '<div class="form-group">'+
                            '<label class="control-label">Inclusion Title :</label>'+
                            '<input type="text" name="title" autocomplete="off"  class="form-control input-sm" value="'+$(element).data('title')+'" required'+
                                   'placeholder="Inclusion Title..." />'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                '<button type="submit" onclick="edit_inclusion_form(); return false;" class="btn btn-primary" data-dismiss="modal">Submit</button>'+
                '</form>'+
            '</div>');
         
         $('#inclusion_form')[0].reset();
         
         $('#modal').modal({
            show: true
         });
     }
    
     function edit_inclusion_form()
     {
         var id = $('.inclusion-modal').find('input[name="id"]').val();
         var title = $('.inclusion-modal').find('input[name="title"]').val();
         
         $.ajax({
         url: "{{URL::to('dashboard/package-inclusion/edit')}}",
         type:'post',
         data: {'id':id,'title':title},
         success : function (data){
             $('input[value="'+data.id+'"]').siblings('span.text-title').text(data.title);
             if(data.package_id != null)
             {
                window.location.reload();
             }
        }
       });
     }
     
     function delete_inclusion(event,element)
     {  
         var id = $(element).data('id');

         $.ajax({
         url: "{{URL::to('dashboard/package-inclusion/delete')}}",
         type:'post',
         data: {'id':id},
         success : function (data){
                if(data)
                {   
                    $('input[value="'+data.id+'"]').closest('.inclusion').closest('li').remove();
                }
          }
       });
    }
     
     function submit_price_summary()
     {
        $.ajax({
         url: '/dashboard/package/price-summary',
         type:'post',
         data: {'desc' : $('#price_summary_add').find('input[name="price_summary_desc"]').val(),
                'cost' : $('#price_summary_add').find('input[name="price_summary_cost"]').val()
               },
            
         success : function (data){
             
             $('<li><div class="price_summary sortable_handle">'+
                        '<div class="row">'+
                            '<div class="col-md-5"><span class="glyphicon glyphicon-move"></span>'+
                                '<div class="form-group">'+
                                   '<input type="hidden" name="PackagePriceSummary['+$('.price_summaries .price_summary').length+'][id]" value="'+data.id+'">'+
                                   '<input class="weight" type="hidden" name="PackagePriceSummary['+$('.price_summaries .price_summary').length+'][weight]" value="'+$('.price_summaries .price_summary').length+'">'+
                                   '<span class="text-desc">'+data.desc+'</span>'+
                                '</div> '+   
                            '</div>'+
                            '<div class="col-md-5">'+
                                '<div class="form-group">'+
                                   '<span class="text-cost">'+data.cost+'</span>'+
                                '</div>'+   
                            '</div>'+
                            '<div class="col-md-2">'+
                                '<div class="action">'+
                                    '<a href="#" onclick="price_summary_modal_show(event,this)" data-id="'+data.id+'" data-desc="'+data.desc+'" data-cost="'+data.cost+'"  class="edit-action">'+
                                    '<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span></a>'+
                                    '<a href="#" onclick="delete_price_summary(event,this); return false;" data-id="'+data.id+'"  class="delete-action">'+
                                    '<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div></li>')
                .appendTo('.price_summaries .sortable');
             
      }
       });
         
     }
     
     function price_summary_modal_show(event,element)
     {
        event.preventDefault();
        
        $('.modal-body').html('<div class="container-fluid price-summary-modal">'+
                                '<form id="price_summary_form" action="">'+
                                '<input type="hidden" name="id" value="'+$(element).data('id')+'">'+
                                '<div class="row">'+
                                    '<div class="col-md-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Description :</label>'+
                                            '<input type="text" name="desc" class="form-control input-sm" value="'+$(element).data('desc')+'" required'+
                                                   'placeholder="Description..." />'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-md-6">'+
                                        '<div class="form-group">'+
                                            '<label class="control-label">Cost :</label>'+
                                            '<input type="text" name="cost" value="'+$(element).data('cost')+'" class="form-control input-sm" required'+
                                                   'placeholder="Cost..." />'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                                '<button type="submit" onclick="edit_price_summary_form(); return false;" class="btn btn-primary" data-dismiss="modal">Submit</button>'+
                              '</form>'+
                            '</div>'); 
         
         $('#price_summary_form')[0].reset();
         
         $('#modal').modal({
            show: true
         });
         
     }
     
     function edit_price_summary_form()
     {
         var id = $('.price-summary-modal').find('input[name="id"]').val();
         var desc = $('.price-summary-modal').find('input[name="desc"]').val();
         var cost = $('.price-summary-modal').find('input[name="cost"]').val();
         
         $.ajax({
         url: "{{URL::to('dashboard/package-pricesummary/edit')}}",
         type:'post',
         data: {'id':id,'desc':desc,'cost':cost},
         success : function (data){
            $('input[value="'+data.id+'"]').siblings('span.text-desc').text(data.desc);
            $('input[value="'+data.id+'"]').closest('.price_summary').find('span.text-cost').text(data.cost);
             if(data.package_id != null)
             {
                window.location.reload();
             }
            }
         });
     
     }
     
     function delete_price_summary(event,element)
     {
         event.preventDefault();
         
         var id = $(element).data('id');

         $.ajax({
         url: "{{URL::to('dashboard/package-pricesummary/delete')}}",
         type:'post',
         data: {'id':id},
         success : function (data){
                if(data)
                {
                    $('input[value="'+data.id+'"]').closest('.price_summary').closest('li').remove();
                }
          }
       });
    }
     
     function submit_hotel_detail()
     {
        $.ajax({
         url: '/dashboard/package/hotel-detail',
         type:'post',
         data: {'destination' : $('#hotel_detail_add').find('input[name="hotel_detail_destination"]').val(),
                'hotel' : $('#hotel_detail_add').find('input[name="hotel_detail_hotel"]').val(),
                'meal_plan' : $('#hotel_detail_add').find('input[name="hotel_detail_meal_plan"]').val()
               },
            
         success : function (data){
             
             $('<li><div class="hotel_detail sortable_handle">'+
                    '<div class="row">'+
                        '<div class="col-md-4">'+
                            '<div class="form-group"><span class="glyphicon glyphicon-move"></span>'+
                               '<input type="hidden" name="PackageHotelDetail['+$('.hotel_details .hotel_detail').length+'][id]" value="'+data.id+'">'+
                               '<input class="weight" type="hidden" name="PackageHotelDetail['+$('.hotel_details .hotel_detail').length+'][weight]" value="'+$('.hotel_details .hotel_detail').length+'">'+
                               '<span class="text-destination">'+data.destination+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                               '<span class="text-hotel">'+data.hotel+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="form-group">'+
                               '<span class="text-meal_plan">'+data.meal_plan+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="action">'+
                                '<a href="#" onclick="hotel_detail_modal_show(event,this)" data-id="'+data.id+'" data-destination="'+data.destination+'" data-hotel="'+data.hotel+'" data-meal_plan="'+data.meal_plan+'"  class="edit-action">'+
                                '<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span></a>'+
                                '<a href="#" onclick="delete_hotel_detail(event,this); return false;" data-id="'+data.id+'"  class="delete-action">'+
                                '<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div></li>')
                .appendTo('.hotel_details .sortable');
         }
            
        });
     }
     
     function hotel_detail_modal_show(event,element)
     {
        event.preventDefault();
         
        $('.modal-body').html('<div class="container-fluid hotel-detail-modal">'+
                '<form id="hotel_detail_form" action="">'+
                '<input type="hidden" name="id" value="'+$(element).data('id')+'" >'+
                    '<div class="row">'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Destination :</label>'+
                                '<input type="text" name="destination" class="form-control input-sm" required'+
                                       'placeholder="Destination..." value="'+$(element).data('destination')+'" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Hotel :</label>'+
                                '<input type="text" name="hotel" class="form-control input-sm" required'+
                                      'placeholder="Hotel..." value="'+$(element).data('hotel')+'" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Meal Plan:</label>'+
                                '<input type="text" name="meal_plan" class="form-control input-sm" required'+
                                      ' placeholder="Meal Plan..." value="'+$(element).data('meal_plan')+'"/>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                    '<button type="submit" onclick="edit_hotel_detail_form(); return false;" class="btn btn-primary" data-dismiss="modal">Submit</button>'+
                '</form>'+
            '</div>');
         
         $('#hotel_detail_form')[0].reset();
         
         $('#modal').modal({
            show: true
         });
         
     }
     
     function edit_hotel_detail_form()
     {
        var id = $('.hotel-detail-modal').find('input[name="id"]').val();
         var destination = $('.hotel-detail-modal').find('input[name="destination"]').val();
         var hotel = $('.hotel-detail-modal').find('input[name="hotel"]').val();
         var meal_plan = $('.hotel-detail-modal').find('input[name="meal_plan"]').val();
         
         $.ajax({
         url: "{{URL::to('dashboard/package-hoteldetail/edit')}}",
         type:'post',
         data: {'id':id,'destination':destination,'hotel':hotel,'meal_plan':meal_plan},
         success : function (data){
            $('input[value="'+data.id+'"]').siblings('span.text-destination').text(data.destination);
            $('input[value="'+data.id+'"]').closest('.hotel_detail').find('span.text-hotel').text(data.hotel);
            $('input[value="'+data.id+'"]').closest('.hotel_detail').find('span.text-meal_plan').text(data.meal_plan);
            if(data.package_id != null)
            {
                window.location.reload();
            }
         }
         });
     }
     
     function delete_hotel_detail(event,element)
     {
         event.preventDefault();
         
         var id = $(element).data('id');

         $.ajax({
         url: "{{URL::to('dashboard/package-hoteldetail/delete')}}",
         type:'post',
         data: {'id':id},
         success : function (data){
                if(data)
                {
                     $('input[value="'+data.id+'"]').closest('.hotel_detail').closest('li').remove();
                }
          }
       });
    }
     
     function submit_itinerary()
     {  
        var count = $('.itinerary').length + 1;
        $('#itinerary_day_num').attr('value','Day ' + count);
       
       var myFormData = new FormData();
       
       myFormData.append('day_num', $('#itinerary_add').find('input[name="itinerary_day_num"]').val());
       myFormData.append('destination', $('#itinerary_add').find('input[name="itinerary_destination"]').val());
       myFormData.append('desc' , $('#itinerary_add').find('textarea[name="itinerary_desc"]').val());
       myFormData.append('image', $('#itinerary_image').prop('files')[0]);
 
        $.ajax({
         url: '/dashboard/package/itinerary',
         type:'post',
         data: myFormData,
         processData: false,
         contentType: false,

         success : function (data){
             
             $('<li><div class="itinerary sortable_handle">'+
                    '<div class="row">'+
                        '<div class="col-md-6"><span class="glyphicon glyphicon-move"></span>'+
                            '<div class="form-group days">'+
                                '<input type="hidden" name="PackageItinerary['+$('.itineraries .itinerary').length+'][id]" value="'+data.id+'">'+
                                '<input class="weight" type="hidden" name="PackageItinerary['+$('.itineraries .itinerary').length+'][weight]" value="'+$('.itineraries .itinerary').length+'">'+
                                '<span class="text-day_num">'+data.day_num+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<div class="form-group">'+
                                '<span class="text-destination">'+data.destination+'</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row">'+
                        '<div class="col-md-10">'+
                            '<div class="form-group">'+
                              '<span class="text-desc">'+data.desc+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="action">'+
                                '<a href="#" onclick="itinerary_modal_show(event,this)" data-id="'+data.id+'" data-destination="'+data.destination+'" data-day_num="'+data.day_num+'" data-desc="'+data.desc+'"  class="edit-action">'+
                                '<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span></a>'+
                                '<a href="#" onclick="delete_itinerary(event,this); return false;" data-id="'+data.id+'"  class="delete-action">'+
                                '<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">'+
                    '<div class="panel panel-default">'+
                        '<div class="panel-heading" role="tab" id="headingOne">'+
                          '<h4 class="panel-title">'+
                            '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">'+
                              'Add Hotel Details'+
                            '</a>'+
                          '</h4>'+
                        '</div>'+
                        '<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">'+
                          '<div class="panel-body">'+
                                '<div id="package_hotel_detail_add">'+
                                   '<input type="hidden" name="package_hotel_detail_destination_id" value="'+data.destination_id+'">'+
                                   '<input type="hidden" name="package_hotel_detail_itinerary_id" value="'+data.id+'">'+
                                   '<div class="row">'+
                                       '<div class="col-md-2">'+
                                            '<div class="form-group" id="hotels_input" >'+
                                               '<input id="hotel_title"'+ 
                                                      'type="text" '+
                                                      'placeholder="Hotels.."'+ 
                                                      'class="form-control input-sm">'+
                                                '<input type="hidden" name="package_hotel_detail_hotel">'+
                                            '</div>'+
                                       '</div>'+
                                       '<div class="col-md-2">'+
                                            '<select name="package_hotel_detail_category" class="form-control package_category">'+
                                                '<option value="">Select Category</option>'+
                                                '<option value="Standard">Standard</option>'+
                                                '<option value="Delux">Delux</option>'+
                                                '<option value="Luxuary">Luxuary</option>'+
                                                '<option value="Premium">Premium</option>'+
                                           '</select>'+
                                       '</div>'+
                                       
                                       '<div class="col-md-2">'+
                                            '<input type="text" name="package_hotel_detail_startdate" id="start_date" class="form-control input-sm" placeholder="Start Date.."/>'+
                                       '</div>'+
 
                                       '<div class="col-md-2">'+
                                            '<input type="text" name="package_hotel_detail_enddate" id="end_date" class="form-control input-sm" placeholder="End Date..."/>'+
                                       '</div>'+
                                       
                                       '<div class="col-md-2">'+
                                            '<input type="text" name="package_hotel_detail_price" class="form-control input-sm" placeholder="Price..."/>'+
                                       '</div>'+

                                       '<div class="col-md-2">'+
                                            '<button name="add" onclick="submit_package_hotel_detail(); return false;" class="btn btn-primary btn-sm pull-right">Add</button>'+
                                       '</div>'+
                                   '</div>'+
                               '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                '</div>'+
            '</li>')
              .appendTo('.itineraries .sortable');
            
             var count = $('.itinerary').length + 1;
             $('#itinerary_day_num').attr('value','Day ' + count);
             
            $('#start_date').daterangepicker({ 
                singleDatePicker: true,
                format: 'YYYY-MM-DD',
                showTodayButton:true
            });

            $('#end_date').daterangepicker({ 
                singleDatePicker: true,
                format: 'YYYY-MM-DD',
                showTodayButton:true
            }); 
         }
        });
     }
     
     function itinerary_modal_show(event,element)
     {
        event.preventDefault();
         
        $('.modal-body').html('<div class="container-fluid itinerary-modal">'+
                '<form id="itinerary_form" action="">'+
                    '<input type="hidden" name="id" value="'+$(element).data('id')+'" >'+
                    '<div class="row">'+
                        '<div class="col-md-6">'+
                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">No. of Days :</label>'+
                                        '<input type="text" name="day_num" class="form-control input-sm" value="'+$(element).data('day_num')+'" required'+
                                               'placeholder="day_num..." />'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                        '<label class="control-label">Destination :</label>'+
                                        '<input type="text" name="destination" class="form-control input-sm" value="'+$(element).data('destination')+'" required'+
                                              ' placeholder="Destination..." />'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Description:</label>'+
                                '<textarea name="desc" class="form-control input-sm" value="" placeholder="Description..." rows="4">'+$(element).data('desc')+'</textarea>'+
                           ' </div>'+
                       ' </div>'+
                    '</div>'+
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                    '<button type="submit" onclick="edit_itinerary_form(); return false;" class="btn btn-primary" data-dismiss="modal">Submit</button>'+
                '</form>'+
            '</div>');
         
         
        $('#itinerary_form')[0].reset();
         
         $('#modal').modal({
            show: true
         }); 
     }
     
     function edit_itinerary_form()
     {
         var id = $('.itinerary-modal').find('input[name="id"]').val();
         var day_num = $('.itinerary-modal').find('input[name="day_num"]').val();
         var destination = $('.itinerary-modal').find('input[name="destination"]').val();
         var desc = $('.itinerary-modal').find('textarea[name="desc"]').val();
         
         $.ajax({
         url: "{{URL::to('dashboard/package-itinerary/edit')}}",
         type:'post',
         data: {'id':id,'day_num':day_num,'destination':destination,'desc':desc},
         success : function (data){
            $('input[value="'+data.id+'"]').siblings('span.text-day_num').text(data.day_num);
            $('input[value="'+data.id+'"]').closest('.itinerary').find('span.text-destination').text(data.destination);
            $('input[value="'+data.id+'"]').closest('.itinerary').find('span.text-desc').text(data.desc);
            
            if(data.package_id != null)
            {
                window.location.reload();
            }
         }
         });
     }
     
     function delete_itinerary(event,element)
     {
        var id = $(element).data('id');
            
         $.ajax({
         url: "{{URL::to('dashboard/package-itinerary/delete')}}",
         type:'post',
         data: {'id':id},
         success : function (data){
                if(data)
                {
                    $('input[value="'+data.id+'"]').closest('.itinerary').closest('li').remove();
                  
                    var count = $('.itinerary').length + 1;
                    $('#itinerary_day_num').attr('value','Day ' + count);
                }
          }
       });
     }
     
     function submit_package_hotel_detail(event,element)
     {
         $.ajax({
         url: '/dashboard/package/new-hotel-detail',
         type:'post',
         data: {'hotel' : $('#package_hotel_detail_add').find('input[name="package_hotel_detail_hotel"]').val(),
                'category' : $('#package_hotel_detail_add').find('select.package_category').val(),
                'startdate' : $('#package_hotel_detail_add').find('input[name="package_hotel_detail_startdate"]').val(),
                'enddate' : $('#package_hotel_detail_add').find('input[name="package_hotel_detail_enddate"]').val(),
                'price' : $('#package_hotel_detail_add').find('input[name="package_hotel_detail_price"]').val(),
                'destination_id' : $('#package_hotel_detail_add').find('input[name="package_hotel_detail_destination_id"]').val(),
                'package_itinerary_id' : $('#package_hotel_detail_add').find('input[name="package_hotel_detail_itinerary_id"]').val(),
               },
            
         success : function (data){
             
//             $('#package_hotel_detail_add').find('input[name="package_hotel_detail_hotel"]').attr('value','');
             
             $('<li><div class="package_hotel_detail sortable_handle">'+
                    '<div class="row">'+
                        '<div class="col-md-2">'+
                            '<div class="form-group"><span class="glyphicon glyphicon-move"></span>'+
                               '<input type="hidden" name="NewPackageHotelDetail['+$('.package_hotel_details .package_hotel_detail').length+'][id]" value="'+data.id+'">'+
                               '<input class="weight" type="hidden" name="NewPackageHotelDetail['+$('.package_hotel_details .package_hotel_detail').length+'][weight]" value="'+$('.package_hotel_details .package_hotel_detail').length+'">'+
                               '<span class="text-hotel">'+data.hotel+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="form-group">'+
                               '<span class="text-category">'+data.category+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="form-group">'+
                               '<span class="text-start_date">'+data.start_date+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="form-group">'+
                               '<span class="text-end_date">'+data.end_date+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="form-group">'+
                               '<span class="text-price">'+data.price+'</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<div class="action">'+
                                '<a href="#" onclick="package_hotel_detail_modal_show(event,this)" data-id="'+data.id+'" data-category="'+data.category+'" data-startdate="'+data.start_date+'" data-enddate="'+data.end_date+'" data-hotel="'+data.hotel+'" data-price="'+data.price+'"  class="edit-action">'+
                                '<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span></a>'+
                                '<a href="#" onclick="package_delete_hotel_detail(event,this); return false;" data-id="'+data.id+'"  class="delete-action">'+
                                '<span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div></li>')
                .appendTo('.itinerary .sortable_handle');
         }
            
        }); 
     }
     
     function package_hotel_detail_modal_show(event,element)
     {
        event.preventDefault();
         
        $('.modal-body').html('<div class="container-fluid package-hotel-detail-modal">'+
                '<form id="package_hotel_detail_form" action="">'+
                '<input type="hidden" name="id" value="'+$(element).data('id')+'" >'+
                    '<div class="row">'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Hotel :</label>'+
                                '<input type="text" name="hotel" class="form-control input-sm" required'+
                                       'placeholder="Hotel..." value="'+$(element).data('hotel')+'" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Category :</label>'+
                                '<input type="text" name="category" class="form-control input-sm" required'+
                                      'placeholder="Cateogry..." value="'+$(element).data('category')+'" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Price :</label>'+
                                '<input type="text" name="price" class="form-control input-sm" required'+
                                      ' placeholder="Price..." value="'+$(element).data('price')+'"/>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row">'+
                         '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">Start Date:</label>'+
                                '<input type="text" name="start_date" class="form-control input-sm" required'+
                                      ' placeholder="Start Date..." value="'+$(element).data('startdate')+'"/>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-4">'+
                            '<div class="form-group">'+
                                '<label class="control-label">End Date:</label>'+
                                '<input type="text" name="end_date" class="form-control input-sm" required'+
                                      ' placeholder="End Date..." value="'+$(element).data('enddate')+'"/>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+         
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
                    '<button type="submit" onclick="package_edit_hotel_detail_form(); return false;" class="btn btn-primary" data-dismiss="modal">Submit</button>'+
                '</form>'+
            '</div>');
         
         $('#package_hotel_detail_form')[0].reset();
         
         $('#modal').modal({
            show: true
         });
         
     }
     
     function package_edit_hotel_detail_form()
     {
         var id = $('.package-hotel-detail-modal').find('input[name="id"]').val();
         var category = $('.package-hotel-detail-modal').find('input[name="category"]').val();
         var hotel = $('.package-hotel-detail-modal').find('input[name="hotel"]').val();
         var start_date = $('.package-hotel-detail-modal').find('input[name="start_date"]').val();
         var end_date = $('.package-hotel-detail-modal').find('input[name="end_date"]').val();
         var price = $('.package-hotel-detail-modal').find('input[name="price"]').val();
         
         $.ajax({
         url: "{{URL::to('dashboard/package/new-hotel-detail/edit')}}",
         type:'post',
         data: {'id':id,'category':category,'hotel':hotel,'start_date':start_date,'end_date':end_date,'price':price},
         success : function (data){
            $('input[value="'+data.id+'"]').siblings('span.text-hotel').text(data.hotel);
            $('input[value="'+data.id+'"]').closest('.package_hotel_detail').find('span.text-category').text(data.category);
            $('input[value="'+data.id+'"]').closest('.package_hotel_detail').find('span.text-start_date').text(data.start_date);
            $('input[value="'+data.id+'"]').closest('.package_hotel_detail').find('span.text-end_date').text(data.end_date);
            $('input[value="'+data.id+'"]').closest('.package_hotel_detail').find('span.text-price').text(data.price);
            if(data.package_id != null)
            {
                window.location.reload();
            }
         }
         });
     }
     
     function package_delete_hotel_detail(event,element)
     {
         event.preventDefault();
         
         var id = $(element).data('id');

         $.ajax({
         url: "{{URL::to('dashboard/package-hoteldetail/delete')}}",
         type:'post',
         data: {'id':id},
         success : function (data){
                if(data)
                {
                     $('input[value="'+data.id+'"]').closest('.package_hotel_detail').closest('li').remove();
                }
          }
       });
    }
     
     
     $('.price_summary').hover(function() {
        $(this).find('.edit-action').show();
        $(this).find('.delete-action').show();
        },
        function () {
            $(this).find('.edit-action').hide();
            $(this).find('.delete-action').hide();
        }
    );
    $('.package_itinerary_wrapper').hover(function() {
        $(this).find('.edit-action').show();
        $(this).find('.delete-action').show();
        },
        function () {
             $(this).find('.edit-action').hide();
             $(this).find('.delete-action').hide();
        }
    );
     
    $('.hotel_detail').hover(function() {
        $(this).find('.edit-action').show();
        $(this).find('.delete-action').show();
        },
        function () {
            $(this).find('.edit-action').hide();
            $(this).find('.delete-action').hide();
        }
    );
    $('li.package_hotel_detail').hover(function() {
        $(this).find('.edit-action-hotel').show();
        $(this).find('.delete-action-hotel').show();
        },
        function () {
            $(this).find('.edit-action-hotel').hide();
            $(this).find('.delete-action-hotel').hide();
        }
    );
    $('.inclusion').hover(function() {
        $(this).find('.edit-action').show();
        $(this).find('.delete-action').show();
        },
        function () {
            $(this).find('.edit-action').hide();
            $(this).find('.delete-action').hide();
        }
    );
    
</script>

<script>
  app
  .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
  }).directive('modal', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
               $(function() {
                    $(element).autocomplete({
                        source: '{{URL::to("hotels.json")}}',
                        minLength: 1,
                        select: function(event, ui) {
                           // Prevent value from being put in the input:
                            this.value = ui.item.id;
                            this.title = ui.item.value;
                            $('input[name="package_hotel_detail_hotel"]').attr('value',this.value);
                            $('input[name="package_hotel_detail_hotel_title"]').attr('value',this.title);
                        },

                        html: true, // optional (jquery.ui.autocomplete.html.js required)

                        // optional (if other layers overlap autocomplete list)
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("z-index", 1000);
                        }
                    });
                });
               
           }
      };
  }).directive('editHotel', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
               $(function() {
                    $(element).autocomplete({
                        source: '{{URL::to("hotels.json")}}',
                        minLength: 1,
                        select: function(event, ui) {
                           // Prevent value from being put in the input:
//                            this.value = ui.item.id;
//                            this.title = ui.item.value;
                            $(this).siblings('input[name="edit_hotel_id"]').first().attr('value', ui.item.id);
                            $(this).siblings('input[name="edit_hotel_title"]').first().attr('value', ui.item.value);
                        },

                        html: true, // optional (jquery.ui.autocomplete.html.js required)

                        // optional (if other layers overlap autocomplete list)
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("z-index", 1055);
                        }
                    });
                });
               
           }
      };
  }).directive('editDestination', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
                 $(function() {
 
                    $(element).autocomplete({
                        source: '{{URL::to("destinations.json")}}',
                        minLength: 1,
                        select: function(event, ui) {
                           // Prevent value from being put in the input:
                                
                            $(this).siblings('input[name="edit_itinerary_destination_id"]').first().attr('value', ui.item.id);

                        },

                        html: true, // optional (jquery.ui.autocomplete.html.js required)

                        // optional (if other layers overlap autocomplete list)
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("z-index", 1055);
                        }
                    });
                });
           }
      };
  }).directive('startDate', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
                $(element).daterangepicker({ 
                    singleDatePicker: true,
                    format: 'YYYY-MM-DD',
                    showTodayButton:true
                });               
           }
      };
  }).directive('endDate', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
                $(element).daterangepicker({ 
                    singleDatePicker: true,
                    format: 'YYYY-MM-DD',
                    showTodayButton:true
                });
           }
      };
  }).directive('editModal', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
                $(element).modal('hide');
           }
      };
  })
  .controller('ItinerariesController', function($scope,$http,$rootScope,$element){  
      
  $scope.itineraries = JSON.parse(decodeURIComponent('{{ rawurlencode($itineraries) }}'));
      
  $scope.arrayObjectIndexOf = function(arr, obj){
      for(var i = 0; i < arr.length; i++){
          if(angular.equals(arr[i]['data'], obj)){
              return i;
          }
      };
      return -1;
  }      
      
  $scope.count = $scope.itineraries.length + 1;   
      
  $scope.itinerary_day_num = 'Day '+ $scope.count;  
      
  // Itineraries 
      
  $scope.ItineraryImage = function(files)
  {
      if (files != null) {
        $scope.image_file = files[0];
      }
  }   
      
  $scope.submit_itinerary = function(e)
  {
      var elem = angular.element( document.querySelector( '#dest_id' ) );
        
      $scope.itinerary_destination_id = elem.val();
      
      $http({
            url: '/dashboard/package/itinerary',
            method: "POST",
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: function () {
                var mydata = new FormData();
                //need to convert our json object to a string version of json otherwise
                // the browser will do a 'toString()' on the object which will result 
                // in the value '[Object object]' on the server.
                
                mydata.append("day_num",$scope.itinerary_day_num);
                mydata.append("destination_id",$scope.itinerary_destination_id);
                mydata.append("destination",$scope.itinerary_destination);
                mydata.append("desc",$scope.itinerary_desc);
                mydata.append("image", $scope.image_file);
                
                return mydata;
            },
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries.push({'data':data,'hotel':[]});
          
                $scope.count = $scope.itineraries.length + 1;
          
                var iti_hotel = angular.element( document.querySelector( '#PackageItineraryHotelDetails' ) );
      
                iti_hotel.attr('value',$scope.itineraries);    
            
                $scope.itinerary_day_num = 'Day '+ $scope.count; 
          
            }).error(function(data, status, headers, config) {
              $scope.status = status;
      });
      
      e.preventDefault();
  }
  
  $scope.getDestination = function(id)
  {
        $http({
            url: '/get/destination',
            method:'get',
            params: {'id':id},
            }).success(function(data, status, headers, config) 
            { 
                $scope.destination_label = data.label;        
            
            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });       
  
  }
  
  $scope.itinerary_modal_show = function(e,key,data)
  {
      $scope.getDestination(data.destination_id);
      
      $scope.edit_itinerary_id = data.id;
      
      $scope.edit_itinerary_day_num = data.day_num;
      
      $scope.edit_itinerary_desc = data.desc
      
      $scope.edit_itinerary_destination = data.destination;
      
      angular.element( document.querySelector( '#Itinerarymodal'+key ) ).modal({
            show: true
      });   
  }
  
  $scope.edit_itinerary_form = function(e,index)
  {
       var edit_itinerary_id = angular.element( document.querySelector( '#edit_itinerary_id'+index ) );
        
        $scope.edit_itinerary_id = edit_itinerary_id.val();
      
        var edit_day_num = angular.element( document.querySelector( '#edit_day_num'+index ) );
        
        $scope.edit_itinerary_day_num = edit_day_num.val(); 
      
        var edit_destination = angular.element( document.querySelector( '#edit_destination'+index ) );
        
        $scope.edit_itinerary_destination = edit_destination.val(); 
      
        var edit_desc = angular.element( document.querySelector( '#edit_desc'+index ) );
        
        $scope.edit_itinerary_desc = edit_desc.val(); 
      
        var edit_itinerary_destination_id = angular.element( document.querySelector( '#edit_itinerary_destination_id'+index ) );
        
        $scope.edit_itinerary_destination_id = edit_itinerary_destination_id.val(); 
      
      
        $http({
            url: '/dashboard/package-itinerary/edit',
            method:'POST',
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: function () {
                var mydata = new FormData();
                
                mydata.append("id",$scope.edit_itinerary_id);
                mydata.append("day_num",$scope.edit_itinerary_day_num);
                mydata.append("destination_id",$scope.edit_itinerary_destination_id);
                mydata.append("destination",$scope.edit_itinerary_destination);
                mydata.append("desc",$scope.edit_itinerary_desc);
                mydata.append("image", $scope.image_file);
                
                return mydata;
            },
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries[index].data = data;
            
                angular.element( document.querySelector( '#Itinerarymodal'+index ).modal('hide') );       
            
            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });   
      
      e.preventDefault();
       
  }
  
  $scope.delete_itinerary = function(e,index,data)
  {
      $http({
            url: '/dashboard/package-itinerary/delete',
            method: "POST",
            params:{
                'id':data.id,
            },
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries.splice(index, 1);
          
                $scope.count = $scope.itineraries.length + 1;   
      
                $scope.itinerary_day_num = 'Day '+ $scope.count;
          
            }).error(function(data, status, headers, config) {
              $scope.status = status;
      });
  }
  
  
  // Package Hotel Details
  
  $scope.submit_package_hotel_detail = function(e,key)
  {
      var hotel_detail_destination_id = angular.element( document.querySelector( '#hotel_detail_destination_id'+key ) );
        
      $scope.package_hotel_detail_destination_id = hotel_detail_destination_id.val(); 
      
      var hotel_detail_itinerary_id = angular.element( document.querySelector( '#hotel_detail_itinerary_id'+key ) );
        
      $scope.package_hotel_detail_itinerary_id = hotel_detail_itinerary_id.val();
      
      var package_hotel_detail_hotel_id = angular.element( document.querySelector( '#hotel_id'+key ) );
        
      $scope.package_hotel_detail_hotel_id = package_hotel_detail_hotel_id.val();
      
      var package_hotel_detail_hotel_name = angular.element( document.querySelector( '#hotel_name'+key ) );
        
      $scope.package_hotel_detail_hotel_name = package_hotel_detail_hotel_name.val();
      
      var package_hotel_detail_category = angular.element( document.querySelector( '#package_hotel_detail_category'+key ) );
        
      $scope.package_hotel_detail_category = package_hotel_detail_category.val();
      
      var package_hotel_detail_startdate = angular.element( document.querySelector( '#start_date'+key ) );
        
      $scope.package_hotel_detail_startdate = package_hotel_detail_startdate.val();
      
       var package_hotel_detail_enddate = angular.element( document.querySelector( '#end_date'+key ) );
        
      $scope.package_hotel_detail_enddate = package_hotel_detail_enddate.val();
      
      var package_hotel_detail_price = angular.element( document.querySelector( '#hotel_price'+key ) );
        
      $scope.package_hotel_detail_price = package_hotel_detail_price.val();
      
      $http({
           url: '/dashboard/package/new-hotel-detail',
            method: "POST",
            params:{
                'destination_id' : $scope.package_hotel_detail_destination_id,
                'package_itinerary_id' : $scope.package_hotel_detail_itinerary_id,
                'hotel_id' : $scope.package_hotel_detail_hotel_id,
                'hotel' : $scope.package_hotel_detail_hotel_name,
                'category' : $scope.package_hotel_detail_category,
                'startdate' : $scope.package_hotel_detail_startdate,
                'enddate' : $scope.package_hotel_detail_enddate,
                'price' : $scope.package_hotel_detail_price
            }
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries[key].hotel.push(data);
          
            }).error(function(data, status, headers, config) {
              $scope.status = status;
      });
      e.preventDefault();
  } 
  
  $scope.getHotel = function(id)
  {
        $http({
            url: '/get/hotel',
            method:'get',
            params: {'id':id},
            }).success(function(data, status, headers, config) 
            { 
                $scope.edit_hotel_label = data.title; 
            
            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });
  }
  
  $scope.package_hotel_detail_modal_show = function(e,index,data,itinerary_key)
  {
      $scope.getHotel(data.hotel_id);
      
      $scope.edit_hotel_detail_id = data.id;
      
      $scope.edit_hotel_category = data.category;
      
      $scope.edit_hotel_start_date = data.start_date
      
      $scope.edit_hotel_end_date = data.end_date;
      
      $scope.edit_hotel_price = data.price;
      
      angular.element( document.querySelector( '#HotelDetailmodal'+itinerary_key+index ) ).modal({
            show: true
      }); 
        
  }
  
  $scope.edit_hotel_detail_form = function(e,itinerary_key,hotel_key)
  {
        var category = angular.element( document.querySelector( '#edit_package_hotel_detail_category'+itinerary_key+hotel_key ) );
        
        $scope.edit_hotel_category = category.val();
      
        var start_date = angular.element( document.querySelector( '#edit_package_hotel_detail_start_date'+itinerary_key+hotel_key ) );
        
        $scope.edit_hotel_start_date = start_date.val();
      
        var end_date = angular.element( document.querySelector( '#edit_package_hotel_detail_end_date'+itinerary_key+hotel_key ) );
        
        $scope.edit_hotel_end_date = end_date.val();
      
        var price = angular.element( document.querySelector( '#edit_package_hotel_detail_price'+itinerary_key+hotel_key ) );
        
        $scope.edit_hotel_price = price.val();
      
       var hotel_id = angular.element( document.querySelector( '#edit_package_hotel_id'+itinerary_key+hotel_key ) );
      
       var hotel = angular.element( document.querySelector( '#edit_package_hotel_title'+itinerary_key+hotel_key ) );

        $http({
            url: '/dashboard/package/new-hotel-detail/edit',
            method:'POST',
            params: {'id':$scope.edit_hotel_detail_id,
                    'category' : $scope.edit_hotel_category,
                    'start_date':$scope.edit_hotel_start_date,
                    'end_date':$scope.edit_hotel_end_date,
                    'price':$scope.edit_hotel_price,
                    'hotel_id':hotel_id.val(),
                    'hotel':hotel.val() 
               },
            }).success(function(data, status, headers, config) 
            { 
                 $scope.itineraries[itinerary_key].hotel[hotel_key] = data;
                 
                 if(data.package_id != null)
                 {
                    window.location.reload();
                 }
            
//               angular.element( document.querySelector( '#HotelDetailmodal'+itinerary_key+hotel_key ).modal('hide') );
                 angular.element( document.querySelector( '#HotelDetailmodal'+itinerary_key+hotel_key ) ).modal({
                        hide: true
                 });   

            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });   
      
      e.preventDefault();
  
  }
  
  $scope.package_delete_hotel_detail = function(e,index,k,data)
  {
        $http({
            url: '/dashboard/package-hoteldetail/delete',
            method: "POST",
            params:{
                'id':data.id,
            },
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries[index].hotel.splice(k, 1);
          
            }).error(function(data, status, headers, config) {
              $scope.status = status;
      }); 
    
  }
  
  });    
</script>  
@stop
