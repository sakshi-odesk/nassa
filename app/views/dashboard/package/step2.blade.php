@extends('layout.dashboard')

@section('content')
<section id="main-content">
    <section class="wrapper site-min-height">
         <h3><i class="fa fa-angle-right"></i>Add New Package</h3>
        
        <div class="row mt">
            {{ Form::open(array('action' => array('DashboardController@postPackageStep2'),'files'=> true, 'role' => 'form','id'=>'package_form'))}}           
            
            <input type="hidden" name="id" @if(isset($package)) value="{{$package['id']}}" @endif>
            
             <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Image :</label>
                                @if(isset($package))
                                    @if(isset($clone))
                                        <input type="file" name="main_image"></input><br/>
                                    @else
                                        <input type="file" name="main_image"></input><br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                @if(!is_null($package->main_image))
                                                <div class="thumbnail">
                                                    <img class="img-responsive" src='{{asset($package->main_image)}}' height="100px"></img>
                                                </div>
                                                @endif
                                            </div>
                                        </div>    
                                    @endif
                                @else
                                    <input type="file" name="main_image"></input>
                                @endif
                            </div>
                        </div>
                    </div>

                    <?php 
                            if(isset($package))
                            {
                                $package_image = PackageImage::where('package_id','=',$package->id)->get();
                            }
                    ?>
                                  
                    <div class="row">
              
                        <div class="col-md-4">
                            <div class="form-group">
                                  <label class="control-label">Slide 1 :</label>
                                   @if(isset($package))
                                      @if(isset($clone))
                                       <input type="file" name="PackageImage[]" ></input><br/>
                                      @else
                                       <input type="file" name="PackageImage[]" ></input><br/>

                                        <div class="row">
                                          <div class="col-md-6">
                                        @if(isset($package_image[0]))
                                           @if(!is_null($package_image[0]->image))
                                              <div class="thumbnail">
                                                  <img class="img-responsive" src='{{ asset( $package_image[0]->image ) }}' height="100px">
                                                </div>
                                           @endif
                                        @endif
                                          </div> 
                                        </div>
                                        @endif
                                   @else
                                        <input type="file" name="PackageImage[]" >
                                   @endif
                            </div> 
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                  <label class="control-label">Slide 2 :</label>
                                   @if(isset($package))
                                      @if(isset($clone))
                                       <input type="file" name="PackageImage[]" ></input><br/>
                                      @else
                                       <input type="file" name="PackageImage[]" ></input><br/>

                                        <div class="row">
                                          <div class="col-md-6">
                                        @if(isset($package_image[1]))
                                           @if(!is_null($package_image[1]->image))
                                              <div class="thumbnail">
                                                  <img class="img-responsive" src='{{ asset( $package_image[1]->image ) }}' height="100px">
                                                </div>
                                           @endif
                                        @endif
                                          </div> 
                                        </div>
                                        @endif
                                   @else
                                        <input type="file" name="PackageImage[]" >
                                   @endif
                            </div> 
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                  <label class="control-label">Slide 3 :</label>
                                   @if(isset($package))
                                      @if(isset($clone))
                                       <input type="file" name="PackageImage[]" ></input><br/>
                                      @else
                                       <input type="file" name="PackageImage[]" ></input><br/>

                                        <div class="row">
                                          <div class="col-md-6">
                                        @if(isset($package_image[2]))
                                           @if(!is_null($package_image[2]->image))
                                              <div class="thumbnail">
                                                  <img class="img-responsive" src='{{ asset( $package_image[2]->image ) }}' height="100px">
                                                </div>
                                           @endif
                                        @endif
                                          </div> 
                                        </div>
                                        @endif
                                   @else
                                        <input type="file" name="PackageImage[]" >
                                   @endif
                            </div> 
                        </div>

                    </div>


                    <div class="login-footer">
                        <a href="{{ URL::route('dashboard.package.edit',array($package->id)) }}" class="btn btn-primary btn-sm"><b>Back</b></a>
                       <button type="submit" name="save" class="btn btn-primary btn-sm pull-right"><b>Next</b></button>
                   </div>
        
            </div><!-- /.Container-fluid -->
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>                  
@stop                        