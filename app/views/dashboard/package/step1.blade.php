@extends('layout.dashboard')

@section('content')
<section id="main-content" ng-app="Packages">
    <section class="wrapper site-min-height">
         <h3><i class="fa fa-angle-right"></i>Add New Package</h3>
        
        <div class="row mt">
            @if(isset($package))
                @if(isset($clone))
                    {{ Form::open(array('action' => array('DashboardController@postPackageCloning'),'files'=>true, 'role' => 'form'))}}
                @else
                    {{ Form::open(array('action' => array('DashboardController@postPackageEdit'),'files'=>true, 'role' => 'form'))}}
                @endif
            @else
            {{ Form::open(array('action' => array('DashboardController@postPackageAdd'),'files'=> true, 'role' => 'form','id'=>'package_form'))}}           
            @endif
            <input type="hidden" name="id" @if((isset($package)) && (!isset($clone))) value="{{$package['id']}}" @endif>
             <div class="container-fluid">
                 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                   <div class="form-group">
                                        <label class="control-label">Menu :</label>
                                        <select id="package_type_id" class="form-control input-sm" name="package_type_id" required>
                                            <option value="">Please Select...</option>
                                            @if(!isset($package))
                                                @if(isset($package_types))
                                                    @foreach($package_types as $type)
                                                        <option value="{{ $type->id }}">
                                                        {{ $type->title }}</option>
                                                    @endforeach
                                                @endif
                                            @else
                                                @if(isset($package_types))
                                                    @foreach($package_types as $type)
                                                        <option value="{{ $type->id }}" @if(isset($package) && ($package['package_type_id'] == $type->id)) {{'selected'}} @endif>
                                                        {{ $type->title }}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Title :</label>
                                        <input type="text" name="title" class="form-control input-sm" required
                                               placeholder="Package Title..." {{isset($package) ? 'value="'.$package['title']. '"' : ''}} />
                                    </div>
                                </div> 
                                
                                <div class="col-md-3">
                                   <div class="form-group">
                                        <label  class="control-label">Duration :</label>
                                        <input type="text" name="duration" class="form-control input-sm" required
                                               placeholder="Package Duration..." {{isset($package) ? 'value="'.$package['duration']. '"' : ''}} />
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                     <div class="form-group">
                                        <label class="control-label">Route Map :</label>
                                        <input type="text" name="route_map" class="form-control input-sm" required
                                               placeholder="Package Route Map..." {{isset($package) ? 'value="'.$package['route_map']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>
                            
                             <?php 
                                  if(isset($package))
                                  {  
                                        $package_facilities = $package->package_facilities()->getResults();  
                                        $arr = $package_facilities->fetch('facility_id');
                                        $arr = $arr->toArray();
                                  }  
                            ?>
                            
                            <div class="row">
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label class="control-label"> Description :</label>
                                       <textarea name="desc" class="form-control" placeholder="Description  ..." rows="5">@if(isset($package)){{$package->desc}}@endif</textarea>
                                   </div>
                                 </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Facility :</label>
                                        <select id="PackageFacility" multiple="multiple" class="form-control input-sm" size="7" name="PackageFacility[]" required>
                                            @if(!isset($package))
                                                @if(isset($facilities))
                                                        @foreach($facilities as $facility)
                                                        <option value="{{ $facility->id}}">{{$facility->title}}</option>
                                                        @endforeach
                                                @endif
                                            @else
                                                @if(isset($facilities))
                                                    @foreach($facilities as $facility)
                                                    <option value="{{ $facility->id}}" @if(isset($package) && (in_array($facility->id,$arr))) {{'selected'}} @endif >
                                                        {{$facility->title}}</option>
                                                   @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                   <div class="form-group">
                                     <label class="control-label"> Term & Condition :</label>
                                       <textarea name="terms_conditions" id="tc" class="form-control" placeholder="Term & Condition..." rows="5">@if(isset($package)){{$package->terms_conditions}}@endif</textarea>
                                   </div>
                                </div>
                                <div class="col-md-6">
                                   <div class="form-group">
                                     <label class="control-label"> Policy :</label>
                                       <textarea name="policy" id="policy" class="form-control" placeholder="Policy..." rows="5">@if(isset($package)){{$package->policy}}@endif</textarea>
                                   </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                   <div class="form-group">
                                     <label class="control-label"> Faq :</label>
                                       <textarea name="faq" id="faq" class="form-control" placeholder="Faq..." rows="5">@if(isset($package)){{$package->faq}}@endif</textarea>
                                   </div>
                                </div>
                                 
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='special_offer' type="checkbox" value="1" > Special Offer
                                                @else
                                                    @if($package['special_offer'] == 0)
                                                    <input  name='special_offer' type="checkbox" value="1" > Special Offer
                                                    @else
                                                    <input  name='special_offer' type="checkbox" value="1" checked > Special Offer
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
      
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='hotpick' type="checkbox" value="1" > Hot Pick
                                                @else
                                                    @if($package['hotpick'] == 0)
                                                    <input  name='hotpick' type="checkbox" value="1" > Hot Pick
                                                    @else
                                                    <input  name='hotpick' type="checkbox" value="1" checked > Hot Pick
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>

      
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                @if(!isset($package))
                                                <input name='recommended' type="checkbox" value="1" > Recommended
                                                @else
                                                    @if($package['recommended'] == 0)
                                                    <input  name='recommended' type="checkbox" value="1" > Recommended
                                                    @else
                                                    <input  name='recommended' type="checkbox" value="1" checked > Recommended
                                                    @endif
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
      
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="seo_page_title">Seo Page Title :</label>
                                            <input type="text" 
                                               placeholder="Seo page title" 
                                               id="seo_page_title" 
                                               name='seo_page_title' 
                                               {{isset($package) ? 'value="'.$package['seo_page_title']. '"' : ''}} 
                                                   class="form-control input-sm">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_desc">Seo Meta Desc :</label>
                                        <textarea id="seo_meta_desc" name="seo_meta_desc" class="form-control input-sm"  placeholder="SEO meta description  ..." rows="3">{{isset($package) ? $package['seo_meta_desc']: ''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="seo_meta_kw">Seo Meta Keyword :</label>
                                        <textarea id="seo_meta_kw" name="seo_meta_kw" class="form-control input-sm"  placeholder="SEO meta Keyword..." rows="3">{{isset($package) ? $package['seo_meta_kw']: ''}}</textarea>
                                    </div>
                                </div>
                            </div>
                            </div>
        
                        </div>
                    </div>

                    <div class="login-footer">
                       <button type="submit" name="save" class="btn btn-primary btn-sm pull-right"><b>Next</b></button>
                   </div>
        
            </div><!-- /.Container-fluid -->
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>                  
@stop  

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/summernote/summernote.css')}}

@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.min.js')}} 
{{ HTML::script('theme/nassatravels/assets/js/summernote/summernote.js')}} 

<script>
$(document).ready(function(){
   
    $('#tc').summernote({ 
            height:200,
            onImageUpload:function(files,editor,welEditable){
                sendFile(files[0],editor,welEditable);
            }
    });
    
    $('#policy').summernote({ 
            height:200,
            onImageUpload:function(files,editor,welEditable){
                sendFile(files[0],editor,welEditable);
            }
    });

    $('#faq').summernote({ 
            height:200,
            onImageUpload:function(files,editor,welEditable){
                sendFile(files[0],editor,welEditable);
            }
    });

    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: '{{URL::to("summernote/upload")}}',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
            }
        });
    }
    
    
});
</script>
@stop