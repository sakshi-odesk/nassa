@extends('layout.dashboard')

@section('css')
<style>
    .sortable { list-style-type: none; padding: 0; width: 100%; }
    .sortable li  { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1em; height: auto;}
    .sortable li span.glyphicon-move { position: absolute; margin-left: -1.3em;  padding-top:1px; cursor:default; color:#aaaaaa; font-weight:normal;}
    .sortable .form-group { margin-bottom:15px;}
  
    label.error -{
      color: #f56954;
  }
</style>
@stop 

@section('content')
<section id="main-content" >
    <section class="wrapper site-min-height">
         <h3><i class="fa fa-angle-right"></i>Add New Package</h3>
        
        <div class="row mt" >
            {{ Form::open(array('action' => array('DashboardController@postPackageStep4'),'files'=> true, 'role' => 'form','id'=>'package_form'))}}           
            <input type="hidden" name="id" @if((isset($package)) && (!isset($clone))) value="{{$package['id']}}" @endif>
             <div class="container-fluid">
                    
                 <input type="hidden" name="destinations" ng-model="destinations" value="<% destinations %>">
                 
                <div class="row" >
                    <div class="col-md-12" ng-controller="HotelPriceController" ng-cloak>
                        
                        <input type="hidden" name="destinations" ng-model="destinations" value="<% destinations %>">
                        <input type="hidden" id="PackageItinerary" name="hotel_price" value="<% hotel_details %>">
                        
                         <fieldset>
                            <legend>Hotel Details</legend>
                            <div class="package_hotelprice_wrapper">
                                <div class="hotelPrice">
                                    <ul class="sortable">
                                        <li ng-repeat="(key,value) in hotel_details">
                                             <div class="hotel-details sortable_handle">
                                                <div class="row">
                                                    <div class="col-md-12"><span class="glyphicon glyphicon-move"></span>
                                                        <div class="form-group days">
                                                          
                                                            
                                                            
                                                            <span class="text-day_num"><% value.date_range %></span>
                                                              
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                <div class="well hotelprice_categories" ng-repeat="(k,v) in value.data">
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <span><% v.cat %></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <th>Destination</th>
                                                                        <th>Hotel 1</th>
                                                                        <th>Hotel 2</th>
                                                                    </thead>    
                                                                    <tbody> 
                                                                        <tr ng-repeat="dest in v.destinations">
                                                                            <td><span><% dest.id.label %></span></td>
                                                                            <td><span ><% dest.hotel1 %><span></td>
                                                                            <td><span><% dest.hotel2 %><span></td>
                                                                        </tr>
                                                                    </tbody>    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <span><% v.price_per_person %></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <span><% v.discounted_price %></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <span><% v.extra_adult %></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <span><% v.child_with_bed %></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <span><% v.infant %></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                            
                                                </div>
                                                            
                                                <div class="row">
                                                    <div class="col-md-2 pull-right">
                                                        <div class="action">
                                                            <a href="javascript:;" ng-click="delete_hotelprice($event,key)" class="delete-action">
                                                                <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                            
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div id="HotelPrice_add">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" ng-model="hotelprice_date" name="hotelprice_date"
                                                   class="form-control input-sm" placeholder="Add Date Range"/>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="well">
                                    <div class="row">
                                         <div class="col-md-12">
                                            <div class="form-group">
                                                <select class="form-control" name="hotelprice_category" ng-model="form.hotelprice_category">
                                                    <option ng-selected="<% hotel_categories %>" value="<% category.title %>" ng-repeat="category in hotel_categories"><% category.title %></option>
                                                </select>    
                                            </div> 
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <th>Destination</th>
                                                        <th>Hotel 1</th>
                                                        <th>Hotel 2</th>
                                                    </thead>    
                                                    <tbody> 
                                                        <tr ng-repeat="(key,value) in destinations">
                                                            <td><% value.label.label %></td>
                                                                
                                                            <td>
                                                                <input type="text" ng-model="form.destinations[value.destination_id].hotel1" 
                                                                       name="" id="hotel_title1" class="hotel_title1 form-control" placeholder="Please select Hotel">
                                                                
                                                                <input type="hidden" name="edit_hotel1" id="edit_hotel1_<% value.destination_id %>"
                                                                       class="hotel_title1 form-control">
                                                            </td>
                                                            
                                                            <td>
                                                                <input type="text" id="hotel_title2" ng-model="form.destinations[value.destination_id].hotel2" placeholder="Please select Hotel"
                                                                       class="hotel_title2 form-control">
                                                                
                                                                <input type="hidden" name="edit_hotel2" id="edit_hotel2_<% value.destination_id %>"
                                                                       class="hotel_title2 form-control">
                                                            </td>
                                                        </tr>
                                                    </tbody>    
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                  
<!--
                                     <div class="col-md-2">
                                      <div class="form-group" id="destination_input" >
                                        <label class="control-label">Destinations :</label>
                                          @if(isset($hotel))
                                            <input id="destination_title" type="hidden" name="destination_id" value="{{$destination->id}}" >
                                          @else
                                            <input id="destination_title" type="hidden" name="destination_id" value="" >
                                          @endif
                                            <input id="topic_title" 
                                                    type="text" 
                                                    placeholder="Search Destinations" 
                                                    class="form-control input-sm" 
                                                    onclick="change_val(); return false;"
                                                    {{isset($hotel) ? 'value="'.$destination['label']. '"' : ''}} >
                                        </div>
                                      </div>
-->
                                
                                    <div class="row">
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                               <input type="text" name="hotelprice_per_person" class="form-control" 
                                                      ng-model="form.hotelprice_per_person" placeholder="Per Person">
                                            </div> 
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                               <input type="text" name="hotelprice_discounted_price" class="form-control" 
                                                      ng-model="form.hotelprice_discounted_price" placeholder="Discounted Price">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                               <input type="text" name="hotelprice_extra_adult" class="form-control" 
                                                      ng-model="form.hotelprice_extra_adult" placeholder="Extra Adult">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                               <input type="text" name="hotelprice_child_with_bed" class="form-control" 
                                                      ng-model="form.hotelprice_child_with_bed" placeholder="Child With Bed">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                               <input type="text" name="hotelprice_infant" class="form-control" 
                                                      ng-model="form.hotelprice_infant" placeholder="Infant">
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button name="add" ng-click="submit($event);" class="btn btn-primary btn-sm pull-right">Add Categories</button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-offset-10 col-md-2">
                                            <div class="form-group">
                                                <button name="add" ng-click="add_date_range($event);" class="btn btn-primary btn-sm pull-right">Add Date Range</button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </fieldset>
                    </div>
                </div>
                     
                @if(Auth::user()->role == 'admin') 
                <div class="row">
                    <div class="col-md-12">
                          <div class="form-group">
                            <div class="checkbox">

                                <label>
                                    @if(!isset($package))
                                    <input name='published' type="checkbox" value="1"> Publish this Package
                                    @else
                                        @if(strtotime($package['published']) == 0)
                                        <input name='published' type="checkbox" value="1"> Publish this Package
                                        @else
                                        <input name='published' type="checkbox" value="1" checked> Publish this Package
                                        @endif
                                    @endif
                                </label>
                            </div>
                        </div>
                    </div>
                </div>  
                @endif
                                                  
                @if(Auth::user()->role == 'supplier') 
                    @if(in_array('publish',$package_access))                              
                        <div class="row">
                            <div class="col-md-12">
                                  <div class="form-group">
                                    <div class="checkbox">

                                        <label>
                                            @if(!isset($package))
                                            <input name='published' type="checkbox" value="1"> Publish this Package
                                            @else
                                                @if(strtotime($package['published']) == 0)
                                                <input name='published' type="checkbox" value="1"> Publish this Package
                                                @else
                                                <input name='published' type="checkbox" value="1" checked> Publish this Package
                                                @endif
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    @endif
                @endif
                                               
                @if(Auth::user()->role == 'allusers') 
                    @if(in_array('publish',$package_access))                              
                <div class="row">
                    <div class="col-md-12">
                          <div class="form-group">
                            <div class="checkbox">
                              
                                <label>
                                    @if(!isset($package))
                                    <input name='published' type="checkbox" value="1"> Publish this Package
                                    @else
                                        @if(strtotime($package['published']) == 0)
                                        <input name='published' type="checkbox" value="1"> Publish this Package
                                        @else
                                        <input name='published' type="checkbox" value="1" checked> Publish this Package
                                        @endif
                                    @endif
                                </label>
                            </div>
                        </div>
                    </div>
                </div>  
               @endif
              @endif

                <div class="login-footer">
                    <a href="{{ URL::route('package-step3',array($package->id)) }}" class="btn btn-primary btn-sm"><b>Back</b></a>
                   <button type="submit" name="save" class="btn btn-primary btn-sm pull-right"><b>Save</b></button>
               </div>
        
            </div><!-- /.Container-fluid -->
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>                  
@stop                  
            
@section('js')
<script> 
    $(function() {
        $( ".sortable" ).sortable({
            handle:'.sortable_handle',
            stop: function(event, ui) {
                //add input weight hidden value according to position.
                $('.inclusions .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.price_summaries .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.hotel_details .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.package_hotel_details .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.itineraries .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
            }
        });
    });
    
    $(function() {
        $('.hotel_title1').autocomplete({
            source: '{{URL::to("hotels.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                $(this).siblings('input[name="edit_hotel1"]').first().attr('value', ui.item.obj);
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });

  
    
    $(function() {
        $('.hotel_title2').autocomplete({
            source: '{{URL::to("hotels.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                $(this).siblings('input[name="edit_hotel2"]').first().attr('value', ui.item.obj);
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
    
    $('.hotel_title1').on('change',function(){
        
        
    });
 </script>   
                     
 <script>   
    
  app
  .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
  })
  .controller('HotelPriceController', function($scope,$http,$rootScope,$element){
      
    $scope.destinations = JSON.parse(decodeURIComponent('{{ rawurlencode($destinations) }}'));  
    
    $scope.hotelprice_category = 'Standard';
      
    $scope.hotel_categories =   JSON.parse(decodeURIComponent('{{ rawurlencode($hotel_categories) }}'));
    
  
    $scope.arrayObjectIndexOf = function(arr, obj){
      for(var i = 0; i < arr.length; i++){
          if(angular.equals(arr[i]['data'], obj)){
            
            return i;
            
          }
      };
      return -1;
    } 
    
    $scope.temp = [];
      
    $scope.form = [];
    $scope.hotel_details = JSON.parse(decodeURIComponent('{{ rawurlencode($hotel_price) }}'));
    
    $scope.submit = function(e)
    {
        e.preventDefault();
           
       
        angular.forEach($scope.destinations, function(v, k){

            $scope.form.destinations[v.destination_id].id = v.label;

            var edit_hotel1 = angular.element( document.querySelector( '#edit_hotel1_'+v.destination_id ) );

            $scope.form.destinations[v.destination_id].hotel1_obj = edit_hotel1.val();

            var edit_hotel2 = angular.element( document.querySelector( '#edit_hotel2_'+v.destination_id ) );

            $scope.form.destinations[v.destination_id].hotel2_obj = edit_hotel2.val();

        });
        
        $scope.temp.push({'cat':$scope.form.hotelprice_category,
                          'destinations' : $scope.form.destinations,
                          'price_per_person': $scope.form.hotelprice_per_person,
                          'discounted_price': $scope.form.hotelprice_discounted_price,
                          'extra_adult':$scope.form.hotelprice_extra_adult,
                          'child_with_bed':$scope.form.hotelprice_child_with_bed,
                          'infant' :$scope.form.hotelprice_infant,
                         });

        $scope.form = [];
    }
    
    $scope.add_date_range = function(e){
        e.preventDefault();
        
        if($scope.temp.length > 0)
        {
            $scope.hotel_details.push({'date_range' : $scope.hotelprice_date,
                                       'data' : $scope.temp
                                      });

            $scope.temp = [];
            $scope.hotelprice_date = '';
        }
        else
        {
            alert('Please fill all fields');
        }
    
      
    }
    
    $scope.delete_hotelprice = function(e,index)
    {
        $scope.hotel_details.splice(index, 1);
    }
    
  });

    
</script>            
@stop