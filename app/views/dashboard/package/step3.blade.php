@extends('layout.dashboard')

@section('css')

<style>
    .sortable { list-style-type: none; padding: 0; width: 100%; }
    .sortable li  { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1em; height: auto;}
    .sortable li span.glyphicon-move { position: absolute; margin-left: -1.3em;  padding-top:1px; cursor:default; color:#aaaaaa; font-weight:normal;}
    .sortable .form-group { margin-bottom:15px;}
  
    label.error -{
      color: #f56954;
  }
</style>
@stop 

@section('content')
<section id="main-content" >
    <section class="wrapper site-min-height">
         <h3><i class="fa fa-angle-right"></i>Add New Package</h3>
        
        <div class="row mt" >
            {{ Form::open(array('action' => array('DashboardController@postPackageStep3'),'files'=> true, 'role' => 'form','id'=>'package_form'))}}           
            <input type="hidden" name="id" @if((isset($package)) && (!isset($clone))) value="{{$package['id']}}" @endif>
             <div class="container-fluid">
                    
                <div class="row" >
                    <div class="col-md-6" ng-controller="ItinerariesController" ng-cloak>
                        <fieldset>
                            <legend>Itineraries</legend>
                            <div class="package_itinerary_wrapper">
                                <div class="itineraries">
                                    <ul class="sortable">
                                        <li ng-repeat="(key,value) in itineraries">
                                            <div class="itinerary sortable_handle">
                                                <div class="row">
                                                    <div class="col-md-6"><span class="glyphicon glyphicon-move"></span>
                                                        <div class="form-group days">
                                                            
                                                            <input type="hidden" id="PackageItinerary" 
                                                                   name="PackageItineraries" 
                                                                   value="<% itineraries %>">

                                                            <input type="hidden" name="PackageItinerary[<% key %>][id]" value="<% value.data.id %>">
                                                            <input class="weight" type="hidden" name="PackageItinerary[<% key %>][weight]" value="<% key %>">
                                                            <span class="text-day_num"><% value.data.day_num %></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <span class="text-destination"><% value.data.destination %></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                          <span class="text-desc"><% value.data.desc %></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                          <span class="text-desc"><% value.data.meal %></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 pull-right">
                                                        <div class="action">
                                                            <a href="javascript:;" ng-click="itinerary_modal_show($event,key,value.data)" class="edit-action" style="float:left">
                                                                <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                            </a>
                                                            <a href="javascript:;" ng-click="delete_itinerary($event,key,value.data)" class="delete-action">
                                                                <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                                            </a>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>

                                             <!-- Itinerary Modal -->
                                            <div class="modal fade" id="Itinerarymodal<%key%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit Detail</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                      <div class="container-fluid itinerary-modal">
                                                            <form id="itinerary_form" action="">
                                                                <input type="hidden" id="edit_itinerary_id<%key%>" name="edit_itinerary_id" ng-model="edit_itinerary_id" ng-value="<% value.data.id %>" >

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">No. of Days :</label>
                                                                            <input type="text" id="edit_day_num<%key%>"  name="edit_itinerary_day_num" class="form-control input-sm" 
                                                                                   ng-model="edit_itinerary_day_num" value="<% value.data.day_num %>" 
                                                                                   placeholder="day_num..." />
                                                                        </div>
                                                                     </div>

                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Destination :</label>
                                                                            <input type="text" edit-destination id="edit_destination_id" ng-model="destination_label"                                                                                              class="form-control input-sm" placeholder="Search Destination..." />
                                                                            <input type="hidden" name="edit_itinerary_destination_id" 
                                                                                   id="edit_itinerary_destination_id<%key%>" ng-model="edit_itinerary_destination_id">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Destination Description</label>
                                                                            <input type="text" id="edit_destination<%key%>" name="edit_itinerary_destination" class="form-control input-sm" ng-model="edit_itinerary_destination" value="<% value.data.destination %>"
                                                                                   placeholder="Destination Description.." />
                                                                        </div>
                                                                     </div>

                                                                     <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Image :</label>
                                                                            <input type="file" name="edit_itinerary_image" 
                                                                                   onchange="angular.element(this).scope().ItineraryImage(this.files)"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Description:</label>
                                                                            <textarea name="edit_itinerary_desc" id="edit_desc<%key%>" class="form-control input-sm" ng-model="edit_itinerary_desc" placeholder="Description..." rows="4"><% value.data.desc %></textarea>
                                                                       </div>
                                                                   </div>
                                                                </div>
                                                                    
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Meal :</label>
                                                                            <select name="edit_itinerary_meal" id="edit_meal<%key%>" multiple class="form-control input-sm" ng-model="edit_itinerary_meal">
                                                                                <option value="Breakfast">Breakfast</option>
                                                                                <option value="Lunch">Lunch</option>
                                                                                <option value="Dinner">Dinner</option>
                                                                            </select>
                                                                       </div>
                                                                   </div>
                                                                </div>

                                                               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                               <button type="submit" ng-click="edit_itinerary_form($event,key);"  class="btn btn-primary" data-dismiss="modal">Submit</button>
                                                            </form>
                                                        </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>         
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div id="itinerary_add">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" ng-model="itinerary_day_num" name="itinerary_day_num"
                                                   class="form-control input-sm" placeholder="No. of days..."/>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="destination_id" class="form-control input-sm" placeholder=" Search Destination..."/>
                                            <input type="hidden" id="dest_id" name="itinerary_destination_id" ng-model="itinerary_destination_id">
                                        </div>  
                                    </div>
                                </div>
                                
                                <div class="row">
                                     <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="itinerary_destination" ng-model="itinerary_destination" class="form-control input-sm" placeholder="Destination Desc..."/>
                                        </div> 
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="file" id="itinerary_image" name="itinerary_image"
                                                   onchange="angular.element(this).scope().ItineraryImage(this.files)"
                                                   ng-model="itinerary_image"/>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="itinerary_desc" ng-model="itinerary_desc" class="form-control" placeholder="Description  ..." rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <select name="itinerary_meal" multiple ng-model="itinerary_meal" class="form-control input-sm">
                                                <option value="Breakfast">Breakfast</option>
                                                <option value="Lunch">Lunch</option>
                                                <option value="Dinner">Dinner</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin-top: 60px;">
                                        <button name="add" ng-click="submit_itinerary($event);" class="btn btn-primary btn-sm pull-right">Add</button>        
                                    </div>
                                </div>
                            </div>

                        </fieldset>   
                    </div>
                                
                    <div class="col-md-6" ng-controller="InclusionController">
                        <fieldset>
                            <legend>Inclusion</legend> 
                            
                            <div class="package_inclusion_wrapper">
                                <div class="inclusions" >
                                    <ul class="sortable">
                                        <li ng-repeat="(key,value) in inclusions">
                                            <div class="inclusion sortable_handle">
                                                <input type="hidden" id="PackageInclusions" 
                                                                   name="PackageInclusions" 
                                                                   value="<% inclusions %>">
                                                
                                                <div class="row">
                                                    <div class="col-md-2"><span class="glyphicon glyphicon-move"></span>
                                                        <div class="form-group">
                                                            <input type="hidden" name="PackageInclusion[<% key %>][id]" value="<% value.data.id %>">
                                                            <input class="weight" type="hidden" name="PackageInclusion[][weight]" value="">
                                                            <span class="text-title"><% value.data.title %></span>
                                                        </div>        
                                                    </div>
                                                    
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <span class="text-title"><% value.data.desc %></span>
                                                        </div>        
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="action">
                                                            <a href="javascript:;" ng-click="inclusion_modal_show($event,key,value.data);" class="edit-action">
                                                            <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                                            </a><a href="javascript:;" ng-click="delete_inclusion($event,key,value.data)" class="delete-action">
                                                            <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                    
<!--                                        Inclusion Modal   -->
                                            <div class="modal fade" id="PackageInclusions<%key%>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Edit Detail</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                        <div class="container-fluid inclusion-modal">
                                                            <form id="inclusion_form" action="">
                                                            <input type="hidden" id="edit_inclusion_id<% key %>" name="edit_inclusion_id" ng-model="edit_inclusion_id" ng-value="<% value.data.id %>">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Inclusion Title :</label>
                                                                                                                                        
                                                                        <input type="text" edit-inclusion id="edit_inclusion_title<%key%>" 
                                                                               ng-model="edit_inclusion_title" class="form-control input-sm" 
                                                                               placeholder="Title..." />
                                                                        
                                                                        <input type="hidden" name="edit_inclusion_title_value" 
                                                                                   id="edit_inclusion_title_value<%key%>" ng-model="edit_inclusion_title_value">
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-md-6">
                                                                   <div class="form-group">
                                                                        <label class="control-label">Description:</label>
                                                                        <input type="text" id="edit_inclusion_desc<%key%>"
                                                                               name="edit_inclusion_desc"
                                                                               ng-model="edit_inclusion_desc" class="form-control input-sm" 
                                                                               placeholder="Description..." />
                                                                   </div>
                                                                </div>
                                                            </div>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            <button type="submit" ng-click="edit_inclusion_form($event,key);" class="btn btn-primary" data-dismiss="modal">Submit</button>
                                                            </form>
                                                        </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>        
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div id="inclusion_add">
                                <div class="row">
                                    <div class="col-md-4">
                                         <div class="form-group">
                                            <input type="text" id="inclusion_title" 
                                                   name="inclusion_title" ng-model="inclusion_title"
                                                   class="form-control input-sm" placeholder="Inclusion Title"/>
                                         </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                         <div class="form-group">
                                            <input type="text" id="inclusion_desc" name="inclusion_desc" ng-model="inclusion_desc"
                                                   class="form-control input-sm" placeholder="Inclusion Desc"/>
                                         </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button name="add" ng-click="submit_inclusion($event);" class="btn btn-primary btn-sm pull-right">Add</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>        
                    </div>            
                </div>

                <div class="login-footer">
                   <a href="{{ URL::route('package-step2',array($package->id)) }}" class="btn btn-primary btn-sm"><b>Back</b></a>
                   <button type="submit" name="save" class="btn btn-primary btn-sm pull-right"><b>Next</b></button>
               </div>
        
            </div><!-- /.Container-fluid -->
            {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>                  
@stop                  
            
@section('js')
<script> 
    $(function() {
        $("#destination_id").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                $('input[name="itinerary_destination_id"]').attr('value',ui.item.id);
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });
    
    $(function() {
        $("#inclusion_title").autocomplete({
            source: '{{URL::to("inclusion.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                $('input[name="inclusion_title"]').attr('value',ui.item.value);
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });
    });

    $(function() {
        $( ".sortable" ).sortable({
            handle:'.sortable_handle',
            stop: function(event, ui) {
                //add input weight hidden value according to position.
                $('.inclusions .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.price_summaries .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.hotel_details .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.package_hotel_details .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
                
                $('.itineraries .sortable li').each(function(i,v){
                    $(v).find('input.weight').first().attr('value', i);
                });
            }
        });
    });
    
    
 </script>   
                     
 <script>   
    
  app
  .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
  }).directive('editDestination', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
                 $(function() {
 
                    $(element).autocomplete({
                        source: '{{URL::to("destinations.json")}}',
                        minLength: 1,
                        select: function(event, ui) {
                           // Prevent value from being put in the input:
                            $(this).siblings('input[name="edit_itinerary_destination_id"]').first().attr('value', ui.item.id);
                        },

                        html: true, // optional (jquery.ui.autocomplete.html.js required)

                        // optional (if other layers overlap autocomplete list)
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("z-index", 1055);
                        }
                    });
                });
           }
      };
  }).directive('editModal', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
               $(element).modal('hide');
           }
      };
  })
  .controller('ItinerariesController', function($scope,$http,$rootScope,$element){  
      
  $scope.itineraries = JSON.parse(decodeURIComponent('{{ rawurlencode($itineraries) }}'));
      
      
  $scope.arrayObjectIndexOf = function(arr, obj){
      for(var i = 0; i < arr.length; i++){
          if(angular.equals(arr[i]['data'], obj)){
              return i;
          }
      };
      return -1;
  }      
      
      
  $scope.count = $scope.itineraries.length + 1;
      
  $scope.itinerary_day_num = 'Day '+ $scope.count;  
      
  // Itineraries 
      
  $scope.ItineraryImage = function(files)
  {
      if (files != null) {
        $scope.image_file = files[0];
      }
  }   
      
  $scope.submit_itinerary = function(e)
  {
      var elem = angular.element( document.querySelector( '#dest_id' ) );
        
      $scope.itinerary_destination_id = elem.val();
      
      if($scope.itinerary_day_num && $scope.itinerary_destination_id && $scope.itinerary_destination && $scope.itinerary_desc && $scope.image_file && $scope.itinerary_meal )
      {
          $http({
                url: '/dashboard/package/itinerary',
                method: "POST",
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: function () {
                    var mydata = new FormData();

                    mydata.append("day_num",$scope.itinerary_day_num);
                    mydata.append("destination_id",$scope.itinerary_destination_id);
                    mydata.append("destination",$scope.itinerary_destination);
                    mydata.append("desc",$scope.itinerary_desc);
                    mydata.append("image", $scope.image_file);
                    mydata.append("meal", $scope.itinerary_meal);

                    return mydata;
                },
                }).success(function(data, status, headers, config) 
                { 
                    $scope.itineraries.push({'data':data});

                    $scope.count = $scope.itineraries.length + 1;  

                    $scope.itinerary_day_num = 'Day '+ $scope.count;

                }).error(function(data, status, headers, config) {
                  $scope.status = status;
          });
      }
      else
      {
          alert('Please fill all fields');
      }
      
      e.preventDefault();
  }
  
  $scope.getDestination = function(id)
  {
        $http({
            url: '/get/destination',
            method:'get',
            params: {'id':id},
            }).success(function(data, status, headers, config) 
            { 
                $scope.destination_label = data.label;        
            
            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });
  }
  
  $scope.itinerary_modal_show = function(e,key,data)
  {
      $scope.getDestination(data.destination_id);
      
      $scope.edit_itinerary_id = data.id;
      
      $scope.edit_itinerary_day_num = data.day_num;
      
      $scope.edit_itinerary_desc = data.desc
      
      $scope.edit_itinerary_destination = data.destination;
      
      if(data.meal != null)
      {
        $scope.edit_itinerary_meal = data.meal.split(',');
      }
      else
      {
          $scope.edit_itinerary_meal = null;
      }
      
      angular.element( document.querySelector( '#Itinerarymodal'+key ) ).modal({
            show: true
      });   
  }
  
  $scope.edit_itinerary_form = function(e,index)
  {
       var edit_itinerary_id = angular.element( document.querySelector( '#edit_itinerary_id'+index ) );
        
        $scope.edit_itinerary_id = edit_itinerary_id.val();
      
        var edit_day_num = angular.element( document.querySelector( '#edit_day_num'+index ) );
        
        $scope.edit_itinerary_day_num = edit_day_num.val(); 
      
        var edit_destination = angular.element( document.querySelector( '#edit_destination'+index ) );
        
        $scope.edit_itinerary_destination = edit_destination.val(); 
      
        var edit_desc = angular.element( document.querySelector( '#edit_desc'+index ) );
        
        $scope.edit_itinerary_desc = edit_desc.val(); 
      
        var edit_itinerary_destination_id = angular.element( document.querySelector( '#edit_itinerary_destination_id'+index ) );
        
        $scope.edit_itinerary_destination_id = edit_itinerary_destination_id.val(); 
      
      
        var edit_meal = angular.element( document.querySelector( '#edit_meal'+index ) );
        
        $scope.edit_itinerary_meal = edit_meal.val(); 
      
        $http({
            url: '/dashboard/package-itinerary/edit',
            method:'POST',
            withCredentials: true,
            headers: {'Content-Type': undefined },
            transformRequest: function () {
                var mydata = new FormData();
                
                mydata.append("id",$scope.edit_itinerary_id);
                mydata.append("day_num",$scope.edit_itinerary_day_num);
                mydata.append("destination_id",$scope.edit_itinerary_destination_id);
                mydata.append("destination",$scope.edit_itinerary_destination);
                mydata.append("desc",$scope.edit_itinerary_desc);
                mydata.append("image", $scope.image_file);
                mydata.append("meal", $scope.edit_itinerary_meal);
                
                return mydata;
            },
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries[index].data = data;
            
                angular.element( document.querySelector( '#Itinerarymodal'+index ).modal('hide') );
            
            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });   
      
      e.preventDefault();
       
  }
  
  $scope.delete_itinerary = function(e,index,data)
  {
      $http({
            url: '/dashboard/package-itinerary/delete',
            method: "POST",
            params:{
                'id':data.id,
            },
            }).success(function(data, status, headers, config) 
            { 
                $scope.itineraries.splice(index, 1);
          
                $scope.count = $scope.itineraries.length + 1;   
      
                $scope.itinerary_day_num = 'Day '+ $scope.count;
          
            }).error(function(data, status, headers, config) {
              $scope.status = status;
      });
  }

  })
  
  .directive('editInclusion', function($http, $timeout) {
      return {
           restrict: 'A', 
           link: function(scope, element, attr) {
              
                $(function() {
                    $(element).autocomplete({
                        source: '{{URL::to("inclusion.json")}}',
                        minLength: 1,
                        select: function(event, ui) {
                           // Prevent value from being put in the input:
//                            $('input[name="inclusion_title"]').attr('value',ui.item.value);
                            $(this).siblings('input[name="edit_inclusion_title_value"]').first().attr('value', ui.item.value);
                        },

                        html: true, // optional (jquery.ui.autocomplete.html.js required)

                        // optional (if other layers overlap autocomplete list)
                        open: function(event, ui) {
                            $(".ui-autocomplete").css("z-index", 1055);
                        }
                    });
                });
                
           }
      };
  })
  
  .controller('InclusionController',function($scope,$http,$element){
  
      $scope.inclusions = JSON.parse(decodeURIComponent('{{ rawurlencode($inclusions) }}'));
      
      $scope.submit_inclusion = function(e){
          
        if($scope.inclusion_title && $scope.inclusion_desc)
        {
              $http({
                url: '/dashboard/package/inclusion',
                method: "POST",
                params: {'title':$scope.inclusion_title,
                         'desc':$scope.inclusion_desc
                        },
                }).success(function(data, status, headers, config) 
                { 
                    $scope.inclusions.push({'data':data});

                }).error(function(data, status, headers, config) {
                  $scope.status = status;
              });
        }
        else
        {
            alert('Please fill all fields');
        }
        e.preventDefault();
          
      };
      
      $scope.inclusion_modal_show = function(e,key,data){
            
          $scope.edit_inclusion_id = data.id;
          
          $scope.edit_inclusion_title = data.title;
          
          $scope.edit_inclusion_desc = data.desc;
         
          angular.element( document.querySelector( '#PackageInclusions'+key ) ).modal({
            show: true
          });   
          
          e.preventDefault();
      
      }
      
      $scope.edit_inclusion_form = function(e,index){
      
         var edit_inclusion_id = angular.element( document.querySelector( '#edit_inclusion_id'+index ) );
        
         $scope.edit_inclusion_id = edit_inclusion_id.val();
          
         var edit_inclusion_title = angular.element( document.querySelector( '#edit_inclusion_title'+index ) );
        
         $scope.edit_inclusion_title = edit_inclusion_title.val(); 
          
         var edit_inclusion_title_value = angular.element( document.querySelector( '#edit_inclusion_title_value'+index ) );
        
         $scope.edit_inclusion_title_value = edit_inclusion_title_value.val(); 
          
         var edit_inclusion_desc = angular.element( document.querySelector( '#edit_inclusion_desc'+index ) );
        
         $scope.edit_inclusion_desc = edit_inclusion_desc.val(); 
          
          
         $http({
            url: '/dashboard/package-inclusion/edit',
            method: "POST",
            params: {'id':$scope.edit_inclusion_id,
                     'title':$scope.edit_inclusion_title,
                     'desc':$scope.edit_inclusion_desc
                    },
            }).success(function(data, status, headers, config) 
            { 
                $scope.inclusions[index].data = data;
            
                angular.element( document.querySelector( '#PackageInclusions'+index ).modal('hide') );
          
            }).error(function(data, status, headers, config) {
              $scope.status = status;
        });
          
        e.preventDefault();
      }
      
      $scope.delete_inclusion = function(e,index,data){
      
          $http({
                url: '/dashboard/package-inclusion/delete',
                method: "POST",
                params: {'id':data.id,
                        },
                }).success(function(data, status, headers, config) 
                { 
                    $scope.inclusions.splice(index, 1);

                }).error(function(data, status, headers, config) {
                  $scope.status = status;
            });

            e.preventDefault();
      
      }
  });
    
</script>            
@stop            