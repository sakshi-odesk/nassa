@extends('layout.dashboard')

@section('content')
<section id="main-content">
      <section class="wrapper">
         <div class="row mt">
             @if(Session::has('success'))
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                {{Session::get('success')}}
                            </div>
                        </div>
                    </div>
                </div>
             @endif
              <div class="col-md-12">
                  <div class="content-panel">
                      <table id="pgrid" class="table table-striped table-advance table-hover">
                          <h4>
                              <i class="fa fa-angle-right"></i>Packages

                              @if(Auth::user()->role == 'admin') 
                                <a href="{{URL::route('dashboard.package.form')}}" class="btn btn-info btn-sm pull-right">
                                <i class="fa fa-building-o"></i>&nbsp; Add New Package</a>
                              @endif

                             @if(Auth::user()->role == 'supplier')
                             @if(in_array('create',$package_access))

                                <a href="{{URL::route('dashboard.package.form')}}" class="btn btn-info btn-sm pull-right">
                                <i class="fa fa-building-o"></i>&nbsp; Add New Package</a>
                            @endif
                            @endif

                             @if(Auth::user()->role == 'allusers')
                             @if(in_array('create',$package_access))

                                <a href="{{URL::route('dashboard.package.form')}}" class="btn btn-info btn-sm pull-right">
                                <i class="fa fa-building-o"></i>&nbsp; Add New Package</a>
                            @endif
                            @endif

                          </h4>
                          <hr>
                          <thead>
                          <tr>
                              <th><i class="fa fa-bullhorn"></i> Date</th>
                              <th class="hidden-phone"><i class="fa fa-question-circle"></i> Title</th>
                              <th><i class="fa fa-bookmark"></i> Hot Picks</th>
                              <th><i class="fa fa-bookmark"></i> Status</th>
                              <th><i class=" fa fa-edit"></i> Action</th>
                          </tr>
                          </thead>
                          <tfoot>
                          <tr>
                              <th></th>
                              <th>Title</th>
                              <th>Hot Picks</th>
                              <th></th>
                              <th></th>
                          </tr>
                          </tfoot>
                          <tbody>
                            @foreach($packages as $package)
                            
                            @if(Auth::user()->role != 'admin')
                            @if(Auth::user()->id == $package->user_id)
                            <?php $package_category = $package->package_category()->getResults(); ?>
                            <tr>
                                <td>{{ $package->created_at }}</td>
                                <td>{{ $package->title }}</td>
                               
                                @if($package->hotpick == 1)
                                <td>
                                    <a href="{{URL::route('package-remove-hotpicks',array($package->id))}}" class="btn btn-success btn-xs">
                                    <span class="label label-success" data-toggle="tooltip" data-original-title="Click here to remove package from hot picks section">Yes</span>
                                    </a>
                                </td>
                                @else
                                <td>
                                    <a href="{{URL::route('package-add-hotpicks',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="label label-danger" data-toggle="tooltip" data-original-title="Click here to add package on hot picks section">No</span>
                                    </a>
                                </td>
                                @endif
                                
                                @if(in_array('publish',$package_access))
                                <td>
                                    @if(strtotime($package->published) != 0)
                                    <a href="{{URL::route('package-unpublished',array($package->id))}}" class="btn btn-success btn-xs">
                                    <span class="label label-success" data-toggle="tooltip" data-original-title="Click to Unpublish package">Published</span>
                                    </a>
                                    @else
                                    <a href="{{URL::route('package-published',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="label label-danger" data-toggle="tooltip" data-original-title="Click to Publish package">Unpublished</span>
                                    </a>
                                    @endif
                                </td>
                                @else
                                <td>
                                    @if(strtotime($package->published) != 0)
                                    <span class="label label-success">Published</span>
                                    @else
                                    <span class="label label-danger">Unpublished</span>
                                    @endif
                                </td>
                                @endif
                                <td>
                                    <a href="{{URL::route('packages-details',array($package->slug))}}" target="_blank" class="btn btn-warning btn-xs">
                                    <span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-original-title="View"></span>
                                    </a>
                                    &nbsp;
                                    <a href="{{URL::route('dashboard.package.edit',array($package->id))}}" class="btn btn-info btn-xs">
                                    <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                    </a>
                                    &nbsp;
                                  @if(Auth::user()->role == 'admin') 
                                  @if(in_array('delete',$package_access))
                                    <a href="{{URL::route('dashboard.package.delete',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                    </a>
                                  @endif
                                  @endif

                                  @if(Auth::user()->role == 'supplier') 
                                  @if(in_array('delete',$package_access))
                                    <a href="{{URL::route('dashboard.package.delete',array($package->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                    </a>
                                  @endif
                                  @endif

                                  @if(Auth::user()->role == 'allusers') 
                                  @if(in_array('delete',$package_access))
                                  <a href="{{URL::route('dashboard.package.delete',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                  </a>
                                  @endif
                                  @endif

                                  &nbsp;
                                   @if(Auth::user()->role == 'admin') 
                                  <a href="{{URL::route('dashboard.package.cloning',array($package->id))}}" class="btn btn-primary btn-xs">
                                    <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                  </a>
                                  @endif
                               </td>
                            </tr>
                            @endif
                            
                            @else
                            <?php $package_category = $package->package_category()->getResults(); ?>
                            <tr>
                                <td>{{ $package->created_at }}</td>
                                <td>{{ $package->title }}</td>
                                @if($package->hotpick == 1)
                                <td>
                                    <a href="{{URL::route('package-remove-hotpicks',array($package->id))}}" class="btn btn-success btn-xs">
                                    <span class="label label-success" data-toggle="tooltip" data-original-title="Click here to remove package from hot picks section">Yes</span>
                                    </a>
                                </td>
                                @else
                                <td>
                                    <a href="{{URL::route('package-add-hotpicks',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="label label-danger" data-toggle="tooltip" data-original-title="Click here to add package on hot picks section">No</span>
                                    </a>
                                </td>
                                @endif
                                <td>
                                    @if(strtotime($package->published) != 0)
                                    <a href="{{URL::route('package-unpublished',array($package->id))}}" class="btn btn-success btn-xs">
                                    <span class="label label-success" data-toggle="tooltip" data-original-title="Click to Unpublish package">Published</span>
                                    </a>
                                    @else
                                    <a href="{{URL::route('package-published',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="label label-danger" data-toggle="tooltip" data-original-title="Click to Publish package">Unpublished</span>
                                    </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{URL::route('packages-details',array($package->slug))}}" target="_blank" class="btn btn-warning btn-xs">
                                    <span class="glyphicon glyphicon-eye-open" data-toggle="tooltip" data-original-title="View"></span>
                                    </a>
                                    &nbsp;
                                    <a href="{{URL::route('dashboard.package.edit',array($package->id))}}" class="btn btn-info btn-xs">
                                    <span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-original-title="Edit"></span>
                                    </a>
                                    &nbsp;
                                  @if(Auth::user()->role == 'admin') 
                                  @if(in_array('delete',$package_access))
                                    <a href="{{URL::route('dashboard.package.delete',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                    </a>
                                  @endif
                                  @endif

                                  @if(Auth::user()->role == 'supplier') 
                                  @if(in_array('delete',$package_access))
                                    <a href="{{URL::route('dashboard.package.delete',array($package->id))}}" class="btn btn-danger btn-xs">
                                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                    </a>
                                  @endif
                                  @endif

                                  @if(Auth::user()->role == 'allusers') 
                                  @if(in_array('delete',$package_access))
                                  <a href="{{URL::route('dashboard.package.delete',array($package->id))}}" class="btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-original-title="Delete"></span>
                                  </a>
                                  @endif
                                  @endif
                                  &nbsp;
                                  @if(Auth::user()->role == 'admin') 
                                  <a href="{{URL::route('dashboard.package.cloning',array($package->id))}}" class="btn btn-primary btn-xs">
                                    <span class="fa fa-files-o" data-toggle="tooltip" data-original-title="Clone"></span>
                                  </a>
                                  @endif
                               </td>
                            </tr>                          
                            
                            @endif                      
                            @endforeach
                            
                          </tbody>
                      </table>
                  </div><!-- /content-panel -->
              </div><!-- /col-md-12 -->
          </div><!-- /row --> 
      </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop

@section('css')
{{HTML::style('theme/nassatravels/assets/css/dataTables.bootstrap.css')}}
@stop
@section('js')
{{ HTML::script('theme/nassatravels/assets/js/jquery.dataTables.js') }}
{{ HTML::script('theme/nassatravels/assets/js/dataTables.bootstrap.js') }}

<script type="text/javascript">

$(document).ready(function(){

    // Setup - add a text input to each footer cell
    $('#pgrid tfoot th').each( function () {

        if ($.inArray($(this).index(), [0,2,3,4]) === -1) {
            var title = $('#pgrid thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } else {
            $(this).html('');
        }
    } );

    // DataTable
    var table = $('#pgrid').DataTable({
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': [1,2,3,4],
        }],
        initComplete: function (){
          var r = $('#pgrid tfoot tr');
          r.find('th').each(function(){
            $(this).css('padding', 8);
          });
          $('#pgrid thead').append(r);
          $('#search_0').css('text-align', 'center');
        },
    });

    // Apply the search
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    });
});
</script>

@stop

