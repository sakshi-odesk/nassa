@extends('layout.dashboard')

@section('content')

<section id="main-content">
    <section class="wrapper site-min-height">
          <h3><i class="fa fa-angle-right"></i>Edit Package Itinerary</h3>
         <div class="row mt">
            {{ Form::open(array('action' => array('DashboardController@postPackageItineraryEdit'),'files'=> true, 'role' => 'form'))}}
             
            <div class="container-fluid">
                <input type="hidden" name="id" {{isset($itinerary) ? 'value="'.$itinerary['id']. '"' : ''}} >
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">No. of Days :</label>
                                        <input type="text" name="day_num" class="form-control input-sm" required
                                               placeholder="day_num..." {{isset($itinerary) ? 'value="'.$itinerary['day_num']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Destination :</label>
                                        <input type="text" name="destination" class="form-control input-sm" required
                                               placeholder="Destination..." {{isset($itinerary) ? 'value="'.$itinerary['destination']. '"' : ''}} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Description:</label>
                                <textarea name="desc" class="form-control input-sm" placeholder="Description..." rows="4">{{isset($itinerary) ? $itinerary['desc']: ''}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="login-footer">
                        <button type="submit" name="save" class="btn btn-primary btn-sm pull-left">Submit</button>
                    </div>
            </div><!-- /.Container-fluid -->
             {{Form::close()}}
        </div>
         
    </section><! --/wrapper -->
</section>
@stop