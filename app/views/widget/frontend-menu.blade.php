 <div class="menu">
    <ul>
        <li class="active"><a href="{{URL::route('home')}}">Home</a></li>
        <li><a href="{{URL::route('domestic')}}">Domestic</a></li>
        <li><a href="{{URL::route('international')}}">International</a></li>
        <li><a href="{{URL::route('hotels')}}">Hotels</a></li>
        <li><a href="{{URL::route('flights')}}">Flights</a></li>
        <li><a href="{{URL::route('cars-volvos')}}">Cars &amp; Volvos</a></li>
        <li><a href="{{URL::route('special-offers')}}">Special Offers</a></li>
        <li><a href="{{URL::route('blog')}}">Blog</a></li>
        <li><a href="{{URL::route('testimonials')}}">Testimonial</a></li>
        <li><a href="{{URL::route('contactus')}}">Contact Us</a></li>
    </ul>
</div>