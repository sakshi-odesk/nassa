<div class="footer">
    <ul>
        <li><a style="text-decoration:none;" href="{{URL::route('home')}}">Home</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('aboutus')}}">About Us</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('domestic')}}">Domestic Packages</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('international')}}">International Packages</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('hotels')}}">Hotels</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('flights')}}">Flights</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('cars-volvos')}}">Cars &amp; Volvos</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('special-offers')}}">Special Offers</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('policy')}}">Policy</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('term')}}">T &amp; C</a></li>
        <li><a style="text-decoration:none;" href="{{URL::route('contactus')}}">Contact Us</a></li>
    </ul>
</div>