 <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="#"><img src="/theme/nassatravels/assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered">{{Auth::user()->email}}</h5>
              	                
                  <li class="mt">
                      <a href="{{URL::route('dashboard')}}">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                    
                  @if(Auth::user()->role == 'admin')                 
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-building"></i>
                          <span>Hotels</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.hotel-category.list')}}">Hotel Categories List</a></li>
                          <li><a  href="{{URL::route('dashboard.hotel.list')}}">Hotels List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-plane"></i>
                          <span>Packages</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.package-category.list')}}">Pacakge Categories List</a></li>
                          <li><a  href="{{URL::route('dashboard.package.list')}}">Packages List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-list"></i>
                          <span>Blogs</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.blog-category.list')}}">Blog Categories</a></li>
                          <li><a  href="{{URL::route('dashboard.blog.list')}}">Blogs List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-pencil"></i>
                          <span>Pages</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.page.list')}}">Pages List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-th"></i>
                          <span>Campaigns</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.campaign.list')}}">Campaigns List</a></li>
                          <li><a  href="{{URL::route('dashboard.campaign.submission-list')}}">CampaignsSubmissions List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-sitemap"></i>
                          <span>Destinations</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.destination.view')}}">Destinations</a></li>
                          <li><a  href="{{URL::route('popular-destinations')}}">Popular Destinations</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-bar-chart-o"></i>
                          <span>SlideShows</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.slide-show.list')}}">SlideShows List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-gift"></i>
                          <span>Facilities</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.facility.list')}}">Facility List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-thumbs-o-up"></i>
                          <span>Feedback</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.feedback.list')}}">Feedback List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-users"></i>
                          <span>Users</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.admin.list')}}">Admin</a></li>
                          <li><a  href="{{URL::route('dashboard.supplier.list')}}">Suppliers</a></li>
                          <li><a  href="{{URL::route('dashboard.operator.list')}}">Operators</a></li>
                          <li><a  href="{{URL::route('dashboard.allusers.list')}}">All Users</a></li>
                      </ul>
                     </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>Settings</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.setting.list')}}">Settings List</a></li>
                      </ul>
                  </li>
                  @endif
                  
                  @if(Auth::user()->role == 'supplier')
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-building"></i>
                          <span>Hotels</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.hotel.list')}}">Hotels List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-plane"></i>
                          <span>Packages</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{URL::route('dashboard.package.list')}}">Packages List</a></li>
                      </ul>
                  </li>
                  @endif
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>