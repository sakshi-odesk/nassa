<div class="header">
    <img src="{{asset('theme/front/images/NassaTravels_logo.png')}}" class="logo" align="left">
    <div class="topright">
        <div class="phone-wrapper">
            <div class="left"><img src="{{asset('theme/front/images/callus.png')}}" width="32px"></div>
            <div class="right">
                <div class="phone">011 - 66081234, +91 - 8588872474</div>
                <div class="phone">+91 - 8800799881, +91 - 7838779981</div>
            </div>
        </div>
            <br>

        <div class="social-wrapper"><img src="{{asset('theme/front/images/social.png')}}" usemap="#Map" border="0" height="45px"></div>
        <map name="Map" id="Map">
            <area shape="rect" coords="5,4,49,57" href="https://www.facebook.com/NassaTravels" target="_blank">
            <area shape="rect" coords="56,3,100,57" href="https://twitter.com/nassatravels" target="_blank">
            <area shape="rect" coords="109,4,148,58" href="https://plus.google.com/116157609037166436816" target="_blank">
        </map>
    </div>
</div>