<header class="header black-bg">
      <div class="sidebar-toggle-box">
          <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Nassa Travels"></div>
      </div>
    <!--logo start-->
    <a href="{{URL::route('dashboard')}}" class="logo"><b>Nassa Travels</b></a>
    <!--logo end-->

    <div class="top-menu">
        <ul class="nav pull-right top-menu">
            <li><a class="logout" href="{{URL::route('user.logout')}}">Logout</a></li>
        </ul>
    </div>
</header>