@extends('layout.frontend')

@section('css')    
<style>
.san
{
  border: 1px solid #CCCCCC;
    color: #666666;
    margin: 0px 10px 0;
    padding: 10px;
    width: 275px;
	float:right;
}
.san1
{
  border: 1px solid #CCCCCC;
    color: #666666;
    margin: 8px 10px 0;
    padding: 10px;
    width: 275px;
	height:60px;
	float:right;
}
mid
{
height:400px!important;

}

.clear
{
clear:both;
}
.but
{
width:150px;
}
.abttitle
{
color:#666666!important;
margin:10px 10px 0;
font-size:15px;
}
.sss
{
 height:30px;
}
</style>
@stop

@section('content')

<div class="mid" style="height:550px;">

<div class="left" style="margin-left:220px;width:50%;">

<div class="search" style="height:540px;">
<div class="tag1">Flights Booking</div>
    
    {{Form::open(array('route'=>'flight.store','role'=>'form','files'=>'true'))}}

    <div class="abttitle">
&nbsp;From City:
<select class="san" style="width:300px; margin-top:0px;margin-right:" name="fcity" id="fcity">
					                       <option value="">-- Select From City --</option>	 
						   				<option value="Bangalore">  Bangalore</option>
                            			<option value="Chennai">Chennai	</option>
                            										 <option value="Delhi">
										  Delhi								 		 </option>
                            										 <option value="Goa">
										  Goa								 		 </option>
                            										 <option value="Hyderabad">
										  Hyderabad								 		 </option>
                            										 <option value="Kolkata">
										  Kolkata								 		 </option>
                            										 <option value="Mumbai">
										  Mumbai								 		 </option>
                            										 <option value="Pune">
										  Pune								 		 </option>
                            										 <option value="Agartala">
										  Agartala								 		 </option>
                            										 <option value="Agra">
										  Agra								 		 </option>
                            										 <option value="Agatti Island">
										  Agatti Island								 		 </option>
                            										 <option value="Ahmedabad">
										  Ahmedabad								 		 </option>
                            										 <option value="Aizawl">
										  Aizawl								 		 </option>
                            										 <option value="Allahabad">
										  Allahabad								 		 </option>
                            										 <option value="Amritsar">
										  Amritsar								 		 </option>
                            										 <option value="Aurangabad">
										  Aurangabad								 		 </option>
                            										 <option value="Bagdogra">
										  Bagdogra								 		 </option>
                            										 <option value="Bangalore">
										  Bangalore								 		 </option>
                            										 <option value="Belgaum">
										  Belgaum								 		 </option>
                            										 <option value="Bellary">
										  Bellary								 		 </option>
                            										 <option value="Bhavnagar">
										  Bhavnagar								 		 </option>
                            										 <option value="Bhopal">
										  Bhopal								 		 </option>
                            										 <option value="Bhubaneshwar">
										  Bhubaneshwar								 		 </option>
                            										 <option value="Bhuj">
										  Bhuj								 		 </option>
                            										 <option value="Bombay">
										  Bombay								 		 </option>
                            										 <option value="Calcutta">
										  Calcutta								 		 </option>
                            										 <option value="Calicut">
										  Calicut								 		 </option>
                            										 <option value="Chandigarh">
										  Chandigarh								 		 </option>
                            										 <option value="Chennai">
										  Chennai								 		 </option>
                            										 <option value="Cochin">
										  Cochin								 		 </option>
                            										 <option value="Coimbatore">
										  Coimbatore								 		 </option>
                            										 <option value="Dehradun">
										  Dehradun								 		 </option>
                            										 <option value="Delhi">
										  Delhi								 		 </option>
                            										 <option value="Dharamshala">
										  Dharamshala								 		 </option>
                            										 <option value="Dibrugarh">
										  Dibrugarh								 		 </option>
                            										 <option value="Dimapur">
										  Dimapur								 		 </option>
                            										 <option value="Diu">
										  Diu								 		 </option>
                            										 <option value="Gaya">
										  Gaya								 		 </option>
                            										 <option value="Goa">
										  Goa								 		 </option>
                            										 <option value="Gorakhpur">
										  Gorakhpur								 		 </option>
                            										 <option value="Guwahati">
										  Guwahati								 		 </option>
                            										 <option value="Gwalior">
										  Gwalior								 		 </option>
                            										 <option value="Hubli">
										  Hubli								 		 </option>
                            										 <option value="Hyderabad">
										  Hyderabad								 		 </option>
                            										 <option value="Imphal">
										  Imphal								 		 </option>
                            										 <option value="Indore">
										  Indore								 		 </option>
                            										 <option value="Jabalpur">
										  Jabalpur								 		 </option>
                            										 <option value="Jaipur">
										  Jaipur								 		 </option>
                            										 <option value="Jamshedpur">
										  Jamshedpur								 		 </option>
                            										 <option value="Jammu">
										  Jammu								 		 </option>
                            										 <option value="Jamnagar">
										  Jamnagar								 		 </option>
                            										 <option value="Jodhpur">
										  Jodhpur								 		 </option>
                            										 <option value="Jorhat">
										  Jorhat								 		 </option>
                            										 <option value="Kandla">
										  Kandla								 		 </option>
                            										 <option value="Kanpur">
										  Kanpur								 		 </option>
                            										 <option value="Khajuraho">
										  Khajuraho								 		 </option>
                            										 <option value="Kolhapur">
										  Kolhapur								 		 </option>
                            										 <option value="Kolkata">
										  Kolkata								 		 </option>
                            										 <option value="Kullu">
										  Kullu								 		 </option>
                            										 <option value="Latur">
										  Latur								 		 </option>
                            										 <option value="Leh">
										  Leh								 		 </option>
                            										 <option value="Lilabari">
										  Lilabari								 		 </option>
                            										 <option value="Lucknow">
										  Lucknow								 		 </option>
                            										 <option value="Ludhiana">
										  Ludhiana								 		 </option>
                            										 <option value="Madras">
										  Madras								 		 </option>
                            										 <option value="Madurai">
										  Madurai								 		 </option>
                            										 <option value="Mangalore">
										  Mangalore								 		 </option>
                            										 <option value="Mumbai">
										  Mumbai								 		 </option>
                            										 <option value="Mysore">
										  Mysore								 		 </option>
                            										 <option value="Nagpur">
										  Nagpur								 		 </option>
                            										 <option value="Nasik">
										  Nasik								 		 </option>
                            										 <option value="Nanded">
										  Nanded								 		 </option>
                            										 <option value="New Delhi">
										  New Delhi								 		 </option>
                            										 <option value="Pantnagar">
										  Pantnagar								 		 </option>
                            										 <option value="Pathankot">
										  Pathankot								 		 </option>
                            										 <option value="Patna">
										  Patna								 		 </option>
                            										 <option value="Porbandar">
										  Porbandar								 		 </option>
                            										 <option value="PortBlair">
										  PortBlair								 		 </option>
                            										 <option value="Pune">
										  Pune								 		 </option>
                            										 <option value="Raipur">
										  Raipur								 		 </option>
                            										 <option value="Rajahmundry">
										  Rajahmundry								 		 </option>
                            										 <option value="Rajkot">
										  Rajkot								 		 </option>
                            										 <option value="Ranchi">
										  Ranchi								 		 </option>
                            										 <option value="Salem">
										  Salem								 		 </option>
                            										 <option value="Shillong">
										  Shillong								 		 </option>
                            										 <option value="Sholapur">
										  Sholapur								 		 </option>
                            										 <option value="Shimla">
										  Shimla								 		 </option>
                            										 <option value="Silchar">
										  Silchar								 		 </option>
                            										 <option value="Srinagar">
										  Srinagar								 		 </option>
                            										 <option value="Surat">
										  Surat								 		 </option>
                            										 <option value="Tezpur">
										  Tezpur								 		 </option>
                            										 <option value="Tiruchirapally">
										  Tiruchirapally								 		 </option>
                            										 <option value="Tirupati">
										  Tirupati								 		 </option>
                            										 <option value="Trivandrum">
										  Trivandrum								 		 </option>
                            										 <option value="Tuticorin">
										  Tuticorin								 		 </option>
                            										 <option value="Udaipur">
										  Udaipur								 		 </option>
                            										 <option value="Vadodara">
										  Vadodara								 		 </option>
                            										 <option value="Varanasi">
										  Varanasi								 		 </option>
                            										 <option value="Vijaywada">
										  Vijaywada								 		 </option>
                            										 <option value="Visakhapatnam">
										  Visakhapatnam								 		 </option>
    </select>

</div>
<div class="clear"></div>
<div class="abttitle">
&nbsp;To City:
<select style="width:300px;margin-top:-5px; float:right;" class="san" name="tocity" id="tocity"  >
					                            <option value="">-- Select To City --</option>	 
						   																	 <option value="Bangalore">
										  Bangalore								 		 </option>
                            										 <option value="Chennai">
										  Chennai								 		 </option>
                            										 <option value="Delhi">
										  Delhi								 		 </option>
                            										 <option value="Goa">
										  Goa								 		 </option>
                            										 <option value="Hyderabad">
										  Hyderabad								 		 </option>
                            										 <option value="Kolkata">
										  Kolkata								 		 </option>
                            										 <option value="Mumbai">
										  Mumbai								 		 </option>
                            										 <option value="Pune">
										  Pune								 		 </option>
                            										 <option value="Agartala">
										  Agartala								 		 </option>
                            										 <option value="Agra">
										  Agra								 		 </option>
                            										 <option value="Agatti Island">
										  Agatti Island								 		 </option>
                            										 <option value="Ahmedabad">
										  Ahmedabad								 		 </option>
                            										 <option value="Aizawl">
										  Aizawl								 		 </option>
                            										 <option value="Allahabad">
										  Allahabad								 		 </option>
                            										 <option value="Amritsar">
										  Amritsar								 		 </option>
                            										 <option value="Aurangabad">
										  Aurangabad								 		 </option>
                            										 <option value="Bagdogra">
										  Bagdogra								 		 </option>
                            										 <option value="Bangalore">
										  Bangalore								 		 </option>
                            										 <option value="Belgaum">
										  Belgaum								 		 </option>
                            										 <option value="Bellary">
										  Bellary								 		 </option>
                            										 <option value="Bhavnagar">
										  Bhavnagar								 		 </option>
                            										 <option value="Bhopal">
										  Bhopal								 		 </option>
                            										 <option value="Bhubaneshwar">
										  Bhubaneshwar								 		 </option>
                            										 <option value="Bhuj">
										  Bhuj								 		 </option>
                            										 <option value="Bombay">
										  Bombay								 		 </option>
                            										 <option value="Calcutta">
										  Calcutta								 		 </option>
                            										 <option value="Calicut">
										  Calicut								 		 </option>
                            										 <option value="Chandigarh">
										  Chandigarh								 		 </option>
                            										 <option value="Chennai">
										  Chennai								 		 </option>
                            										 <option value="Cochin">
										  Cochin								 		 </option>
                            										 <option value="Coimbatore">
										  Coimbatore								 		 </option>
                            										 <option value="Dehradun">
										  Dehradun								 		 </option>
                            										 <option value="Delhi">
										  Delhi								 		 </option>
                            										 <option value="Dharamshala">
										  Dharamshala								 		 </option>
                            										 <option value="Dibrugarh">
										  Dibrugarh								 		 </option>
                            										 <option value="Dimapur">
										  Dimapur								 		 </option>
                            										 <option value="Diu">
										  Diu								 		 </option>
                            										 <option value="Gaya">
										  Gaya								 		 </option>
                            										 <option value="Goa">
										  Goa								 		 </option>
                            										 <option value="Gorakhpur">
										  Gorakhpur								 		 </option>
                            										 <option value="Guwahati">
										  Guwahati								 		 </option>
                            										 <option value="Gwalior">
										  Gwalior								 		 </option>
                            										 <option value="Hubli">
										  Hubli								 		 </option>
                            										 <option value="Hyderabad">
										  Hyderabad								 		 </option>
                            										 <option value="Imphal">
										  Imphal								 		 </option>
                            										 <option value="Indore">
										  Indore								 		 </option>
                            										 <option value="Jabalpur">
										  Jabalpur								 		 </option>
                            										 <option value="Jaipur">
										  Jaipur								 		 </option>
                            										 <option value="Jamshedpur">
										  Jamshedpur								 		 </option>
                            										 <option value="Jammu">
										  Jammu								 		 </option>
                            										 <option value="Jamnagar">
										  Jamnagar								 		 </option>
                            										 <option value="Jodhpur">
										  Jodhpur								 		 </option>
                            										 <option value="Jorhat">
										  Jorhat								 		 </option>
                            										 <option value="Kandla">
										  Kandla								 		 </option>
                            										 <option value="Kanpur">
										  Kanpur								 		 </option>
                            										 <option value="Khajuraho">
										  Khajuraho								 		 </option>
                            										 <option value="Kolhapur">
										  Kolhapur								 		 </option>
                            										 <option value="Kolkata">
										  Kolkata								 		 </option>
                            										 <option value="Kullu">
										  Kullu								 		 </option>
                            										 <option value="Latur">
										  Latur								 		 </option>
                            										 <option value="Leh">
										  Leh								 		 </option>
                            										 <option value="Lilabari">
										  Lilabari								 		 </option>
                            										 <option value="Lucknow">
										  Lucknow								 		 </option>
                            										 <option value="Ludhiana">
										  Ludhiana								 		 </option>
                            										 <option value="Madras">
										  Madras								 		 </option>
                            										 <option value="Madurai">
										  Madurai								 		 </option>
                            										 <option value="Mangalore">
										  Mangalore								 		 </option>
                            										 <option value="Mumbai">
										  Mumbai								 		 </option>
                            										 <option value="Mysore">
										  Mysore								 		 </option>
                            										 <option value="Nagpur">
										  Nagpur								 		 </option>
                            										 <option value="Nasik">
										  Nasik								 		 </option>
                            										 <option value="Nanded">
										  Nanded								 		 </option>
                            										 <option value="New Delhi">
										  New Delhi								 		 </option>
                            										 <option value="Pantnagar">
										  Pantnagar								 		 </option>
                            										 <option value="Pathankot">
										  Pathankot								 		 </option>
                            										 <option value="Patna">
										  Patna								 		 </option>
                            										 <option value="Porbandar">
										  Porbandar								 		 </option>
                            										 <option value="PortBlair">
										  PortBlair								 		 </option>
                            										 <option value="Pune">
										  Pune								 		 </option>
                            										 <option value="Raipur">
										  Raipur								 		 </option>
                            										 <option value="Rajahmundry">
										  Rajahmundry								 		 </option>
                            										 <option value="Rajkot">
										  Rajkot								 		 </option>
                            										 <option value="Ranchi">
										  Ranchi								 		 </option>
                            										 <option value="Salem">
										  Salem								 		 </option>
                            										 <option value="Shillong">
										  Shillong								 		 </option>
                            										 <option value="Sholapur">
										  Sholapur								 		 </option>
                            										 <option value="Shimla">
										  Shimla								 		 </option>
                            										 <option value="Silchar">
										  Silchar								 		 </option>
                            										 <option value="Srinagar">
										  Srinagar								 		 </option>
                            										 <option value="Surat">
										  Surat								 		 </option>
                            										 <option value="Tezpur">
										  Tezpur								 		 </option>
                            										 <option value="Tiruchirapally">
										  Tiruchirapally								 		 </option>
                            										 <option value="Tirupati">
										  Tirupati								 		 </option>
                            										 <option value="Trivandrum">
										  Trivandrum								 		 </option>
                            										 <option value="Tuticorin">
										  Tuticorin								 		 </option>
                            										 <option value="Udaipur">
										  Udaipur								 		 </option>
                            										 <option value="Vadodara">
										  Vadodara								 		 </option>
                            										 <option value="Varanasi">
										  Varanasi								 		 </option>
                            										 <option value="Vijaywada">
										  Vijaywada								 		 </option>
                            										 <option value="Visakhapatnam">
										  Visakhapatnam								 		 </option>
                                              </select>

</div>

<div class="clear"></div>
<div class="abttitle">
&nbsp;Trip Type:
<div style="float:right;width:292px; height:30px; margin-right:12px;">
<div class="booking-right">
 <label>
 <input type="radio" checked="checked" onclick="hide_trip_city();" value="One Way Trip" id="One_way" name="One_way">
  </label>
  <strong>One Way </strong> &nbsp;&nbsp;
   <label>
 <input type="radio" onclick="unhide_trip_city();" value="Round Trip" id="One_way" name="One_way">
  </label>
  <strong>Round Trip</strong></div>

</div>
</div>


<div class="clear"></div>
<div class="abttitle">
&nbsp;Arrival Time.:
<input  style="width:274px; margin-left:79px;" name="arrive" type="text" value="" id="inputField" class="date" globalnumber="643" onclick="hide_trip_city();">
</div>
<div class="clear"></div>
<div class="abttitle"  id="return_off" style="display:none;">
&nbsp;Departure Time.:
<!--<input type="text"  name="departure"  class="san"  value="" />-->

<input style="width:274px; margin-left:54px;" name="dept"  type="text" value="" id="inputField1" class="date" globalnumber="122" onclick="unhide_trip_city();">
</div>

<div class="clear"></div>
<div class="abttitle">
&nbsp;Name:
<input type="text"  name="name"  class="san"   value="" /></div>

<div class="clear"></div>
<div class="abttitle">
&nbsp;Email Id:
<input type="text"  name="email"  class="san"   value="" />
</div>

<div class="clear"></div>
<div class="abttitle">
&nbsp;Mobile No.:
<input type="text"  name="mobile"  class="san"   value="" />
</div>
<div class="clear"></div>
<div align="right" style="padding:15px;">
<input type="submit" value="Submit" name="submit" class="but">
</div>
    {{Form::close()}}
</div>
</div>
</div>
@stop

@section('js')

<script type="text/javascript">
function hide_trip_city()
{

    //document.getElementById('flt_to_cty').style.display = "none";
    document.getElementById('return_off').style.display = "none";
    document.getElementById('vali_city').style.display = "none";
}
function unhide_trip_city()
{
    //document.getElementById('flt_to_cty').style.display = "block";
    document.getElementById('return_off').style.display = "block";
    document.getElementById('vali_city').style.display = "block";
}
</script>
@stop