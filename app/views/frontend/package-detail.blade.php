@extends('layout.frontend-v3')

@section('css')

{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
<style>
button.nt-btn {
    padding: 7px 15px;
    text-align: center;
    font-size: 16px;
    border-radius: 4px;
    display: block;
    margin: 10px 0px;
}

.tabs-inner ul.pkg-tabs li{
    padding : 5px 41px;
}
    
.content-hide{
    display: none;
}
/*    custom loader css*/
    
.loading{
  display: none;
}
.loader{
    text-align: center;
    margin-top: 50px;
}    
</style>
@stop


@section('content')

<div class="loader" text-center>Loading 
    <img src="http://sampsonresume.com/labs/pIkfp.gif">
</div> 

<div class="container" ng-app="PackageDetails">
    <div id="content" class="row content-hide" ng-controller="PackageController">
      <div class="col-lg-12 breadcrumb"> 
            <a href="{{ URL::to('/') }}" class="active"> Home ></a> 
            <a href="javascript:;" class="active"> Holidays ></a> 
            <span> {{ $package->title }}</span> 
      </div>
      
      <!--packages row-->
      <div class="clearfix"></div>
        
      <div class="col-lg-12 prod-wrapper push-top-20">
        <div class="clearfix pkg-top-info">
          <div class="pkg_details col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <h1 class="page-header" style="margin:0;">{{ $package->title  }}</h1>
            <p class="clearfix ">{{ $package->duration }}</p>
            <p class="clearfix tour-info">{{ $package->route_map }}</p>
            <p class="desc text-12"> <span class="clearfix"></span> </p>
          </div>
        </div>
        <div class="prod-row">
          <div class="inner-1">
            <div class="clearfix prod-listing-row">
              <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 listing_otr clearfix">
                <div class="pkg-slider hidden-xs clearfix">
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner img-rounded">
                        
                        <?php $count=1; ?>
                       @foreach($package->package_images as $image)

                            <div class='item @if($count==1) active @endif'>
                                @if(!empty($image->image))
                                <img   src="{{ asset($image->image) }} " alt="image" style="width:825px; height:400px;">
                                @else
                                <img src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}"alt="image" style="width:800px;height:400px;">
                                @endif
                            </div>

                        <?php $count = $count + 1; ?>
                        @endforeach
                    </div>
                      
                    <!-- Controls --> 
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a> <a class="right carousel-control" href="#carousel-example-generic" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a> </div>
                </div>
                <div class="pkg-overview clearfix">
                  @if(!empty($package->desc))
                    <h2 class="section-title">Overview </h2>
                    <p class="">{{ $package->desc }}</p>
                  @endif  
                </div>
                  
                <div id="pkg-info-tabs" class="pkg-info-tabs panel hidden-xs clearfix">
                  <div class="tabs-inner">
                    <ul class="info-tabs static-tabs pkg-tabs list-inline">
                      <li><a href="#inclusions">Inclusions</a></li>
                      <li><a href="#itenerary">Itinerary</a></li>
                      <li><a href="#hotel-details">Hotel Details</a></li>
                      <li><a href="#price-summery">Price Summary</a></li>
                      <li><a href="#other-info">Other Info</a></li> 
                    </ul>
                  </div>
                </div>
                <!--/.pkg-info-tabs-->
                
                <div id="inclusions" class="pkg-inclusion clearfix">
                  <h2 class="section-title"><i class="fa fa-list-ul"></i>Inclusions </h2>
                  <div class="incl-list panel">
                    <div class="panel-body">
                      <ul class="list-bullet">
                        @foreach($package->package_inclusions as $inclusion) 
                        <li>{{ $inclusion->title }} <span class="text-12 fade-color"> - {{ $inclusion->desc }} </span></li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                <div id="itenerary" class="pkg-itinerary clearfix">
                  <h2 class="section-title"><i class="fa fa-map-marker"></i>Itinerary </h2>
                    
                  <div class="panel">
                    @foreach($package->package_itineraries as $itineraries) 
                        <div class="itry-row clearfix">
                        	<div class="itry-heading clearfix">
                            	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 pull-left orange-fill itry-day inner-pad">{{$itineraries->day_num}}</div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 pull-left gray-fill itry-day-info inner-pad">{{$itineraries->destination}}</div>
                            </div><!--/.itry-heading-->
                            
                            <div class="prod-listing-top listing_otr clearfix panel-body">
                            	<div class="pkg_image col-lg-3 col-md-3 col-sm-3 col-xs-3 hidden-xs row">
                                    @if(!empty($itineraries->image))
                                    <img class="img-responsive" src="{{ asset($itineraries->image) }}" /></div>
                                    @else
                                    <img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" /></div>
                                    @endif
                                <div class="hotel_details col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    
                                     <p class="desc text-12">
                                      {{$itineraries->desc}}
                                     </p>
                                    
                                     @if(!empty($itineraries->meal))
                                     <?php  $meal = explode(',',$itineraries->meal); ?>
                                    
                                     <ul class="pkg-incl food-incl list-inline">
                                        @if(in_array('Breakfast',$meal))
                                        <li><i class="fa fa-cutlery"></i><span class="text-12" style="margin-left:10px;">Breakfast</span></li>
                                        @endif
                                         
                                        @if(in_array('Lunch',$meal)) 
                                        <li><i class="fa fa-cutlery"></i><span class="text-12" style="margin-left:10px;">Lunch</span></li>
                                        @endif
                                         
                                        @if(in_array('Dinner',$meal)) 
                                        <li><i class="fa fa-cutlery"></i><span class="text-12" style="margin-left:10px;">Dinner</span></li>
                                        @endif 
                                    </ul>
                                    @endif
                                </div>
                            </div>
                          </div><!--/.itry-row-->
                    @endforeach    
                      
                  </div>
                  <!--/.inner--> 
                  
                </div>
                  
                <div id="hotel-details" class="pkg-hotels clearfix">
                  <h2 class="section-title"><i class="fa fa-building"></i>Hotel Details </h2>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel bhoechie-tab-container">
                      
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 bhoechie-tab-menu">
                        <div class="list-group" ng-repeat="(key,value) in hotels_details">
                            <a href="javascript:;" ng-click="ClassActive($event,key)" 
                                class="list-group-item text-center" 
                                ng-class="{active:selected_index == key}"
                                ng-if="value.destination" 
                                ng-repeat="(k,v) in value.destination">
                                <h4><% v.label %></h4>
                            </a>
                        </div>
                    </div>
                      
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 bhoechie-tab"> 
                        
                          <!-- flight section -->
                          <div class="bhoechie-tab-content"
                               ng-class="{active:selected_index == key}" id="tab<% key %>" 
                               ng-repeat="(key,value) in hotels_details track by $index">
                              
                            <div class="row" ng-if="value.hotels[0]">
                              <div class="pkg_image col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-left">
                                  <img class="img-responsive" ng-if="value.hotels[0].main_image" ng-src="/<% value.hotels[0].main_image %>">
                                  <img class="img-responsive" ng-if="!value.hotels[0].main_image" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}"> 
                              </div>
                              <div class="hotel-desc col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left">
                                <p class="clearfix hotel-name">
                                    <% value.hotels[0].title %>
                                    <span class="hotel-star">
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star-empty"></i> 
                                    </span>
                                        <!--/.hotel-star--> 
                                </p>
<!--
                                <div class="tripadvisor-rating"> 
                                    <img src="{{ asset('theme/front-v4/images/ta-rating.png') }}" /> 
                                </div>
                                <a   view-hotel class="view-hotel-detail btn btn-default" >View hotel details</a> 
-->
                              </div>
                            </div>
                              
                            <center class="pkg_image col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <p class="text-18">
                                  <img class="img-responsive" src="{{ asset('theme/front-v4/images/or-divider.png') }}" alt="OR" />
                              </p>
                            </center>
                              
                            <div class="row" ng-if="value.hotels[1]">
                              <div class="pkg_image col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-left"> 
                                   <img class="img-responsive" ng-src="/<% value.hotels[1].main_image %>">
                                   <img class="img-responsive" ng-if="!value.hotels[1].main_image" src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}"> 
                              </div>
                              <div class="hotel-desc col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left">
                                <p class="clearfix hotel-name"><% value.hotels[1].title %> 
                                    <span class="hotel-star"> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star"></i> 
                                        <i class="glyphicon glyphicon-star-empty"></i> 
                                    </span>
                                    <!--/.hotel-star--> 
                                </p>
<!--
                                <div class="tripadvisor-rating"> <img src="{{ asset('theme/front-v4/images/ta-rating.png') }}" /> </div>
                                <a  view-hotel class="view-hotel-detail btn btn-default">View Hotel Details</a> 
-->
                                </div>
                            </div>
                      </div>
                        
                    </div>
                  </div>
                  
                  <!--Hotel Details Popup Start-->
                  <div class="hotel-detail-popup col-lg-10 col-md-10 col-sm-10 col-xs-10 clearfix">
                    <div class="close"><i class="fa fa-close close2"></i></div>
                    <div class="inner">
                      <p class="clearfix hotel-name-big">Hotel Shimla View</p>
                      <div class="hotel-star"> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star"></i> <i class="glyphicon glyphicon-star-empty"></i> </div>
                      <!--/.hotel-star-->
                      
                      <div class="tripadvisor-rating"> <img src="images/ta-rating.png" /> </div>
                      <div class="hotel-gallery clearfix"> </div>
                      <!--hotel gallery-->
                      
                      <div class="hotel-overview">
                        <h4>Hotel Overview</h4>
                        <strong>Location:</strong>
                        <p>Located in the heart of Srinagar, in the picturesque land of Kashmir, The Pride Inn is a short drive from City Centre of Lal Chowk.</p>
                        <strong>Hotel Facilities:</strong>
                        <p>Wi-Fi, wireless internet access, tour and travel arrangements, car parking, Shikara facility (on demand)</p>
                        <strong>Room Amenities</strong>
                        <p>Wooden furniture, running hot and cold water (24-hour), laundry service, room service, cable television, telephone</p>
                        <strong>Dining</strong>
                        <p>Tulip: A multi cuisine restaurant that offers Continental, Indian, Mughlai and Chinese dishes</p>
                        <p><i class="fa fa-wifi" style="margin-right:5px;"></i>Internet: Available | <i class="fa fa-car" style="margin-right:5px;"></i>Parking: Available</p>
                      </div>
                    </div>
                  </div>
                  <!--Hotel Details Popup Ends--> 
                  
                </div>
                <!--/.pkg-hotels-->
                
                <div id="price-summery" class="price-sum clearfix">
                  <h2 class="section-title"><i class="fa fa-inr"></i>Price Summary </h2>
                  <div class="incl-list panel"> 
                        <div class="incl-list panel">
                            <div class="panel-body">
                              <ul class="list-bullet">
                                <li> Price Per Person 
                                    <span class="text-12 fade-color" ng-if="!hotels_details.discounted_price"> - <% hotels_details.price_per_person %>  </span>
                                    <span class="text-12 fade-color" ng-if="hotels_details.discounted_price"> - <% hotels_details.discounted_price %>  </span>
                                 </li>
                                <li> Extra adult <span class="text-12 fade-color"> - <% hotels_details.extra_adult %> </span></li>
                                <li> Child with bed <span class="text-12 fade-color"> - <% hotels_details.child_with_bed %> </span></li>
                                <li> infant <span class="text-12 fade-color"> - <% hotels_details.infant %>  </span></li>
                              </ul>
                            </div>
                        </div>
                              
                  </div>
                </div>
                <!--/.price-sum-->
                
                <div id="other-info" class="other-info clearfix">
                  <h2 class="section-title"><i class="fa fa-file-text"></i>Other Info</h2>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 panel bhoechie-tab-container">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu-2">
                      <div class="list-group"> 
                        <a href="#" class="list-group-item active text-center">
                        <h4>Terms & Conditions</h4>
                        </a> 
                        <a href="#" class="list-group-item text-center">
                        <h4>Cancellation Policy</h4>
                        </a> 
                        <a href="#" class="list-group-item text-center">
                        <h4>FAQs</h4>
                        </a> 
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-2"> 
                      <!-- flight section -->
                      <div class="bhoechie-tab-content-2 active">
                          {{ $package->terms_conditions }}
                      </div>
                      <!-- train section -->
                      <div class="bhoechie-tab-content-2">
                        {{ $package->policy }}
                      </div>
                      
                      <!-- hotel search -->
                      <div class="bhoechie-tab-content-2" >
                          {{ $package->faq }}
<!--
                        <ul class="tnc list-bullet text-12">
                          <li></li>
                        </ul>
-->
                      </div>
                    </div>
                  </div>
                </div>
                <!--/.other-info--> 
                
              </div>
              <!--/.listing_top--> 
              
              <!--Price Right Floating-->
              
              <div id="floating-price" class="panel floating-price price-box inner col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-left pull-left-xs price_box_wrapper hidden-xs" style="padding: 0;">
                <div class=""> 
                  <!--Check Price update -->
                  <div class="check-update-price"> <span class="text-11">Hotel Category</span>
                    <div class="styled-select-2">
                       <select class="selectbox styled-select-2" ng-model="hotel_category" 
                               ng-change="update_package_category($event, hotel_category)">
                        <option ng-repeat="(key,value) in package.category"><% value %></option>
                       </select>
                    </div>
                  </div>
                  <div class="check-update-price"> <span class="text-11">Valid From</span>
                    <div class="styled-select-2">
                      <select class="selectbox styled-select-2"
                              ng-model="hotel_date"  ng-change="update_hotel($event)">
                        <option ng-repeat="(key,value) in data"><% value %></option>
                      </select>
                    </div>
                  </div>
                  <!--Check Price update -->
                  
                  <div class="nt-get clearfix box-inner box-inner-2"> <span class="nt-get">
                    
                      <div ng-if="hotels_details.discounted_price">
                        <div>
                        <p class="was-price">
                          <span class="INR">
                            <span class="fa fa-inr"></span>
                          </span>
                          <% Math.round(hotels_details.price_per_person) || 0%>
                        </p>
                        </div>

                        <div>
                            <p class="main-price">
                              <span class="INR">
                                <span class="fa fa-inr"></span>
                              </span>
                             <% Math.round(hotels_details.discounted_price) || 0%>
                            </p>
                        </div>
                    </div> 
                      
                    <div  ng-if="!hotels_details.discounted_price">
                        <p class="main-price">
                          <span class="INR">
                            <span class="fa fa-inr"></span>
                          </span>
                           <% Math.round(hotels_details.price_per_person) || 0 %> 
                        </p>
                    </div>
                      
                      
                    <div class="">
                      <p class="price_per hidden-xs text-12">(Price Per Person)</p>
                    </div>
                    </span> </div>
                  <!--<div class="cta box-inner-2"> <a href="#" class="nt-btn btn-gray">View Details</a> </div>-->
                  <div class="cta box-inner-2">
<!--					  <a href="#" class="nt-btn btn-orange">Book Now</a> </div>-->
					  <a href="javascript:;" class=" xhr-modal fresh nt-btn btn-orange" id="book" 
						 data-url="{{Url::route('package-book',[$package->id]) }}">Book Now</a> 
                </div>
                <!--/.floating-price--> 
                
              </div>
              <!--Price Right Floating Ends--> 
              
            </div>
          </div>
        </div>
        <!--/.prod-row--> 
      </div>
    </div>
    <!-- /.row --> 
</div>
    </div>
@stop

@section('js')

<script src="{{ asset('theme/front-v3/js/vertical_tabs.js') }}"></script>


<!--Popup script--> 
<script>
$(document).ready(function() {
        
        $(".search-again-btn").click(function(e) {
          $("body").append(''); $(".search-again-popup").slideToggle(); 
          //$(".overlay2").show();
          $(".close2").click(function(e) { 
          //$(".lb-popup").hide(); 
          }); 
        }); 

		 $('#loader').addClass('on'); 
		
    });
		 
</script> 
<!--Popup script end--> 


<!--Popup script Hotel Details--> 

<!--Popup script end--> 


<!--Fixed-->
<script>
    $(window).scroll(function(){
      if ($(this).scrollTop() > 360) {
          $('#floating-price').addClass('fixed');
      } else {
          $('#floating-price').removeClass('fixed');
      }
    });
</script>

<!--Fixed 2-->
<script>
    $(window).scroll(function(){
      if ($(this).scrollTop() > 1020) {
          $('#pkg-info-tabs').addClass('fixed-tabs tabs-scroll');
      } else {
          $('#pkg-info-tabs').removeClass('fixed-tabs tabs-scroll');
      }
    });
    
</script>

<script>
 var app = angular.module('PackageDetails',[]);

    
  app
  .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
  })
	 .controller('PackageController', function($scope,$http,$rootScope,$element){ 
    
      $scope.hotel_category = 'Standard';
      
      $scope.index = 0;
      
      $scope.Math = window.Math;
              
      $scope.package = JSON.parse(decodeURIComponent('{{ rawurlencode($package) }}'));
      
      $scope.class = '';
      
      var i = 0;
      
      angular.forEach($scope.package.category, function(v, k){   
            
          if(i == 0)
          {
             $scope.hotel_category = v;
          }
          i++;
      }); 
      
      $scope.update_hotel = function(e)
      {
          $http({
                url: '/package/hotel/search',
                method: "GET",
                params:{
                    'package_id':$scope.package.id,
                    'category':$scope.hotel_category,
                    'dates':$scope.hotel_date
                },
                }).success(function(data, status, headers, config) 
                { 
                   $scope.hotels_details = [];
              
                   $scope.hotels_details = data;
              
                   var k = 0;

                   angular.forEach($scope.hotels_details, function(value, key){
                       
                         if(k == 0)
                         {
                           $scope.selected_index = key;  
                         }
                         k++;    
                   });
                
                
                }).error(function(data, status, headers, config) {
                  $scope.status = status;
          });
      }
      
      $scope.update_package_category = function(e, hotel_category)
      {
            $http({
                  url: '/package/category/search',
                  method: "GET",
                  params:{
                          'package_id':$scope.package.id,
                          'category':$scope.hotel_category,
                          },
                  }).success(function(data) 
                  { 
                     $scope.data = data;   

                      var new_updated_date = 0;
              
                         angular.forEach($scope.data, function(v, k){   

                          if(new_updated_date == 0)
                          {
                             $scope.hotel_date = v;
                          }

                          new_updated_date++;
                           
                          $scope.update_hotel();

                      });
                
                    $('.loader').addClass('loading');
                    $('#content').removeClass('content-hide');
                  });
      }

      $scope.update_package_category();
    
      $scope.update_hotel();

      $scope.ClassActive = function(e,index)
      {
          $scope.selected_index = index;
          $scope.index = 1;
      }
      
     })
      .directive('viewHotel', function($http, $timeout) {
          return {
               restrict: 'A', 
               link: function(scope, element, attr) {

                   $(element).click(function(e) {
                      $("body").append(''); $(".hotel-detail-popup").slideToggle(); 
                      $(".overlay2").show();
                      $(".close2").click(function(e) { 
                      $(".hotel-detail-popup, .overlay2").hide(); 
                      }); 
                  }); 

               }
          };
  });
  
	
</script>
    <div class="overlay2"></div>

@stop