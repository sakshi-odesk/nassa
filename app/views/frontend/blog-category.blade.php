@extends('layout.frontend')

@section('css')
{{HTML::style('theme/front/css/jquery.bxslider.css')}}
 <style>
.main_right_category {
    padding-left: 15px;
    margin-bottom: 20px;
}
.main_right_category h1 {
    padding: 0px;
    margin: 0px;
    margin-bottom: 10px;
    padding-bottom: 7px;
    border-bottom: 2px #ff760c solid;
    color: #474747;
    font-size: 18px;
    font-weight: normal;
}
.main_right_category ul {
    padding: 0px;
    margin: 0px;
    list-style: none;
}
.main_right_category ul li a {
    font-size: 12px;
    text-decoration: none;
    color: #474747;
    line-height: 1.7;
}
.main_right_category ul li a:hover {
    color: #ff760c;
}
/*****************post********************/
.left_post {
    padding-left: 15px;
    margin-bottom: 20px;
}
.left_post ul {
    list-style: none;
    padding: 0px;
    margin: 0px;
    height: 34px;
    border-bottom: 2px #ff760c solid !important;
}

.left_post li.active{
    border-bottom: 0px #ff760c solid !important;
}
.left_post ul li {
    float: left;
    padding: 10px;
    cursor: pointer;
}
.left_post ul .active {
    background: #ff760c;
   /* border-bottom: 1px #ff760c solid !important; */
    color: #fff;
}
.l_post {
    margin-top: 10px;
}
.l_post .list {
    height: 75px;
    border-bottom: 1px #ddd solid;
    cursor: pointer;
}
.l_post .list .list_title {
    font-weight: bold;
    color: #474747;
}
.l_post .list .date_post {
    color: #474747;
}
.l_post .list:hover .list_title {
    color: #ff760c;
}
.tags li {
    float: left;
    padding: 5px;
    border: 1px #ddd solid;
    margin: 2px;
}
.tags li:hover {
    background: #FF760C;
}
.tags li:hover a {
    color: #fff!important;
}
.post_left {
    /*border:1px #ccc solid;*/
    height: auto;
    margin-top: 15px;
}
.post_left .posts {
    height: 400px;
/*    width: 325px;*/
    float: left;
    margin-left: 23px;
}
.post_left .imgs {
    background-size: 100%;
    height: 200px;
}
.post_left .post_titles {
    text-decoration: none;
    color: #FF760C;
    font-size: 20px;
    line-height: 1.5;
}
.post_des {
    color: #333333;
    line-height: 1.5;
}
.date_show {
    padding: 5px;
    background: #FF760C;
    color: #fff;
    font-weight: bold;
    float: left;
    margin-left: -10px;
}
</style>
@stop

@section('content')
<div class="blog_mid">
    <br>
    <br>
    <table cellspacing="0" cellpadding="0" border="0" width="100%" class="blog_table">
        <tbody>
            <tr>
              <td>
                <div class="search">
                    <div class="tag1">Blogs about {{ $blog_category->title }}</div>
                </div>
              </td>
            </tr>
            <tr>
                <td width="700px" valign="top">
<!--
                    <div class="bx-wrapper" style="max-width: 100%; margin: 0px auto;">
                        <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative;">
                            <ul class="slider" style="width: auto; position: relative;">
-->

                    <!-- items mirrored twice, total of 12 -->
<!--
                            </ul>
                        </div>
                        <div class="bx-controls bx-has-controls-direction">
                            <div class="bx-controls-direction">
                                <a href="" class="bx-prev">Prev</a>
                                <a href="" class="bx-next">Next</a>
                            </div>
                        </div>
                    </div>
-->

                    <div class="post_left">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          @foreach($blogs as $blog)
                            <tr>
                                <td>
                                    <div class="posts">
                                        <a href= "{{ URL::route('blog-view',array($blog->slug)) }}">
                                          <div class="imgs" style="background: url({{asset($blog->blog_image)}}) no-repeat;">
                                            <div class="corner"></div>
                                            <div class="date_show">
                                               {{ $blog->created_at }}
                                            </div>
                                          </div>
                                        </a>
                                        <a href="{{ URL::route('blog-view',array($blog->slug)) }}" class="post_titles">{{ $blog->title }}
                                        </a>
                                        <p class="post_des">{{ $blog->body }}</p>
                                    </div>
                                </td>
                            </tr>
                          @endforeach
                        </table>
                    </div>
                </td>

                <td valign="top">
                    <div class="main_right_category">
                        <h1>Category</h1>
                        <ul>
                            @foreach($blog_categories as $blog_category)
                            <li><a href="{{ URL::route('blog.show',array($blog_category->slug)) }}">{{ $blog_category->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="left_post">
                        <ul>
                            <li onclick="javascript:$('#aa2').css({'background':'#fff','color':'#000'});$('#aa1').css({'background':'#FF760C', 'color':'#fff'});" class="active" id="aa1">Latest Post
                            </li>


                            <li onclick="javascript:$('#aa2').css({'border-bottom':'1px #ff760c solid','background':'#ff760c','color':'#fff'});$('#aa1').css({'border-bottom':'4px #fff solid','background':'#fff','color':'#000'});" id="aa2">Popular Post
                            </li>
                        </ul>

                        <?php $blogs = Blog::orderBy('created_at')->take(5)->get();  ?>
                          <div id="latest_b" class="l_post">
                            @foreach($blogs as $blog)
                              <div class="list">
                                  <a href="{{ URL::route('blog-view',array($blog->slug)) }}">
                                    <img src= "{{ asset($blog->blog_image) }}" 
                                         height="60px" width="120px" style="float:left; margin-right:10px;" />
                                  </a>
                                  <a href=""><p class="list_title">{{ $blog->title }}</p></a>

                                  <span class="date_post">{{ $blog->created_at }}</span>
                              </div>
                            @endforeach
                          </div>
                          <div id="popular_b" class="l_post" style="display:none;">
                            @foreach($blogs as $blog)
                              <div class="list">
                                  <a href="{{ URL::route('blog-view',array($blog->slug)) }}"><img src="{{ asset($blog->blog_image) }}" 
                                                   height="60px" width="120px" style="float:left; margin-right:10px;" />
                                  </a>
                                  <a href="#"><p class="list_title">{{ $blog->title }}</p></a>
                                  <span class="date_post">{{ $blog->created_at }}</span>
                              </div>
                            @endforeach
                          </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
</div>
@stop

@section('js')
{{HTML::script('theme/front/js/jquery.min.js')}}
{{HTML::script('theme/front/js/jquery.easing.1.3.js')}}
{{HTML::script('theme/front/js/jquery.bxslider.min.js')}}
{{HTML::script('theme/front/js/css-pop.js')}}

<script>
    $(document).ready(function(){
        $('#aa1').click(function(){
            $('#latest_b').show();
            $('#popular_b').hide();
        });
        $('#aa2').click(function(){
            $('#latest_b').hide();
            $('#popular_b').show();
        });
    });
</script>

@stop