@extends('layout.frontend-v3')

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}

<link href="{{ asset('theme/front-v4/css/carousel.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('theme/front-v3/css/vertical_tabs.css') }}" rel="stylesheet">-->
<style>
.carousel{
    margin-bottom: 0;
    padding: 20px 30px;
}
/* The controlsy */
.carousel-control{
	left: -12px;
    height: 40px;
	width: 40px;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    margin-top: 155px;
}
.carousel-control.right{
	right: -12px;
}
/* The indicators */
.carousel-indicators{
	right: 50%;
	top: auto;
	bottom: -10px;
	margin-right: -19px;
}
/* The colour of the indicators */
.carousel-indicators li{
	background: #cecece;
	border: none;
}
.carousel-indicators .active{
background: #03497E;
}
.color{
    color:#444444;
}
.season-special-text{
    color: #ffffff;
}
.season-special-text:hover{
    color: #ffffff;
}    
	
</style>
@stop

@section('content')
<div class="banner-container top-widget-area push-bottom-20">
  <div class="container">
    <div class="row">
     <div class="col-lg-8 col-xs-12 form-widget-wrapper img-rounded">
       <h2 class="text-white">Plan Your Travel</h2>
          <div class="form-widget-inner img-rounded push-bottom-20">
              
            <!--Tabs Start-->
            <div class="form-nav-tabs">
              <div class="panel-heading">
                <ul class="nav nav-tabs nt-std-tab search-form-tab">
                  @if(!Input::get('redirect'))   
                  <li class="col-xs-3 text-center text-12 no-padding nav_new active" id="holidays"><a href="#tab3default" data-toggle="tab"><i class="fa fa-tree"></i>Holidays</a></li> 
                  @elseif(Input::get('redirect') == 'holidays')      
                  <li class="col-xs-3 text-center text-12 no-padding nav_new active" id="holidays"><a href="#tab3default" data-toggle="tab"><i class="fa fa-tree"></i>Holidays</a></li>                         @else
                  <li class="col-xs-3 text-center text-12 no-padding nav_new" id="holidays"><a href="#tab3default" data-toggle="tab"><i class="fa fa-tree"></i>Holidays</a></li>
                  @endif

                  @if(Input::get('redirect') == 'flights')
                  <li class="col-xs-3 text-center text-12 no-padding nav_new active"   id="flights"><a href="#tab1default" data-toggle="tab"><i class="fa fa-plane"></i>Flights</a></li>
                  @else
                  <li class="col-xs-3 text-center text-12 no-padding nav_new"   id="flights"><a href="#tab1default" data-toggle="tab"><i class="fa fa-plane"></i>Flights</a></li>
                  @endif

                  @if(Input::get('redirect') == 'hotels')
                  <li class="col-xs-3 text-center text-12 no-padding nav_new active" id="hotels"><a href="#tab2default" data-toggle="tab"><i class="fa fa-hotel"></i>Hotels</a></li>
                  @else
                  <li class="col-xs-3 text-center text-12 no-padding nav_new" id="hotels"><a href="#tab2default" data-toggle="tab"><i class="fa fa-hotel"></i>Hotels</a></li>
                  @endif

                  @if(Input::get('redirect') == 'transport')          
                  <li class="col-xs-3 text-center text-12 no-padding nav_new active" id="transport"><a href="#tab4default" data-toggle="tab"><i class="fa fa-car"></i>Transport</a></li>
                  @else
                  <li class="col-xs-3 text-center text-12 no-padding nav_new" id="transport"><a href="#tab4default" data-toggle="tab"><i class="fa fa-car"></i>Transport</a></li>
                  @endif
                  
                </ul>
              </div>

                                  <div class="main-form-wrapper clearfix" ng-app="HomeModule">
                                        <div class="tab-content clearfix">  
                                          @if(!Input::get('redirect')) 
                                          <div class="tab-pane fade in active" id="tab3default">
                                          @elseif(Input::get('redirect') == 'holidays') 
                                           <div class="tab-pane fade in active" id="tab3default">
                                          @else   
                                           <div class="tab-pane fade in" id="tab3default">
                                          @endif                                 
                                                
                                             {{ Form::open(array('action' => 'PackageCategoryController@getSearchPackage','method'=>'get')) }}
                                           <!--GOING TO OPTION-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix "> 
                                             	<div class="col-md-9 col-sm-8 col-xs-12">
                                                  <label>Going To</label>
<!--                                                  <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">-->
                                                    
                                              <input id="topic_title" type="text" placeholder="Please select your Destination" class="inputbox input-color input-block autowidth" required />                                                                                                   
                                                    <input id="destination_title" type="hidden" name="destination_id"> 
                                                </div>  
                                             </div>
                                             <!--/From / To Options-->
                                             
                                             <!--Date Options-->
                                                                                                                                     
                                             <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix"> 
                                             	<div class="col-xs-4">
                                                <label>Start Date</label>
                                                	 <div class="">
<!--                                                     <form class="form-horizontal">-->
                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                               <input type="text" name="start_date" id="start_date" class="form-control input-color"/> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>
<!--                                                     </form>-->
                                                  </div>
                                                </div>
                                                
                                                <div class="col-md-3 col-sm-3">
                                                    <label>End Date</label>
                                                    <div class="">
<!--                                                     <form class="form-horizontal">-->
                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                               <input type="text" name="end_date" id="end_date" class="form-control input-color" /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>
<!--                                                     </form>-->
                                                  </div>
                                                </div>
                                                
                                             </div>
                                           
                                              
                                             <!--Date Options-->
                                             
                                             
                                             <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
<!--                                             	<a class="search nt-btn btn-orange" href="#">Search</a>-->
                                               
                                                 <button type="submit" name="submit" class="search nt-btn btn-orange" onclick="valid_destination()">Search</button>
                                               
                                             </div>
                                             
                                             <div class=" divider clearfix"></div>
                                             	 
                                                 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 pull-right text-center">
                                                 	<p>Don't have a plan yet?</p>
                                                    <a class="nt-btn btn-green btn-block" href="#">I Wish to Explore all Deals</a>
                                                 </div>
                                           
                                              {{ Form::close() }}
                                           </div>
                                            
                                          <!--/.tab-pane--> 

                                          <!--FLIGHTS FORM SECTION-->
                                        
                                          @if(Input::get('redirect')=='flights')
                                          <div class="tab-pane fade in active" id="tab1default"  ng-controller="FlightController">
                                          @else
                                          <div class="tab-pane fade in" id="tab1default"  ng-controller="FlightController">
                                          @endif
								            {{ Form::open(array('action' => 'HomeController@postFlightSearch','method'=>'get')) }}
                                          	<div class="col-md-12 col-sm-12 col-xs-12 clearfix"> 
                                            <!--One/Round Way Options-->
                                              <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                                                  <div class="hidden-xs">
                                                    <a id="one-way-option-main" class="" tabindex="1">
                                                       <div class="radio">
                                                        <label style="font-size: 1em; padding:0;">
                                                          <input type="radio" name="type" ng-model="radiotype" value="One_way">
                                                          One Way
                                                          <span class="cr" ><i class="cr-icon fa fa-circle" ng-click="radio('One_way');" ></i></span>
                                                        </label>
                                                      </div>
                                                    </a>
                                                  </div><!--/.hidden-xs-->
                                              </div><!--/.one-way-box-->
                                              
                                              <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                                                  <div class="hidden-xs">
                                                    <a id="one-way-option-main" class="" tabindex="1">
                                                       <div class="radio">
                                                        <label style="font-size: 1em; padding:0;">
                                                          <input type="radio" name="type" ng-model="radiotype" value="Round_way">
                                                          Round Trip
                                                          <span class="cr"><i class="cr-icon fa fa-circle" ng-click="radio('Round_way');"></i></span>
                                                        </label>
                                                      </div>
                                                    </a>
                                                  </div><!--/.hidden-xs-->
                                              </div><!--/.one-way-box-->
                                              
                                             </div> 
                                             <!--/One/Round Way Options-->
                                             
                                             <!--From / To Options-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix"> 
                                             	<div class="col-md-5 col-sm-5">
                                                  <label>From</label>
<!--                                                  <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name" class=>-->
													<input id="flight_title" 
                                                         type="text" 
                                                         placeholder="Please select your Destination" 
                                                         class="inputbox input-color input-block autowidth" 
												         name="fcity" required>
<!--													 <input type="hidden" name="destination_id">-->
													
                                                 </div>
                                                 <div class="col-md-1 col-sm-1 hidden-xs" style="margin-top:30px;  max-width:20px; padding:0; text-align:center;"><i class="fa fa-exchange" style="vertical-align:middle; font-size:20px; color:#94a2c4;"></i></div>
                                                 <div class="col-md-5 col-sm-5">
                                                  <label>To</label>
<!--                                                  <input class="inputbox input-color input-block autowidth" type="text" placeholder="City Name">-->
													 <input id="flight_to_title" 
                                                         type="text" 
                                                         placeholder="Please select your Destination" 
                                                         class="inputbox input-color input-block autowidth" 
												        name="tcity" required> 
<!--													  <input type="hidden" name="destination_id">-->
                                                 </div>
                                             </div>
                                             <!--/From / To Options-->
                                             
                                             <!--Date Options-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix"> 
                                             	<div class="col-md-3 col-sm-3">
                                                    <label>Departure </label>
                                                    <div class="">
<!--                                                     <form class="form-horizontal">-->
                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
															   <input type="text" name="dept" id="Departure_date" class="form-control input-color" value="" /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>
<!--                                                     </form>-->

                                                  </div>
                                                </div>
                                                
                                                <div class="col-md-3 col-sm-3">
                                                    <label>Arrival </label>
                                                    <div class="">
<!--                                                     <form class="form-horizontal">-->
                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
															   <input type="text" id="Arrival_date" class="form-control input-color" value="" name="arrival"  /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>
<!--                                                     </form>-->
                                                  </div>
                                                </div>
                                                
                                             </div>
                                             <!--Date Options-->
                                             
                                             
                                             <!--People Options-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix"> 
                                             
                                             	<div class="col-xs-3">
                                                  <label class="text-12" >Adult (12+ yrs) </label>
                                                  <div class="input-group number-spinner">
                                                      <span class="input-group-btn data-dwn">
                                                          <button class="btn btn-default" data-dir="dwn" ng-click="decrement($event,'adult');">
															  <span class="fa fa-minus"name="adult"></span>
														  </button>
                                                      </span>
                                                      <input type="text" class="form-control text-center input-color"  min="1" max="7" name="adult" ng-model="adult" >
                                                      <span class="input-group-btn data-up">
                                                          <button class="btn btn-default" data-dir="up" ng-click="increment($event,'adult');">
															  <span class="fa fa-plus">
															  </span>
														  </button>
                                                      </span>
                                                  </div>
                                              </div>
                                              
                                              <div class="col-xs-3">
                                                <label class="text-12" >Children (0-12 yrs) </label>
                                                <div class="input-group number-spinner">
                                                    <span class="input-group-btn data-dwn">
                                                        <button class="btn btn-default" data-dir="dwn" ng-click="decrement($event,'children');" >
															<span class="fa fa-minus"></span>
														</button>
                                                    </span>
                                                    <input type="text" class="form-control text-center input-color" value="1" min="1" max="7" name="children" ng-model="children">
                                                    <span class="input-group-btn data-up">
                                                        <button class="btn btn-default" data-dir="up" ng-click="increment($event,'children');">
															<span class="fa fa-plus"></span>
														</button>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-3">
                                                <label class="text-12" >Infant (0-2 yrs) </label>
                                                <div class="input-group number-spinner">
                                                    <span class="input-group-btn data-dwn">
                                                        <button class="btn btn-default" data-dir="dwn" ng-click="decrement($event,'infant');">
															<span class="fa fa-minus"></span>
														</button>
                                                    </span>
                                                    <input type="text" class="form-control text-center input-color" value="1" min="1" max="7" name="infant" ng-model="infant">
                                                    <span class="input-group-btn data-up">
                                                        <button class="btn btn-default" data-dir="up" ng-click="increment($event,'infant');">
															<span class="fa fa-plus"></span>
														</button>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <label class="text-12">Class</label>
                                                <div class="styled-select">
                                                  <select class="selectbox styled-select" name="class">
                                                  <option>Economy</option>
                                                  <option>Business</option>
                                                  </select>
                                                </div>
                                            </div>
                                                
                                                
                                             </div>
                                             <!--People Options-->
                                             
                                             <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
<!--                                             	<a class="search nt-btn btn-orange" href="#">Search</a>-->
												 <button type="submit" name="save" class="search nt-btn btn-orange">Submit</button>
                                             </div>
                                            {{ Form::close() }}
                                          </div><!--/.tab-pane-->    

                                          <!--HOTEL FORM SECTION-->
                                            @if(Input::get('redirect') == 'hotels')
                                            <div class="tab-pane fade in active" id="tab2default" ng-controller="HotelController">
                                            @else
                                            <div class="tab-pane fade in" id="tab2default" ng-controller="HotelController">
                                            @endif
                                            {{ Form::open(array('action' => 'HomeController@getHotelSearch','method'=>'get')) }}
                                               
                                           <!--GOING TO OPTION-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix"> 
                                             	<div class="col-md-9 col-sm-8 col-xs-12">
                                                  <label>Going To</label>
                                                  <input id="hotels_title" class="inputbox input-color input-block autowidth" type="text" name="gcity" placeholder="Please select your City" required>
                                                 </div>
                                                 
                                             </div>
                                             <!--/From / To Options-->
                                             
                                             <!--Date Options-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix"> 
                                             	<div class="col-md-3 col-sm-3">
                                                    <label>Check in </label>
                                                    <div class="">

                                                       <fieldset>
                                                       <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                              <input type="text" id="checkin_date" class="form-control input-color active" value="" name="checkin" /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>
                                                 </div>
                                                </div>
                                                
                                                <div class="col-md-3 col-sm-3">
                                                    <label>Check out</label>
                                                    <div class="">
                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                             <input type="text" id="checkout_date" class="form-control input-color" value="" name="checkout"  /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>
                                                    </div>
                                                </div>
                                                
                                             </div>
                                             <!--Date Options-->
                                             
                                             
                                             <!--People Options-->
                                              <div class="col-md-12 col-sm-12 clearfix room-info box-divider">
                                            
                                                <div class="col-md-1 col-sm-1 room-count">
                                                    <div class="row room-title">
                                                      Room &nbsp;<span>1</span>
                                                     </div>
                                                </div>
                                                
                                                <div class="col-xs-3">
                                                    <label class="text-12">Adult (12+ yrs) </label>
                                                    <div class="input-group number-spinner">
                                                        <span class="input-group-btn data-dwn">
                                                            <button class="btn btn-default" data-dir="dwn" ng-click="decrementval($event,'adult1')";>
                                                                    <span class="fa fa-minus" name="adult1"></span>
                                                            </button>
                                                        </span>
                                                        <input type="text" class="form-control text-center input-color"  value="1" min="1" max="7" name="adult1" ng-model="adult1">
                                                        <span class="input-group-btn data-up">
                                                            <button class="btn btn-default" data-dir="up" ng-click="incrementval($event,'adult1')";>
                                                              <span class="fa fa-plus"></span></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xs-3">
                                                  <label class="text-12">Children (0-12 yrs) </label>
                                                  <div class="input-group number-spinner">
                                                      <span class="input-group-btn data-dwn">
                                                          <button class="btn btn-default" data-dir="dwn" ng-click="decrementval($event,'children1')";>
                                                            <span class="fa fa-minus"></span></button>
                                                      </span>
                                                      <input type="text" class="form-control text-center input-color" value="1" min="1" max="7" name="children1" ng-model="children1">
                                                      <span class="input-group-btn data-up">
                                                          <button class="btn btn-default" data-dir="up" ng-click="incrementval($event,'children1')";>
                                                            <span class="fa fa-plus"></span></button>
                                                      </span>
                                                  </div>
                                              </div>
                                                    
                                            </div>
                                             <!--People Options-->
<!--
                                             
                                             <div class="col-md-12 col-sm-12 clearfix">
                                                <div class="col-md-3 col-sm-3">
                                                    <a href="#" class="add-room btn nt-btn btn-default">Add Room <i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
-->
                                             
                                             <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                             	 <button type="submit" name="save" class="search nt-btn btn-orange">Submit</button>
                                             </div>
                                                {{ Form::close() }}
                                          </div>    
                                            
                                          <!--Car/Bus/Volvo FORM SECTION-->
                                          @if(Input::get('redirect') == 'transport')      
                                          <div class="tab-pane fade in active" id="tab4default" ng-controller="TransportController">
                                          @else
                                          <div class="tab-pane fade in" id="tab4default" ng-controller="TransportController">
                                          @endif
											  {{ Form::open(array('action' => 'HomeController@postTransportSearch','method'=>'post'))}}
                                          	<div class="col-md-12 col-sm-12 col-xs-12 clearfix"> 
                                            <!--One/Round Way Options-->
                                              <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                                                  <div class="hidden-xs">
                                                    <a id="one-way-option-main" class="" tabindex="1">
                                                       <div class="radio">
                                                        <label style="font-size: 1em; padding:0;">
                                                          <input type="radio" name="radioway" ng-model="radioway" value="One_way">
                                                          One Way
                                                          <span class="cr"><i class="cr-icon fa fa-circle" ng-click="radiotransport('One_way');"></i></span>
                                                        </label>
                                                      </div>
                                                    </a>
                                                  </div><!--/.hidden-xs-->
                                              </div><!--/.one-way-box-->
                                              
                                              <div id="one-way-box" class="col-sm-2 col-xs-6 no-padding">
                                                  <div class="hidden-xs">
                                                    <a id="one-way-option-main" class="" tabindex="1">
                                                       <div class="radio">
                                                        <label style="font-size: 1em; padding:0;">
                                                          <input type="radio" name="radioway" ng-model="radioway" value="Round_way">
                                                          Round Trip
                                                          <span class="cr"><i class="cr-icon fa fa-circle" ng-click="radiotransport('Round_way');"></i></span>
                                                        </label>
                                                      </div>
                                                    </a>
                                                  </div><!--/.hidden-xs-->
                                              </div><!--/.one-way-box-->
                                              
                                             </div> 
                                             <!--/One/Round Way Options-->
                                             
                                             <!--From / To Options-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 push-bottom-10 row clearfix"> 
                                             	<div class="col-md-5 col-sm-5">
                                                  <label>Picking Up</label>
                                                  <input class="inputbox input-color input-block autowidth" type="text" placeholder="Please select your Destination"name="picking" id="transport_to_title" required>
                                                 </div>
                                                 <div class="col-md-1 col-sm-1 hidden-xs" style="margin-top:30px;  max-width:20px; padding:0; text-align:center;"><i class="fa fa-exchange" style="vertical-align:middle; font-size:20px; color:#94a2c4;"></i></div>
                                                 <div class="col-md-5 col-sm-5">
                                                  <label>Dropping Off</label>
                                                  <input class="inputbox input-color input-block autowidth" type="text" placeholder="Please select your Destination" name="dropping" id="transport_title">
                                                 </div>
                                             </div>
                                             <!--/From / To Options-->
                                             
                                             <!--Date Options-->
                                             <div class="col-md-12 col-sm-12 col-xs-12 row push-bottom-10 clearfix"> 
                                             	<div class="col-md-3 col-sm-3">
                                                    <label>Departure</label>
                                                    <div class="">

                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
															   <input type="text" name="dept"  id="Departure1_date" class="form-control input-color" value="" /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>


                                                  </div>
                                                </div>
                                                
                                                <div class="col-md-3 col-sm-3">
                                                    <label>Arrival </label>
                                                    <div class="">

                                                       <fieldset>
                                                        <div class="control-group">
                                                          <div class="controls">
                                                           <div class="input-prepend input-group">
                                                             <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
															   <input type="text" name="arrival"  id="Arrival1_date" class="form-control input-color" value="" /> 
                                                           </div>
                                                          </div>
                                                        </div>
                                                       </fieldset>

                                                  </div>
                                                </div>
                                                
                                             </div>
                                             <!--Date Options-->
                                             
                                             <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
<!--                                             	<a class="search nt-btn btn-orange" href="#">Send Enquiry</a>-->
												 <button type="submit" class="search nt-btn btn-orange">Send Enquiry</button>
                                             </div>
                                              {{ Form::close() }}
                                          </div>
                                            
                                        </div>
                                  </div>
                            </div>
                        <!--Tabs Ends-->
                    </div>
                  </div>
                 <div class="top-widget-banner-right col-lg-4 hidden-md hidden-sm hidden-xs">
                  <div class="top-widget-banner-1 push-bottom-20">
                      
					 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="padding:0px;">
						 <ol class="carousel-indicators">
                          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
						 <div class="carousel-inner img-rounded" >
							<?php $count=1; ?>
							 @foreach($rafting as $slides1)
							 <div class='item @if($count==1) active @endif'>
								 @if(!empty($slides1->main_image))
								 <a href="{{ $slides1->url }}" target="_blank">
								 <img src="{{ asset($slides1->main_image) }}" style="height:288px; width:344px;"/>
									 </a>
									 @else
								  <img src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" style="height:288px; width:344px;"/>
								 @endif
							 </div>
							 <?php $count = $count + 1;?>
							@endforeach
						 </div>
					  </div>
				  </div>
                     
                  <div class="top-widget-banner-2"> 
                      
                    @if(count($honeymoon_special) > 1)
                    <div id="carousel-example-generic-honeymoon" class="carousel slide" data-ride="carousel" style="padding:0px;">
						 <ol class="carousel-indicators">
                          <li data-target="#carousel-example-generic-honeymoon" data-slide-to="0" class="active"></li>
                          <li data-target="#carousel-example-generic-honeymoon" data-slide-to="1"></li>
                          <li data-target="#carousel-example-generic-honeymoon" data-slide-to="2"></li>
                        </ol>
						 <div class="carousel-inner img-rounded" >
							<?php $count=1;?>
							 @foreach($honeymoon_special as $slides)
							 <div class='item @if($count==1) active @endif'>
								 @if(!empty($slides->main_image))
								 <a href="{{ $slides->url }}" target="_blank">
								 <img src="{{ asset($slides->main_image) }}" style="height:152px; width:344px;"/>
								 </a>
								 @else
                                 <a href="{{ $slides->url }}" target="_blank">
								    <img src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" style="height:288px; width:344px;"/>
                                 </a>
								 @endif
							 </div>
							 <?php $count = $count + 1;?>
							@endforeach
						 </div>
				    </div>
                    @else
                        @foreach($honeymoon_special as $slides)
                        <a href="{{ $slides->url }}" target="_blank">
                		  <img class="img-responsive" src="{{ asset($slides->main_image) }}" />
                        </a>
                        @endforeach
                    @endif
                      
<!--                      <img src="{{ asset('theme/front-v4/images/widget_sliders/top_widget_banner_2/top-widget-banner-2.jpg') }}"/> -->
                  </div>
                     
                </div>
                  
              </div>
              <!-- /.row -->
          </div>
          <!-- /.container -->
            
      </div><!--/.top-widget-area-->
      
<div class="body-container clearfix">
      	<div class="container container-fluid">
            
        	<div class="row">
            	<div class="col-lg-4 col-md-6 col-sm-6">
                	<h2 class="section-title">Season's Special</h2>
                    <div class="box-rd">
						<div id="carousel-example-generic-special" class="carousel slide" data-ride="carousel" style="padding:0px;">
                             <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic-special" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic-special" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic-special" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner img-rounded" >
                                <?php $count=1; ?>
                                @foreach($season_special as $slides)
                                <div class='item @if($count==1) active @endif'>
                                    @if(!empty($slides->main_image))
                                    <a href="{{ $slides->url }} " target="_blank" >
                                     <img src="{{ asset($slides->main_image) }} " alt="image" style="width:360px; height:255px;" >
                                    </a>
                                    @else
                                     <img src="{{ asset('theme/front-v4/images/img-placeholder.jpg') }}"  style="width:360px; height:255px;" >
                                    @endif
                                    <div class="carousel-caption">
                                        <a href="{{ $slides->url }}" class="season-special-text" target="_blank"><h2>{{ $slides->title}}</h2></a>
                                    </div>

                                </div>
                                <?php $count = $count + 1;?>
                                @endforeach

                            </div>
				            <!-- Controls -->
						</div>
			         </div>
				</div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                	<h2 class="section-title">Hot Picks!</h2>
                	<div class="list-display hot-picks panel box-rd">
                    	
                        <div class="with-nav-tabs">
                          <div class="panel-heading">
                            <ul class="nav nav-tabs nt-std-tab">
                                <li class="active col-xs-4 text-center no-padding"><a class="nt-tab" href="#hotpickdomestic" data-toggle="tab">Domestic</a></li>
                                <li class="col-xs-4 text-center no-padding"><a class="nt-tab" href="#hotpickint" data-toggle="tab">International</a></li>
                                <li class="col-xs-4 text-center no-padding"><a class="nt-tab" href="#hotpickflthotel" data-toggle="tab">Flight + Hotel</a></li>
                            </ul>
                          </div>
                          <div class="panel-body hot-picks">
                              <div class="tab-content">
                                   
                                  <div class="tab-pane fade in active" id="hotpickdomestic">
                                      
                                      @foreach($domestic_hot_picks_packages as $domestic)
                                      <a href="{{URL::route('packages-details',array($domestic->slug))}}">
                                      <div class="clearfix list-group-item">
                                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <div class="img-box-sml1">
                                                @if(!empty($domestic->main_image))
                                                <img class="img-responsive" src="{{ asset($domestic->main_image) }}" />
                                                @else
                                                <img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                                                @endif
                                            </div>
                                          </div>
                                          <div class="pull-left">
                                              <a class="color" href="{{URL::route('packages-details',array($domestic->slug))}}">
                                                  @if(strlen($domestic->title) > 20)
                                                  <p class="text-16">{{substr($domestic->title,0,15)}}...</p>
                                                  @else
                                                  <p class="text-16">{{ $domestic->title }}</p>
                                                  @endif
                                                  <p class="text-12" style="margin-bottom:5px;">{{ $domestic->duration }}</p>
                                                  <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i> {{ round($domestic->min_price) }}/- onwards</p>
                                              </a>
                                          </div>
                                        </div>
                                        </a>
                                      @endforeach
                                    
                                  </div><!--/tab1-->
                                  
                                  <div class="tab-pane fade" id="hotpickint">
                                        @foreach($international_hot_picks_packages as $international)
                                         <a href="{{URL::route('packages-details',array($international->slug))}}">
                                            <div class="clearfix list-group-item">
                                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="img-box-sml1">
                                                    @if(!empty($international->main_image))
                                                    <img class="img-responsive" src="{{asset($international->main_image)}}" />
                                                    @else
                                                    <img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                                                    @endif
                                                </div>
                                              </div>
                                              <div class="pull-left">
                                                  <a class="color" href="{{URL::route('packages-details',array($international->slug))}}">
                                                      <p class="text-16">{{substr($international->title,0,50)}}</p>
                                                      <p class="text-12" style="margin-bottom:5px;">{{ $international->duration }}</p>
                                                      <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i> {{ round($international->min_price) }}/- onwards</p>
                                                  </a>      
                                              </div>
                                            </div>
                                        </a> 
                                        @endforeach            
                          
                                  </div><!--/tab2-->
                                  
                                  <div class="tab-pane fade" id="hotpickflthotel">
                                        @foreach($hotels_hot_picks as $hotel)
                                        <a href="{{URL::route('hotel-details',array($hotel->slug))}}">
                                            <div class="clearfix list-group-item">
                                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="img-box-sml1">
                                                    @if(!empty($hotel->main_image))
                                                    <img class="img-responsive" src="{{ asset($hotel->main_image) }}" />
                                                    @else
                                                    <img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                                                    @endif
                                                </div>
                                              </div>
                                              <div class="pull-left">
                                                <a class="color" href="{{URL::route('hotel-details',array($hotel->slug))}}">        
                                                   <p class="text-16">{{substr($hotel->title,0,50)}}</p>
                                                   <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i> {{ round($hotel->price) }}/- onwards</p>
                                                </a>    
                                              </div>
                                            </div>
                                        </a>
                                        @endforeach  
                                  </div><!--/tab3-->
                                  
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                	<h2 class="section-title">Travel Guide</h2>
                    
                    @if(count($travel_guide) > 1)
                    <div id="carousel-example-generic-travel" class="carousel slide" data-ride="carousel" style="padding:0px;">
						 <ol class="carousel-indicators">
                          <li data-target="#carousel-example-generic-travel" data-slide-to="0" class="active"></li>
                          <li data-target="#carousel-example-generic-travel" data-slide-to="1"></li>
                          <li data-target="#carousel-example-generic-travel" data-slide-to="2"></li>
                        </ol>
						 <div class="carousel-inner img-rounded" >
							<?php $count=1;?>
							 @foreach($travel_guide as $slides)
							 <div class='item @if($count==1) active @endif'>
								 @if(!empty($slides->main_image))
								 <a href="{{ $slides->url }}" target="_blank">
								    <img src="{{ asset($slides->main_image) }}" style="height:246px; width:360px;"/>
								 </a>
								 @else
                                 <a href="{{ $slides->url }}" target="_blank">
								    <img src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" style="height:246px; width:360px;"/>
                                 </a>
								 @endif
							 </div>
							 <?php $count = $count + 1;?>
							@endforeach
						 </div>
				    </div>
                    @else
                    <div class="box-rd">
                        @foreach($travel_guide as $slides)
                        <a href="{{ $slides->url }}" target="_blank">
                		  <img class="img-responsive" src="{{ asset($slides->main_image) }}" />
                        </a>
                        @endforeach
                    </div>
                    @endif
                </div>
                
            </div>
            
            <div class="row push-bottom-20">
                <div class="recmd-hotels col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <h2 class="section-title">Recommended Hotels</h2>
                  <div class="panel">
                    <div class="carousel slide" id="Carousel">
                      <ol class="carousel-indicators">
                        <li class="" data-slide-to="0" data-target="#Carousel"></li>
                        <li data-slide-to="1" data-target="#Carousel" class=""></li>
                        <li data-slide-to="2" data-target="#Carousel" class="active"></li>
                      </ol>

                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php $count = 1; ?>
                            @foreach($hotels as $values)   
                                       
                                <div class="item  @if($count == 1) active @endif">
                                  <div class="row"> 
                                    @foreach($values as $hotel)   
                                     <a href="#">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                          <div class="thumbnail"> 
                                              @if(!empty($hotel->main_image))
                                              <img  src="{{ asset($hotel->main_image) }}" alt="Image" style="max-width:100%;height:340px;">
                                              @else
                                              <img src="{{ asset('theme/front-v4/images/hotels/hotel01.jpg') }}" alt="Image" style="max-width:100%;height:340px;">
                                              @endif
                                          </div>  

                                           <div class="text-16">{{ $hotel->title }} <span class="text-14 fade-color">
                                             <i class="fa fa-map-marker "></i>{{ $hotel->label }}</span>
                                            </div>
                                  
                                          <!--                                          /.hotel-star-->
                                          
                                          <?php $x = 0; ?>                                          
                                          @for($x = 0; $x < $hotel->star_category; $x++)
                                            <span class="hotel-star"> 
                                              <i class="glyphicon glyphicon-star"></i>                                              
                                            </span>  
                                          @endfor
                                          
                                            <div class="text-12 fade-color">Starting from <span class="text-highlight">
                                              <i class="fa fa-inr"></i> {{ round($hotel->price) }}/-</span>
                                            </div>
                                          </div>
                                        </a> 
                                    @endforeach
                                </div>
                                  <!--.row--> 
                              </div>
                        <?php $count = $count + 1; ?>
                        @endforeach     
                      </div>
                      
               <!--.carousel-inner--> 
                      <a class="left carousel-control" href="#Carousel" data-slide="prev">‹</a> 
                      <a class="right carousel-control" href="#Carousel" data-slide="next">›</a> 
                    </div>
                  </div>
                </div>
              
                   
                
                <div class="pop-dest pull-right col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h2 class="section-title">Popular Destinations</h2>
                    <div class="list-display panel box-rd">
                        <div class="with-nav-tabs">
                          <div class="panel-heading">
                            <ul class="nav nav-tabs nt-std-tab">
                              <li class="active col-xs-6 text-center no-padding"><a class="nt-tab" href="#pop-dest-tab1default" data-toggle="tab">Domestic</a></li>
                              <li class="col-xs-6 text-center no-padding"><a class="nt-tab" href="#pop-dest-tab2default" data-toggle="tab">International</a></li>
                            </ul>
                          </div>
                            
                          <div class="panel-body1">
                            <div class="tab-content">
                                
                              <div class="tab-pane fade in active" id="pop-dest-tab1default">
                                   @foreach($domestics as $domestic)  
                                    <div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                            <a href="{{URL::route('destinations-packages',array($domestic->slug))}}">
                                            @if(!empty($domestic->main_image))
                                        	<img class="img-responsive" src="{{ asset($domestic->main_image) }}" />
                                            @else
                                                <img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                                            @endif
                                            </a>
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <a class="color" href="{{URL::route('destinations-packages',array($domestic->slug))}}">
                                              <p class="text-16">{{ $domestic->label }}</p>
                                              <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i> {{ round($domestic->min_price) }}/- onwards</p>
                                          </a>      
                                      </div>
                                    </div>
                                  @endforeach
                                  
                            </div>
                  <!--/tab1-->
                  
                          <div class="tab-pane fade" id="pop-dest-tab2default">
                                @foreach($internationals as $international)  
                                  	<div class="clearfix list-group-item">
                                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                      	<div class="img-box-sml1">
                                            <a href="{{URL::route('destinations-packages',array($international->slug) )}}">
                                            @if(!empty($international->main_image))
                                        	<img class="img-responsive" src="{{ asset($international->main_image) }}" />
                                            @else
                                            <img class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                                            @endif
                                            </a>
                                        </div>
                                      </div>
                                      <div class="pull-left">
                                          <a class="color" href="{{URL::route('destinations-packages',array($domestic->slug))}}">
                                              <p class="text-16">{{ $international->label }}</p>
                                              <p class="text-14"><span class="small fade-color">starting @ </span><i class="fa fa-inr"></i> {{ round($international->min_price) }}/- onwards</p>
                                          </a>    
                                      </div>
                                    </div>
                                @endforeach
                              
                          </div>
                  <!--/tab2--> 
                </div>
              </div>
            </div>
          </div>
        </div><!--/.pop-dest-->
                
                <div class="col-lg-8 col-md-8 col-sm-12">
          <h2 class="section-title">Why US?</h2>
          <div class="panel box-rd">
            <div class="panel-body">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img  src="{{ asset('theme/front-v4/images/expert-advice.png') }}" />
                  <p class="text-14">Get Expert Advice</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <img  src="{{ asset('theme/front-v4/images/24hr.png') }}" />
                 
                  <p class="text-14">24 / 7 Services</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i class="fa fa-thumbs-up" style="font-size: 60px; color: #B5BDC9;"></i>
                  <p class="text-14">Best Price Guarantee</p>
                </div>
              </div>
              <div class="row push-top-20">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i class="fa fa-magic" style="font-size: 60px; color: #B5BDC9;"></i>
                  <p class="text-14">Customize Solutions</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"><img  src="{{ asset('theme/front-v4/images/save_money.png') }}" /> 
                  <p class="text-14">Save Money</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center text-12"> <i class="fa fa-smile-o" style="font-size: 60px; color: #B5BDC9;"></i>
                  <p class="text-14">We Value Your Time</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        
                <div class="col-lg-8 col-md-8 col-sm-12">
            <h2 class="section-title">Subscribe to Our Newsletter</h2>

            <div class="panel box-rd">
                <div class="panel-body subscribe-box">
                  <h4>Get Latest Deals In Your Inbox</h4>
                  <p>Sign up to get the latest deals and offers directly into your inbox!</p>
                  <div id="mc_embed_signup" class="email-signup-wrapper">
<!--                    <form id="mc-embedded-subscribe-form" class="validate" novalidate target="_blank" name="mc-embedded-subscribe-form" method="post" action="">-->
                      <div class="input-group">
                        <input id="mce-EMAIL" class="required email form-control inputbox autowidth newsletter-input" type="email" name="EMAIL" placeholder="E-mail address" value="">
                        <span class="input-group-btn"> <a href="javascript:;" onclick="sendemail()"><i class="fa fa-paper-plane"></i></a> </span> </div>
<!--                    </form>-->
                  </div>
                </div>
            </div>
        </div>    
        
                <div class="col-lg-12 col-md-12 col-sm-12" style="overflow:hidden;">
            <h2 class="section-title">We Are Social</h2>
            <div class="panel box-rd">
                <div class="panel-body"> 

                  <!-- Facebook -->
                  <div class="col-md-3 col-xs-12"> <a href="https://www.facebook.com/sharer/sharer.php?u=" title="Share on Facebook" target="_blank" class="btn btn-facebook"><i class="fa fa-facebook" style="font-size: 35px;"></i> Share on Facebook</a> </div>
                  <!-- Google+ -->
                  <div class="col-md-3 col-xs-12"> <a href="https://plus.google.com/share?url=" title="Share on Google+" target="_blank" class="btn btn-googleplus"><i class="fa fa-google-plus" style="font-size: 35px;"></i> Share on Google+</a> </div>
                  <!-- LinkedIn -->
                  <div class="col-md-3 col-xs-12"> <a href="http://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=" title="Share on LinkedIn" target="_blank" class="btn btn-linkedin"><i class="fa fa-linkedin" style="font-size: 35px;"></i> Share on LinkedIn</a> </div>
                  <!-- Twitter -->
                  <div class="col-md-3 col-xs-12"> <a href="http://twitter.com/home?status=" title="Share on Twitter" target="_blank" class="btn btn-twitter"><i class="fa fa-twitter" style="font-size: 35px;"></i> Share on Twitter</a> </div>
                </div>
            </div>
        </div>
            </div>
            
             <!--/Bottom Links-->
            <div class="row push-bottom-20">
                <div class="col-lg-12 col-md-12 col-sm-12 text-12">
                  <h4>India Holidays</h4>
                 <p>
                  <a href="{{ URL::route('home') }}">Kerala Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Himachal Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">South India Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Rajasthan Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Kathmandu Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Uttarakhand Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Goa Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Golden Triangle Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Kashmir Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Andaman Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Leh Ladakh Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">North East Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Manali Volvo Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Kashmir Holiday Packages with Flights</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Manali Group Tour</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Goa Holiday Packages with Flights</a> &nbsp;           
                  <a href="{{ URL::route('home') }}">Dharamshala Holiday Packages</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Kerala Holiday Packages with Flights</a> &nbsp; 
                  <a href="{{ URL::route('home') }}">Norht East Holiday Packages</a> &nbsp;</p>
                </div>
            </div>
            <!--/Bottom Links--> 
        </div>
       </div>
          
      </div>
</div> <!--/.body-container--> 
@stop

@section('js')
{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js') }}

<script type="text/javascript">
 $(document).ready(function() {
     
    $('#start_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
     
    $('#end_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
	 
	 
    $('#Departure_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
     
    $('#Arrival_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
	 
    $('#Departure1_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
     
    $('#Arrival1_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
   
    $('#checkin_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
     
    $('#checkout_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
	 
                                   
 });
    
    
$(function() {
    $("#topic_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
       
              select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                 $('input[name="destination_id"]').attr('value',this.value);
                
            },
     
            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
    });

}); 
	
  
 function valid_destination()
  {
      var valid_des = $('#destination_title').val();
    
      if(!valid_des)
      {
        alert ("Please enter valid destination");
      }
     
    }

  
$(function() {
    $("#flight_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
               $('input[name="destination_id"]').attr('value',this.value);
             },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
    });
}); 
	
$(function() {
    $("#flight_to_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                 $('input[name="destination_id"]').attr('value',this.value);
                
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
    });
});
	
$(function() {
    $("#transport_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                 $('input[name="destination_id"]').attr('value',this.value);
                
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
    });
});

$(function() {
    $("#transport_to_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                 $('input[name="destination_id"]').attr('value',this.value);
                
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)
            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
    });
});
  
  $(function() {
    $("#hotels_title").autocomplete({
            source: '{{URL::to("destinations.json")}}',
            minLength: 1,
            select: function(event, ui) {
               // Prevent value from being put in the input:
                this.value = ui.item.id;
                 $('input[name="destination_id"]').attr('value',this.value);                
            },

            html: true, // optional (jquery.ui.autocomplete.html.js required)            
            // optional (if other layers overlap autocomplete list)
            open: function(event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
    });
}); 
  
$('#mce-EMAIL').keyup(function(e){
    if(e.keyCode == 13)
    {
        sendemail();
    }
    });  
	
function sendemail()
  {
   var email = $('#mce-EMAIL').val();

    if(!email)
    {
      alert("Kindly enter your e-mail id !")
    }
  else
    {
       $.ajax({
          type: "POST",
          url: '/newslettersubscription',
          data: {'email':email},
          success: function(msg) 
          {

            if(msg == 'false')
              {
                 alert( "Your email already exists !" );
              }
            else
              {
                alert( "You have subscribed for newsletters successfully !" );
              }
          }
        });
      }
    }

	
  var app = angular.module('HomeModule',[]);
  app.controller('FlightController',function($scope){			
    $scope.adult = 1 ;
    $scope.children = 1 ;
    $scope.infant = 1 ;
    $scope.radiotype = '';
    

    $scope.increment = function(e,val) {
         e.preventDefault();

        if(val == 'adult')
        {
            $scope.adult++;
        }
        if(val == 'children')
        {
            $scope.children++;
        }

        if(val == 'infant')
        {
            $scope.infant++;
        }
      }

    $scope.decrement = function(e,val) {
         e.preventDefault();

          if(val == 'adult')
          {
            $scope.adult--;
          }
        if(val == 'children')
        {
            $scope.children--;
        }

        if(val == 'infant')
        {
            $scope.infant--;
        }
    }

    $scope.radio = function(val1)
    {
        $scope.radiotype = val1;
    }
		
    }).controller('HotelController', function($scope){
        $scope.adult1 = 1;
        $scope.children1 = 1;
    
    $scope.incrementval = function(e,val){
    e.preventDefault();
      
      if(val =='adult1')
        {
          $scope.adult1++;
        }
     
      if(val == 'children1')
        {
          $scope.children1++;
        }    
    }
    
    $scope.decrementval = function(e,val){    
    e.preventDefault();
      
      if(val == 'adult1')
        {
          $scope.adult1--;
        }
    
      if(val == 'children1')
         {
          $scope.children1--;         
         }
      }    
  }).controller('TransportController', function($scope){
     
        $scope.radioway = '';
      
        $scope.radiotransport = function(val1)
        {
            $scope.radioway = val1;
        }
  });
    
</script>
@stop