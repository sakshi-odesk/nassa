@extends('layout.frontend')

@section('css')
{{HTML::style('theme/front/css/jquery.bxslider.css')}}
@stop

@section('content')

<div class="mid">

<div class="left">
<div class="search">
<div class="tag1">Plan your travel</div>

<div>
<select name="cat" id="cat" >
<option value="0" >Select Category</option>
@foreach($hotel_categories as $hotel_category)    
<option value="{{$hotel_category->slug}}">{{$hotel_category->title}}</option>
@endforeach    
</select>
</div>


<div align="right" style="padding:10px;">
<input type="submit"  name="submit" value="Search Hotel" onclick="getHotel()"  />
</div>
</div>

<br />

<img src="{{asset('theme/front/images/leftban.png')}}" width="350px"  />
<br /><br />

<img src="{{asset('theme/front/images/leftban.png')}}" width="350px"  />


<div class="conta">
<p>Need Help For Package Booking?</p>
<div class="lin">Contact us and our executives will assist you to plan a best holiday of you!</div>

<div class="nos"><span>7838779981<br />
sales@nassatravels.com</span> OR <a href="">Send Enquiry</a>
</div>

</div>


</div>

<div class="right">

<div class="banner">
<!------------------banner------------->
<ul class="bxslider">
<li><img src="{{asset('theme/front/images/2014_25_05_48_06_Post-5_Kerala.jpg')}}"/></li>
</ul>
<!------------------banner------------->
</div>
    
<br />
<div class="pack" >
<div class="tabs_" >
<div class="intabs"  ><p>Popular Destinations</p></div>
</div>
<div class="promain">

    @foreach($hotel_categories as $hotel_category)
    <a href="{{URL::route('hotel-details',array($hotel_category->slug))}}">
    <div class="repd" style="background:url('{{asset($hotel_category->main_image)}}') no-repeat center top;background-size:100% 100%;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{{asset($hotel_category->main_image)}}', sizingMethod='scale');
    -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{{asset($hotel_category->main_image)}}', sizingMethod='scale')";">
    <p class="texts">{{$hotel_category->title}}<br />
    <!--<span style="color:#99CC00"></span>-->
    </p>
    </div>
    </a>
    @endforeach
</div>

</div>
</div>
</div>

@stop

@section('js')
{{HTML::script('theme/front/js/jquery-004.js')}}
{{HTML::script('theme/front/js/jquery.min.js')}}
{{HTML::script('theme/front/js/jquery.easing.1.3.js')}}
{{HTML::script('theme/front/js/jquery.bxslider.min.js')}}
{{HTML::script('theme/front/js/css-pop.js')}}
@stop