@extends('layout.frontend')

@section('content')
<div class="mid">

<div class="left">

<div class="search">
<div class="tag1">Plan your travel</div>
<div>
<select name="cat" id="cat" >
<option value="0" >Select Category</option>
@foreach($domestic_packages as $package)
<option value="{{$package->slug}}">{{$package->title}}</option>
@endforeach    
</select>
</div>

<div>
<input type="text"  class="date" id="inputField"  value="05-DEC-2013" />
<input type="text"  class="date" id="inputField1" value="20-DEC-2013" />
</div>


<div align="right" style="padding:10px;">
<input type="submit"  name="submit" value="Search Packages" onclick="getDomestic()" />
</div>
</div>

<br />

<img src="{{asset('theme/front/images/leftban.png')}}" width="350px"  />
<br /><br />

<img src="{{asset('theme/front/images/leftban.png')}}" width="350px"  />

</div>

<div class="right">

<p class="abttitle"></p>
	Nassa Travels&nbsp; is one of the fastest growing travel company in India with a young and enthusiastic team working 24 *7 to provide the best travel services. It is a single platform providing all travel related services including destination knowledge, guidance, pricing and booking facility for domestic hotel bookings, holiday packages, and car rentals. We not just to sell we make your travel simple,comfortable and memorable.<br />
	<br />
	We are specialised in providing all kinds of packages including :- Family Tours Adventure Tours Camps, Wildlife, Safari etc Rafting Special Honeymoon Tours Corporate Packages Group Tours School/College Groups.
</div>
</div>
@stop