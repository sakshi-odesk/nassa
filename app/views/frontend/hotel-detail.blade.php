@extends('layout.frontend')

@section('css')
<style type="text/css">
#blanket {
   background-color:#111;
   opacity: 0.65;
   *background:none;
   position:absolute;
   z-index: 9001;
   top:0px;
   left:0px;
   width:100%;
}    
#popUpDiv1 {
	position:fixed;
	background:#F7FAFF;
	width:500px;
	height:370px;
	z-index: 9002;
	border-radius:5px;

}
#popUpDiv1 h1
{
text-align:left;
font-size:16px;
color:#FF6600;
padding-left:15px;
}

#popUpDiv1 .line_1
{

border-bottom:1px #e2ebfb solid;
}
#popUpDiv1 tr td
{
padding:10px;
color:#666666;
}

#popUpDiv1 tr td a
{
color:#FF9900;
text-decoration:none;
}

#popUpDiv1 tr td input[type="text"]
{
background:#fff;
border:1px #b6c8ed solid;
padding:5px;
width:215px;
font-family:arial;
font-size:12px;
}

#popUpDiv1 tr td input[type="submit"]
{
background:#ff760c;
border:2px #be6928 solid;
color:#fff;
font-weight:bold;
padding:5px 15px 5px 15px;
font-family:arial;
}


#popUpDiv1 tr td textarea
{
background:#fff;
border:1px #b6c8ed solid;
width:475px;
height:70px;
font-family:arial;
font-size:12px;
}

#popUpDiv1 .closepop1 {float:right;margin-right:2px; margin-top:2px;}
    
.right
{
line-height:1.7;
color:#333333;
}
.san
{
  border: 1px solid #CCCCCC;
    color: #666666;
    margin: 0px 10px 0;
    padding: 10px;
    width: 275px;
	float:right;
}
.san1
{
  border: 1px solid #CCCCCC;
    color: #666666;
    margin: 8px 10px 0;
    padding: 10px;
    width: 275px;
	height:60px;
	float:right;
}
mid
{
height:400px!important;
}

.clear
{
clear:both;
}
.but
{
width:150px;
}
.abttitle
{
color:#666666!important;
margin:10px 10px 0;
font-size:15px;
}
.sss
{
 height:30px;
}    
    
</style>
@stop

@section('content')
<div class="mids">


<p class="texto"><a href="{{URL::route('home')}}">Home</a><span class="textg">&raquo;</span><a href="{{URL::route('hotels')}}">Hotel</a><span class="textg">&raquo; {{$hotel_category->title}}</span></p>

<p class="cont">Total <strong> Packages</strong> for {{$hotel_category->title}}</p>

@foreach($hotels as $hotel)
  <?php $hotel_image = HotelImage::where('hotel_id','=',$hotel->id)->first(); ?>
<div class="prorap"  >
<!----------left------>
    <div class="pleft">
        <div class="ttag">

            <h1 style="margin-left:12px;">{{$hotel->title}}<span style="color:#003399; font-size:15px;">{{$hotel->location}}</span></h1>
            <br />
            <div class="mleft" style="width:97%; text-align:justify;"  >
              @if($hotel_image)
                <img src="{{asset($hotel_image->path)}}" height="120px" width="170px" style="padding:2px; border:1px #ddd dotted; margin-bottom:4px; float:left; margin-right:10px;"  />
              @endif
            <div class="rout">
              {{$hotel->body}}
            <br />
            <img src="{{asset('theme/front/images/facility.png')}}"  />
            </div>
            </div>
        </div>

    </div>
<!---------end left----->

    <!----------right------>
    <div class="pright">
        <div class="rate">
            <p>Rs. {{$hotel->price}}/-</p>
            <span>(Price Per Adult)</span>
        </div>
    <div class="bnbut" onclick="popup('popUpDiv1')" >Book Now</div>
    </div>
<!---------end right----->
</div>
@endforeach
<br />
</div>


{{Form::open(array('action'=>'HotelCategoryController@postBookNow','files'=>'true','role'=>'true'))}}
<!--POPUP-->    
    <div id="blanket" style="display:none;"></div>
    
	<div id="popUpDiv1" style="display:none; margin-top:250px;">
        
    <a href="javascript:void(0)" onclick="popup('popUpDiv1')" class="closepop1" ><img src="{{asset('theme/front/images/close.png')}}"  /></a>
    
    <h1>Send Query For -</h1>
    <div class="line_1"></div>
<!--    <input type="hidden"  name="titles"  value="The Park Hotel" />-->
        <input type="hidden"  name="url"  value="{{Request::url()}}" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    
    <tr>
    <td><input type="text" name="name" id="name" placeholder="Full Name" required /></td>
    <td align="left"><input type="text" name="place" id="place" placeholder="Place" required /></td>
    </tr>
    
    <tr>
    <td><input type="text" name="mobile" id="mobile" placeholder="Mobile No.*" required /></td>
    <td align="left"><input type="text" name="email" id="email" placeholder="Email Address*" required /></td>
    </tr>

<!--
    <tr>
    <td colspan="2"><input type="text" name="departure_date" id="departure_date" placeholder="Departure Date" /> IE: (YYYY-MM-DD)</td>
    </tr>
-->

    <tr>
    <td colspan="2"><textarea name="comments" id="query" required placeholder="Tell us your package requirements for a customised pakcages" ></textarea></td>
    </tr>
    
     <tr>
    <td colspan="2"><input type="checkbox" name="" id=""  checked="checked"  readonly="readonly" /> I authorize Nassa Travels.com contact me. <a href="">Know More</a></td>
      </tr>
    
    <tr>
    <td>
    <input type="submit" name="submit" id="submit" value="Send" />
    
    </td>
    <td></td>
    </tr>
    </table>
	</div>	
<!-- / POPUP-->     
{{Form::close()}}

@stop

@section('js')
<script type="text/javascript">

function toggle(div_id) {
	var el = document.getElementById(div_id);
	if ( el.style.display == 'none' ) {	el.style.display = 'block';}
	else {el.style.display = 'none';}
}
    
function blanket_size(popUpDivVar) {
    
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}
	var blanket = document.getElementById('blanket');
	blanket.style.height = blanket_height + 'px';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/8-200;//200 is half popup's height
	popUpDiv.style.top = popUpDiv_height + 'px';
}
    
function window_pos(popUpDivVar) {
    
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	window_width=window_width/3-100;//200 is half popup's width
	popUpDiv.style.left = window_width + 'px';
}

function popup(windowname) {
	blanket_size(windowname);
	window_pos(windowname);
	toggle('blanket');
	toggle(windowname);		
}
</script>

@stop