@extends('layout.frontend-v3')

@section('css')
{{ HTML::style('theme/nassatravels/assets/css/jquery-ui.css')}}
<style>
/*
.radio label input[type="radio"] {
    display: none;
}    
*/
  button.nt-btn {
    padding: 7px 15px;
    text-align: center;
    font-size: 16px;
    border-radius: 4px;
    display: block;
    margin: 10px 0px;
}	  
.filter_checkbox{
    right:25px;
}
.color.duration{
    color:#fff;
} 
.color{
    color:#444444;
}
.content-hide{
    display: none;
}    
/*    custom loader css*/
    
.loading{
  display: none;
}
.loader{
    text-align: center;
    margin-top: 50px;
}    
    
</style>    
@stop

@section('content')

<div class="loader" text-center>Loading 
    <img src="http://sampsonresume.com/labs/pIkfp.gif">
</div> 

 <div class="container" ng-app="PackageListing" ng-controller="PackagesController">
    <div id="content" class="row content-hide">
      <div class="col-lg-12 breadcrumb"> 
          <a href="{{ URL::to('/') }}" class="active">Home ></a>
          <span>{{ $destination->label }} Holidays </span>
      </div>
        
      <div class="clearfix"></div>
      <div class="col-lg-12 clearfix">
        <h1 class="page-header">{{ $destination->label }} Holidays</h1>
      </div>
        
     <div class="col-lg-12 clearfix">
    <div class="search-again-wrapper">
        <div class="inner">

            <div class="col-md-3 col-sm-3 hidden-xs">
                <label>Going to </label>
                <div class="going-to text-16">{{ $destination->label }}</div>
            </div>

            <div class="col-md-2 col-sm-2 hidden-xs">
                <label>Start Date </label>
                <div class="">
                <i class="fa fa-calendar" style="font-size:20px; margin-right:5px;"></i>
					<?php 
          
                      $start_date = Input::get('start_date');
                    
                      if(!empty($start_date))
                      {
                            echo date("D, d F'y",strtotime( $start_date ));
                      }
                      else
                      {
                          echo date("-",strtotime( $start_date ));
                      }
                      
                  ?>
                </div>
            </div>

            <div class="col-md-2 col-sm-2 hidden-xs">
                <label>End Date </label>
                <div class="">
                    <i class="fa fa-calendar" style="font-size:20px; margin-right:5px;"></i>
						<?php 
                        
                          $end_date = Input::get('end_date'); 
                    
                          if(!empty($end_date))
                          {
                              echo date("D, d F'y",strtotime($end_date));
                          }
                          else
                          {
                              echo date("-",strtotime($end_date));
                          }
                      
                        ?>
                </div>
            </div>

            <div class="col-md-1 col-sm-1 hidden-xs">
                <label class="text-12 center-block text-center">Nights </label>
                <p class="text-center text-18">
				<?php
					$startDate = new datetime();
					$startDate->setTimestamp(strtotime(Input::get('start_date')));
            		$endDate = new datetime();
					$endDate->setTimestamp(strtotime(Input::get('end_date'))); 
					echo $difference = $endDate->diff($startDate)->format("%d");
					?>
				</p>
            </div>

<!--
            <div class="col-md-1 col-sm-1 hidden-xs">
                <label class="text-12 center-block text-center">People </label>
                <p class="text-center text-18">2</p>
            </div>
-->

            <div class="col-md-2 col-sm-2 	">
                <a href="#" class="btn nt-btn btn-default search-again-btn">Search Again</a>
            </div>
        </div>

   <!--Modify Search Popup-->
    <div class="search-again-popup">
		{{ Form::open(array('action' => 'PackageCategoryController@getSearchPackage','method'=>'get')) }}
            <div class="search-again-wrapper">
                <div class="search-again-inner">

                    <div class="col-md-5 col-sm-5">
                        <label>Going to </label>
                        <input id="topic_title" 
                             type="text" 
                             placeholder="Destinations" 
                             @if(isset($destination)) value="{{ $destination->label }}" @endif   
                             class="inputbox input-color input-block autowidth">  
                        <input id="destination_title" type="hidden" name="destination_id" @if(isset($destination)) value="{{ $destination->id }}" @endif >
                    </div>

                    <div class="col-md-2 col-sm-2">
                        <label>Start Date </label>
                        <div class="">
                           <fieldset>
                            <div class="control-group">
                              <div class="controls">
                               <div class="input-prepend input-group">
                                 <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
								  <input type="text" name="start_date" id="start_date" class="form-control input-color" ng-model="firstdate" ng-change="differenceInDays();"/> 
                               </div>
                              </div>
                            </div>
                           </fieldset>
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-2">
                        <label>End Date </label>
                        <div class="">
                           <fieldset>
                            <div class="control-group">
                              <div class="controls">
                               <div class="input-prepend input-group">
                                 <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
								   <input type="text" name="end_date" id="end_date" class="form-control input-color"ng-model="seconddate" ng-change="differenceInDays();"  /> 
                               </div>
                              </div>
                            </div>
                           </fieldset>
                        </div>
                    </div>

                    <div class="col-md-1 col-sm-1">
                        <label class="text-12 center-block text-center">Nights </label>
                        <p class="text-center text-18" ng-bind="diff">
						
						</p>
                    </div>
                    <div class="col-md-3 col-sm-3 pull-right">
                        <button type="submit" name="submit" class="search nt-btn btn-orange btn-sm btn-block">Search</button>
                    </div>

                    <div class="col-md-3 col-sm-3 pull-right">
                        <button type="submit" name="submit" class="search nt-btn btn-gray btn-sm btn-block">Reset</button>
                    </div>

                    </div>

            </div>
		{{ Form::close() }}
    </div>
  </div>

<!--/.Modify Search Popup-->


    </div><!--/.search-again-wrapper-->    
      
      <!--packages row-->
      <div class="clearfix"></div>
        
      <div class="col-lg-3 refine-search push-top-20 hidden-xs">
        <h3>Refine Search</h3>
        <div class="inner panel push-bottom-30">
            
          <div class="budget clearfix">
            <h4>Budget <a href="#" class="ex-co fa fa-minus pull-right"></a></h4>
              
              <?php 
                    $price = Input::get('price');  
                    $duration = Input::get('duration');  
                ?>
              
               <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">Upto Rs. 19999/-</span>
                        @if(!empty($price) && ($price == '0-19999'))
                        <input type="checkbox" value="0-19999" class="filter_checkbox price" checked>
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        @else
                        <input type="checkbox" value="0-19999" class="filter_checkbox price">
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        @endif
                        
                    </label>
                </div>

                <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">Above Rs. 20000/-</span>
                        
                        @if(!empty($price) && ($price == '20000-900000'))
                        <input type="checkbox" value="20000-900000" class="filter_checkbox price" checked>
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        @else
                        <input type="checkbox" value="20000-900000" class="filter_checkbox price">
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        @endif
                        
                    </label>
                </div>
          </div>
          <!--/.budget-->
          
          <div class="pkg-duration clearfix">
            <h4>Duration<a href="#" class="ex-co fa fa-minus pull-right"></a></h4>
            
            <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">3 Nights / 4 Days</span>
                        @if(!empty($duration) && in_array('3Nights/4Days',Input::get('duration')))
                        <input type="checkbox" value="3Nights/4Days" class="filter_checkbox duration" checked>
                        @else
                        <input type="checkbox" value="3Nights/4Days" class="filter_checkbox duration">
                        @endif
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                      
                    </label>
                </div>
               
                <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">4 Nights / 5 Days</span>
                        @if(!empty($duration) && in_array('4Nights/5Days',Input::get('duration')))
                        <input type="checkbox" value="4Nights/5Days" class="filter_checkbox duration" checked>
                        @else
                        <input type="checkbox" value="4Nights/5Days" class="filter_checkbox duration">
                        @endif
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>

                <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">5 Nights / 6 Days</span>
                        @if(!empty($duration) && in_array('5Nights/6Days',Input::get('duration')))
                        <input type="checkbox" value="5Nights/6Days" class="filter_checkbox duration" checked>
                        @else
                        <input type="checkbox" value="5Nights/6Days" class="filter_checkbox duration">
                        @endif
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>

                <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">6 Nights / 7 Days</span>
                        @if(!empty($duration) && in_array('6Nights/7Days',Input::get('duration')))
                        <input type="checkbox" value="6Nights/7Days" class="filter_checkbox duration" checked>
                        @else
                        <input type="checkbox" value="6Nights/7Days" class="filter_checkbox duration">
                        @endif
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>

                <div class="checkbox">
                    <label class="checkbox-row">
                        <span class="text-14">7 Nights / 8 Days</span>
                        @if(!empty($duration) && in_array('7Nights/8Days',Input::get('duration')))
                        <input type="checkbox" value="7Nights/8Days" class="filter_checkbox duration" checked>
                        @else
                        <input type="checkbox" value="7Nights/8Days" class="filter_checkbox duration">
                        @endif
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                </div>   
              
          </div>
          <!--/.duration--> 
          
        </div>
      </div>
      <!--/.refine-search-->
        
      <div class="col-lg-9 prod-wrapper push-top-20">
        <div class="clearfix top-info"> <span style="margin-top:20px; display: inline-block;">
             Showing {{ $count }} Holiday Packages for {{ $destination->label }}
            </span>
          <div class="pull-right sort-by"> <span class="text-12">Sort By: &nbsp;</span>
            <div class="styled-select">
              <select class="selectbox styled-select" onchange="getval(this);">
                  <option value="popular">Most Popular</option>
                  <option value="high" @if(Input::get('sort') == 'high') selected @endif >Price High - Low</option>
                  <option value="low" @if(Input::get('sort') == 'low') selected @endif>Price Low - High</option>
               </select>
            </div>
          </div>
        </div>
          
        <div class="prod-row" ng-repeat="(key,value) in packages" ng-cloak> 
            <!--recommended Tag-->
                <div class="recommended" ng-if="value.recommended == 1">
                    <img src="{{ asset('theme/front-v3/images/recommended.png') }}" alt="recommended">
                </div>
            <!--recommended Tag close-->
            
            
          <div class="inner">
            <div class="panel prod-listing-row clearfix">
              <div class="prod-listing-top col-lg-9 col-md-9 col-sm-9 col-xs-12 clearfix box-inner"> 
                
                 <!--Offer Ribbon-->
                    <div class="offer-ribbon" ng-if="special_offer == 1">
                        <img src="{{ asset('theme/front-v3/images/offer_ribbon.png') }}" alt="recommended">
                    </div>
                <!--Offer Ribbon close-->  
                  
                  <div class="hotel_image col-lg-4 col-md-4 col-sm-4 col-xs-12 row">
                     <a href="/packages/<% value.slug %>">
                            <img ng-if="value.main_image" class="img-responsive" src="/<% value.main_image %>" />
                            <img ng-if="!value.main_image" class="img-responsive" src="{{ asset('theme/front-v3/images/img-placeholder.jpg') }}" />
                    </a>
                </div>
                  
                <div class="hotel_details col-lg-8 col-md-8 col-sm-8 col-xs-12">
						<p class="clearfix prod-name" > 
                            
                            <a class="color" ng-href="/packages/<% value.slug %>"><% value.title %></a>
						
						<span class="text-12 fade-color prod-sub" ng-show="value.flight_flag == 1" >
							(with Flights)
						</span>
						
						</p> 
                    <br />
                    <p class="clearfix tour-duration"><a class="color duration" ng-href="/packages/<% value.slug %>"><% value.duration %> </a> </p>

                    <p class="clearfix tour-info">
                        <a class="color" ng-href="/packages/<% value.slug %>"><% value.route_map %></a>
                    </p>

                     <p class="desc text-12 hidden-xs">
                        
                      <span ng-if=value.desc !="" class="pull-left append_bottom10 text-11 fade-color ">
                        <% value.desc | limitTo: form.limit[value.id] %>
                        <a href="javascript:;" ng-click ="addmore(value.desc,value.id)" ng-if="value.desc.length != form.limit[value.id]">more</a>
                        <a href="javascript:;" ng-click ="addless(value.desc,value.id)" ng-if="value.desc.length == form.limit[value.id]" >less</a>
                            <!--                        ng-if="value.desc.length == limit"-->
                        </span>
                      <span class="clearfix"></span>
                    
                     </p>
                </div>
                  
                <div class="listing_top col-lg-10 col-md-10 col-sm-10 col-xs-12 clearfix">
                  <p class="text-12">Included in the package:</p>
                  <ul class="pkg-incl list-inline">
                    <li ng-repeat="(key,value) in value.package_facilities"><img src="/<% value.main_image %>" /><span class="incl-name text-12"><% value.title %></span></li>
                  </ul>
                </div>
              </div>
              <!--/.listing_top-->
              
              <div class="price-box col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-right pull-right-xs price_box_wrapper" style="padding: 0;"> 
                
                <!--Check Price update -->
                <div class="check-update-price"> <span class="text-11">Hotel Category</span>
                  <div class="styled-select-2">
                    <select class="selectbox styled-select-2" ng-model="form.hotel_category[value.id]" 
                            ng-options="k as v for (k,v) in value.category"
                            ng-change="update_package_category(value.id)">
                    </select>
                  </div>
                </div>
                  
                <div class="check-update-price"> <span class="text-11">Valid From</span>
                  <div class="styled-select-2">
                    <select class="selectbox styled-select-2" ng-model="form.hotel_date[value.id]" ng-change="update_hotel_price(value.id)">
                          <option ng-repeat="(key,value) in form.new_hotel_date[value.id]"><% value %></option>
                    </select>
                     </div>
                </div>
                <!--Check Price update -->
                  
                <div class="nt-get clearfix box-inner box-inner-2">
                    <span class="nt-get">
                        
                    <div ng-if="form.discounted_price[value.id]">
                        <div>
                        <p class="was-price">
                          <span class="INR">
                            <span class="fa fa-inr"></span>
                          </span>
                          <% Math.round(form.price_per_person[value.id]) %>
                        </p>
                        </div>

                        <div>
                            <p class="main-price">
                              <span class="INR">
                                <span class="fa fa-inr"></span>
                              </span>
                             <% Math.round(form.discounted_price[value.id]) %>
                            </p>
                        </div>
                    </div> 
                      
                    <div  ng-if="!form.discounted_price[value.id]">
                        <p class="main-price">
                          <span class="INR">
                            <span class="fa fa-inr"></span>
                          </span>
                           <% Math.round(form.price_per_person[value.id]) %> 
                        </p>
                    </div>      
                      
                    <div class="">
                        <p class="price_per hidden-xs append_bottom30 text-12">(Price Per Person)</p>
                        </span>
                    </div>
                    
                </div>    
                    
                <div class="cta box-inner-2">
                  <a class="nt-btn btn-gray" href="/packages/<% value.slug %>">View Details</a>
                </div>
                <div class="cta box-inner-2">
					<a href="javascript:;"  class="xhr-modal fresh nt-btn btn-orange"  
                       data-url="{{ URL::to('package/book/<% value.id %>') }}">Book Now</a> 
				  </div>
              </div>
            </div>
          </div>
        </div>
        <!--/.prod-row--> 
        
      </div>
    </div>
    <!-- /.row --> 
    
  </div>

@stop

     
@section('js')

{{ HTML::script('theme/nassatravels/assets/js/jquery.ui.autocomplete.html.js') }}

<script type="text/javascript">
  
 $(function() {
    $("#topic_title").autocomplete({
        source: '{{URL::to("destinations.json")}}',
        minLength: 1,
        select: function(event, ui) {
           // Prevent value from being put in the input:
            this.value = ui.item.id;
             $('input[name="destination_id"]').attr('value',this.value);
        },

        html: true, // optional (jquery.ui.autocomplete.html.js required)

        // optional (if other layers overlap autocomplete list)
        open: function(event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        }
    });
});     
 
var uri = new URI(window.location.href);    
    
  $(document).ready(function() {

    $('#start_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
     
    $('#end_date').daterangepicker({ 
        singleDatePicker: true,
        format: 'DD-MM-YYYY',
        showTodayButton:true
    });
      
     $(".search-again-btn").click(function(e) {
          $("body").append(''); 
          $(".search-again-popup").slideToggle(); 
          $(".overlay2").show();
          
          $(".close2").click(function(e) { 
                $(".lb-popup, .overlay2").hide(); 
          }); 
    }); 
      
      
    var uri = new URI(window.location.href);
    
    $('.duration').click(function(e){

          var value = this.value;
         
          if(this.checked == true)
          {
              window.location = uri.addSearch("duration[]",value);
          }
          else
          {
              window.location = uri.removeSearch('duration[]',value);
          }
    });
    
    $('.price').click(function(e){

          var value = this.value;

          if(this.checked == true)
          {
              uri.removeSearch('price')
              window.location = uri.addSearch("price",value);
          }
          else
          {
              window.location = uri.removeSearch('price');
          }
    }); 
                                   
 });
    
function getval(sel)
{
      var value = $(sel).val();

      if(value == 'high')
      {
          window.location = uri.removeSearch('sort','low');
          window.location = uri.addSearch("sort",value);
      }

      if(value == 'low')
      {
          window.location = uri.removeSearch('sort','high');
          window.location = uri.addSearch("sort",value);
      }
}  
 
  
</script>

<script>
var app = angular.module('PackageListing',[]);

  app.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
  })
  .controller('PackagesController', function($scope,$http,$rootScope,$element){ 
      
    $scope.Math = window.Math;
      
    $scope.form = []; 
      
    $scope.cate = 'Standard';  
      
    $scope.packages = JSON.parse(decodeURIComponent('{{ rawurlencode($new_packages) }}'));
      
    if($scope.packages.length == 0)
    {
        $('.loader').addClass('loading');
        $('#content').removeClass('content-hide');
    }
      
      
    $scope.show_price = 0;

    $scope.firstdate = '{{ Input::get('start_date') }}';
    $scope.seconddate = '{{ Input::get('end_date') }}';

    $scope.differenceInDays = function() {
		   
        var dt1 = $scope.firstdate.split('-'),
            dt2 = $scope.seconddate.split('-'),
            one = new Date(dt1[2], dt1[1]-1, dt1[0]),
            two = new Date(dt2[2], dt2[1]-1, dt2[0]);

        var millisecondsPerDay = 1000 * 60 * 60 * 24;
        var millisBetween = two.getTime() - one.getTime();
        var days = millisBetween / millisecondsPerDay;

        $scope.diff = Math.floor(days) || 0;
    }
	 
    $scope.differenceInDays();

   $scope.update_package_category = function(id)
    {
        $http({
            url: '/package-listing/search/category',
            method: "GET",
            params:{
                    'id':id,
                     'category':$scope.form.hotel_category[id],
                    },
            }).success(function(data) 
            { 
                  $scope.form.new_hotel_date[id] = data;

                   var new_updated_date = 0;

                     angular.forEach($scope.form.new_hotel_date[id], function(v, k){ 

                          if(new_updated_date == 0)
                          {      
                                $scope.form.hotel_date[id] = v;
                          }
                             new_updated_date++;
                      }); 
          
                    $scope.update_hotel_price(id);
            
                    $('.loader').addClass('loading');
                    $('#content').removeClass('content-hide');
            });
    }
    
    $scope.update_hotel_price = function(id){
      
        $http({
            url: '/package-listing/search/price',
            method: "get",
            params: {'id':id,
                     'category':$scope.form.hotel_category[id],
                     'date':$scope.form.hotel_date[id]
                    },
            }).success(function(data, status, headers, config) 
            { 
                $scope.form.price_per_person[id] = data['price'];

                $scope.form.discounted_price[id] = data['discounted_price'];

            }).error(function(data, status, headers, config) {

                $scope.status = status;
        });
    }
      
    angular.forEach($scope.packages, function(value, key){
        
        if(!$scope.form.price_per_person)
        {
            $scope.form.price_per_person = {};
            
            if(!$scope.form.price_per_person[value.id])
            {
                $scope.form.price_per_person[value.id] = {};
            }
        }
        else
        {
            if(!$scope.form.price_per_person[value.id])
            {
                $scope.form.price_per_person[value.id] = {};
            }
        }
        
        if(!$scope.form.limit)
        {
            $scope.form.limit = {};
            
            if(!$scope.form.limit[value.id])
            {
                $scope.form.limit[value.id] = 100;
            }
        }
        else
        {
            if(!$scope.form.limit[value.id])
            {
                $scope.form.limit[value.id] = 100;
            }
        }
        
        if(!$scope.form.hotel_category)
        {
            $scope.form.hotel_category = {};
        }
        
        if(!$scope.form.hotel_date)
        {
            $scope.form.hotel_date = {};
        }
      
        if(!$scope.form.new_hotel_date)
        {
          $scope.form.new_hotel_date = {};

          if(!$scope.form.new_hotel_date[value.id])
          {
              $scope.form.new_hotel_date[value.id] = [];
          }
        }
        else
        {
            if(!$scope.form.new_hotel_date[value.id])
            {
                $scope.form.new_hotel_date[value.id] = [];
            }
        }  
        
              
        if(!$scope.form.discounted_price)
        {
            $scope.form.discounted_price = {};
            
            if(!$scope.form.discounted_price[value.id])
            {
                $scope.form.discounted_price[value.id] = {};
            }
        }
        else
        {
            if(!$scope.form.discounted_price[value.id])
            {
                $scope.form.discounted_price[value.id] = {};
            }
        }
        
        $scope.form.price_per_person[value.id] = value.default_price;
        $scope.form.discounted_price[value.id] = value.default_discounted_price;
        

        
        angular.forEach(value.category, function(val, kay){   

            if(!$scope.form.hotel_category[value.id])
            {
                $scope.form.hotel_category[value.id] = val; 
            }
        }); 
      
         $scope.update_package_category(value.id);
         $scope.update_hotel_price(value.id);
        
    }); 
         
      
    $scope.addmore = function(desc,id){
    
        $scope.form.limit[id] = desc.length;
    }
    
    $scope.addless = function(desc,id){
    
        $scope.form.limit[id] = 100;
    } 
  });
    
</script>    
     
@stop     