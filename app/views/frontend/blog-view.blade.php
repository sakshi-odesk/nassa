@extends('layout.frontend')

@section('css')
{{HTML::style('theme/front/css/jquery.bxslider.css')}}
 <style>
.main_right_category {
    padding-left: 15px;
    margin-bottom: 20px;
}
.main_right_category h1 {
    padding: 0px;
    margin: 0px;
    margin-bottom: 10px;
    padding-bottom: 7px;
    border-bottom: 2px #ff760c solid;
    color: #474747;
    font-size: 18px;
    font-weight: normal;
}
.main_right_category ul {
    padding: 0px;
    margin: 0px;
    list-style: none;
}
.main_right_category ul li a {
    font-size: 12px;
    text-decoration: none;
    color: #474747;
    line-height: 1.7;
}
.main_right_category ul li a:hover {
    color: #ff760c;
}
/*****************post********************/
.left_post {
    padding-left: 15px;
    margin-bottom: 20px;
}
.left_post ul {
    list-style: none;
    padding: 0px;
    margin: 0px;
    height: 34px;
    border-bottom: 2px #ff760c solid !important;
}

.left_post li.active{
    border-bottom: 0px #ff760c solid !important;
}
.left_post ul li {
    float: left;
    padding: 10px;
    cursor: pointer;
}
.left_post ul .active {
    background: #ff760c;
   /* border-bottom: 1px #ff760c solid !important; */
    color: #fff;
}
.l_post {
    margin-top: 10px;
}
.l_post .list {
    height: 75px;
    border-bottom: 1px #ddd solid;
    cursor: pointer;
}
.l_post .list .list_title {
    font-weight: bold;
    color: #474747;
}
.l_post .list .date_post {
    color: #474747;
}
.l_post .list:hover .list_title {
    color: #ff760c;
}
.tags li {
    float: left;
    padding: 5px;
    border: 1px #ddd solid;
    margin: 2px;
}
.tags li:hover {
    background: #FF760C;
}
.tags li:hover a {
    color: #fff!important;
}
.post_left {
    /*border:1px #ccc solid;*/
    height: auto;
    margin-top: 15px;
}
.post_left .posts {
    height: 400px;
    width: 325px;
    float: left;
    margin-left: 23px;
}
.post_left .imgs {
    background-size: 100%;
    height: 200px;
}
.post_left .post_titles {
    text-decoration: none;
    color: #FF760C;
    font-size: 20px;
    line-height: 1.5;
}
.post_des {
    color: #333333;
    line-height: 1.5;
}
.date_show {
    padding: 5px;
    background: #FF760C;
    color: #fff;
    font-weight: bold;
    float: left;
    margin-left: -10px;
}
</style>
@stop

@section('content')

<div class="blog_mid">
    <br />
    <br />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="blog_table">
        <tr>
            <td valign="top" width="700px">

                <div class="post_left">
                    <h1 class="post_titles">{{ $blog->title }}</h1>
                    <span class="date_post">Nassa Travels | {{ $blog->created_at }}</span>
                    <div class="line"></div>
                    <img src="{{ asset($blog->blog_image) }}" style="height:300px;width:100%">
                    <div class="post_des">{{ $blog->body }}</div>

                    <div class="comment">
                        <div class="fb-comments" 
                             data-href="<?php //echo $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];?>" 
                             data-numposts="5" data-colorscheme="light">
                        </div>
                    </div>
                </div>
            </td>
            <td valign="top">
                <div class="main_right_category">
                    <h1>Category</h1>
                    <ul>
                        @foreach($blog_categories as $blog_category)
                        <li><a href="{{ URL::route('blog.show',array($blog_category->slug)) }}">{{ $blog_category->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
              
                <div class="left_post">
                    <ul>
                        <li id="aa1" class="active" onclick="document.getElementById('aa2').style.background='#fff';document.getElementById('aa2').style.color='#000000';document.getElementById('aa1').style.background='#FF760C';document.getElementById('aa1').style.color='#ffffff';">Latest Post</li>
                        <li id="aa2" onclick="document.getElementById('aa2').style.background='#FF760C';document.getElementById('aa2').style.color='#ffffff';document.getElementById('aa1').style.background='#ffffff';document.getElementById('aa1').style.color='#000';">Popular Post</li>
                    </ul>
                  
                    <?php $blogs = Blog::orderBy('created_at')->take(5)->get();  ?>
                    <div id="latest_b" class="l_post">
                      @foreach($blogs as $blog)
                        <div class="list">
                            <a href="{{ URL::route('blog-view',array($blog->slug)) }}">
                              <img src= "{{ asset($blog->blog_image) }}" 
                                   height="60px" width="120px" style="float:left; margin-right:10px;" />
                            </a>
                            <a href=""><p class="list_title">{{ $blog->title }}</p></a>

                            <span class="date_post">{{ $blog->created_at }}</span>
                        </div>
                      @endforeach
                    </div>
                    <div id="popular_b" class="l_post" style="display:none;">
                      @foreach($blogs as $blog)
                        <div class="list">
                            <a href="{{ URL::route('blog-view',array($blog->slug)) }}"><img src="{{ asset($blog->blog_image) }}" 
                                             height="60px" width="120px" style="float:left; margin-right:10px;" />
                            </a>
                            <a href="#"><p class="list_title">{{ $blog->title }}</p></a>
                            <span class="date_post">{{ $blog->created_at }}</span>
                        </div>
                      @endforeach
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <br />
</div>
<div class="line"></div>


@stop


@section('js')

{{HTML::script('theme/front/js/jquery.min.js')}}
{{HTML::script('theme/front/js/jquery.easing.1.3.js')}}
{{HTML::script('theme/front/js/jquery.bxslider.min.js')}}
{{HTML::script('theme/front/js/css-pop.js')}}

<script>
 $(document).ready(function(){
      $('#aa1').click(function(){
          console.log(this);
          $('#latest_b').show();
          $('#popular_b').hide();
      });
      $('#aa2').click(function(){
          $('#latest_b').hide();
          $('#popular_b').show();
      });
});
</script>

<script>
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '318343434991735',
    xfbml      : true,
    version    : 'v2.0'
  });
  $(window).triggerHandler('fbAsyncInit');
  };

  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
//  function create_callback(){
//      $.ajax({
//          url:"fb_comment.php",
//          type:"POST",
//          data : {id:1, action:"create"}
//      });
//  }
//  function remove_callback(){
//     $.ajax({
//          url : "fb_comment.php",
//          type: "POST",
//          data :{id:1 action : "remove"},
//    });
//  }
//  $(window).bind('fbAsyncInit', function() {
//      FB.Event.subscribe('comment.create', create_callback);
//      FB.Event.subscribe('comment.remove', remove_callback);
//  });

</script>
@stop