@extends('layout.login')

@section('css')
@stop

@section('content')
	  <div id="login-page">
	  	<div class="container">
	  	
		     {{ Form::open(array( 'action' => 'UserController@postLogin','files' => true,'role' => 'form','class'=>'form-login')) }}
		        <h2 class="form-login-heading">NassaTravels Admin Signin</h2>
                @foreach($errors->all(':message') as $message) 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                {{ $message }} 
                            </div>
                        </div>
                    </div>
                @endforeach
                @if(Session::has('status'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success" role="alert">
                                {{Session::get('status')}}
                            </div>
                        </div>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{Session::get('error')}}
                            </div>
                        </div>
                    </div>
                @endif
          
          
		        <div class="login-wrap">
		            <input type="text" class="form-control" name="loginemail" placeholder="Email ID" required>
		            <br>
		            <input type="password" class="form-control" name="password" placeholder="Password" required>
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
		                </span>
		            </label>
                                   
		            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                  
                  
                  
<!--
		            <hr>
		            
		            <div class="login-social-link centered">
		            <p>or you can sign in via your social network</p>
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
		            </div>
		            <div class="registration">
		                Don't have an account yet?<br/>
		                <a class="" href="#">
		                    Create an account
		                </a>
		            </div>
-->
		
		        </div>
          
		          <!-- Modal -->
                {{ Form::open(array('action' => 'RemindersController@postRemind','files' => true,'role' => 'form','class'=>'form-login')) }}
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                      <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
		                          <input type="text" name="email" placeholder="Email" class="form-control placeholder-no-fix">
		
		                      </div>
		                      <div class="modal-footer">
<!--		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>-->
		                          <button class="btn btn-theme" type="submit">Submit</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->
                {{Form::close()}}
		
		    
	  	
	  	</div>
	  </div>
@stop
