<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		//return View::make('hello');
	}
    
    /**
     * Show the home page for this site.
     * 
     * @return Response.
     */
    public function getIndex()
    {
         $domestic_packages = PackageCategory::domesticPackages();
        
         $international_packages = PackageCategory::internationalPackages();
        
         $domestic_hot_picks_packages = Package::getHotPick(1);
        
         $international_hot_picks_packages = Package::getHotPick(2);
        
         $hotels_hot_picks = Hotel::getHotPick();
		
         $season_special = DB::table('slides')
						->join('slide_shows','slide_show_id','=','slide_shows.id')
						->select('slides.main_image','slides.title','slides.url')
						->where('slide_shows.id','=',3)
						->get();
		
		 $rafting = DB::table('slides')
						->join('slide_shows','slide_show_id','=','slide_shows.id')
						->select('slides.main_image','slides.title','slides.url')
						->where('slide_shows.id','=',4)
						->get();
        
        $honeymoon_special = DB::table('slides')
						->join('slide_shows','slide_show_id','=','slide_shows.id')
						->select('slides.main_image','slides.title','slides.url')
						->where('slide_shows.id','=',5)
						->get();
        
        $travel_guide = DB::table('slides')
						->join('slide_shows','slide_show_id','=','slide_shows.id')
						->select('slides.main_image','slides.title','slides.url')
						->where('slide_shows.id','=',6)
						->get();
        
         $feedbacks = DB::table('feedbacks')
                        ->select('feedbacks.name','feedbacks.message','feedbacks.created_at')
                        ->orderBy('created_at','DESC')->get();
        
         $hotel_categories = DB::table('hotels')
                             ->join('destinations','destinations.id','=','hotels.destination_id')
                             ->select('hotels.title as title','hotels.main_image','hotels.location','hotels.price','hotels.star_category',
                                      'destinations.label')
                             ->where('hotels.recommended','=',1)
                             ->take(12)->get();
        
        $hotels = array_chunk($hotel_categories, 3);

        return View::make('frontend.home',array('domestics'=>$domestic_packages,
                                                'internationals'=>$international_packages,
                                                'domestic_hot_picks_packages' => $domestic_hot_picks_packages,
                                                'international_hot_picks_packages' => $international_hot_picks_packages,
                                                'hotels_hot_picks' => $hotels_hot_picks,
                                                'hotels'=>$hotels,
                                                'season_special'=>$season_special,
                                                'rafting'=>$rafting,
                                                'travel_guide' => $travel_guide,
                                                'honeymoon_special' => $honeymoon_special,
                                                'feedbacks'=>$feedbacks));
    }
    
    /**
     * Show the form for creating a new resource.
     * 
     * @return Response.
     */
    public function getContact()
    {
        return View::make('contactus.contactus');
    }
    
    /**
     * Store a newly created resource in storage.
     * 
     * @return Response.
     */
    public function postContact()
    {
        $input = Input::except('_token');
        
        $rules = array (
            'email' => 'required',
        );        
        
        $email = Setting::get('admin email');
        
        $enquiry_email = Setting::get('Enquiry recepient');
        
        //Validate data
        $validator = Validator::make ($input, $rules);

        //If everything is correct than run passes.
        if ($validator->passes())
        {
            $data = array('input'=>$input);
            
            Mail::send('emails.contactform',$data, function($message) use($input,$email) {
           
                $message->from($email, 'Nassa Travels');

                $message->to($input['email'])->subject('Your enquiry has been submitted to nassatravels.com');

            });
            
            Mail::send('emails.contact-us',$data, function($message) use($input,$email,$enquiry_email) {
           
                $message->from($input['email'], 'Nassa Travels');

                $message->to($email)
                        ->cc($enquiry_email)
                        ->subject('New Enquiry has been received');

            });
            
            $contact = Contact::create($input);
            
            if($contact)
            {
                return Redirect::route('contactus')->with('success','Your enquiry as been submnitted successfully!');
            }
        }
        else
        {
            return Redirect::route('contactus')->withErrors($validator);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     * 
     * @return Response.
     */
    
    public function getFlights()
    {
              
        return View::make('frontend.flights');
    }
    
    /**
     * Show the form for creating a new resource.
     * 
     * @return Response.
     */
    
    public function getCarsVolvos()
    {
        return View::make('frontend.cars-volvos');
    }
    
    /**
     * Show the form for creating a new resource.
     * 
     * @return Response.
     */
    
    public function getSpecialOffers()
    {
       $page = Page::where('title','=','Special Offers')->first();
        
        return View::make('frontend.term',array('page'=>$page));
    }
    
    /**
     * Show the form for displaying Ploicy.
     * 
     * @return Response.
     */
    
    public function getPolicy()
    {
        $page = Page::where('title','=','Policy')->first();
      
        return View::make('frontend.term',array('page'=>$page));
    }
    
    /**
     * Show the form for displaying term & condition.
     * 
     * @return Response.
     */
    
    public function getTerm()
    {
        $page = Page::where('title','=','Term & Conditions')->first();

        return View::make('frontend.term',array('page'=>$page));
    }
    
     /**
     * Show the form for displaying feedbacks.
     * 
     * @return Response.
     */
    
    public function getTestimonials()
    {
        $feedbacks = DB::table('feedbacks')
                        ->select('feedbacks.name','feedbacks.message','feedbacks.created_at')
                        ->orderBy('created_at','DESC')
                        ->whereNotNull('feedbacks.published')->get();
        
        $domestic_packages = PackageCategory::domesticPackages();
        
        return View::make('frontend.testimonials',array('feedbacks'=>$feedbacks,'domestic_packages'=>$domestic_packages));
    }
    
    /**
     * Show the form for displaying about us details.
     * 
     * @return Response.
     */
    
    public function getAboutUs()
    {
        $domestic_packages = PackageCategory::domesticPackages();
        
        return View::make('frontend.aboutus',array('domestic_packages'=>$domestic_packages));
    
    }
    
    /**
     * Send mail from feedback and enquiry
     * 
     * @return Response.
     */
    
    public function postFeedbackEnquiry()
    {
        $input = Input::except('_token');
        
        $email = Setting::get('admin email');
        
        $enquiry_email = Setting::get('Enquiry recepient');

        $data = array('input'=>$input);
        
        Mail::send('emails.contactform',$data, function($message) use($input,$email) {
           
                $message->from($email, 'Nassa Travels');

                $message->to($input['email'])->subject('Flight Booking Details');

        });
        
        Mail::send('emails.contact-us',$data, function($message) use($input,$email,$enquiry_email) {
           
                $message->from($input['email'], 'Nassa Travels');

                $message->to($email)
                        ->cc($enquiry_email)
                        ->subject('New Feedback has been received');

        });
        
        return Redirect::back();
    }
    
    public function getBlog()
    {
        $blogs = Blog::whereNotNull('published')->orderBy('created_at')->get();
      
        $blogs_categories = BlogCategory::all();
      
        return View::make('frontend.blogs',array('blogs'=>$blogs,'blog_categories' => $blogs_categories));
    }
  
    public function postNewsletterSubscription()
    {
         $input = Input::all();

          if($input)
          {
             $letter = NewsletterSubscription::where('email','=',$input['email'])->first();

             if(empty($letter))
             {
                $newsletter = new NewsletterSubscription;

                $newsletter->email = isset($input['email']) ? $input['email'] : null;

                $newsletter->save();

                return Response::json($newsletter);
             }
             else
             {
                return 'false';
             }
          }   
    
     }
  

    public function getHotelSearch()
    {
      
        $input = Input::except('_token');
//      dd($input);
        $email = Setting::get('admin email');
      
        $data = array('input'=>$input);
        
        Mail::send('emails.emailhotels', $data, function($message) use($email){
                
        $message->to($email,'Nassa Travels')->subject('Hotel Booking Details');        
                  
        });

        return Redirect::back();
      }

  
	public function postFlightSearch()
	{
		$input = Input::except('_token');
		
		$email = Setting::get('admin email');
		
        $data = array('input'=>$input);
//        dd($data);
        Mail::send('emails.emailflights',$data, function($message) use($email) {
         
        $message->to($email,'Nassa Travels')->subject('Flight Booking Details');

        });
        
        return Redirect::back();
	}
	
	public function postTransportSearch()
	{
		$input = Input::except('_token');
		
		$email = Setting::get('admin email');
		
        $data = array('input'=>$input);
//        dd($data);
        Mail::send('emails.emailtransport',$data, function($message) use($email) {
         
        $message->to($email,'Nassa Travels')->subject('Transport Booking Details');

        });
        
        return Redirect::back();
	}
}
