<?php

class HotelCategoryController extends BaseController
{
    /**
     * get Action for Hotels View
     * 
     * @return Rendered view
     */
    
    public function getHotels()
    {
        $hotel_categories = DB::table('hotel_categories')
                        ->select('hotel_categories.title','hotel_categories.slug','hotel_categories.main_image')->get();
        
        return View::make('frontend.hotels',array('hotel_categories'=>$hotel_categories));
    }
    
    /**
     * get Action for HotelDetails View
     * 
     * @return Rendered view
     */
    
    public function getHotelDetails($slug)
    {
        if(!empty($slug))
        {
            $hotel_category = HotelCategory::where('slug','=',$slug)->first();
            
            if($hotel_category)
            {
                $hotels = DB::table('hotels')
                            ->select('hotels.id','hotels.title','hotels.slug','hotels.location','hotels.price','hotels.body')
                            ->where('hotels.hotel_category','=',$hotel_category->id)
                            ->whereNotNull('hotels.published')->get();
                
                return View::make('frontend.hotel-detail',array('hotels'=>$hotels,'hotel_category'=>$hotel_category));
            }
        }
    }
    
    public function postBookNow()
    {
        $input = Input::except('_token');
        
        $email = Setting::get('admin email');
         
        $data = array('input'=>$input,'heading'=>'Book Now');
      
        Mail::send('emails.book-now',$data, function($message) use($input,$email) {
           
                $message->from($email, 'Nassa Travels');

                $message->to($input['email'])->subject('Booking Details');

        });
        
        return Redirect::to($input['url']);
        
    }
    
    public function getHotelJson()
    {
        $term = Input::get('term');
        
        $hotels = DB::table('hotels')->select('id','title','slug','destination_id','user_id','hotel_category','star_category','location',
                                             'price','main_image','special_offer','hotpick','recommended')
                                     ->where('title','like','%'.$term.'%')->get();

        $list = array();
        
        foreach($hotels as $hotel)
        {
            $str = str_ireplace($term,'<span class="term">'.$term.'</span>',$hotel->title);
            array_push($list,array('id'=>$hotel->id,'label'=>$str,'value'=>$hotel->title,'obj'=>json_encode($hotel)));
        }
         
        return Response::json($list);
    }
    
    public function getHotelAddHotpicks($id)
    {
        $hotel = Hotel::find($id);
        
        $hotel->hotpick = 1;
        
        $hotel->save();
        
        return Redirect::back()->with('success','Your hotel has been added on hot picks');
    }
    
    public function getHotelRemoveHotpicks($id)
    {
        $hotel = Hotel::find($id);
        
        $hotel->hotpick = 0;
        
        $hotel->save();
        
        return Redirect::back()->with('success','Your hotel has been removed from hot picks');
    }
    
    
    
}