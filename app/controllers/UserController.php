<?php

class UserController extends Controller{

     /**
     * function for checking user login email password
     * 
     * 
     * @return true or false.
     */
    private function _login($email, $password, $remember_me = 0)
    {
   
        if (Auth::attempt(array('email' => $email, 
            'password' => $password, 
            'active' => 1) , $remember_me))
        
        {
            User::updateLastLogin($email);
            return true;
          }


        return false;
    }

    /**
     * get action for creating login form.
     * 
     * @return user login form
     */
    
    public function getLogin()
    {
      
      
            if(Auth::check())
              {

                  return Redirect::intended('dashboard');
              }       
        
      
        return View::make('user.login');
    }
    
    /**
     * post action for for user login.
     * 
     * 
     */
    
    public function postLogin()
    {   
      
       $user = User::where('email','=',Input::get('loginemail'))->first();
      
        $package_access = explode('/',$user['packages_access']);
        $hotel_access = explode('/',$user['hotels_access']);
//            dd( $package_access);
   
      if(in_array('none',$package_access))
                  {
                    if(in_array('none',$hotel_access))
                    {
                    return Redirect::to('/user/login')->withErrors(array('You do not have access to dashboard, please contact admin!'));
                    }
                    else
                    {
        
                      if($this->_login(Input::get('loginemail') , Input::get('password'), (bool)Input::get('remember_me', '0')))
                        {

                          return Redirect::intended('dashboard');

                        }
                  
                    }
                  }
   
                    
             else
                {
        
                  if($this->_login(Input::get('loginemail') , Input::get('password'), (bool)Input::get('remember_me', '0')))
                    {
                         
                      return Redirect::intended('dashboard');

                    }
                  
                  else 
                    {
                    return Redirect::to('/user/login')->withErrors(array('Wrong Login details or Your Account isn\'t active!'));
                    }     
                }
   
       
        }
    
    /**
     * get action for user logout and redirect to root.
     * 
     * 
     */
    public function getLogout()
    {
        if(!Auth::check())
        {
            return Redirect::route('user.login');
        }
        
        Auth::logout();
        
        return Redirect::route('user.login');
    }

}