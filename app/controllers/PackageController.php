<?php

class PackageController extends BaseController
{
    /**
     * get Action for Domestic Packages details
     * 
     * 
     * @return rendered view
     */
    
    public function getPackageDetails($title_slug)
    {
        if(!empty($title_slug))
        {
            $package = Package::where('slug','=',$title_slug)->first();
            
            $type = PackageType::find($package->package_type_id);

            $package->package_inclusions = DB::table('package_inclusions')
                                            ->select('package_inclusions.title','package_inclusions.desc')
                                            ->where('package_inclusions.package_id','=',$package->id)->get();

            $package->package_itineraries = DB::table('package_itineraries')
                                            ->select('package_itineraries.day_num','package_itineraries.destination',
                                                     'package_itineraries.desc','package_itineraries.image','package_itineraries.meal')
                                            ->where('package_itineraries.package_id','=',$package->id)->get();

            $package->package_images = DB::table('package_images')
                                        ->select('package_images.image')
                                        ->where('package_id','=',$package->id)->get();
            
            $category = array();

            if(!empty($package->hotel_price))
            {
                foreach(json_decode($package->hotel_price) as $key=>$value)
                {
                    foreach($value->data as $k=>$v)
                    {
                        if(!isset($category[$v->cat]))
                        {
                            $category[$v->cat] = $v->cat;
                        }
                    }
                }
            }

            $package->category = $category;

            return View::make('frontend.package-detail',array('package'=>$package,
                                                              'domestic'=>'true',
                                                              'type'=>$type));
        }
    }
    
    /**
     * get Action for International Packages details
     * 
     * 
     * @return rendered view
     */
    
    public function getInternationalPackageDetails($title_slug)
    {
        if(!empty($title_slug))
        {
            $package = Package::where('slug','=',$title_slug)->first();
            $packages_category = $package->package_category()->getResults();
            
            if($packages_category['package_type_id'] == 2)
            {
				$type = PackageType::find($packages_category['package_type_id'] );
				
                $package->package_inclusions = DB::table('package_inclusions')
                                                ->select('package_inclusions.title')
                                                ->where('package_inclusions.package_id','=',$package->id)->get();
                
                $package->package_itineraries = DB::table('package_itineraries')
                                                ->select('package_itineraries.day_num','package_itineraries.destination',
                                                         'package_itineraries.desc','package_itineraries.image','package_itineraries.meal')
                                                ->where('package_itineraries.package_id','=',$package->id)->get();
                
                $package->package_images = DB::table('package_images')
											->select('package_images.image')
											->where('package_id','=',$package->id)->get();
                
                
                $package->package_hotel_details = DB::table('package_hotel_details')
                                                    ->join('hotels','package_hotel_details.hotel_id','=','hotels.id')
                                                    ->select('package_hotel_details.destination_id','package_hotel_details.hotel',
                                                             'package_hotel_details.category','package_hotel_details.start_date',
                                                             'package_hotel_details.end_date','package_hotel_details.price',
                                                             'package_hotel_details.package_itinerary_id','package_hotel_details.hotel_id',
                                                             'hotels.main_image','hotels.title')
                                                    ->where('package_hotel_details.package_id','=',$package->id)
                                                    ->whereNotNull('package_hotel_details.destination_id')
                                                    ->whereNotNull('package_hotel_details.hotel_id')
                                                    ->get();
				
				
                $hotels_categories =    DB::table('package_hotel_details')
                                                   ->select('package_hotel_details.destination_id','package_hotel_details.hotel',
                                                             'package_hotel_details.category','package_hotel_details.start_date',
                                                             'package_hotel_details.end_date','package_hotel_details.price',
                                                             'package_hotel_details.package_itinerary_id','package_hotel_details.hotel_id')
                                                    ->where('package_hotel_details.package_id','=',$package->id)
                                                    ->groupBy('package_hotel_details.category')
                                                    ->get();
                
                $hotels_dates =    DB::table('package_hotel_details')
                                                    ->select(\DB::raw("DATE_FORMAT(start_date, '%M %e, %Y') AS start_date_format"),
                                                             \DB::raw("DATE_FORMAT(end_date, '%M %e, %Y') AS end_date_format"),
                                                             'package_hotel_details.start_date',
                                                             'package_hotel_details.end_date')
                                                    ->where('package_hotel_details.package_id','=',$package->id)
                                                    ->whereNotNull('package_hotel_details.destination_id')
                                                    ->whereNotNull('package_hotel_details.hotel_id')
                                                    ->groupBy('package_hotel_details.start_date')
                                                    ->groupBy('package_hotel_details.end_date')
                                                    ->get();
              
              $package_type = DB::table('packages')
                              ->join ('package_types', 'packages.package_category_id','=','package_types.id')
                              ->where ('packages.package_category_id','=',$package->id)
                              ->get();
                
                $dates = array();
                
                foreach($hotels_dates as $value)
                {
                    array_push($dates,array('label' => $value->start_date_format.' - '.$value->end_date_format,
                                            'value' => $value->start_date.'='.$value->end_date));
                }

                $hotels_details = Package::package_hotel_details_formatted($package->package_hotel_details);
                
                return View::make('frontend.package-detail',array('package'=>$package,
                                                                  'package_category'=>$packages_category,
                                                                  'hotels_details' => isset($hotels_details) ? json_encode($hotels_details) : json_encode(array()),
                                                                  'hotels_categories' => isset($hotels_categories) ? json_encode($hotels_categories) : json_encode(array()),
                                                                  'hotels_dates' => isset($dates) ? json_encode($dates) : json_encode(array()),
                                                                  'international'=>'true','type'=>$type));
            }
        }
    }
    
    public function getBookNow($id)
    {
		$package = Package::find($id);
		
		return View::make('frontend.Booknow',array('package'=>$package));
	}
    
    public function postBookNow()
    {
        $input = Input::except('_token');
        
        $email = Setting::get('admin email');
         
        $data = array('input'=>$input,'heading'=>'Book Now');
		
        Mail::send('emails.book-now',$data, function($message) use($input,$email) {
         
        $message->to($email, 'Nassa Travels')->subject('Booking Details');

        });
        
        return Redirect::back();
    }
    
    /**
     * get Action for Searching Packages hotels details
     * 
     * 
     * @return rendered view
     */
    public function PackageHotelSearch()
    {
        $package = Package::find(Input::get('package_id'));
        
        $hotels_details = array();
      
      
        foreach(json_decode($package->hotel_price) as $key=>$value)
        {
            if(Input::get('dates') == $value->date_range)
            {
                foreach($value->data as $k=>$v)
                {
                    if($v->cat == Input::get('category'))
                    {
                        foreach($v->destinations as $dest_id=>$dest_value)
                        {
                                                   
                            if(!isset($hotels_details[$dest_id]))
                            {
                                if(!isset($hotels_details[$dest_id]['destination']))
                                {
                                    $hotels_details[$dest_id]['destination'] = array();

                                    $destination = Destination::find($dest_id);

                                    array_push($hotels_details[$dest_id]['destination'],$destination);
                                }
                              
//                <--- getting images in hotels details in package search (view details) -->
                  
                                $hotel_data1 = json_decode($dest_value->hotel1_obj, true);
                                $hotel_data2 = json_decode($dest_value->hotel2_obj, true);

                              
                                $hotel_new1 = Hotel::where('id','=',$hotel_data1['id'])->first();
                                $hotel_new2 = Hotel::where('id','=',$hotel_data2['id'])->first();
                              
                                                            
                                $hotel1 = isset($hotel_data1) ? json_decode($hotel_new1) : json_encode(array());
                                $hotel2 = isset($hotel_data2) ? json_decode($hotel_new2) : json_encode(array());

                              
                                if(!isset($hotels_details[$dest_id]['hotels']))
                                {
                                                                   
                                  $hotels_details[$dest_id]['hotels'] = array();

                                    array_push($hotels_details[$dest_id]['hotels'],(array) $hotel1);
                                    array_push($hotels_details[$dest_id]['hotels'],(array) $hotel2);
                                }
                            }
                        }
                        
                        $hotels_details['price_per_person'] = isset($v->price_per_person) ? $v->price_per_person : null;
                        $hotels_details['discounted_price'] = isset($v->discounted_price) ? $v->discounted_price : null;
                        $hotels_details['extra_adult'] = isset($v->extra_adult) ? $v->extra_adult : null;
                        $hotels_details['child_with_bed'] = isset($v->child_with_bed) ? $v->child_with_bed :null;
                        $hotels_details['infant'] = isset($v->infant) ? $v->infant : null;
                    }
                }
            }
        }
        
        
        return Response::json($hotels_details);
    }
     
    public function getPackageCategorySearch()
    {
        $input = Input::all();

        $package = Package::find(Input::get('package_id'));

        $array = array();

        foreach (json_decode($package->hotel_price) as $key=>$value)
        {  
            foreach($value->data as $k=>$v)  
            {
                if($v->cat == Input::get('category'))
                {       
                    array_push($array,$value->date_range);
                }
            }
        }
        return $array;
    }
    
    public function getPackageAddHotpicks($id)
    {
        $package = Package::find($id);
        
        $package->hotpick = 1;
        
        $package->save();
        
        return Redirect::back()->with('success','Your package has been added on hot picks');
    }
    
    public function getPackageRemoveHotpicks($id)
    {
        $package = Package::find($id);
        
        $package->hotpick = 0;
        
        $package->save();
        
        return Redirect::back()->with('success','Your package has been removed from hot picks');
    }
    
}