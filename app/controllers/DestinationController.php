<?php

class DestinationController extends Controller
{
    /**
     * Get Action for fetch destinations records from database
     * 
     * @return array in json
     */
    
    public function getDestinationJson()
    {
        $term = Input::get('term');
        
        $destinations = DB::table('destinations')->select('id','label')
                                                 ->where('deleted_at','=',null)
                                                 ->where('label','like','%'.$term.'%')->get();
        
        $list = array();
        
        foreach($destinations as $destination)
        {
            $str = str_ireplace($term,'<span class="term">'.$term.'</span>',$destination->label);
            array_push($list,array('id'=>$destination->id,'label'=>$str,'value'=>$destination->label));
        }
        return Response::json($list);
    }
    
    public function getPopularDestinations()
    {
        $popular_destinations = Destination::where('popular_destination','=',1)->paginate(10);
        
        return View::make('dashboard.destination.popular-destinations',array('popular_destinations' => $popular_destinations));
    }
    
    public function getPopularDestinationRemove($id)
    {
        $destination = Destination::find($id);
        
        $destination->popular_destination = 0;
        
        $destination->save();
        
        return Redirect::back()->with('success','You have removed popular destination !');
    }
    
}