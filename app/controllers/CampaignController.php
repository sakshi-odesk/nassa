<?php

class CampaignController extends Controller
{
    /**
     * get Action for showing campaign detail.
     * 
     * @return rendered view.
     */
    
    public function getView($slug)
    {
        $campaign = Campaign::where('slug','=',$slug)->first();
        
        $reviews = CampaignReview::where('campaign_id','=',$campaign->id)->get();
        
        return View::make('campaign.default',array('campaign'=>$campaign,'reviews'=>$reviews));
    }

   public function postAjaxCampaignSubmit()
   {
      header("Access-Control-Allow-Origin: *");

      $input = array('campaign_id'=>Input::get('id'),
                     'name'=>Input::get('name'),
                     'phone_no'=>Input::get('phone_no'),
                     'email'=>Input::get('email'),
                     'best_time_to_call'=>Input::get('best_time_to_call'),
                     'subscribe'=>Input::get('subscribe',0),
                     'referer'=>Input::get('referer'),
                     'ip'=>$_SERVER['REMOTE_ADDR'],
                     'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
                     'request'=>$_SERVER['HTTP_REFERER'],
                    );

       $campaign = Campaign::find(Input::get('id'));

       $email = Setting::get('admin email');

       $recepient_email = Setting::get('Enquiry recepient');

       $data = array('input'=>$input,'title'=>$campaign->title);

       Mail::send('emails.campaign-submission',$data, function($message) use($email,$recepient_email,$data) {

              $message->from($email, 'Nassa Travels');

              $message->to($recepient_email)
                      ->cc($email)
                      ->subject('Enquiry for '.$data['title']);

       });

      if(stristr($_SERVER['HTTP_REFERER'], 'kashmir-packages.com'))
      {
          CampaignSubmission::create($input);
             return Response::json(true);
      }

    }
    
    /**
     * get Action for submitting CampaignSubmission details
     * 
     * @return Redirect to campaign url.
     */
    public function postCampaignSubmissionAdd($slug)
    {	

       $input = array('campaign_id'=>Input::get('id'),
                       'name'=>Input::get('name'),
                       'phone_no'=>Input::get('phone_no'),
                       'email'=>Input::get('email'),
                       'best_time_to_call'=>Input::get('best_time_to_call'),
                       'subscribe'=>Input::get('subscribe',0),
                       'referer'=>Input::get('referer'),
                       'ip'=>$_SERVER['REMOTE_ADDR'],
                       'user_agent'=>$_SERVER['HTTP_USER_AGENT'],
                       'request'=>$_SERVER['HTTP_REFERER'],
                      );

        // Rules for validate email or password
        $rules = array (
            'phone_no' => 'Required|min:10',
            'email' => 'Required|email',
            'name' => 'Required',
        );
        $message = array('min' => 'The phone number must be at least 10 digits.');
        
        $campaign = Campaign::find(Input::get('id'));
        
        $validator = Validator::make(Input::all(), $rules,$message);
        
        // check if validator fails or success
        
        if ($validator->fails())
        {
            // validator fails then redirect with errors
            return Redirect::route('campaign.view',array($campaign['slug']))->withErrors($validator);
        } 
        else
        {   
            
            $email = Setting::get('admin email');
            
            $recepient_email = Setting::get('Enquiry recepient');
            
            $data = array('input'=>$input,'title'=>$campaign->title);
            
            Mail::send('emails.campaign-submission',$data, function($message) use($email,$recepient_email,$data) {

                    $message->from($email, 'Nassa Travels');

                    $message->to($recepient_email)
                            ->cc($email)
                            ->subject('Enquiry for '.$data['title']);
            });
            
            $campaign_submission = CampaignSubmission::create($input);
            
            if(is_null($campaign->redirect))
            {
                return Redirect::route('campaign.view',array($campaign['slug']));
            }
            else
            {
                return Redirect::to($campaign->redirect.'?ncid='.$campaign->id);
            }
        }
    }
}