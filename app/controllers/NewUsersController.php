<?php

class NewUsersController extends Controller{
    
    /**
     * Constructer
     * Auth filter for all dashboard template.
     * 
     */

    public function __construct()
    {
        $this->beforeFilter('auth');
        
        $this->beforeFilter('addChild',array('only'=> array('getDestinationAddChild')));
        $this->beforeFilter('addSibling',array('only'=> array('getDestinationAddSibling')));
        $this->beforeFilter('destination_delete',array('only'=> array('getDestinationDelete')));
    }
    
    /**
     * Get Action for creating a dashboard template.
     * 
     * @return render dashboard index.
     */
    
    public function getIndex()
    {
        $campaigns = Campaign::CampaignChart();
        $blogs = Blog::BlogChart();
        $pages = Page::PageChart();
        $hotels = Hotel::HotelChart();
        $packages = Package::PackageChart();
        $slide_shows = SlideShow::SlideShowChart();
        
        return View::make('dashboard.index',array('campaigns'=>$campaigns,
                                                  'blogs'=>$blogs,
                                                  'pages'=>$pages,
                                                  'hotels'=>$hotels,
                                                  'packages'=>$packages,
                                                  'slide_shows'=>$slide_shows));
    }
    
    /**
     * Get Action for showing blog-category list template
     * 
     * @return rendered view
     */
    
    public function getBlogCategoryList()
    {
        return View::make('dashboard.blog-category.list')->with('categories',BlogCategory::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing blog category template.
     * 
     * @return rendered view
     */
    
    public function getBlogCategoryAdd()
    {
        return View::make('dashboard.blog-category.form');
    }
    
    /**
     * Post Action for submitting blog category details.
     * 
     * 
     * @return Redirect to blog-category list.
     */
    
    public function postBlogCategoryAdd()
    {
        $input = Input::except('_token');
        
        $input['slug'] = Util::uniqueSlug(Input::get('title'),'BlogCategory');
        
        $ca = BlogCategory::create($input);
        if($ca)
        {
            return Redirect::route('dashboard.blog-category.list')->with('success','New BlogCategory has been created successfully!');
        }
    }
    
    /**
     * Get Action for showing Editing blog-category template.
     *take blog-category id as argument.
     * 
     * @param string $id. 
     * @return rendered view
     */
    
    public function getBlogCategoryEdit($id)
    {
        $blog_category = BlogCategory::find($id);
        
        if($blog_category)
        {
            return View::make('dashboard.blog-category.form',array('blog_category'=>$blog_category));
        }
    }
    
    /**
     *Post Action for Editing blog-category details
     * 
     * @return Redirect to blog-category list.
     */
    
    public function postBlogCategoryEdit()
    {
        $input = Input::except('_token');
        
        $input['slug'] = Util::uniqueSlug(Input::get('title'),'BlogCategory');
        
        $blog_category = BlogCategory::find($input['id'])->update($input);

        if($blog_category)
        {
            return Redirect::route('dashboard.blog-category.list')->with('success' , "Your BlogCategory has been edited successfully!");
        }
    }
    
    /**
     * Get Action for showing deleting BlogCategory template.
     * take blog-category $id  as argument
     * 
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getBlogCategoryDelete($id)
    {
        $blog_category = BlogCategory::find($id);
        
        if($blog_category)
        {
            return View::make('dashboard.blog-category.delete',array('blog_category'=>$blog_category));
        }
    }
    
    /**
     * Post Action for deleting blog-category details.
     * 
     * @return Redirect to blog-category list.
     */
    
    public function postBlogCategoryDelete()
    {
        $blog_category = BlogCategory::find(Input::get('id'));
        
        if($blog_category)
        {
            $blog_category->delete();
            
            return Redirect::route('dashboard.blog-category.list')->with('success',"Your BlogCategory has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.blog-category.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for Creating BlogCategory Cloning.
     * 
     * @return rendered view.
     */
    
    public function getBlogCategoryCloning($id)
    {
        $blog_category = BlogCategory::find($id);
        
        if($blog_category)
        {
            return View::make('dashboard.blog-category.form',array('blog_category'=>$blog_category,'clone'=>'true'));
        }
    }
    
    public function postBlogCategoryCloning()
    {
        $input = Input::except('_token');
        
        if($input)
        {
            $input['slug'] = Util::uniqueSlug(Input::get('title'),'BlogCategory');
            BlogCategory::create($input);
            
            return Redirect::route('dashboard.blog-category.list')->with('success','You have created clone for BlogCategory successfully!');
        }
    }
    
    /**
     * Get Action for showing page list template.
     * 
     * @return rendered view.
     */
    
    public function getPageList()
    {
        return View::make('dashboard.page.list')->with('pages',Page::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing page template.
     * 
     * @return rendered view.
     */
    public function getPageAdd()
    {
        return View::make('dashboard.page.form');
    }
    
    /**
     * Post Action for submitting page details.
     * 
     * @return Redirect to page list.
     */
    
    public function postPageAdd()
    {
        $newpage = array( 
            'title' => Input::get('title'), 
            'body' => Input::get('body'), 
            'user_id' => Auth::user()->id, 
            'slug' => Util::uniqueSlug(Input::get('title'),'Page'),
            'seo_meta_desc' => Input::get('seo_meta_desc'),
            'seo_meta_kw' => Input::get('seo_meta_kw'),
            'seo_page_title' => Input::get('seo_page_title'),
            ); 
        
        if(Input::get('published') == 1)
        {
            $newpage['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($newpage['published']);
        }
        
        $file = Input::file('page_image'); 
        
        if($file) 
        { 
            // define input file name.
            $filename = Util::rand_filename($file->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($file,$filename);
            
            $newpage['page_image'] = $upload_success;
        } 
        
        $page = Page::create($newpage); 
        
        if($page) 
        { 
            return Redirect::route('dashboard.page.list')->with('success' , "New Page has been created successfully!"); 
        } 
        else 
        { 
            return "Failed to apply changes";
        } 
        return $page; 
    }
    
    /**
     * Get Action for showing editing page template.
     * take page id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getPageEdit($id)
    {
        $page = Page::find($id);
        
        if($page)
        {
            return View::make('dashboard.page.form',array('page'=>$page));
        }
    }
    
    /**
     * Post Action for editing page details.
     * 
     * @return Redirect to page list.
     */
    
    public function postPageEdit()
    {
        $page = Page::find(Input::get('id'));
        
        $file = Input::file('page_image');
        
        if($page) 
        { 
            if($page->title != Input::get('title')) 
            { 
                $page->title = Input::get('title'); 
                $page->slug = Util::uniqueSlug(Input::get('title'),'Page'); 
            }
            if($file)
            {
                $filename = Util::rand_filename($file->getClientOriginalExtension());
            
                $upload_success = Util::upload_file($file,$filename);
            
                $page->page_image = $upload_success;
            }
            
            if(Input::get('published') == 1)
            {
                $page['published'] = date('Y-m-d H:i:s');
            }
            else
            {
                unset($page['published']);
            }
            
            $page->seo_meta_desc = Input::get('seo_meta_desc'); 
            $page->seo_meta_kw = Input::get('seo_meta_kw'); 
            $page->seo_page_title = Input::get('seo_page_title'); 
            $page->body = Input::get('body'); 
            $page->save();
            return Redirect::route('dashboard.page.list')->with('success' , "Your Page has been edited successfully!"); 
        } 
    }
    
    /**
     * Get Action for showing deleting page template.
     * take page id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getPageDelete($id)
    {
        $page = Page::find($id);
        
        if($page)
        {
            return View::make('dashboard.page.delete',array('page'=>$page));
        }
    }
    
    /**
     * Post Action for deleting page details.
     * 
     * @return Redirect to page list.
     */
    
    public function postPageDelete()
    {
        $page = Page::find(Input::get('id'));
        
        if($page)
        {
            $page->delete();
            
            return Redirect::route('dashboard.page.list')->with('success',"Your Page has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.page.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for creating page clone.
     * take page id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getPageCloning($id)
    {
        $page = Page::find($id);
        
        if($page)
        {
            return View::make('dashboard.page.form',array('page'=>$page,'clone'=>'true'));
        }
    }
    
    /**
     * Post Action for creating page clone.
     * 
     * 
     * @return Redirect to page list.
     */
    public function postPageCloning()
    {
        $newpage = array( 
            'title' => Input::get('title'), 
            'body' => Input::get('body'), 
            'user_id' => Auth::user()->id, 
            'slug' => Util::uniqueSlug(Input::get('title'),'Page'),
            'seo_meta_desc' => Input::get('seo_meta_desc'),
            'seo_meta_kw' => Input::get('seo_meta_kw'),
            'seo_page_title' => Input::get('seo_page_title'),
            ); 
        
        if(Input::get('published') == 1)
        {
            $newpage['published'] = null;
        }
        else
        {
            unset($newpage['published']);
        }
        
        $file = Input::file('page_image'); 
        
        if($file) 
        { 
            // define input file name.
            $filename = Util::rand_filename($file->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($file,$filename);
            
            $newpage['page_image'] = $upload_success;
        } 
        
        $page = Page::create($newpage); 
        
        if($page) 
        { 
            return Redirect::route('dashboard.page.list')->with('success' , "New Page Clone has been created successfully!"); 
        } 
        else 
        { 
            return "Failed to apply changes";
        } 
    }
    
    
    /**
     * Get Action for showing blog list template.
     * 
     * @return rendered view.
     */
    
    public function getBlogList()
    {
        return View::make('dashboard.blog.list')->with('blogs',Blog::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing blog template.
     * 
     * @return rendered view.
     */
    public function getBlogAdd()
    {
        $blog_category = BlogCategory::all();
       
        return View::make('dashboard.blog.form',array('blog_categories'=>$blog_category)); 
    }
    
    /**
     * Post Action for submitting blog details.
     * 
     * @return Redirect to page list.
     */
    
    public function postBlogAdd()
    {
        $blog = array( 
            'title' => Input::get('title'),
            'blog_category_id' => Input::get('blog_category_id'),
            'body' => Input::get('body'), 
            'user_id' => Auth::user()->id, 
            'slug' => Util::uniqueSlug(Input::get('title'),'Blog'),
            'slider_title' => Input::get('slider_title'),
            'show_on_slider' => Input::get('show_on_slider','0'),
            'seo_meta_desc' => Input::get('seo_meta_desc'),
            'seo_meta_kw' => Input::get('seo_meta_kw'),
            'seo_page_title' => Input::get('seo_page_title'),
            ); 
        
        if(Input::get('published') == 1)
        {
            $blog['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($blog['published']);
        }
        
        $blog_image = Input::file('blog_image'); 
        $slider_image = Input::file('slider_image'); 
        
        if($blog_image) 
        { 
            // define input file name.
            $filename = Util::rand_filename($blog_image->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($blog_image,$filename);
            
            $blog['blog_image'] = $upload_success;
        } 
        
        if($slider_image) 
        { 
            // define input file name.
            $filename = Util::rand_filename($slider_image->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($slider_image,$filename);
            
            $blog['slider_image'] = $upload_success;
        } 
         
        $blog = Blog::create($blog); 
        
        if($blog) 
        { 
            return Redirect::route('dashboard.blog.list')->with('success' , "New Blog has been created successfully!"); 
        } 
        else 
        { 
            return "Failed to apply changes";
        }
        
        return $blog; 
       
    }
    
    /**
     * Get Action for showing editing blog template.
     * take blog id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getBlogEdit($id)
    {
        $blog = Blog::find($id);
        
        $blog_category = BlogCategory::all();
        
        if($blog)
        {
            return View::make('dashboard.blog.form',array('blog'=>$blog,'blog_categories'=>$blog_category));
        }
    }
    
    /**
     * Post Action for editing blog details.
     * 
     * @return Redirect to blog list.
     */
    
    public function postBlogEdit()
    {
        
        $blog = Blog::find(Input::get('id'));
        
        $blog_image = Input::file('blog_image');
        $slider_image = Input::file('slider_image');
        
        if($blog) 
        { 
            if($blog->title != Input::get('title')) 
            { 
                $blog->title = Input::get('title'); 
                $blog->slug = Util::uniqueSlug(Input::get('title'),'Blog'); 
            }
            if($blog_image)
            {
                $filename = Util::rand_filename($blog_image->getClientOriginalExtension());
            
                $upload_success = Util::upload_file($blog_image,$filename);
            
                $blog->blog_image = $upload_success;
            }
            if($slider_image)
            {
                $filename = Util::rand_filename($slider_image->getClientOriginalExtension());
            
                $upload_success = Util::upload_file($slider_image,$filename);
            
                $blog->slider_image = $upload_success;
            }
            
            
            if(!is_null(Input::get('published')))
            {
                if(is_null($blog['published']))
                {   
                    $blog['published'] = date('Y-m-d H:i:s');
                }
            }
            else
            {
                $blog['published'] = null;
            }
           
            $blog->blog_category_id = Input::get('blog_category_id'); 
            $blog->seo_meta_desc = Input::get('seo_meta_desc'); 
            $blog->seo_meta_kw = Input::get('seo_meta_kw'); 
            $blog->seo_page_title = Input::get('seo_page_title'); 
            $blog->slider_title = Input::get('slider_title'); 
            $blog->show_on_slider = Input::get('show_on_slider',0); 
            $blog->body = Input::get('body'); 
            $blog->save();
            return Redirect::route('dashboard.blog.list')->with('success' , "Your Blog has been edited successfully!"); 
        } 
        
    }
    
    /**
     * Get Action for showing deleting blog template.
     * take blog id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getBlogDelete($id)
    {
        $blog = Blog::find($id);
        
        if($blog)
        {
            return View::make('dashboard.blog.delete',array('blog'=>$blog));
        }
    }
    
    /**
     * Post Action for deleting blog details.
     * 
     * @return Redirect to blog list.
     */
    
    public function postBlogDelete()
    {
        $blog = Blog::find(Input::get('id'));
        
        if($blog)
        {
            $blog->delete();
            
            return Redirect::route('dashboard.blog.list')->with('success',"Your Blog has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.blog.list')->with('error' , "An error happened!");
        }
    }
    
     /**
     * Get Action for creating blog clone.
     * take blog id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getBlogCloning($id)
    {
        $blog = Blog::find($id);
        
        $blog_category = BlogCategory::all();
        
        if($blog)
        {
            return View::make('dashboard.blog.form',array('blog'=>$blog,'blog_categories'=>$blog_category,'clone'=>'true'));
        }
    }
    
    /**
     * Post Action for creating  blog clone.
     * 
     * @return Redirect to blog list.
     */
    
    public function postBlogCloning()
    {
        $blog = array( 
            'title' => Input::get('title'),
            'blog_category_id' => Input::get('blog_category_id'),
            'body' => Input::get('body'), 
            'user_id' => Auth::user()->id, 
            'slug' => Util::uniqueSlug(Input::get('title'),'Blog'),
            'slider_title' => Input::get('slider_title'),
            'show_on_slider' => Input::get('show_on_slider','0'),
            'seo_meta_desc' => Input::get('seo_meta_desc'),
            'seo_meta_kw' => Input::get('seo_meta_kw'),
            'seo_page_title' => Input::get('seo_page_title'),
            );    
        
        $blog_image = Input::file('blog_image');
        $slider_image = Input::file('slider_image');
        
        if($blog) 
        { 
            if($blog_image)
            {
                $filename = Util::rand_filename($blog_image->getClientOriginalExtension());
            
                $upload_success = Util::upload_file($blog_image,$filename);
            
                $blog['blog_image'] = $upload_success;
            }
            if($slider_image)
            {
                $filename = Util::rand_filename($slider_image->getClientOriginalExtension());
            
                $upload_success = Util::upload_file($slider_image,$filename);
            
                $blog['slider_image'] = $upload_success;
            }
            
            if(Input::get('published') == 1)
            {
                $blog['published'] = null;
            }
            
            Blog::create($blog);
           return Redirect::route('dashboard.blog.list')->with('success' , "Your Blog Clone has been created successfully!"); 
        } 
        
    }
    
     /**
     * Get Action for showing hotel-category list template
     * 
     * @return rendered view
     */
    
    public function getHotelCategoryList()
    {
        return View::make('dashboard.hotel-category.list')->with('categories',HotelCategory::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing hotel category template.
     * 
     * @return rendered view
     */
    
    public function getHotelCategoryAdd()
    {
        return View::make('dashboard.hotel-category.form');
    }
    
    /**
     * Post Action for submitting hotel category details.
     * 
     * 
     * @return Redirect to hotel-category list.
     */
    
    public function postHotelCategoryAdd()
    {
        $hotel_category = array( 
                'title' => Input::get('title'),
                'slug' => Util::uniqueSlug(Input::get('title'),'HotelCategory')
                ); 
        
        $main_image = Input::file('main_image');
        
        if($main_image)
        {   
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $hotel_category['main_image'] = $upload_success;
        }
        
        $hotel_category = HotelCategory::create($hotel_category);
        
        if($hotel_category)
        {
            return Redirect::route('dashboard.hotel-category.list')->with('success','New HotelCategory has been created successfully!');
        }
    }
    
    /**
     * Get Action for showing Editing hotel-category template.
     *take hotel-category id as argument.
     * 
     * @param string $id. 
     * @return rendered view
     */
    
    public function getHotelCategoryEdit($id)
    {
        $hotel_category = HotelCategory::find($id);
        
        if($hotel_category)
        {
            return View::make('dashboard.hotel-category.form',array('hotel_category'=>$hotel_category));
        }
    }
    
    /**
     *Post Action for Editing hotel-category details
     * 
     * @return Redirect to hotel-category list.
     */
    
    public function postHotelCategoryEdit()
    {
        $hotel_category = HotelCategory::find(Input::get('id'));
        
        $main_image = Input::file('main_image');
        
        if($main_image)
        {
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $hotel_category['main_image'] = $upload_success;
        }
        
        $hotel_category['title'] = Input::get('title');
        $hotel_category['slug'] = Util::uniqueSlug(Input::get('title'),'HotelCategory');
        $hotel_category->save();
        
        if($hotel_category)
        {
            return Redirect::route('dashboard.hotel-category.list')->with('success' , "Your HotelCategory has been edited successfully!");
        }
    }
    
    /**
     * Get Action for showing deleting HotelCategory template.
     * take hotel-category $id  as argument
     * 
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getHotelCategoryDelete($id)
    {
        $hotel_category = HotelCategory::find($id);
        
        if($hotel_category)
        {
            return View::make('dashboard.hotel-category.delete',array('hotel_category'=>$hotel_category));
        }
    }
    
    /**
     * Post Action for deleting hotel-category details.
     * 
     * @return Redirect to hotel-category list.
     */
    
    public function postHotelCategoryDelete()
    {
        $hotel_category = HotelCategory::find(Input::get('id'));
        
        if($hotel_category)
        {
            $hotel_category->delete();
            
            return Redirect::route('dashboard.hotel-category.list')->with('success',"Your HotelCategory has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.hotel-category.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for creating hotel-category cloning.
     *take hotel-category id as argument.
     * 
     * @param string $id. 
     * @return rendered view
     */
    
    public function getHotelCategoryCloning($id)
    {
        $hotel_category = HotelCategory::find($id);
        
        if($hotel_category)
        {
            return View::make('dashboard.hotel-category.form',array('hotel_category'=>$hotel_category,'clone'=>'true'));
        }
    }
    
    /**
     *Post Action for creating hotel-category clone
     * 
     *  
     * @return Redirect to hotel-category list.
     */
    
    public function postHotelCategoryCloning()
    {
        $hotel_category = array( 
                'title' => Input::get('title'),
                'slug' => Util::uniqueSlug(Input::get('title'),'HotelCategory')
                );

        $main_image = Input::file('main_image');
        
        if($main_image)
        {
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $hotel_category['main_image'] = $upload_success;
        }
        
        $hotel_category = HotelCategory::create($hotel_category);
              
        if($hotel_category)
        {
            return Redirect::route('dashboard.hotel-category.list')->with('success' , "Your HotelCategory Clone has been created successfully!");
        }
    }
    
    /**
     * Get Action for showing hotel list template.
     * 
     * @return rendered view.
     */
    
    public function getHotelList()
    {
        return View::make('dashboard.hotel.list')->with('hotels',Hotel::OrderBy('created_at','desc')->get());
    }
    
    /**
     * Get Action for showing hotel template.
     * 
     * @return rendered view.
     */
    public function getHotelAdd()
    {
        $hotel_category = HotelCategory::all();
       
        return View::make('dashboard.hotel.form',array('hotel_categories'=>$hotel_category)); 
    }
    
    /**
     * Post Action for submitting hotel details.
     * 
     * @return Redirect to hotel list.
     */
    
    public function postHotelAdd()
    {
        $hotel = array( 
            'title' => Input::get('title'),
            'hotel_category' => Input::get('hotel_category'),
            'destination_id' => Input::get('destination_id'),
            'slug' => Util::uniqueSlug(Input::get('title'),'Hotel'),
            'location' => Input::get('location'),
            'price' => Input::get('price'),
            'special_offer' => Input::get('special_offer','0'),
            'hotpick' => Input::get('hotpick',0),
            'recommended' => Input::get('recommended',0),
            'body' => Input::get('body'),
            'seo_meta_desc' => Input::get('seo_meta_desc'),
            'seo_meta_kw' => Input::get('seo_meta_kw'),
            'seo_page_title' => Input::get('seo_page_title'),
            'main_image' => 'main_image',
            ); 
        
        if(Input::get('published') == 1)
        {
            $hotel['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($hotel['published']);
        }
      
         $image = Input::file('image');
      
          if(!is_null($image))    
          {
              $filename = Util::rand_filename($image->getClientOriginalExtension());

              $upload_success = Util::upload_file($image,$filename);

              $hotel['main_image'] = $upload_success;
          }
        
        $hotel = Hotel::create($hotel); 
        
        $main_images = Input::file('main_image');
        
        if($main_images) 
        { 
            // define input file name.
            foreach($main_images as $main_image)
            {   
                if(!is_null($main_image))    
                {
                    $filename = Util::rand_filename($main_image->getClientOriginalExtension());

                    $upload_success = Util::upload_file($main_image,$filename);

                    $hotel_image = array('hotel_id'=>$hotel->id);
                    $hotel_image['path'] = $upload_success;

                    $hotel_image = HotelImage::create($hotel_image);
                }
            }
        } 
        
        if($hotel) 
        { 
            return Redirect::route('dashboard.hotel.list')->with('success' , "New Hotel has been created successfully!"); 
        } 
        else 
        { 
            return "Failed to apply changes";
        }
        
    }
    
    /**
     * Get Action for showing editing hotel template.
     * take hotel id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getHotelEdit($id)
    {
        $hotel = Hotel::find($id);
        
        $hotel_category = HotelCategory::all();
        $hotel_image = HotelImage::where('hotel_id','=',$id)->get();
        
        if($hotel)
        {
            return View::make('dashboard.hotel.form',array('hotel'=>$hotel,'hotel_categories'=>$hotel_category,'hotel_images'=>$hotel_image));
        }
    }
    
    /**
     * Post Action for editing hotel details.
     * 
     * @return Redirect to hotel list.
     */
    
    public function postHotelEdit()
    {
        
        $hotel = Hotel::find(Input::get('id'));
        
        $main_images = Input::file('main_image');
       
        if($hotel) 
        { 
            if($hotel->title != Input::get('title')) 
            {
                $hotel->title = Input::get('title'); 
                $hotel->slug = Util::uniqueSlug(Input::get('title'),'Hotel'); 
            }
          
            if($main_images)
            {
                foreach($main_images as $main_image)
                {
                    if(!is_null($main_image))    
                    {
                        $filename = Util::rand_filename($main_image->getClientOriginalExtension());

                        $upload_success = Util::upload_file($main_image,$filename);

                        $hotel_image = array('hotel_id'=>Input::get('id'));
                        $hotel_image['path'] = $upload_success;

                        $hotel_image = HotelImage::create($hotel_image);
                    }
                }
                
            }
            
            if(!is_null(Input::get('published')))
            {
                if(is_null($hotel['published']))
                {   
                    $hotel['published'] = date('Y-m-d H:i:s');
                }
            }
            else
            {
                $hotel['published'] = null;
            }
            
            $hotel->destination_id = Input::get('destination_id');    
            $hotel->hotel_category = Input::get('hotel_category');
            $hotel->location = Input::get('location');
            $hotel->price = Input::get('price'); 
            $hotel->special_offer = Input::get('special_offer',0); 
            $hotel->hotpick = Input::get('hotpick',0);
            $hotel->recommended = Input::get('recommended',0);
            $hotel->body = Input::get('body'); 
            $hotel->seo_meta_desc = Input::get('seo_meta_desc'); 
            $hotel->seo_meta_kw = Input::get('seo_meta_kw'); 
            $hotel->seo_page_title = Input::get('seo_page_title');
          
          
            $image = Input::file('image');
      
            if(!is_null($image))    
            {
                $filename = Util::rand_filename($image->getClientOriginalExtension());

                $upload_success = Util::upload_file($image,$filename);

                $hotel->main_image = $upload_success;
            }
            $hotel->save();
            
            return Redirect::route('dashboard.hotel.list')->with('success' , "Your Hotel has been edited successfully!"); 
        } 
        
    }
    
     /**
     * Get Action for showing deleting Hotel template.
     * take hotel $id  as argument
     * 
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getHotelDelete($id)
    {
        $hotel = Hotel::find($id);
        
        if($hotel)
        {
            return View::make('dashboard.hotel.delete',array('hotel'=>$hotel));
        }
    }
    
    /**
     * Post Action for deleting hotel details.
     * 
     * @return Redirect to hotel list.
     */
    
    public function postHotelDelete()
    {
        $hotel = Hotel::find(Input::get('id'));
        
        if($hotel)
        {
            $hotel->delete();
            
            return Redirect::route('dashboard.hotel.list')->with('success',"Your Hotel has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.hotel.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for Deleting hotel images using ajax.
     * take hotel image id as argument.
     * 
     * @param string $id.
     * @return true.
     */
    
    public function getHotelImageDelete($id)
    {
        $hotel_image = HotelImage::find($id);
        
        if($hotel_image)
        {
            if(Request::ajax())
            {
                $hotel_image->delete();
                
                return Response::json('true');
            }
            else
            {
                $hotel_image->delete();
                
                return Redirect::route('dashboard.hotel.edit',array($hotel_image->hotel_id));
            }
        }
    }
    
    
    /**
     * Get Action for creating editing hotel clone.
     * take hotel id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getHotelCloning($id)
    {
        $hotel = Hotel::find($id);
        
        $hotel_category = HotelCategory::all();
        $hotel_image = HotelImage::where('hotel_id','=',$id)->get();
        
        if($hotel)
        {
            return View::make('dashboard.hotel.form',array('hotel'=>$hotel,'hotel_categories'=>$hotel_category,'hotel_images'=>$hotel_image,'clone'=>'true'));
        }
    }
    
    /**
     * Post Action for creating a hotel clone
     * 
     * @return Redirect to hotel list.
     */
    public function postHotelCloning()
    {
        
        $hotel = array( 
            'title' => Input::get('title'),
            'hotel_category' => Input::get('hotel_category'),
            'destination_id' => Input::get('destination_id'),
            'slug' => Util::uniqueSlug(Input::get('title'),'Hotel'),
            'location' => Input::get('location'),
            'price' => Input::get('price'),
            'special_offer' => Input::get('special_offer','0'),
            'hotpick' => Input::get('hotpick',0),
            'recommended' => Input::get('recommended',0),
            'body' => Input::get('body'),
            'seo_meta_desc' => Input::get('seo_meta_desc'),
            'seo_meta_kw' => Input::get('seo_meta_kw'),
            'seo_page_title' => Input::get('seo_page_title'),
            'main_image' => 'main_image',
            );
        
        if(Input::get('published') == 1)
        {
            $hotel['published'] = null;
        }
        else
        {
            unset($hotel['published']);
        }
      
         $image = Input::file('image');
      
          if(!is_null($image))    
          {
              $filename = Util::rand_filename($image->getClientOriginalExtension());

              $upload_success = Util::upload_file($image,$filename);

              $hotel['main_image'] = $upload_success;
          }
        
        $hotel = Hotel::create($hotel); 
        
        $main_images = Input::file('main_image');
        
        if($main_images) 
        { 
            // define input file name.
            foreach($main_images as $main_image)
            {   
                if(!is_null($main_image))    
                {
                    $filename = Util::rand_filename($main_image->getClientOriginalExtension());

                    $upload_success = Util::upload_file($main_image,$filename);

                    $hotel_image = array('hotel_id'=>$hotel->id);
                    $hotel_image['path'] = $upload_success;

                    $hotel_image = HotelImage::create($hotel_image);
                }
            }
            
        } 
        
        if($hotel) 
        { 
            return Redirect::route('dashboard.hotel.list')->with('success' , "New Hotel has been created successfully!"); 
        } 
        else 
        { 
            return "Failed to apply changes";
        }
    }
    
    /**
     * Get Action for showing Package category list template.
     * 
     * @return rendered view.
     */
    
    public function getPackageCategoryList()
    {
        return View::make('dashboard.package-category.list')->with('categories',PackageCategory::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing PackageCategory template.
     * 
     * @return rendered view.
     */
    public function getPackageCategoryAdd()
    {
        $package_type = PackageType::all();
       
        return View::make('dashboard.package-category.form',array('package_types'=>$package_type)); 
    }
    
    /**
     * Post Action for submitting package-category details.
     * 
     * @return Redirect to package-category list.
     */
    
    public function postPackageCategoryAdd()
    {
       $package_category = array(
                            'title'=>Input::get('title'),
                            'slug' => Util::uniqueSlug(Input::get('title'),'PackageCategory'),
                            'package_type_id' => Input::get('package_type_id'),
                            'body' => Input::get('body'),
                            'seo_meta_desc'=>Input::get('seo_meta_desc'),
                            'seo_meta_kw'=> Input::get('seo_meta_kw'),
                            'seo_page_title'=>Input::get('seo_page_title'));
        
        if(Input::get('published') == 1)
        {
            $package_category['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($package_category['published']);
        }
        
        $main_image = Input::file('main_image');
        
        if($main_image)
        {
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($main_image,$filename);
            
            $package_category['main_image'] = $upload_success;
        }
        
        $package_category = PackageCategory::create($package_category);
        
        if($package_category)
        {
            return Redirect::route('dashboard.package-category.list')->with('success','New PackageCategory has been created successfully!');
        }
        else
        {
            return "Failed to apply changes";
        }
    }
    
    
     /**
     * Get Action for showing editing package-category template.
     * take package-category id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getPackageCategoryEdit($id)
    {
        $package_category = PackageCategory::find($id);
        
        $package_type = PackageType::all();
               
        if($package_category)
        {
            return View::make('dashboard.package-category.form',array('package_category'=>$package_category,'package_types'=>$package_type));
        }
    }
    
    /**
     * Post Action for editing package-category details.
     * 
     * @return Redirect to PackageCategory list.
     */
    
    public function postPackageCategoryEdit()
    {
        
        $package_category = PackageCategory::find(Input::get('id'));
        
        $main_image = Input::file('main_image');
        
        if($package_category) 
        { 
            if($package_category->title != Input::get('title')) 
            { 
                $package_category->title = Input::get('title'); 
                $package_category->slug = Util::uniqueSlug(Input::get('title'),'PackageCategory');
            }
            
            if($main_image)
            {
                $filename = Util::rand_filename($main_image->getClientOriginalExtension());

                $upload_success = Util::upload_file($main_image,$filename);
                
                $package_category['main_image'] = $upload_success;
            }
            
            if(!is_null(Input::get('published')))
            {
                $package_category['published'] = date('Y-m-d H:i:s');
            }
            else
            {
              $package_category['published'] = null;
            }

            $package_category->title = Input::get('title'); 
            $package_category->body = Input::get('body'); 
            $package_category->package_type_id = Input::get('package_type_id');
            $package_category->seo_meta_desc =  Input::get('seo_meta_desc');
            $package_category->seo_meta_kw = Input::get('seo_meta_kw');
            $package_category->seo_page_title = Input::get('seo_page_title');
            
            $package_category->save();
            
            return Redirect::route('dashboard.package-category.list')->with('success', "Your PackageCategory has been edited successfully!"); 
        } 
        
    }
    
     /**
     * Get Action for showing deleting PackageCategory template.
     * take PackageCategory $id  as argument
     * 
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getPackageCategoryDelete($id)
    {
        $package_category = PackageCategory::find($id);
        
        if($package_category)
        {
            return View::make('dashboard.package-category.delete',array('package_category'=>$package_category));
        }
    }
    
    /**
     * Post Action for deleting PackageCategory details.
     * 
     * @return Redirect to hotel list.
     */
    
    public function postPackageCategoryDelete()
    {
        $package_category = PackageCategory::find(Input::get('id'));
        
        if($package_category)
        {
            $package_category->delete();
            
            return Redirect::route('dashboard.package-category.list')->with('success',"Your PackageCategory has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.package-category.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for creating package-category clone.
     * take package-category id as argument.
     * 
     * @param string $id.
     * @return rendered view
     */
    
    public function getPackageCategoryCloning($id)
    {
        $package_category = PackageCategory::find($id);
        
        $package_type = PackageType::all();
               
        if($package_category)
        {
            return View::make('dashboard.package-category.form',array('package_category'=>$package_category,'package_types'=>$package_type,'clone'=>'true'));
        }
    }
    
    /**
     * Post Action for creating a package-category clone.
     * 
     * @return Redirect to package-category list.
     */
    
    public function postPackageCategoryCloning()
    {
        $package_category = array(
                            'title'=>Input::get('title'),
                            'slug' => Util::uniqueSlug(Input::get('title'),'PackageCategory'),
                            'package_type_id' => Input::get('package_type_id'),
                            'body' => Input::get('body'),
                            'seo_meta_desc'=> Input::get('seo_meta_desc'),
                            'seo_meta_kw'=>Input::get('seo_meta_kw'),
                            'seo_page_title'=>Input::get('seo_page_title'));
        
        if(Input::get('published') == 1)
        {
            $package_category['published'] = null;
        }
        else
        {
            unset($package_category['published']);
        }
        
        $main_image = Input::file('main_image');
        
        if($main_image)
        {
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($main_image,$filename);
            
            $package_category['main_image'] = $upload_success;
            
            $package_category = PackageCategory::create($package_category);
        }
        else
        {   
            $package_category = PackageCategory::create($package_category);
        }
        
        if($package_category)
        {
            return Redirect::route('dashboard.package-category.list')->with('success','Your PackageCategory Clone has been created successfully!');
        }
        else
        {
            return "Failed to apply changes";
        }
    }
    
     /**
     * Get Action for showing Package list template.
     * 
     * @return rendered view.
     */
    
    public function getPackageList()
    {
        return View::make('dashboard.package.list')->with('packages',Package::OrderBy('created_at','desc')->get());
    }
    
    /**
     * Get Action for showing Package template.
     * 
     * @return rendered view.
     */
    public function getPackageAdd()
    {
        $package_type = PackageType::all();
        
        $facilities = Facility::all();
        
        return View::make('dashboard.package.step1',array('package_types'=>$package_type,
                                                         'facilities'=>$facilities,
                                                        )); 
    }
    
    /**
     * Post Action for submitting package details.
     * 
     * @return Redirect to Package List.
     */
    
    public function postPackageAdd()
    {
      
//       $package->package_type_id = Input::get('package_type_id');
              
        $type = PackageType::find(Input::get('package_type_id'));

        $slug = $type->title.' '.Input::get('title');
            
        // add all Package Input in array.
        $package = array('package_category_id'=>Input::get('package_category_id',1),
                         'package_type_id'=>Input::get('package_type_id'),
                         'slug'=> Util::uniqueSlug($slug,'Package'),
                         'title' => Input::get('title'),
                         'desc' => Input::get('desc'),
                         'route_map' => Input::get('route_map'),
                         'duration' => Input::get('duration'),
                         'special_offer' => Input::get('special_offer',0),
                         'hotpick' => Input::get('hotpick',0),
                         'recommended' => Input::get('recommended',0),
                         'terms_conditions' => Input::get('terms_conditions'),
                         'policy' => Input::get('policy'),
                         'faq' => Input::get('faq'),
                         'seo_meta_desc'=>Input::get('seo_meta_desc'),
                         'seo_meta_kw' => Input::get('seo_meta_kw'),
                         'seo_page_title'=> Input::get('seo_page_title'));
        
        if(Input::get('published') == 1)
        {
            $package['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($package['published']);
        }
        
        $package = Package::create($package);
        
         //get all PackageFacility details.
        $facility_id = Input::get('PackageFacility');

        if($facility_id)
        {
            foreach($facility_id as $facility)
            {
				
				$fac = Facility::find($facility);

                if($fac->title == 'with Flights')
				{
				    $package->flight_flag = 1;
                    
				}
				else
				{
				    $package->flight_flag = 0;
                    
				}
				$package->save();
                //add PackageFacility in array.
                $record = array('facility_id' => $facility);
                
                // create PackageFacility and adding records in Relations with pakcages.
                $facilities = $package->package_facilities()->save(new PackageFacility($record));
            }
        }
        
        if($package)
        {
            return Redirect::route('package-step2',array($package->id));
        }
        else
        {
            return "Failed to apply changes";
        }
    }
    
    
    public function getPackageStep2($id)
    {
        $package = Package::find($id);
        
        return View::make('dashboard.package.step2',array('package' => $package));
    }
    
    public function postPackageStep2()
    {
        $package = Package::find(Input::get('id'));
        
        $main_image = Input::file('main_image');
        
        // Upload Image
        if($main_image)
        {
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $package->main_image = $upload_success;
        }
        
        $package->save();
        
        $package_images = PackageImage::where('package_id','=',$package->id)->get();
        
        $images = Input::file('PackageImage');

        if($package_images->count() > 0) 
        { 
            foreach($package_images as $key=>$package_image)
            {
                if(!is_null($images[$key]))
                {
                    $new_image = PackageImage::find($package_image->id);
                    
                    $filename = Util::rand_filename($images[$key]->getClientOriginalExtension());

                    $new_image->image = Util::upload_file($images[$key],$filename);
                        
                    $new_image->save();
                }
            }
        }
        else
        {
            foreach($images as $image)
            {   
                $new_image = new PackageImage;
              
                $new_image->package_id = $package->id;
              
                if(!is_null($image))    
                {
                    $filename = Util::rand_filename($image->getClientOriginalExtension());

                    $new_image->image = Util::upload_file($image,$filename);
                }
              
                $new_image->save();
            }
        }
        
        return Redirect::route('package-step3',array($package->id));
    }
    
    
    public function getPackageStep3($id)
    {
        $package = Package::find($id);
        
        $itineraries = $package->package_itineraries()->getResults();
        $inclusions = $package->package_inclusions()->getResults();
        
        $arr = array();
        $inclusions_arr = array();
        
        foreach($itineraries as $itinerary)
        {
            array_push($arr,array('data'=>$itinerary));
        }
        
        foreach($inclusions as $inclusion)
        {
            array_push($inclusions_arr,array('data'=>$inclusion));
        }
        
        return View::make('dashboard.package.step3',array('package' => $package,
                                                          'itineraries' => isset($arr) ? json_encode($arr) : json_encode(array()),
                                                          'inclusions' => isset($inclusions_arr) ? json_encode($inclusions_arr) : json_encode(array())
                                                         ));
    }
    
    public function postPackageStep3()
    {
        $package = Package::find(Input::get('id'));
        
         // get all input PackageItinerary.
    
        $itineraries = Input::get('PackageItineraries');
        
        if(!empty($itineraries))
        {
            foreach(json_decode($itineraries)  as $key=>$value)
            {
                
                if(isset($value->data))
                {
                    $itinerary= PackageItinerary::find($value->data->id);
                    
                      //Save PackageItinerary in relations with Packages.
                    $package->package_itineraries()->save($itinerary);
                }
            }
        }

        $inclusions = Input::get('PackageInclusions');
         
        if(!empty($inclusions))
        {
            foreach(json_decode($inclusions)  as $key=>$value)
            {
                if(isset($value->data))
                {
                    $inclusion = PackageInclusion::find($value->data->id);

                      //Save PackageItinerary in relations with Packages.
                      $package->package_inclusions()->save($inclusion);
                }
            }
        }
        
        return Redirect::route('package-step4',array($package->id));
    }
    
    public function getPackageStep4($id)
    {
        $package = Package::find($id);
        
        $itineraries = DB::table('package_itineraries')
                                    ->select('id','package_id','destination_id','desc','day_num','destination','image')
                                    ->groupBy('destination_id')
                                    ->where('package_id','=',$package->id)
                                    ->get();    
        
        $hotel_categories = HotelCategory::all();
        
        $arr = array();
        
        foreach($itineraries as $itinerary)
        {
            if($itinerary->destination_id)
            {
                $destination = Destination::find($itinerary->destination_id);

                array_push($arr,array('destination_id'=>$destination->id,'label'=>$destination));
            }
        }
        
        return View::make('dashboard.package.step4',array('package' => $package,
                                                          'hotel_price' => isset($package->hotel_price) ? $package->hotel_price : json_encode(array()),
                                                          'hotel_categories' => isset($hotel_categories) ? json_encode($hotel_categories) : json_encode(array()),
                                                          'destinations' => isset($arr) ? json_encode($arr) : array(),
                                                         ));
    }
    
    public function postPackageStep4()
    {
        $input = Input::except('_token');
        
        $min_price = array();
        
        $package = Package::find($input['id']);
        
        foreach(json_decode($input['hotel_price']) as $key=>$value)
        {
           foreach($value->data as $k=>$v)
           {
               array_push($min_price,$v->price_per_person);
               
                if($key == 0 & $k == 0)
                {
                    if(isset($v->discounted_price))
                    {
                        $package->default_discounted_price = !empty($v->discounted_price) ? $v->discounted_price : null;
                    }
                }
           }
        }

        foreach(json_decode($input['destinations']) as $key=>$value)
        {
            $destination = Destination::find($value->destination_id);

            if(!empty($destination->min_package_id))
            {
                $old_package = Package::find($destination->min_package_id);

                if($old_package['min_price'] > min($min_price))
                {
                    $destination->min_package_id = $package->id;
                    $destination->save();
                }
            }
            else
            {
                $destination->min_package_id = $package->id;
                $destination->save();
            }
        }
    
        $package->hotel_price = isset($input['hotel_price']) ? $input['hotel_price'] : null;
        
        $package->min_price = isset($min_price) ? min($min_price) : null;
        
        $package->default_price = isset($min_price) ? $min_price[0] : null;
        
        
        if(!is_null(Input::get('published')))
        {
            $package->published = date('Y-m-d H:i:s');
        }
        else
        {   
            $package->published = null;
        }
        
        $package->save();
        
        return Redirect::route('dashboard.package.list')->with('success','Your package has been saved successfully!');
    
    }
    
    public function getPackageInclusions()
    {
        $term = Input::get('term');
        
        $inclusions = DB::table('package_inclusions')->select('title')
                                                 ->groupBy('title')
                                                 ->where('title','like','%'.$term.'%')->get();
        
        $list = array();
        
        foreach($inclusions as $inclusion)
        {
            $str = str_ireplace($term,'<span class="term">'.$term.'</span>',$inclusion->title);
            array_push($list,array('label'=>$str,'value'=>$inclusion->title));
        }
        
        return Response::json($list);
    }
    
    /**
     * Post Action for adding package Inclusion using ajax
     * 
     * @return Response in json.
     */
    
    public function postPackageInclusionAdd()
    {
        $input = Input::all();
        
        $inclusion = PackageInclusion::create($input);

        if($inclusion)
        {
            return Response::json($inclusion);
        }   
    }
    
    /**
     * Post Action for adding package price summary using ajax
     * 
     * @return Response in json.
     */
    
    public function postPackagePriceSummaryAdd()
    {
        $input = Input::all();

        $price_summary = PackagePriceSummary::create($input);

        if($price_summary)
        {
            return Response::json($price_summary);
        }   
    }
    
    /**
     * Post Action for adding package hotel detail using ajax
     * 
     * @return Response in json.
     */
    
    public function postPackageHotelDetailAdd()
    {
        $input = Input::all();
        
        $hotel_detail = PackageHotelDetail::create($input);

        if($hotel_detail)
        {
            return Response::json($hotel_detail);
        }   
    }
    
    /**
     * Post Action for adding package hotel detail using ajax
     * 
     * @return Response in json.
     */
    
    public function postNewPackageHotelDetailAdd()
    {
        $input = Input::all();
             
        if($input)
        {
            $hotel_detail = new PackageHotelDetail;
            
            $hotel_detail->hotel = isset($input['hotel']) ? $input['hotel'] : null;
            $hotel_detail->hotel_id = isset($input['hotel_id']) ? $input['hotel_id'] : null;
            $hotel_detail->category = isset($input['category']) ? $input['category'] : null;
            $hotel_detail->start_date = isset($input['startdate']) ? $input['startdate'] : null;
            $hotel_detail->end_date = isset($input['enddate']) ? $input['enddate'] : null;
            $hotel_detail->price = isset($input['price']) ? $input['price'] : null;
            $hotel_detail->destination_id = isset($input['destination_id']) ? $input['destination_id'] : null;
            $hotel_detail->package_itinerary_id = isset($input['package_itinerary_id']) ? $input['package_itinerary_id'] : null;
            $hotel_detail->save();
            
            return Response::json($hotel_detail);
        }   
    }
    
    /**
     * Post Action for adding package itinerary using ajax
     * 
     * @return Response in json.
     */
    
    public function postPackageItineraryAdd()
    {
       $input = Input::all();    

        $image = Input::file('image');

        $itinerary = new PackageItinerary;
        
        $itinerary->destination_id = $input['destination_id'];
        $itinerary->destination = $input['destination'];
        $itinerary->day_num = $input['day_num'];
        $itinerary->desc = $input['desc'];
        
        if($image)
        {
            $filename = Util::rand_filename($image->getClientOriginalExtension());

            $itinerary->image = Util::upload_file($image,$filename);
        }
        
        $itinerary->save();
      
         if($itinerary)
        {
            return Response::json($itinerary);
        }   
    }
    
    /**
     * Get Action for showing Package editing template.
     * take Package id as argument
     * 
     * @param string $id.
     * @return rendered view.
     */
    public function getPackageEdit($id)
    {
        $package = Package::find($id);
        
        $facilities = Facility::all();
        
        $package_type = PackageType::all();
        
        return View::make('dashboard.package.step1',array('package'=>$package,
                                                         'facilities'=>$facilities,
                                                         'package_types'=>$package_type,
                                                        )); 
    }
    
    /**
     * Post Action for submitting package details.
     * 
     *@return Redirect to Package List. 
     */
    
    public function postPackageEdit()
    {
        $package = Package::find(Input::get('id'));
        
        if($package)
        {
            if($package->title != Input::get('title')) 
            { 
                $package->title = Input::get('title');
             
                $type = PackageType::find(Input::get('package_type_id'));
                
                $slug = $type->title.' '.Input::get('title');
              
                $package->slug = Util::uniqueSlug($slug,'Package');
            }
          
            $package->package_type_id = Input::get('package_type_id');
            $package->desc = Input::get('desc');
            $package->route_map = Input::get('route_map');
            $package->duration = Input::get('duration');
            $package->special_offer = Input::get('special_offer',0);
            $package->hotpick = Input::get('hotpick',0);
            $package->recommended = Input::get('recommended',0);
            $package->seo_meta_desc = Input::get('seo_meta_desc');
            $package->seo_meta_kw = Input::get('seo_meta_kw');
            $package->seo_page_title = Input::get('seo_page_title');
            $package->terms_conditions = Input::get('terms_conditions');
            $package->policy = Input::get('policy');
            $package->faq = Input::get('faq');
            
            $package->save();
            
        } 
        
        // get all input PackageFacility.
        $facilities_id = Input::get('PackageFacility');

        if($facilities_id)
        {
            // get all PackageFacility records from database according to Package Id.
            $facilities = $package->package_facilities()->getResults();

            if($facilities)
            {
                // Delete all PackageFacility records.
                foreach($facilities as $facility)
                {
                    $facility->delete();
                }
            }

            // Add all Input PackageFacility details in database.
            foreach($facilities_id as $facility_id)
            {
                $fac = Facility::find($facility_id);
           
                if($fac->title == 'with Flights')
				{
				    $package->flight_flag = 1;
                    
				}
				else
				{
				    $package->flight_flag = 0;
                    
				}
                $package->save();
				
                $record = array('facility_id' => $facility_id);
                
                $facilities = $package->package_facilities()->save(new PackageFacility($record));
            }
        }
        return Redirect::route('package-step2',array($package->id));
    }
    
    /**
     * get Action for editing package inclusion form
     * 
     * @return rendered view.
     */
    public function getPackageInclusionEdit($id)
    {
        $inclusion = PackageInclusion::find($id);
         
        if($inclusion)
        {   
            if(Request::ajax())
            {
                $inclusion['title'] = Input::get('title');
                $inclusion->save();
                
                $inclusion = PackageInclusion::find($id);
                
                return Response::json($inclusion);
            }
            else
            {
                return View::make('dashboard.package-inclusion.edit',array('inclusion'=>$inclusion));
            }
        }
    }
    
    /**
     * Post Action for Editng package inclusion
     * 
     * @return render view.
     */
    
    public function postPackageInclusionEdit()
    {
        $inclusion = PackageInclusion::find(Input::get('id'));
        
        if($inclusion)
        {
            $inclusion['title'] = Input::get('title');
            $inclusion['desc'] = Input::get('desc');

            $inclusion->save();

            return Response::json($inclusion);    
        }
    }
    
    /**
     * get Action for deleteing package inclusion form
     * 
     * @return rendered view.
     */
    
    public function getPackageInclusionDelete($id)
    {
        $inclusion = PackageInclusion::find($id);
        
        if($inclusion)
        {    
            return View::make('dashboard.package-inclusion.delete',array('inclusion'=>$inclusion));
        }
    }
    
    /**
     * Post Action for Editng package inclusion
     * 
     * @return render view.
     */
    
    public function postPackageInclusionDelete()
    {
        $inclusion = PackageInclusion::find(Input::get('id'));
        
        if($inclusion)
        {
            $inclusion->delete();
            
            return Response::json(array('status'=>'true','id'=>Input::get('id')));
        }
    }
    
    /**
     * get Action for editing package hotel details
     * 
     * @return rendered view
     */
    public function getPackageHotelDetailEdit($id)
    {
        $hotel_detail = PackageHotelDetail::find($id);
        
        if($hotel_detail)
        {
            return View::make('dashboard.package-hoteldetail.edit',array('hotel_detail'=>$hotel_detail));
        }
    }
    
    /**
     * post Action for editing package hotel detail.
     * 
     * @return redirect to package.
     */
    
    public function postPackageHotelDetailEdit()
    {
        $hotel_detail = PackageHotelDetail::find(Input::get('id'));
        
        if($hotel_detail)
        {
            $hotel_detail['destination'] = Input::get('destination');
            $hotel_detail['hotel'] = Input::get('hotel');
            $hotel_detail['meal_plan'] = Input::get('meal_plan');
            $hotel_detail->save();
            
            if(Request::ajax())
            {
                return Response::json($hotel_detail);
            }
            else
            {
                return Redirect::to('dashboard/package/edit/'.$hotel_detail->package_id);
            }
        }
        
    }
    
    /**
     * post Action for editing package hotel detail.
     * 
     * @return redirect to package.
     */
    
    public function postNewPackageHotelDetailEdit()
    {
        $input = Input::all();

        $hotel_detail = PackageHotelDetail::find(Input::get('id'));
        
        if($hotel_detail)
        {
            $hotel_detail['category'] = isset($input['category']) ? $input['category'] : $hotel_detail->category;
            
            if(isset($input['hotel']) && !empty($input['hotel']))
            {
                $hotel_detail['hotel'] = $input['hotel'];
            }
            else
            {
                $hotel_detail['hotel'] = $hotel_detail->hotel;
            }
            
            if(isset($input['hotel_id']) && !empty($input['hotel_id']))
            {
                $hotel_detail['hotel_id'] = $input['hotel_id'];
            }
            else
            {
                $hotel_detail['hotel_id'] = $hotel_detail->hotel_id;
            }
            
            $hotel_detail['start_date'] = isset($input['start_date']) ? $input['start_date'] : $hotel_detail->start_date;
            $hotel_detail['end_date'] = isset($input['end_date']) ? $input['end_date'] : $hotel_detail->end_date;
            $hotel_detail['price'] = isset($input['price']) ? $input['price'] : $hotel_detail->price;
            $hotel_detail->save();
            
            return Response::json($hotel_detail);
        }
        
    }
    /**
     * get Action for deleting package hotel detail.
     * 
     * 
     * @return rendered view.
     */
    
    public function getPackageHotelDetailDelete($id)
    {
        $hotel_detail = PackageHotelDetail::find($id);
        
        if($hotel_detail)
        {
            return View::make('dashboard.package-hoteldetail.delete',array('hotel_detail'=>$hotel_detail));
        }
    }
    
    /**
     * post Action for deleteing package hotel detail.
     * 
     *@return redirect to package 
     */
    public function postPackageHotelDetailDelete()
    {
        $hotel_detail = PackageHotelDetail::find(Input::get('id'));
        
        if($hotel_detail)
        {
            $hotel_detail->delete();
            
            if(Request::ajax())
            {
                return Response::json(array('status'=>'true','id'=>Input::get('id')));
            }
            else
            {
                return Redirect::to('dashboard/package/edit/'.$hotel_detail->package_id);
            }
        }
    }
    
    /**
     * get Action for editing package price summary.
     * 
     * @return rendered view
     */
    public function getPackagePriceSummaryEdit($id)
    {
        $price_summary = PackagePriceSummary::find($id);
        
        if($price_summary)
        {
            return View::make('dashboard.package-pricesummary.edit',array('price_summary'=>$price_summary));
        }
    }
    
    /**
     * post action for editing package price summary
     * 
     * @return redirect to package.
     */
    
    public function postPackagePriceSummaryEdit()
    {
        $price_summary = PackagePriceSummary::find(Input::get('id'));
        
        if($price_summary)
        {
            $price_summary['desc'] = Input::get('desc');
            $price_summary['cost'] = Input::get('cost');
            $price_summary->save();
            
            if(Request::ajax())
            {
                        
                return Response::json($price_summary);
            }
            else
            {
                return Redirect::to('dashboard/package/edit/'.$price_summary->package_id);
            }
        }
    }
    
    /**
     * get Action for deleting package price summary.
     * 
     * @return rendered view.
     */
    
    public function getPackagePriceSummaryDelete($id)
    {
        $price_summary = PackagePriceSummary::find($id);
        
        if($price_summary)
        {   
            return View::make('dashboard.package-pricesummary.delete',array('price_summary'=>$price_summary));
        }
    }
    
    /**
     * post Action for deleting price summary.
     * 
     * @return redirect to package.
     */
    public function postPackagePriceSummaryDelete()
    {
        $price_summary = PackagePriceSummary::find(Input::get('id'));
        
        if($price_summary)
        {
            $price_summary->delete();
            
            if(Request::ajax())
            {
                return Response::json(array('status'=>'true','id'=>Input::get('id')));
            }
            else
            {
                return Redirect::to('dashboard/package/edit/'.$price_summary->package_id);
            }
        }
    }
    
    /**
     * get Action for editing package itinerary.
     * 
     * @return rendered view.
     */
    
    public function getPackageItineraryEdit($id)
    {
        $itinerary = PackageItinerary::find($id);
        
        if($itinerary)
        {
            return View::make('dashboard.package-itinerary.edit',array('itinerary'=>$itinerary));
        }
    }
    
    /**
     * post Action for editing Package itinerary details.
     * 
     * @return redirect to package.
     */
    
    public function postPackageItineraryEdit()
    {
        $input = Input::all();
        
        $itinerary = PackageItinerary::find(Input::get('id'));
        
        if($itinerary)
        {
            $itinerary['day_num'] = isset($input['day_num']) ? $input['day_num'] : $itinerary->day_num;
            $itinerary['destination'] = isset($input['destination']) ? $input['destination'] : $itinerary->destination;
            $itinerary['desc']= isset($input['desc']) ? $input['desc'] : $itinerary->desc;
            
            if(isset($input['destination_id']) && !empty($input['destination_id']))
            {
                $itinerary['destination_id'] = $input['destination_id'];
            }
            else
            {
                $itinerary['destination_id'] = $itinerary->destination_id;
            }
            
            $image = Input::file('image');
            
            if($image)
            {
                $filename = Util::rand_filename($image->getClientOriginalExtension());

                $itinerary->image = Util::upload_file($image,$filename);
            }
            
            $itinerary->save();
        
            return Response::json($itinerary);

        }
    }
    
    /**
     * get Action for deleting package itinerary.
     * 
     * @return rendered view.
     */
    
    public function getPackageItineraryDelete($id)
    {
        $itinerary = PackageItinerary::find($id);
        
        if($itinerary)
        {
            return View::make('dashboard.package-itinerary.delete',array('itinerary'=>$itinerary));
        }
    }
    
    /**
     * post Actionfor deleteing package itinerary.
     * 
     * @return redirect to package.
     */
     
    public function postPackageItineraryDelete()
    {
        $itinerary = PackageItinerary::find(Input::get('id'));
        
        $hotel_details = PackageHotelDetail::where('package_itinerary_id','=',$itinerary->id)->get();
        
        if($itinerary)
        {
            foreach($hotel_details as $hotel_detail)
            {
                $hotel_detail->delete();
            }
            
            $itinerary->delete();
            
            if(Request::ajax())
            {
                return Response::json(array('status'=>'true','id'=>Input::get('id')));
            }
//            else
//            {
//                return Redirect::to('dashboard/package/edit/'.$itinerary->package_id);
//            }
        }
    }
    
    /**
     * Get Action for deleting pacakge  details.
     * take Package id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getPackageDelete($id)
    {
        $package = Package::find($id);
        
        return View::make('dashboard.package.delete',array('package'=>$package));
    }
    
    /**
     * post Action for editing package details.
     * 
     * @return Redirect to package list.
     */
    
    public function postPackageDelete()
    {
       $package = Package::find(Input::get('id'));
        
        if($package)
        {   
            //get all PackageFacility records from database.
            $facilities = $package->package_facilities()->getResults();
            
            if($facilities)
            {
                //Delete all PackageFacility from database.
                foreach($facilities as $facility)
                {
                    $facility->delete();
                }
            }
            
            // get all PackageInclusion records from database.
            $inclusions_record = $package->package_inclusions()->getResults();
            
            if($inclusions_record)
            {
                //Delete all PackageInclusion records from database
                foreach($inclusions_record as $inclusion_record)
                {
                    $inclusion_record->delete();
                }
            }
            
            //get all PackageItinerary records from database.
            $itineraries_record = $package->package_itineraries()->getResults();
                
            if($itineraries_record)
            {
                // Delete all PackageItinerary records from database.
                foreach($itineraries_record as $itinerary_record) 
                {
                    $itinerary_record->delete();
                }
            }
            
            // get All PackageHotelDetail records from database.
            $hotel_details_record = $package->package_hotel_details()->getResults();
                
            if($hotel_details_record)
            {
                //Delete all PackageHotelDetail records from database.
                foreach($hotel_details_record as $hotel_detail_record)
                {
                    $hotel_detail_record->delete();
                }
            }
            
            // get all PackagePriceSummary records form database.
            $price_summaries_record = $package->package_price_summaries()->getResults();
                
            if($price_summaries_record)
            {
                // Delete all PackagePriceSummary records from database.
                foreach($price_summaries_record as $price_summary_record)
                {
                    $price_summary_record->delete();
                }
            }
            
            
            $package->delete();
            
            return Redirect::route('dashboard.package.list')->with('success',"Your Package has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.package.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for creating Package clone.
     * take Package id as argument
     * 
     * @param string $id.
     * @return rendered view.
     */
    public function getPackageCloning($id)
    {
        $package = Package::find($id);
        
        $facilities = Facility::all();
        
        $package_type = PackageType::all();
        
        $itineraries = $package->package_itineraries()->getResults();
        
        $arr = array();
        
        foreach($itineraries as $itinerary)
        {
            $hotel_details = PackageHotelDetail::where('package_itinerary_id','=',$itinerary->id)
                                                 ->where('package_id','=',$package->id)->get();
            
            array_push($arr,array('data'=>$itinerary,'hotel'=>$hotel_details));
        }
        
        return View::make('dashboard.package.form',array('package'=>$package,
                                                         'facilities'=>$facilities,
                                                         'package_types'=>$package_type,
                                                         'itineraries' => isset($arr) ? json_encode($arr) : json_encode(array()),
                                                         'clone'=>'true')); 
    }
    
    /**
     * Post Action for creating a package clone
     * 
     * @return Redirect to Package list.
     */
    
    public function postPackageCloning()
    {
        
//        $hotel_details = Input::get('NewPackageHotelDetail');
//        dd($hotel_details);
        // add all Package Input in array.
        $package = array('package_category_id'=>Input::get('package_category_id',1),
                         'slug'=> Util::uniqueSlug(Input::get('title'),'Package'),
                         'destination_id' => Input::get('destination_id'),
                         'hotel_level' => Input::get('hotel_level'),
                         'title' => Input::get('title'),
                         'price' => Input::get('price'),
                         'desc' => Input::get('desc'),
                         'discounted_price' => Input::get('discounted_price'),
                         'route_map' => Input::get('route_map'),
                         'duration' => Input::get('duration'),
                         'special_offer' => Input::get('special_offer',0),
                         'hotpick' => Input::get('hotpick',0),
                         'recommended' => Input::get('recommended',0),
                         'seo_meta_desc'=>Input::get('seo_meta_desc'),
                         'seo_meta_kw'=> Input::get('seo_meta_kw'),
                         'seo_page_title'=> Input::get('seo_page_title'));
//        dd($package);
        if(Input::get('published') == 1)
        {
            $package['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($package['published']);
        }
        
        $main_image = Input::file('main_image');
        
        // Upload Image
        if($main_image)
        {
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $package['main_image'] = $upload_success;
        }
        
        //Create Package
        $package = Package::create($package);
        
        //get all PackageFacility details.
        $facility_id = Input::get('PackageFacility');
        
        if($facility_id)
        {
            foreach($facility_id as $facility)
            {
                //add PackageFacility in array.
                $record = array('facility_id' => $facility);
                
                // create PackageFacility and adding records in Relations with pakcages.
                $facilities = $package->package_facilities()->save(new PackageFacility($record));
            }
        }
        
        // get all PackageInclusion details.
        $inclusions = Input::get('PackageInclusion');
        
        if($inclusions)
        {
            foreach($inclusions as $inclusion)
            {   
                $inclusion = PackageInclusion::find($inclusion['id']);
                
                $inclusions = array('title'=>$inclusion['title'],
                                   'weight'=>$inclusion['weight']);
                
                // create PackageInclusion.
                $package->package_inclusions()->save(new PackageInclusion($inclusions));
            }
        }

        // get all PackageItinerary
        $itineraries = Input::get('PackageItinerary');
        
        if($itineraries)
        {  
            foreach($itineraries as $itinerary)
            {
              $itinerary = PackageItinerary::find($itinerary['id']);
            
              $itineraries = array('day_num'=>$itinerary['day_num'],
                                  'destination'=>$itinerary['destination'],
                                  'desc'=>$itinerary['desc'],
                                  'weight'=>$itinerary['weight']);
                    
              //Create new PackageItinerary.
              $new_itinerary = new PackageItinerary($itineraries);
                
              //Save PackageItinerary in relations with Packages.
              $itinerary = $package->package_itineraries()->save($new_itinerary);
            }
        }
        
      
        //get all PackageHotelDetail.
        $hotel_details = Input::get('PackageHotelDetail');
        
        if($hotel_details)
        {  
            foreach($hotel_details as $hotel_detail)
            {
                $hotel_detail = PackageHotelDetail::find($hotel_detail['id']);
                
                $hotel_details = array('destination'=>$hotel_detail['destination'],
                                      'hotel'=>$hotel_detail['hotel'],
                                      'meal_plan'=>$hotel_detail['meal_plan'],
                                      'weight'=>$hotel_detail['weight']);
                
              // Create new PackageHotelDetail.
              $new_hotel_detail = new PackageHotelDetail($hotel_details);
            
              // Save PackageHotelDetail in Relations with Packages.
              $hotel_detail = $package->package_hotel_details()->save($new_hotel_detail);
            }
        }
        
        
        //get all NewPackageHotelDetail.
        $hotel_details = Input::get('NewPackageHotelDetail');
        
        if($hotel_details)
        {  
            foreach($hotel_details as $hotel_detail)
            {
                $hotel_detail = PackageHotelDetail::find($hotel_detail['id']);
                
                $hotel_details = array('hotel'=>$hotel_detail['hotel'],
                                      'hotel_id' => $hotel_detail['hotel_id'],
                                      'destination_id' => $hotel_detail['destination_id'],
                                      'package_itinerary_id' => $hotel_detail['package_itinerary_id'],
                                      'category' => $hotel_detail['category'],
                                      'start_date' => $hotel_detail['start_date'], 
                                      'end_date' => $hotel_detail['end_date'], 
                                      'weight'=>$hotel_detail['weight']);
                
              // Create new PackageHotelDetail.
              $new_hotel_detail = new PackageHotelDetail($hotel_details);
            
              // Save PackageHotelDetail in Relations with Packages.
              $hotel_detail = $package->package_hotel_details()->save($new_hotel_detail);
            }
        }
        
        // get all PackagePriceSummary.
        $price_summaries = Input::get('PackagePriceSummary');
        
        if($price_summaries)
        {  
            foreach($price_summaries as $price_summary)
            {
                $price_summary = PackagePriceSummary::find($price_summary['id']);
                
                $price_summaries = array('desc'=>$price_summary['desc'],
                                        'cost'=>$price_summary['cost'],
                                        'weight'=>$price_summary['weight']);
                
              // Create new PackagePriceSummary.
              $new_price_summary = new PackagePriceSummary($price_summaries);
            
              // Save PackageHotelDetail in Relations with Packages.
              $price_summary = $package->package_price_summaries()->save($new_price_summary);
            }
        }
		
		
		//upload package_images

		$main_images = Input::file('PackageImage');

		if($main_images) 
		{ 
		// define input file name.
			foreach($main_images as $main_image)
			{   
				$new_image = new PackageImage;

				$new_image->package_id = $package->id;

				if(!is_null($main_image))    
				{
					$filename = Util::rand_filename($main_image->getClientOriginalExtension());

					$new_image->image = Util::upload_file($main_image,$filename);
				}

				$new_image->save();
			}
		} 
        
        if($package)
        {
            return Redirect::route('dashboard.package.list')->with('success','Your Package clone has been created successfully!');
        }
        else
        {
            return "Failed to apply changes";
        }
    }
    
    /**
     * Get Action for getting package-category details using ajax.
     * take package_type $id as  argument.
     * 
     * @param string $id.
     * @return Response in Json form.
     */
    
    public function getPackageCategory($id)
    {
        $package_category = DB::select('select id,title from package_categories where package_type_id = ?', array($id));
        
        if($package_category)
        {
                return Response::json($package_category);
        }
        else
        {
            return Response::json('false');
        }
    }
    
    /**
     * Get Action for showing Slide show list template.
     * 
     * @return rendered view.
     */
    
    public function getSlideShowList()
    {
        return View::make('dashboard.slide-show.list')->with('slide_shows',SlideShow::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing Slide show template.
     * 
     * @return rendered view.
     */
    public function getSlideShowAdd()
    {
        return View::make('dashboard.slide-show.form'); 
    }
    
    /**
     * post Action for submitting slideshow and slide details.
     * 
     * @return Redirect to SlideShow list.
     */
    
    public function postSlideShowAdd()
    {
        
        $slide_show = array('title'=> Input::get('title'));
        
        if(Input::get('published') == 1)
        {
            $slide_show['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($slide_show['published']);
        }
        
        $slide_show = SlideShow::create($slide_show);
        
        //get slide input details.
        $slides = Input::get('Slide');
        
        $main_images = Input::file('Slide');
         
        if($slides)
        {   
            $i = 0;
            foreach($slides as $slide)
            {   
                // Upload Slide Images.
                if(!is_null($main_images))    
                {   
                    $filename = Util::rand_filename($main_images[$i]['main_image']->getClientOriginalExtension());

                    $upload_success = Util::upload_file($main_images[$i]['main_image'],$filename);
                }
                // Add Slide Input details in array.
                $record = array('slide_show_id' => $slide_show->id,
                                'title'=>$slide['title'],
                                'url'=>$slide['url'],
                                'main_image'=> $upload_success);
                
                // create slideshow and Slide.
                $slide_show->slides()->save(new Slide($record));
                $i++;
            }
        }
        
        return Redirect::route('dashboard.slide-show.list')->with('success','New SlideShow has been created successfully!');
            
    }
    
    /**
     * Get Action for Editing slideshow and slide details.
     * take SlideShow id as argument
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getSlideShowEdit($id)
    {
        $slide_show = SlideShow::find($id);
        
        return View::make('dashboard.slide-show.form',array('slide_show'=>$slide_show));
    }
    
    /**
     * post Action for editing slideshow and slide details.
     * 
     * @return Redirect to slideshow list.
     */
    
    public function postSlideShowEdit()
    {
        $slide_show = SlideShow::find(Input::get('id'));
        
        if($slide_show)
        {
            if(Input::get('published') == 1)
            {
                $slide_show['published'] = date('Y-m-d H:i:s');
            }
            else
            {
                $slide_show['published'] = null;
            }
            $slide_show->title = Input::get('title');
            $slide_show->save();
            
            // get all input slide details.
            $slides = Input::get('Slide');
            
            $main_images = Input::file('Slide');
            
            // check if $slides not empty.
            if($slides)
            {   
                $i = 0;
                foreach($slides as $slide)
                {   
                    // check if $slide['id'] is not empty.
                    if(!empty($slide['id']))
                    {   
                        //get all slide records from database.
                        $record = Slide::find($slide['id']);
                        
                        // check in $main_images is not empty than upload Images.
                        if($main_images[$i]['main_image'])
                        {   
                            $filename = Util::rand_filename($main_images[$i]['main_image']->getClientOriginalExtension());
                        
                            $upload_success = Util::upload_file($main_images[$i]['main_image'],$filename);
                            
                            // update slide details.
                            $record->slide_show_id = $slide_show->id;
                            $record->title = $slide['title'];
                            $record->url = $slide['url'];
                            $record->main_image = $upload_success;
                            $record->save();
                            
                        }
                        
                        //update SlideShow details.
                        $record->slide_show_id = $slide_show->id;
                        $record->title = $slide['title'];
                        $record->url = $slide['url'];
                        $record->save();
                    }
                    else
                    {
                        // check $main_images is not empty than upload images.
                        if(!is_null($main_images[$i]['main_image']))    
                        {   
                            $filename = Util::rand_filename($main_images[$i]['main_image']->getClientOriginalExtension());

                            $upload_success = Util::upload_file($main_images[$i]['main_image'],$filename);

                            $record = array('slide_show_id' => $slide_show->id,
                                        'title'=>$slide['title'],
                                        'url'=>$slide['url'],
                                        'main_image'=> $upload_success);
                            
                            //create SlideShow and Slide.
                            $slide_show->slides()->save(new Slide($record));
                        }
                    }
                    
                    $i++;
                }
            }
            
            return Redirect::route('dashboard.slide-show.list')->with('success','Your SlideShow has been edited successfully!');
            
        }
    }
    
    
    /**
     * Get Action for deleting slideshow and slide details.
     * take SlideShow id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getSlideShowDelete($id)
    {
        $slide_show = SlideShow::find($id);
        
        return View::make('dashboard.slide-show.delete',array('slide_show'=>$slide_show));
    }
    
    /**
     * Post Action for editing slideshow and slide details.
     * 
     * @return Redirect to slideshow list.
     */
    
    public function postSlideShowDelete()
    {
       $slide_show = SlideShow::find(Input::get('id'));
        
        // check $slide_show is not empty than delete records from database.
        if($slide_show)
        {   
            $slide_records = $slide_show->slides()->getResults();
            
            // check $slide_records is not empty than delete slide records from database.
            if($slide_records)
            {
                foreach($slide_records as $slide_record)
                {
                    $slide_record->delete();
                }
            }
            // delete slideshow records from database.
            $slide_show->delete();
            
            return Redirect::route('dashboard.slide-show.list')->with('success',"Your SlideShow has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.slide-show.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for creating slideshow and slide clone.
     * take SlideShow id as argument
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getSlideShowCloning($id)
    {
        $slide_show = SlideShow::find($id);
        
        return View::make('dashboard.slide-show.form',array('slide_show'=>$slide_show,'clone'=>'true'));
    }
    
    /**
     * post Action for creating slideshow and slide clone.
     * 
     * @return Redirect to slideshow list.
     */
    
    public function postSlideShowCloning()
    {
        $slide_show = array('title'=> Input::get('title'));
        
        if(Input::get('published') == 1)
        {
            $slide_show['published'] = null;
        }
        else
        {
            unset($slide_show['published']);
        }
        
        $slide_show = SlideShow::create($slide_show);
        
        //get slide input details.
        $slides = Input::get('Slide');
        
        $main_images = Input::file('Slide');
        
        if($slides)
        {   
            $i = 0;
            foreach($slides as $slide)
            {   
                // Upload Slide Images.
                if(!is_null($main_images[$i]['main_image']))    
                {   
                    $filename = Util::rand_filename($main_images[$i]['main_image']->getClientOriginalExtension());

                    $upload_success = Util::upload_file($main_images[$i]['main_image'],$filename);
                    
                    $record = array('slide_show_id' => $slide_show->id,
                                'title'=>$slide['title'],
                                'url'=>$slide['url'],
                                'main_image'=> $upload_success);
                }
                else
                {
                    $record = array('slide_show_id' => $slide_show->id,
                                'title'=>$slide['title'],
                                'url'=>$slide['url']);
                }
                // Add Slide Input details in array.
                
                
                // create slideshow and Slide.
                $slide_show->slides()->save(new Slide($record));
                $i++;
            }
        }
        
        return Redirect::route('dashboard.slide-show.list')->with('success','Your SlideShow clone has been created successfully!');
    }
    
    
    /**
     * Get Action for showing Campaign list template.
     * 
     * @return rendered view.
     */
    
    public function getCampaignList()
    {
        return View::make('dashboard.campaign.list')->with('campaigns',Campaign::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing Campaign template.
     * 
     * @return rendered view.
     */
    public function getCampaignAdd()
    {
        return View::make('dashboard.campaign.form'); 
    }
    
    /**
     * post Action for submitting campaign details.
     * 
     * @return Redirect to Campaign list.
     */
    public function postCampaignAdd()
    {
        
        // get all input campaign in to array.
        $campaign = array('title'=>Input::get('title'),
                         'slug'=> Util::uniqueSlug(Input::get('title'),'Campaign'),
                         'heading'=>Input::get('heading'),
                         'sub_heading'=>Input::get('sub_heading'),
                         'desc' => Input::get('desc'),
                         'inclusion' => Input::get('inclusion'),  
                         'redirect' => Input::get('redirect'),
                         'published' => Input::get('published',0));
        
        $background_image = Input::file('background_image');
        
        // check $background_image is not empty than upload background_image.
        if($background_image)
        {
            $filename = Util::rand_filename($background_image->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($background_image,$filename);
            
            $campaign['background_image'] = $upload_success;
        }
        
        // create campaign.
        $campaign = Campaign::create($campaign);
        
        // get all input CampaignReview .
        $campaign_reviews = Input::get('CampaignReview');
        
        // check if $campaign_reviews is not empty.
        if($campaign_reviews)
        {  
            foreach($campaign_reviews as $campaign_review)
            {
              //  create CampaignReview.
              $campaign_review = new CampaignReview($campaign_review);
            
              $campaign_review = $campaign->campaign_reviews()->save($campaign_review);
            }
        }
        
        if($campaign)
        {
            return Redirect::route('dashboard.campaign.list')->with('success','New Campaign has been created successfully!');
        }
        
    }
    
    /**
     * Get Action for Editing Campaign details.
     * take Campaign id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getCampaignEdit($id)
    {
        $campaign = Campaign::find($id);
        
        return View::make('dashboard.campaign.form',array('campaign'=>$campaign));
    }
    
    /**
     * post Action for editing Campaign details.
     * 
     * @return Redirect to Campaign list.
     */
    
    public function postCampaignEdit()
    {
        $campaign = Campaign::find(Input::get('id'));
        
        if($campaign)
        {
            if($campaign->title != Input::get('title')) 
            { 
                $campaign->title = Input::get('title'); 
                $campaign->slug = Util::uniqueSlug(Input::get('title'),'Campaign');
            }
            
            $background_image = Input::file('background_image');
           
            // check $background_image is not empty than upload image.
            if($background_image)
            {
                $filename = Util::rand_filename($background_image->getClientOriginalExtension());

                $upload_success = Util::upload_file($background_image,$filename);

                $campaign['background_image'] = $upload_success;
            }
            
            // update Campaign details.
            $campaign->heading = Input::get('heading');
            $campaign->sub_heading = Input::get('sub_heading');
            $campaign->desc = Input::get('desc');
            $campaign->inclusion = Input::get('inclusion');
            $campaign->redirect = Input::get('redirect');
            $campaign->published = Input::get('published',0);
            $campaign->save();
            
            // get all input CampaignReview.
            $campaign_input_reviews = Input::get('CampaignReview');
            
            if($campaign_input_reviews)
            {   
                // get all CampaignReview records from database.
                $campaign_review_records = $campaign->campaign_reviews()->getResults();
                
                if($campaign_review_records)
                {
                    // Delete CampaignReview records from database.
                    foreach($campaign_review_records as $campaign_review_record)
                    {
                        $campaign_review_record->delete();
                    }
                }
                // Create CampaignReview.
                foreach($campaign_input_reviews as $campaign_input_review)
                {
                    $campaign->campaign_reviews()->save(new CampaignReview($campaign_input_review));
                }
            }
            
            return Redirect::route('dashboard.campaign.list')->with('success','Your Campaign has been edited successfully!');
        }
    }
    
    /**
     * Get Action for deleting Campaign details.
     * take Campaign is as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getCampaignDelete($id)
    {
        $campaign = Campaign::find($id);
        
        return View::make('dashboard.campaign.delete',array('campaign'=>$campaign));
    }
    
    /**
     * Post Action for editing Campaign details.
     * 
     * @return Redirect to Campaign list.
     */
    
    public function postCampaignDelete()
    {
        $campaign = Campaign::find(Input::get('id'));
        
        if($campaign)
        {   // get all CampaignReview from database.
            $campaign_reviews = $campaign->campaign_reviews()->getResults();
                
            if($campaign_reviews)
            {
                // Delete CampaignReview records from database.
                foreach($campaign_reviews as $campaign_review)
                {
                    $campaign_review->delete();
                }
            }
            //Delete Campaign records from database.
            $campaign->delete();
            
            return Redirect::route('dashboard.campaign.list')->with('success',"Your Campaign has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.campaign.list')->with('error' , "An error happened!");
        }
    }
    
    /**
     * Get Action for creating Campaign clone.
     * take Campaign id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getCampaignCloning($id)
    {
        $campaign = Campaign::find($id);
        
        return View::make('dashboard.campaign.form',array('campaign'=>$campaign,'clone'=>'true'));
    }
    
    /**
     * post Action for creating Campaign clone.
     * 
     * @return Redirect to Campaign list.
     */
    
    public function postCampaignCloning()
    {
         // get all input campaign in to array.
        $campaign = array('title'=>Input::get('title'),
                         'slug'=> Util::uniqueSlug(Input::get('title'),'Campaign'),
                         'heading'=>Input::get('heading'),
                         'sub_heading'=>Input::get('sub_heading'),
                         'redirect'=>Input::get('redirect'),
                         'desc' => Input::get('desc'));
        
        if(Input::get('published') == 1)
        {
            $campaign['published'] = 0;
        }
        else
        {
            $campaign['published'] = 0;
        }
        
        $background_image = Input::file('background_image');
        
        // check $background_image is not empty than upload background_image.
        if($background_image)
        {
            $filename = Util::rand_filename($background_image->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($background_image,$filename);
            
            $campaign['background_image'] = $upload_success;
        }
        
        // create campaign.
        $campaign = Campaign::create($campaign);
        
        // get all input CampaignReview .
        $campaign_reviews = Input::get('CampaignReview');
        
        // check if $campaign_reviews is not empty.
        if($campaign_reviews)
        {  
            foreach($campaign_reviews as $campaign_review)
            {
              //  create CampaignReview.
              $campaign_review = new CampaignReview($campaign_review);
            
              $campaign_review = $campaign->campaign_reviews()->save($campaign_review);
            }
        }
        
        if($campaign)
        {
            return Redirect::route('dashboard.campaign.list')->with('success','New Campaign has been created successfully!');
        }   
    }
    
    /**
     * get Action for showing Campaign submission list template.
     * 
     * @return rendered view.
     */
    
    public function getCampaignSubmissionList()
    {
        $input = Input::all();
       
        $campaign = Campaign::all();
        
        $partial_query = new CampaignSubmission;
        
        if($input)
        { 
            if(isset($input['search']) && !empty($input['search']))
            {
                $partial_query = $partial_query->where('campaign_id','=',$input['search']);
            }
            $result = $partial_query->OrderBy('created_at','desc')->paginate(10);
            return View::make('dashboard.campaign.submission-list',array('campaign_submissions'=>$result,'campaigns'=>$campaign)); 
            
        }
        else
        {
            
            return View::make('dashboard.campaign.submission-list',
                              array('campaign_submissions'=>CampaignSubmission::OrderBy('created_at','desc')->paginate(10),'campaigns'=>$campaign));
        }
        
    }
    
    /**
     * get Action for showing Campaign submission list template.
     * 
     * @return rendered view.
     */
    
    public function getCampaignSubmission($id)
    {
        $campaign = Campaign::all();
        
        $partial_query = new CampaignSubmission;
        
        if($id)
        { 
            $partial_query = $partial_query->where('campaign_id','=',$id);
           
            $result = $partial_query->paginate(10);
            return View::make('dashboard.campaign.submission-list',array('campaign_submissions'=>$result,'campaigns'=>$campaign,'campaign_id'=>$id)); 
        }
        else
        {
            return View::make('dashboard.campaign.submission-list',array('campaign_submissions'=>$campaign_submissions->paginate(10),'campaigns'=>$campaign));
        }
        
    }
    
    /**
     * Get Action for Downloading Campaign submission details in csv formate.
     * 
     * @return rendered view.
     */
    
    public function getCampaignSubmissionExport($id)
    {  
        //set csv column name in string.
        $output = 'campaign,name,phone,email,best time to call,subscribed,submitted on'.PHP_EOL;
        
        if($id > 0)
        {
            $campaign = Campaign::find($id);
            
            // get CampaignSubmission records according to campaign id.
            $enquiries = CampaignSubmission::where('campaign_id','=',$id)->get();
            
            // check if $enquiries is not empty.
            if($enquiries)
            {
                foreach($enquiries as $enquiry)
                {   
                    $record = $campaign->title.','
                             .$enquiry->name.','
                             ."'$enquiry->phone_no'".','
                             .$enquiry->email.','
                             .$enquiry->best_time_to_call.','
                             .$enquiry->subscribe.','
                             ."'$enquiry->created_at'".PHP_EOL;
                    
                    $output = $output.$record;
                }
                
                //define header for creating a csv file.
                $headers = array(
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="'.$campaign['title'].'.csv"',
                );

                return Response::make($output, 200, $headers);
            }
        }
        else
        {
            $campaigns = Campaign::all();
            
            foreach($campaigns as $campaign)
            {
                $enquiries = $campaign->campaign_submissions()->getResults();
                
                foreach($enquiries as $enquiry)
                {
                     $record = $campaign->title.','
                                 .$enquiry->name.','
                                 ."'$enquiry->phone_no'".','
                                 .$enquiry->email.','
                                 .$enquiry->best_time_to_call.','
                                 .$enquiry->subscribe.','
                                 ."'$enquiry->created_at'".PHP_EOL;

                    $output = $output.$record;
                }   
            }
            
            //define header for creating a csv file.
            $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=Enqiuries.csv',
            );

            return Response::make($output, 200, $headers);
        }
    }
        
    /**
     * get Action for showing setting list template.
     * 
     * @return rendered view.
     */
    
    public function getSettingList()
    {
        return View::make('dashboard.setting.list')->with('settings',Setting::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for creating setting template.
     * 
     * @return rendered view.
     */
    public function getSettingAdd()
    {
        return View::make('dashboard.setting.form');
    }
    
    /**
     * post Action for submitting setting details.
     * 
     * @return Redirect to setting list.
     */
    public function postSettingAdd()
    {
       $file = Input::file('value');
        
       $input = Input::except('_token');
       
        if($file) 
        {   
            // define input file name.
            $filename = Util::rand_filename($file->getClientOriginalExtension());
            
            //upload input file in settings folder.
            $upload_success = Util::upload_file($file,$filename);
            
            $input['value'] = $upload_success; 
        } 
        
        //create setting with input fields.
        $value = Setting::create($input);
        
        if($value)
        {
            return Redirect::route('dashboard.setting.list')->with('success','New setting has been created successfully');
        }
        else
        {
            throw new Exception("Error Processing Request");
        } 
    }
    
    /**
     * Get Action for Editing setting details.
     * take setting id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getSettingEdit($id)
    {
        $setting = Setting::find($id);
        
        return View::make('dashboard.setting.form',array('setting'=>$setting));
    }
    
    /**
     * post Action for editing Setting details.
     * 
     * @return Redirect to setting list.
     */
    
    public function postSettingEdit()
    {
        //load setting with input id.
        $setting = Setting::find(Input::get('id'));
        
        
        if($setting)
        {
            //get input file value.
            $file = Input::file('value');
            
            // get all input value in variable.
            $input = Input::except('_token');

            // check if input file is available.
            if($file) 
            { 
                // define input file name.
                $filename = Util::rand_filename($file->getClientOriginalExtension());
                
                //upload input file in settings folder.
                $upload_success = Util::upload_file($file,$filename);
                
                // save all input values in settings.
                $setting['value'] = $upload_success;
                $setting->name = Input::get('name');
                $setting->desc = Input::get('desc');
                $setting->type = Input::get('type');
                $setting->save();
            }
            else
            {
                //save all input values in settings.
                $setting->name = Input::get('name');
                $setting->desc = Input::get('desc');
                $setting->type = Input::get('type');
                $setting->value = Input::get('value');
                $setting->save();
            
            }
            return Redirect::route('dashboard.setting.list')->with('success','Your setting has been edited sucessfully!');
        }
    }
    
    /**
     * Get Action for deleting Setting details.
     * take Setting is as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getSettingDelete($id)
    {
        $setting = Setting::find($id);
        
        return View::make('dashboard.setting.delete',array('setting'=>$setting));
    }
    
    /**
     * Post Action for deleting Setting details.
     * 
     * @return Redirect to Setting list.
     */
    
    public function postSettingDelete()
    {
        $setting = Setting::find(Input::get('id'));
        
        if($setting)
        {
            $setting->delete();

            return Redirect::route('dashboard.setting.list')->with('success',"Your Setting has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.setting.list')->with('error' , "An error happened!"); 
        }
    }
    
     /**
     * Get Action for creating setting clone.
     * take setting id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getSettingCloning($id)
    {
        $setting = Setting::find($id);
        
        return View::make('dashboard.setting.form',array('setting'=>$setting,'clone'=>true));
    }
    
    /**
     * post Action for editing Setting details.
     * 
     * @return Redirect to setting list.
     */
    public function postSettingCloning()
    {
        $file = Input::file('value');
        
       $input = Input::except('_token');
       
        if($file) 
        {   
            // define input file name.
            $filename = Util::rand_filename($file->getClientOriginalExtension());
            
            //upload input file in settings folder.
            $upload_success = Util::upload_file($file,$filename);
            
            $input['value'] = $upload_success; 
        } 
        
        //create setting with input fields.
        $value = Setting::create($input);
        
        if($value)
        {
            return Redirect::route('dashboard.setting.list')->with('success','Your Setting clone has been created successfully');
        }
        else
        {
            throw new Exception("Error Processing Request");
        } 
        
    }
    
    /**
     * Get Action for showing destinations template
     * 
     * @return rendered view.
     */
    
    public function getDestinationView()
    {
        return View::make('dashboard.destination.view');
    }
    
    /**
     * Get full tree for destinations.
     * 
     * 
     * @return Response in json.
     */
    
    public function getDestinationTree()
    {
        $destination =  Destination::getTree();
        
        return Response::json($destination);
    }
    
    /**
     * Get Action for creating add child form
     * take destination id as argument.
     * 
     * @param string $id 
     * @return rendered view.
     */
    
    public function getDestinationAddChild($id)
    {
        $destination = Destination::find($id);
        
        if($destination)
        {
            return View::make('dashboard.destination.add-child',array('destination'=>$destination));
        }
    }
    
    /**
     * Post Action for submitting a child in destinations.
     * 
     * @return Redirect to Destination view.
     */
    
    public function postDestinationAddChild()
    {
        $destination = Destination::find(Input::get('id'));
        
        
        $destination->num_children = $destination->num_children + 1;
        $destination->save();
        
        if($destination['type'] != 'pin')
        {
            $city = Input::get('city');
            $pin = Input::get('pin');
            $state = Input::get('state');

            if(empty($city))
            {
                $city = null;
            }
            else
            {
                $city = Input::get('city');
            }

            if(empty($pin))
            {
                $pin = null;
            }
            else
            {
                $pin = Input::get('pin');
            }
            
            if(empty($state))
            {
                $state = null;
            }
            else
            {
                $state = Input::get('state');
            }
            
            if(Input::get('published') == 1)
            {
                $published = date('Y-m-d H:i:s');
            }
            else
            {
                $published = null;
            }
            
            $record = $destination->addChild(new Destination(array('label' =>Input::get('label'),
                                                                 'slug'=> Util::uniqueSlug(Input::get('label'),'Destination'),
                                                                 'type' => Input::get('type'),
                                                                 'state'=> $state,
                                                                 'city' => $city,
                                                                 'pin'=> $pin,
                                                                 'country' => Input::get('country'),
                                                                 'seo_meta_desc'=>Input::get('seo_meta_desc'),
                                                                 'seo_meta_kw'=>Input::get('seo_meta_kw'),
                                                                 'seo_page_title'=>Input::get('seo_page_title'),
                                                                 'published'=>$published,
                                                                 'popular_destination' => Input::get('popular_destination',0))  
                                                            ));
            
            
            
            if($record)
            {
                return Redirect::route('dashboard.destination.view')->with('success','You have added child '.Input::get('label').' in '.$destination['label'].' successfully!');
            }
        }
        else
        {
            return Redirect::route('dashboard.destination.view');
        }
    }
    
     /**
     * Get Action for creating add sibling form
     * take destination id as argument.
     * 
     * @param string $id
     * @return rendered view.
     */
    
    public function getDestinationAddSibling($id)
    {
        $destination = Destination::find($id);
        
        if($destination)
        {
            return View::make('dashboard.destination.add-sibling',array('destination'=>$destination));
        }
    }
    
    /**
     * Post Action for submitting a sibling in destinations.
     * 
     * @return Redirect to Destination view.
     */
    
    public function postDestinationAddSibling()
    {
        $destination = Destination::find(Input::get('id'));
        
        $parent_details = Destination::whereRaw('id = ?',array($destination->parent_id))->get();
        
        if($parent_details)
        {
            $parent_details[0]->num_children = $parent_details[0]->num_children + 1;
            $parent_details[0]->save();
        }
        
        if($destination['type'] != 'null')
        {
            $city = Input::get('city');
            $pin = Input::get('pin');
            $state = Input::get('state');

            if(empty($city))
            {
                $city = null;
            }
            else
            {
                $city = Input::get('city');
            }

            if(empty($pin))
            {
                $pin = null;
            }
            else
            {
                $pin = Input::get('pin');
            }
            
            if(empty($state))
            {
                $state = null;
            }
            else
            {
                $state = Input::get('state');
            }
            
            if(Input::get('published') == 1)
            {
                $published = date('Y-m-d H:i:s');
            }
            else
            {
                $published = null;
            }
            
            
            $record = $destination->addSibling(new Destination(array('label' =>Input::get('label'),
                                                                     'slug'=> Util::uniqueSlug(Input::get('label'),'Destination'),
                                                                     'type' => Input::get('type'),
                                                                     'state'=> $state,
                                                                     'city' => $city,
                                                                     'pin'=> $pin,
                                                                     'country' => Input::get('country'),
                                                                     'seo_meta_desc'=>Input::get('seo_meta_desc'),
                                                                     'seo_meta_kw'=>Input::get('seo_meta_kw'),
                                                                     'seo_page_title'=>Input::get('seo_page_title'),
                                                                     'published'=>$published,
                                                                     'popular_destination' => Input::get('popular_destination',0))
                                                            ));

            if($record)
            {
                return Redirect::route('dashboard.destination.view')->with('success','You have added sibling '.Input::get('label').' for '.$destination['label'].' successfully!');
            }
        }
        else
        {
            return Redirect::route('dashboard.destination.view');
        }
    }
    
    /**
     * Get Action for Editing Destinations.
     * take $id as argument.
     * 
     * @param int $id.
     * 
     * @return Rendered View.
     */
    public function getDestinationEdit($id)
    {
        $destination = Destination::find($id);
        
        return View::make('dashboard.destination.edit',array('destination'=>$destination));
    }
    
    /**
     * Post Action for Editing Destinations details.
     * 
     * 
     * @return Redirect to Destination tree.
     */
    
    public function postDestinationEdit()
    {
        $input = Input::except('_token');
        
        $destination = Destination::find($input['id']);
        
        if($destination)
        {
            $destination->label = $input['label'];
            $destination->slug = Util::uniqueSlug(Input::get('label'),'Destination');
            $destination->country = $input['country'] ? $input['country'] : null;
            $destination->state = $input['state'] ? $input['state'] : null;
            $destination->city = $input['city'] ? $input['city'] : null;
            $destination->type = $input['type'];
            $destination->pin = $input['pin'] ? $input['pin'] : null;
            $destination->seo_page_title = $input['seo_page_title'];
            $destination->seo_meta_kw = $input['seo_meta_kw'];
            $destination->seo_meta_desc = $input['seo_meta_desc'];
            $destination->popular_destination = Input::get('popular_destination',0);
            
            $destination->save();
            
            return Redirect::route('dashboard.destination.view')->with('success','You have edited '.$input['label'].' Destination successfully!');
        }
        
    }
    
    /**
     * Get Action for deleting subtree form
     * 
     * 
     * @return rendered view.
     */
    
    public function getDestinationDelete($id)
    {
        $destination = Destination::find($id);
        
        $parent_details = Destination::where('id','=',$destination->parent_id)->get();
        
        if($parent_details)
        {
          $parent_details[0]->num_children = $parent_details[0]->num_children - 1;
          $parent_details[0]->save();
        }
        
        DestinationClosure::where('descendant','=',$id)->delete();
        
        Destination::where('id','=',$id)->delete();
        
        return Redirect::route('dashboard.destination.view')->with('success','You have deleted destination');
    }
    
     /**
     * get Action for showing facility list template.
     * 
     * @return rendered view.
     */
    
    public function getFacilityList()
    {
        return View::make('dashboard.facility.list')->with('facilities',Facility::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for creating facility template.
     * 
     * @return rendered view.
     */
    public function getFacilityAdd()
    {
        return View::make('dashboard.facility.form');
    }
    
    /**
     * post Action for submitting facility details.
     * 
     * @return Redirect to facility list.
     */
    public function postFacilityAdd()
    {
        $facility = array( 
                'title' => Input::get('title'),
                ); 
        
        $main_image = Input::file('main_image');
        
        if($main_image)
        {   
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $facility['main_image'] = $upload_success;
        }
        
      
        $facility = Facility::create($facility);
        
        if($facility)
        {
            return Redirect::route('dashboard.facility.list')->with('success','New Facility has been created successfully');
        }
        else
        {
            throw new Exception("Error Processing Request");
        } 
    }
    
    /**
     * Get Action for Editing facility details.
     * take facility id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getFacilityEdit($id)
    {
        $facility = Facility::find($id);
        
        return View::make('dashboard.facility.form',array('facility'=>$facility));
    }
    
    /**
     * post Action for editing facility details.
     * 
     * @return Redirect to facility list.
     */
    
    public function postFacilityEdit()
    {
        //load facility with input id.
        $facility = Facility::find(Input::get('id'));
        
        $main_image = Input::file('main_image');
        
        if($facility)
        {
            if($main_image)
            {
                $filename = Util::rand_filename($main_image->getClientOriginalExtension());

                $upload_success = Util::upload_file($main_image,$filename);

                $facility['main_image'] = $upload_success;
            }
                
                //save all input values in facility.
                $facility->title = Input::get('title');
                $facility->save();

                return Redirect::route('dashboard.facility.list')->with('success','Your Facility has been edited sucessfully!');
        }
    }
    
    /**
     * Get Action for deleting facility details.
     * take Facility is as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getFacilityDelete($id)
    {
        $facility = Facility::find($id);
        
        return View::make('dashboard.facility.delete',array('facility'=>$facility));
    }
    
    /**
     * Post Action for deleting facility details.
     * 
     * @return Redirect to facility list.
     */
    
    public function postFacilityDelete()
    {
        $facility = Facility::find(Input::get('id'));
        
        if($facility)
        {
            $facility->delete();

            return Redirect::route('dashboard.facility.list')->with('success',"Your Facility has been deleted successfully!");
        }
        else
        {
            return Redirect::route('dashboard.facility.list')->with('error' , "An error happened!"); 
        }
    }
    
    /**
     * Get Action for creating facility clone.
     * take facility id as argument.
     * 
     * @param string $id.
     * @return rendered view.
     */
    
    public function getFacilityCloning($id)
    {
        $facility = Facility::find($id);
        
        return View::make('dashboard.facility.form',array('facility'=>$facility,'clone'=>'true'));
    }
    
    /**
     * post Action for creating facility clone.
     * 
     * @return Redirect to facility list.
     */
    
    public function postFacilityCloning()
    {
        $facility = array( 
                'title' => Input::get('title'),
                ); 
        
        $main_image = Input::file('main_image');
        
        if($main_image)
        {   
            $filename = Util::rand_filename($main_image->getClientOriginalExtension());

            $upload_success = Util::upload_file($main_image,$filename);

            $facility['main_image'] = $upload_success;
        }
        
        $facility = Facility::create($facility);
        
        if($facility)
        {
            return Redirect::route('dashboard.facility.list')->with('success','New Facility has been created successfully');
        }
        else
        {
            throw new Exception("Error Processing Request");
        } 
    }
    
    /**
     * Action for showing feedback list template.
     * 
     * @return rendered view.
     */
    public function getFeedbackList()
    {
        return View::make('dashboard.feedback.list')->with('feedbacks',Feedback::OrderBy('created_at','desc')->paginate(10));
    }
    
    /**
     * Get Action for showing feedback form.
     * 
     * @return rendered view.
     */
    
    public function getFeedbackAdd()
    {
    
        return View::make('dashboard.feedback.form');
    }
    
    /**
     * post Action for creating a feedback details.
     * 
     * @return Redirect to feedback list
     */
    
    public function postFeedbackAdd()
    {
        $feedback = array('name'=>Input::get('name'),
                         'email'=>Input::get('email'),
                         'phone'=>Input::get('phone'),
                         'message'=>Input::get('message'),
                         'ip'=>$_SERVER['REMOTE_ADDR']);
        
        if(Input::get('published') == 1)
        {
            $feedback['published'] = date('Y-m-d H:i:s');
        }
        else
        {
            unset($feedback['published']);
        }
        
        if($feedback)
        {
            Feedback::create($feedback);
            
            return Redirect::route('dashboard.feedback.list')->with('success','Your Feedback has been Submitted successfully!');
        }
    }
    
    /**
     * Get Action for showing edit feedback form.
     * 
     * @return rendered view.
     */
    
    public function getFeedbackEdit($id)
    {
        $feedback = Feedback::find($id);
        
        return View::make('dashboard.feedback.form')->with('feedback',$feedback);
    }
    
    /**
     * post Action for Editing a feedback details.
     * 
     * @return Redirect to feedback list
     */
    
    public function postFeedbackEdit()
    {
        $feedback = Feedback::find(Input::get('id'));
        
        if($feedback)
        {
            if(!is_null(Input::get('published')))
            {
                $feedback['published'] = date('Y-m-d H:i:s');
            }
            else
            {
                $feedback['published'] = null;
            }
           
            $feedback->name = Input::get('name');
            $feedback->email = Input::get('email');
            $feedback->phone = Input::get('phone');
            $feedback->message = Input::get('message');
            $feedback->ip = $_SERVER['REMOTE_ADDR'];
            $feedback->save();
            
            return Redirect::route('dashboard.feedback.list')->with('success','Your Feedback has been Edited successfully!');
        }
    }
    
     /**
     * Get Action for deleting feedback form.  
     * 
     * @return rendered view.
     */
    
    public function getFeedbackDelete($id)
    {
        $feedback = Feedback::find($id);
        
        return View::make('dashboard.feedback.delete')->with('feedback',$feedback);
    }
    
    /**
     * post Action for deleting a feedback details.
     * 
     * @return Redirect to feedback list
     */
    
    public function postFeedbackDelete()
    {
        $feedback = Feedback::find(Input::get('id'));
        
        if($feedback)
        {
            $feedback->delete();
            
            return Redirect::route('dashboard.feedback.list')->with('success','Your Feedback has been Deleted successfully!');
        }
    }
}