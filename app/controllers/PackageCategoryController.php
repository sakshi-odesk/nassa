<?php

class PackageCategoryController extends BaseController
{
    
    /**
     * get Action for Domestic
     * 
     * @return rendered view
     */
    
    public function getDomestic()
    {
        $packages = PackageCategory::domesticPackages();
        
        return View::make('frontend.packages',array('packages'=>$packages,'domestic'=>'true'));
    }
    
    /**
     * get Action for International
     * 
     * @return rendered view
     */
    
    public function getInternational()
    {
         $packages = PackageCategory::internationalPackages();
        
        return View::make('frontend.packages',array('packages'=>$packages,'international'=>'true'));
    }
    
    /**
     * get Action for Domestic Packages
     * 
     * @return rendered view
     */
    
    public function getDestinationsPackages($slug)
    {
        $input = Input::all();
      
       
        if(!empty($slug))
        {
            $destination = Destination::where('slug','=',$slug)->first();
        
            
            $destinationIds = Destination::getChild($destination);
            
            $packages = DB::table('packages')
                            ->join('package_itineraries','package_itineraries.package_id','=','packages.id')
                            ->select('packages.id','packages.title','packages.slug','packages.route_map','packages.default_price','packages.desc',
                                     'packages.duration','packages.main_image','packages.default_discounted_price','packages.hotel_level',
                                     'packages.flight_flag',
                                     'packages.recommended','packages.special_offer','packages.hotel_price')
                            ->whereIn('package_itineraries.destination_id',$destinationIds)
                            ->groupBy('packages.id')
                            ->whereNotNull('packages.min_price')
                            ->whereNotNull('packages.published');
          
          
        
            if(isset($input['duration']))
            {
                $packages = $packages->whereIn(\DB::raw("REPLACE(packages.duration,' ', '')"),$input['duration']);
            }
                
            if(isset($input['price']))
            {
                $price = explode('-',$input['price']);

                $packages = $packages->whereBetween('packages.default_price',$price);
            }
                
            if(isset($input['sort']) &&  $input['sort'] == 'high') 
            {
                $packages = $packages->orderBy('packages.default_price', 'desc');
            }

            if (isset($input['sort']) && $input['sort'] == 'low') {
                $packages = $packages->orderBy('packages.default_price', 'asc');
            }
            
            $packages = $packages->get();
          
                      
            foreach($packages as $package)
            {
                $package->package_facilities = DB::table('package_facilities')
                                                ->join('facilities','facilities.id','=','package_facilities.facility_id')
                                                ->select('facilities.title','facilities.main_image')
                                                ->where('package_facilities.package_id','=',$package->id)
                                                ->get();
                
                $date = array();

                $category = array();
                   
                foreach(json_decode($package->hotel_price) as $key=>$value)
                {
                    if(!isset($date[$value->date_range]))
                    {
                        $date[$value->date_range] = $value->date_range;
                    }
                    
                    foreach($value->data as $k=>$v)
                    {
						if( isset($v->cat) )
						{
							if(!isset($category[$v->cat]))
							{
								$category[$v->cat] = $v->cat;
							}
						}
                    }
                }
                
                $package->dates = $date;
                $package->category = $category;

            }

            return View::make('frontend.package-listing',array('packages'=>$packages,
                                                               'destination' => $destination,
                                                               'new_packages'=>isset($packages) ? json_encode($packages) : json_encode($packages),
                                                               'count'=>count($packages),
                                                               'domestic'=>'true',
                                                               ));
        
        }
    }
    
    /**
     * get Action for International Packages
     * 
     * @return rendered view
     */
    
    public function getInternationalPackages($slug)
    {
       if(!empty($slug))
        {
            $package_category = PackageCategory::where('slug','=',$slug)->first();
            
            if($package_category['package_type_id'] == 2)
            {
				$type = PackageType::find($package_category['package_type_id']);

                $packages = DB::table('packages')
                    ->select('packages.id','packages.title','packages.slug','packages.route_map','packages.price','packages.desc',
                             'packages.duration','packages.main_image','packages.discounted_price','packages.hotel_level',
                             'packages.recommended','packages.special_offer')
                    ->where('packages.package_category_id','=',$package_category->id)
                    ->whereNotNull('packages.published');
                
                if(isset($input['duration']))
                {
                    $packages = $packages->whereIn(\DB::raw("REPLACE(packages.duration,' ', '')"),$input['duration']);
                }
               
                if(isset($input['price']))
                {
                    $price = explode('-',$input['price']);

                    $packages = $packages->whereBetween('packages.price',$price);
                }
                
                if (isset($input['sort']) &&  $input['sort'] == 'high') {
                    $packages = $packages->orderBy('packages.price', 'desc');
                }

                if (isset($input['sort']) && $input['sort'] == 'low') {
                    $packages = $packages->orderBy('packages.price', 'asc');
                }
                
                $packages = $packages->get();
                
                foreach($packages as $package)
                {
                    $package->package_facilities = DB::table('package_facilities')
                                            ->join('facilities','facilities.id','=','package_facilities.facility_id')
                                            ->select('facilities.title','facilities.main_image')
                                            ->where('package_facilities.package_id','=',$package->id)->get();
                    
                    $package->package_hotel_details = DB::table('package_hotel_details')
                                                       ->select('package_hotel_details.destination_id','package_hotel_details.hotel',
                                                                 'package_hotel_details.category','package_hotel_details.start_date',
                                                                 'package_hotel_details.end_date','package_hotel_details.price',
                                                                 'package_hotel_details.package_itinerary_id','package_hotel_details.hotel_id')
                                                        ->where('package_hotel_details.package_id','=',$package->id)
                                                        ->groupBy('package_hotel_details.category')
                                                        ->get();
                    
                    $package->hotel_dates  =    DB::table('package_hotel_details')
                                                    ->select(\DB::raw("DATE_FORMAT(start_date, '%M %e, %Y') AS start_date_format"),
                                                             \DB::raw("DATE_FORMAT(end_date, '%M %e, %Y') AS end_date_format"),
                                                             'package_hotel_details.start_date',
                                                             'package_hotel_details.end_date')
                                                    ->where('package_hotel_details.package_id','=',$package->id)
                                                    ->whereNotNull('package_hotel_details.destination_id')
                                                    ->whereNotNull('package_hotel_details.hotel_id')
                                                    ->groupBy('package_hotel_details.start_date')
                                                    ->groupBy('package_hotel_details.end_date')
                                                    ->get();
                }
                
                $count = $package_category->package()->count();
                
                $package_categories = PackageCategory::internationalPackages();
                
                return View::make('frontend.package-listing',array('package_category'=>$package_category,
                                                                    'packages'=>$packages,
                                                                    'new_packages'=>isset($packages) ? json_encode($packages) : json_encode($packages),
                                                                    'count'=>$count,
                                                                    'international'=>'true','type'=>$type,
                                                                    'package_categories'=>$package_categories));
            }
        }
    }
    
    
    public function getSearchPackage()
    {
        $input = Input::all();
      
     
        if(!empty($input['destination_id']))
        {
            $destination = Destination::find($input['destination_id']);
            
            $destinationIds = Destination::getChild($destination);
            
            $packages = DB::table('packages')
                            ->join('package_itineraries','package_itineraries.package_id','=','packages.id')
                            ->select('packages.id','packages.title','packages.slug','packages.route_map','packages.default_price','packages.desc',
                                     'packages.duration','packages.main_image','packages.default_discounted_price','packages.hotel_level',
                                     'packages.flight_flag',
                                     'packages.recommended','packages.special_offer','packages.hotel_price')
                            ->whereIn('package_itineraries.destination_id',$destinationIds)
                            ->groupBy('packages.id')
                            ->whereNotNull('packages.min_price')
                            ->whereNotNull('packages.published');
          
          
            if(isset($input['duration']))
            {
                $packages = $packages->whereIn(\DB::raw("REPLACE(packages.duration,' ', '')"),$input['duration']);
            }
                
            if(isset($input['price']))
            {
                $price = explode('-',$input['price']);

                $packages = $packages->whereBetween('packages.default_price',$price);
            }
                
            if (isset($input['sort']) &&  $input['sort'] == 'high') 
            {
                $packages = $packages->orderBy('packages.default_price', 'desc');
            }

            if (isset($input['sort']) && $input['sort'] == 'low') {
                $packages = $packages->orderBy('packages.default_price', 'asc');
            }

            $packages = $packages->get();
          
            
            foreach($packages as $package)
            {
                $package->package_facilities = DB::table('package_facilities')
                                                ->join('facilities','facilities.id','=','package_facilities.facility_id')
                                                ->select('facilities.title','facilities.main_image')
                                                ->where('package_facilities.package_id','=',$package->id)
                                                ->get();
                
                $date = array();

                $category = array();
                if(!empty($package->hotel_price))
                {
                    foreach(json_decode($package->hotel_price) as $key=>$value)
                    {
                        if(!isset($date[$value->date_range]))
                        {
                            $date[$value->date_range] = $value->date_range;
                        }


                        foreach($value->data as $k=>$v)
                        { 
						  if(isset($v->cat))
						  {
						    if(!isset($category[$v->cat]))
                            {
                               $category[$v->cat] = $v->cat;
                            }
						  }
                          
                        }
                    }
                }
                
                $package->dates = $date;
                $package->category = $category;
              
              }
            return View::make('frontend.package-search',array('packages'=>!empty($packages) ? $packages : null,
                                                              'destination' => $destination,
                                                              'new_packages'=>isset($packages) ? json_encode($packages) : json_encode($packages),
                                                              'count'=>count($packages),
                                                              'domestic'=>'true',
                                                                ));
        }
        
        return Redirect::back();
    
    }
    
    public function getDestination()
    {
        $destination = Destination::find(Input::get('id'));
        
        if($destination)
        {
            return Response::json($destination);
        }
    }
    
    public function getHotel()
    {
        $hotel = Hotel::find(Input::get('id'));
             
        if($hotel)
        {
            return Response::json($hotel);
        }
    }
    
    public function getPriceSearch()	
    {
        $input = Input::all();
        
        $package = Package::find($input['id']);
        
        $price = '';
        
        $discounted_price = '';
        
        if(!empty($package->hotel_price))
        {
            foreach(json_decode($package->hotel_price) as $key=>$value)
            {
                foreach($value->data as $k=>$v)
                {
                    if($v->cat == $input['category'] && $value->date_range == $input['date'])
                    {
                        $price = $v->price_per_person;
                        $discounted_price = isset($v->discounted_price) ? $v->discounted_price : null;
                    }
                }
            }
        }
        
        return Response::json(array('price' => $price,'discounted_price' => $discounted_price));

    }
  
    public function getPackageCategorySearch()
    {
          $input = Input::all();
        
          $package = Package::find($input['id']);

          $array = array();

          if(!empty($package->hotel_price))
          {
              foreach(json_decode($package->hotel_price) as $key=>$value)
              {
                foreach($value->data as $k=>$v)  
                {   
                    if($v->cat == Input::get('category'))
                    { 
                      array_push($array,$value->date_range);
                    }
                }
              }
          }
          return $array;
    }

}