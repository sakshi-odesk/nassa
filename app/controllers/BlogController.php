<?php

class BlogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$blogs = Blog::orderBy('created_at')->get();
      
        $blogs_categories = BlogCategory::all();
      
        return View::make('frontend.blogs',array('blogs'=>$blogs,'blog_categories' => $blogs_categories));
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
        if(!is_numeric($slug))
        {
            $blog_category = BlogCategory::where('slug','=',$slug)->first();
          
            $blogs = Blog::where('blog_category_id','=',$blog_category->id)->whereNotNull('published')->get();

            return View::make('frontend.blog-category',array('blogs'=>$blogs,
                                                             'blog_categories' => BlogCategory::all(),
                                                             'blog_category' => $blog_category));
        }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
  
  
    /**
     * 
     * 
     * 
     */
  
    public function getBlogView($slug)
    {
      if(!is_numeric($slug))
      {
        $blog = Blog::where('slug','=',$slug)->whereNotNull('published')->first();
        
        return View::make('frontend.blog-view',array('blog'=>$blog,'blog_categories' => BlogCategory::all()));
        
      }
    }
}
