<?php

class SummernoteController extends Controller
{
    /**
     * POST Action for send summernote_images through ajax.
     * 
     * 
     */
    public function postSummernoteImage()
    {            
        if(Input::file('file'))
        {            
            $file = Input::file('file');
            $filename = Util::rand_filename($file->getClientOriginalExtension());
            
            $upload_success = Util::upload_file($file,$filename);
            
            echo URL::asset($upload_success);
        }else
        {
            return "error";
        }
    }
}