<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetImagesColumnNullInAllTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE blogs MODIFY blog_image varchar(255) null');
        
		DB::statement('ALTER TABLE campaigns MODIFY background_image varchar(255) null');
        
		DB::statement('ALTER TABLE hotel_categories MODIFY main_image varchar(255) null');
        
		DB::statement('ALTER TABLE hotel_images MODIFY path varchar(255) null');
        
		DB::statement('ALTER TABLE packages MODIFY  main_image varchar(255) null');
        
		DB::statement('ALTER TABLE package_categories MODIFY  main_image varchar(255) null');
        
		DB::statement('ALTER TABLE pages MODIFY  page_image varchar(255) null');
        
		DB::statement('ALTER TABLE slides MODIFY  main_image varchar(255) null');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
