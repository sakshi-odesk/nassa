<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageHotelDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('package_hotel_details',function($table){
        
            $table->increments('id');
            $table->integer('package_id');
            $table->string('destination');
            $table->string('hotel');
            $table->string('meal_plan');
            $table->integer('weight');
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('package_hotel_details');
	}

}
