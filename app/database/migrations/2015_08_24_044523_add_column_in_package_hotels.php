<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPackageHotels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_hotel_details',function($table){
        
            $table->dateTime('start_date')->after('meal_plan')->nullable();
            $table->dateTime('end_date')->after('start_date')->nullable();
            $table->string('category')->after('end_date')->nullable();
            $table->string('price')->after('category')->nullable();
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_hotel_details',function($table){
        
            if(Schema::hasColumn('package_hotel_details','start_date'))
            {
                $table->dropColumn('start_date');
            }
            
            if(Schema::hasColumn('package_hotel_details','end_date'))
            {
                $table->dropColumn('end_date');
            }
            
            if(Schema::hasColumn('package_hotel_details','category'))
            {
                $table->dropColumn('category');
            }
            
            if(Schema::hasColumn('package_hotel_details','price'))
            {
                $table->dropColumn('price');
            }
        
        });
	}
}
