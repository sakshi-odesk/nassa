<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInCampaigns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns',function($table){
        
            $table->text('inclusion')->after('desc')->nullable();
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaigns',function($table){
        
            if(Schema::hasColumn('campaigns','inclusion'))
            {
                $table->dropColumn('inclusion');
            }
        });
	}

}
