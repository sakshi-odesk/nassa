<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPackageHotelDetail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_hotel_details',function($table){
        
            $table->integer('hotel_id')->after('destination_id')->nullable();
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_hotel_details',function($table){
        
            $table->dropColumn('hotel_id');
        
        });
	}

}
