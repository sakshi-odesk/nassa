<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToHotelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			$table->boolean('hotpick')->default(0)->after('special_offer');
			$table->boolean('recommended')->default(0)->after('hotpick');
		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function(Blueprint $table)
		{
			if(Schema::hasColumn('hotels','hotpick'))
            {
                $table->dropColumn('hotpick');
            }
          
            if(Schema::hasColumn('hotels','recommended'))
            {
                $table->dropColumn('recommended');
            }
		});
	}

}
