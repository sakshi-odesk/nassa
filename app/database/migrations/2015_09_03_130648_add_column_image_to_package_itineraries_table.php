<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnImageToPackageItinerariesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_itineraries', function(Blueprint $table)
		{
			$table->string('image')->after('desc')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_itineraries', function(Blueprint $table)
		{
			 if(Schema::hasColumn('package_itineraries','image'))
            {
                $table->dropColumn('image');
            }
		});
	}

}
