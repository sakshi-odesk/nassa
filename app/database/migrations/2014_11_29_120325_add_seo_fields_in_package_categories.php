<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFieldsInPackageCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_categories',function($table){
        
            $table->text('seo_meta_desc')->after('body')->nullable();
            $table->text('seo_meta_kw')->after('seo_meta_desc')->nullable();
            $table->string('seo_page_title')->after('seo_meta_kw')->nullable();
            $table->boolean('published')->after('seo_page_title');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_categories',function($table){
        
            if(Schema::hasColumn('package_categories','seo_meta_desc'))
            {
                $table->dropColumn('seo_meta_desc');
            }
            
            if(Schema::hasColumn('package_categories','seo_meta_kw'))
            {
                $table->dropColumn('seo_meta_kw');
            }
            
            if(Schema::hasColumn('package_categories','seo_page_title'))
            {
                $table->dropColumn('seo_page_title');
            }
            
                    
            if(Schema::hasColumn('package_categories','published'))
            {
                $table->dropColumn('published');
            }
            
        });
	}

}
