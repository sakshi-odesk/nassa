<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePriceCloumnInPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("UPDATE packages SET price = REPLACE(price, ',', '')");
        
		DB::statement("UPDATE packages SET discounted_price = REPLACE( discounted_price, ',', '' )");
		
        DB::statement("ALTER TABLE `packages` CHANGE `price` `price` DECIMAL(10,3) NULL DEFAULT NULL");
        
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
