<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationsTable extends Migration {

    public function up()
    {
	    Schema::table('destinations', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            Schema::create('destinations', function(Blueprint $t)
            {
                $t->increments('id');
                $t->string('name')->nullable();
                $t->string('type')->nullable();
                $t->string('pin')->nullable();
                $t->string('city')->nullable();
                $t->string('state')->nullable();
                $t->string('country')->nullable();
                $t->integer('parent_id')->unsigned()->nullable();
                $t->integer('position', false, true);
                $t->integer('real_depth', false, true);
                $t->softDeletes();

                $t->foreign('parent_id')->references('id')->on('destinations');
            });
        });
    }

    public function down()
    {
        Schema::table('destinations', function(Blueprint $table)
        {
            Schema::dropIfExists('destinations');
        });
    }
}
