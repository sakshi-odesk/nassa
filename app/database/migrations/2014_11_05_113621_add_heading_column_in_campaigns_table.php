<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeadingColumnInCampaignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns',function($table){
        
            if(Schema::hasColumn('campaigns','subtitle'))
            {
                $table->dropColumn('subtitle');
            }
            
            $table->string('heading')->after('slug');
            $table->string('sub_heading')->after('heading');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
