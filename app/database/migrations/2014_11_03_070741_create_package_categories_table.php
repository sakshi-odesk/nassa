<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('package_categories',function($table){
        
            $table->increments('id');
            $table->integer('package_type_id');
            $table->string('title');
            $table->string('slug');
            $table->string('main_image');
            $table->text('body');
            
            $table->timestamps();
            
            $table->unique('slug');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('package_categories');
	}

}
