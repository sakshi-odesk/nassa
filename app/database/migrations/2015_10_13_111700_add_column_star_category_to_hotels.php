<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStarCategoryToHotels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels', function($table)
		{
			$table->decimal('star_category',2,1)->nullable()->after('hotel_category');
          
                   
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels', function($table)
		{
			$table->dropColoumn('star_category');
		});
	}

}
