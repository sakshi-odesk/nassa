<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('campaigns',function($table){
        
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('subtitle');
            $table->text('desc');
            $table->string('backgruond_image');
            $table->boolean('published');
            
            $table->timestamps();
            
            $table->unique('slug');
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('campaigns');
	}

}
