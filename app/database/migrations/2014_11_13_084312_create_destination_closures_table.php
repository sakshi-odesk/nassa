<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationClosuresTable extends Migration {

    public function up()
    {
        Schema::table('destination_closure', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';

            Schema::create('destination_closure', function(Blueprint $t)
            {
                $t->increments('ctid');

                $t->integer('ancestor', false, true);
                $t->integer('descendant', false, true);
                $t->integer('depth', false, true);

                $t->foreign('ancestor')->references('id')->on('destinations');
                $t->foreign('descendant')->references('id')->on('destinations');
            });
        });
    }

    public function down()
    {
        Schema::table('destination_closure', function(Blueprint $table)
        {
            Schema::dropIfExists('destination_closure');
        });
    }
}
