<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPackageHotelDetails extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_hotel_details',function($table){
        
            $table->integer('destination_id')->after('package_id')->nullable();
            
            $table->integer('package_itinerary_id')->after('destination_id')->nullable();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_hotel_details',function($table){
        
            $table->dropColumn('destination_id');
            
            $table->dropColumn('package_itinerary_id');
            
        });
	}

}
