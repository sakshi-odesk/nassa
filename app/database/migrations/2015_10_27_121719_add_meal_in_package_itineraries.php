<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMealInPackageItineraries extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_itineraries',function($table){
            
            $table->string('meal')->nullable()->after('desc');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_itineraries',function($table){
           
            $table->dropColumn('meal');
            
        });
	}

}
