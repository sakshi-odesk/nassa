<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNassaNewDescPackageInclusionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_inclusions', function(Blueprint $table)
		{
			$table->text('desc')->after('title')->nullable();
          
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_inclusions', function(Blueprint $table)
		{
			  $table->dropColumn('desc');
        
		});
	}

}
