<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnChildrenInDestinations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('destinations',function($table){
        
            $table->integer('children')->after('parent_id')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('destinations',function($table){
        
            if(Schema::hasColumn('destinations','children'))
            {
                $table->dropColumn('children');
            }
            
        });
	}

}
