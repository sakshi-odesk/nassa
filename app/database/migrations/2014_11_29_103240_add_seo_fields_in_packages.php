<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFieldsInPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('packages',function($table){
        
            $table->text('seo_meta_desc')->after('main_image')->nullable();
            $table->text('seo_meta_kw')->after('seo_meta_desc')->nullable();
            $table->string('seo_page_title')->after('seo_meta_kw')->nullable();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('packages',function($table){
        
            if(Schema::hasColumn('packages','seo_meta_desc'))
            {
                $table->dropColumn('seo_meta_desc');
            }
            
            if(Schema::hasColumn('packages','seo_meta_kw'))
            {
                $table->dropColumn('seo_meta_kw');
            }
            
            if(Schema::hasColumn('packages','seo_page_title'))
            {
                $table->dropColumn('seo_page_title');
            }
            
        });
	}

}
