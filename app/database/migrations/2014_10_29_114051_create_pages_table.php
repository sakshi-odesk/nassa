<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages',function($table){
        
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('page_image');
            $table->text('body');
            $table->dateTime('published');
            $table->integer('user_id')->unsigned();
            $table->text('seo_meta_desc');
            $table->text('seo_meta_kw');
            $table->string('seo_page_title');
            
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->index('user_id');
            $table->unique('slug');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pages');
	}

}
