<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCloumnPublished extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE packages MODIFY published datetime null');
		DB::statement('ALTER TABLE destinations MODIFY published datetime null');
		DB::statement('ALTER TABLE feedbacks MODIFY published datetime null');
		DB::statement('ALTER TABLE package_categories MODIFY published datetime null');
        
        Schema::table('hotels',function($table){
        
            $table->dateTime('published')->after('body')->nullable();
        });
        
        Schema::table('slide_shows',function($table){
        
            $table->dateTime('published')->after('title')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
