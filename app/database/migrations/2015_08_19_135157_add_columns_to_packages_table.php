<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('packages', function(Blueprint $table)
		{
			$table->boolean('hotpick')->default(0)->after('special_offer');
			$table->boolean('recommended')->default(0)->after('hotpick');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('packages', function(Blueprint $table)
		{
			if(Schema::hasColumn('packages','hotpick'))
            {
                $table->dropColumn('hotpick');
            }
          
            if(Schema::hasColumn('packages','recommended'))
            {
                $table->dropColumn('recommended');
            }
		});
	}

}
