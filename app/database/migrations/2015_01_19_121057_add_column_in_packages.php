<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('packages',function($table){
        
            $table->string('discounted_price')->after('price');
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('packages',function($table){
        
            if(Schema::hasColumn('packages','discounted_price'))
            {
                $table->dropColumn('discounted_price');
            }
        
        });
	}

}
