<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWeightColunmSetNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE package_inclusions MODIFY weight integer null');
		DB::statement('ALTER TABLE package_itineraries MODIFY weight integer null');
		DB::statement('ALTER TABLE package_hotel_details MODIFY weight integer null');
		DB::statement('ALTER TABLE package_facilities MODIFY weight integer null');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
