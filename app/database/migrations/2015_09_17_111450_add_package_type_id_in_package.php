<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackageTypeIdInPackage extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('packages',function($table){
        
            $table->integer('package_type_id')->after('package_category_id')->nullable();
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('packages',function($table){
        
            if(Schema::hasColumn('packages','package_type_id'))
            {
                $table->dropColumn('package_type_id');
            }

        });
	}

}
