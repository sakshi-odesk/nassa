<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInPackageItinerariesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_itineraries',function($table){
        
            $table->integer('destination_id')->nullable()->after('package_id');
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_itineraries', function($table)
		{
            if(Schema::hasColumn('package_itineraries','destination_id'))
            {
                $table->dropColumn('destination_id');
            }
		});
	}

}
