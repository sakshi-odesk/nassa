<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinPackageIdInDestinations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('destinations',function($table){
        
            $table->integer('min_package_id')->nullable()->after('id');
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('destinations',function($table){
        
            $table->dropColumn('min_package_id');
            
        });
	}

}
