<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blogs',function($table){
        
            $table->increments('id');
            $table->integer('blog_category_id');
            $table->string('title');
            $table->string('slug');
            $table->text('body');
            $table->string('blog_image');
            $table->string('slider_title');
            $table->string('slider_image');
            $table->boolean('show_on_slider');
            $table->dateTime('published');
            $table->integer('user_id')->unsigned();
            $table->text('seo_meta_desc');
            $table->text('seo_meta_kw');
            $table->string('seo_page_title');
            
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->index('slug');
            $table->index('user_id');
            $table->unique('slug');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('blogs');
	}

}
