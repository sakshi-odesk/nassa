<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeoFieldsInDestinations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('destinations',function($table){
        
            $table->string('slug')->after('label')->nullable();
            
            $table->text('seo_meta_desc')->nullable();
            $table->text('seo_meta_kw')->nullable();
            $table->string('seo_page_title')->nullable();
            $table->boolean('published');
            
            $table->timestamps();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('destinations',function($table){
        
            if(Schema::hasColumn('destinations','seo_meta_desc'))
            {
                $table->dropColumn('seo_meta_desc');
            }
            
            if(Schema::hasColumn('destinations','seo_meta_kw'))
            {
                $table->dropColumn('seo_meta_kw');
            }
            
            if(Schema::hasColumn('destinations','seo_page_title'))
            {
                $table->dropColumn('seo_page_title');
            }
            
            if(Schema::hasColumn('destinations','slug'))
            {
                $table->dropColumn('slug');
            }
            
            if(Schema::hasColumn('destinations','published'))
            {
                $table->dropColumn('published');
            }
            
        });
	}

}
