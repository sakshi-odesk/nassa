<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('packages',function($table){
        
            $table->increments('id');
            $table->integer('package_category_id');
            $table->enum('hotel_level',array('budget','deluxe','luxury','standard'));
            $table->string('title');
            $table->string('slug');
            $table->string('price');
            $table->string('route_map');
            $table->string('duration');
            $table->boolean('special_offer');
            $table->string('main_image');
            $table->boolean('published');
            
            $table->timestamps();
            
            $table->unique('slug');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('packages');
	}

}
