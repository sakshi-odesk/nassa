<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDestinationIdInPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('packages',function($table)
        {
            $table->integer('destination_id')->after('package_category_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('packages',function($table)
        {
            if(Schema::hasColumn('packages','destination_id'))
            {
                $table->dropcolumn('destination_id');
            }
        });
	}

}
