<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDestinationIdInHotels extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotels',function($table)
        {
            $table->integer('destination_id')->after('slug');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotels',function($table)
        {
            if(Schema::hasColumn('hotels','destination_id'))
            {
                $table->dropcolumn('destination_id');
            }
        });
	}

}
