<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInCampaginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns',function($table){
        
            $table->string('redirect')->after('background_image')->nullable();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaigns',function($table){
            
            if(Schema::hasColumn('campaigns','redirect'))
            {
                $table->dropColumn('redirect');
            }
            
        });
	}

}
