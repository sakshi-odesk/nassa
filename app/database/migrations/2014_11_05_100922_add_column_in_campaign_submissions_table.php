<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInCampaignSubmissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaign_submissions',function($table){
        
            if(Schema::hasColumn('campaign_submissions','published'))
            {
                $table->dropColumn('published');
            }
            
            $table->boolean('subscribe')->nullable()->after('best_time_to_call');
            $table->integer('campaign_id')->nullable()->after('id');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
