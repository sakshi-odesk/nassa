<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugFieldInCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotel_categories',function($table){
        
            $table->string('slug')->after('title');
            
        });
        
        Schema::table('blog_categories',function($table){
        
            $table->string('slug')->after('title');
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotel_categories',function($table){
        
            if(Schema::hasColumn('hotel_categories','slug'))
            {
                $table->dropColum('slug');
            }
            
        });
        
        Schema::table('blog_categories',function($table){
        
            if(Schema::hasColumn('blog_categories','slug'))
            {
                $table->dropColum('slug');
            }
            
        });
	}

}
