<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNassaNewPopularDestinationDestinationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('destinations', function(Blueprint $table)
		{
			$table->boolean('popular_destination')->after('country')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('destinations', function(Blueprint $table)
		{
			  $table->dropColumn('popular_destination');
        
		});
	}

}
