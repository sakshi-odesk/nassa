<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageColumnInFacilities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('facilities',function($table){
            
            $table->string('main_image')->after('title')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('facilities',function($table){
        
            if(Schema::hasColumn('facilities','main_image'))
            {
                $table->dropColumn('main_image');
            }
            
        });
	}

}
