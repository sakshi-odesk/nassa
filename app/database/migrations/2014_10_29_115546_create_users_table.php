<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table) 
        {
			$table->increments('id');
			$table->string('email', 255);
			$table->string('password', 255);
			$table->datetime('last_login')->nullable();
			$table->datetime('activated_at')->nullable();
			$table->boolean('active');
			$table->string('role', 255)->nullable();
			$table->string('remember_token', 255)->nullable();
			$table->string('reset_password_code', 255)->nullable();
			$table->timestamps();
            
            $table->index('email');
            $table->index('active');
            $table->unique('email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
