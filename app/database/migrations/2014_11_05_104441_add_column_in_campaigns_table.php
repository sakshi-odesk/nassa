<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInCampaignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaigns',function($table){
        
            if(Schema::hasColumn('campaigns','backgruond_image'))
            {
                $table->dropColumn('backgruond_image');
            }
            
            $table->string('background_image')->after('desc');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
