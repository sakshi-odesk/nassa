<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWeightInPriceSummariesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('package_price_summaries',function($table){
        
            $table->integer('weight')->nullable()->after('cost');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('package_price_summaries',function($table){
        
            if(Schema::hasColumn('package_price_summaries','weight'))
            {
                $table->dropColumn('weight');
            }
            
        });
	}

}
