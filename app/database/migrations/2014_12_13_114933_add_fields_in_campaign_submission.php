<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInCampaignSubmission extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('campaign_submissions',function($table){
        
            $table->string('referer')->after('subscribe')->nullable();
            $table->string('request')->after('referer')->nullable();
            $table->string('user_agent')->after('request')->nullable();
            $table->string('ip')->after('user_agent')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('campaign_submissions',function($table){
        
            if(Schema::hasColumn('campaign_submissions','referer'))
            {
                $table->dropColumn('referer');
            }
            if(Schema::hasColumn('campaign_submissions','request'))
            {
                $table->dropColumn('request');
            }
            if(Schema::hasColumn('campaign_submissions','user_agent'))
            {
                $table->dropColumn('user_agent');
            }
            if(Schema::hasColumn('campaign_submissions','ip'))
            {
                $table->dropColumn('ip');
            }
            
            
        });
	}

}
