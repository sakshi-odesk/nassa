<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultPriceInPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('packages',function($table){
        
            $table->decimal('default_price',10,3)->after('price')->nullable();
            $table->decimal('default_discounted_price',10,3)->after('default_price')->nullable();
        
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('packages',function($table){
        
            $table->dropColumn('default_price');
            $table->dropColumn('default_discounted_price');
            
        });
		
	}

}
