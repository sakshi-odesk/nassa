<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetPackageIdColumnNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE package_inclusions MODIFY package_id int null');
		DB::statement('ALTER TABLE package_hotel_details MODIFY package_id int null');
		DB::statement('ALTER TABLE package_itineraries MODIFY package_id int null');
		DB::statement('ALTER TABLE package_price_summaries MODIFY package_id int null');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
