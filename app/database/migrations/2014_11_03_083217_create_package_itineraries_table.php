<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageItinerariesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('package_itineraries',function($table){
        
            $table->increments('id');
            $table->integer('package_id');
            $table->string('day_num');
            $table->string('destination');
            $table->text('desc');
            $table->integer('weight');
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('package_itineraries');
	}

}
