<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hotels',function($table){
        
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->integer('hotel_category');
            $table->string('location');
            $table->string('price');
            $table->string('main_image');
            $table->boolean('special_offer');
            $table->text('body');
            $table->text('seo_meta_desc');
            $table->text('seo_meta_kw');
            $table->string('seo_page_title');
            
            $table->timestamps();
            
            $table->unique('slug');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('hotels');
	}

}
