<?php

class DestinationTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    
        $world = Destination::create(array('label' => 'World',
                                           'slug'=>'world',
                                           'seo_meta_desc'=>'world desc',
                                           'seo_meta_kw'=>'world keyword',
                                           'seo_page_title'=>'world',
                                           'num_children'=>1));
        
        $india = $world->addChild(new Destination(array('label' => 'India',
                                           'slug'=> 'india',
                                           'country'=>'India',
                                           'type'=>'country',
                                           'seo_meta_desc'=>'India desc',
                                           'seo_meta_kw'=>'india keyword',
                                           'seo_page_title'=>'india',
                                           'num_children'=>2)));
        
        $rajasthan = $india->addChild(new Destination(array('label'=>'Rajasthan',
                                        'type'=> 'state',
                                        'state'=>'Rajasthan',
                                        'country' => 'India',
                                        'num_children'=> 2)));
        
        $jaipur = $rajasthan->addChild(new Destination(array('label' => 'Jaipur',
                                         'type' => 'city',
                                         'state'=> 'Rajasthan',
                                         'city'=>'Jaipur',
                                         'country' => 'India',
                                         'num_children' => 2)));
        
        $udaipur = $rajasthan->addChild(new Destination(array('label' => 'Udaipur',
                                                             'type' => 'city',
                                                             'state'=>'Rajasthan',
                                                             'city' =>'Udaipur',
                                                             'country' => 'India')));
        
        $madhyapradesh = $india->addChild(new Destination(array('label'=>'MadhyaPradesh',
                                        'type'=> 'state',
                                        'state'=>'MadhyaPradesh',
                                        'country' => 'India',
                                        'num_children' => 2)));
        
        $ujjain = $madhyapradesh->addChild(new Destination(array('label' => 'Ujjain',
                                         'type' => 'city',
                                         'state'=> 'MadhyaPradesh',
                                         'city'=>'Ujjain',
                                         'country' => 'India',
                                         'num_children' => 1)));
        
        $indore = $madhyapradesh->addChild(new Destination(array('label' => 'Indore',
                                         'type' => 'city',
                                         'state'=> 'MadhyaPradesh',
                                         'city'=>'Indore',
                                         'country' => 'India',
                                         'num_children' => 1)));
        
        $amerfort = $jaipur->addChild(new Destination(array('label' => 'AmerFort',
                                         'type' => 'pin',
                                         'pin'=> 302001,
                                         'state'=> 'Rajasthan',
                                         'city'=>'Jaipur',
                                         'country' => 'India')));
        
        $hawamahal = $jaipur->addChild(new Destination(array('label' => 'HawaMahal',
                                         'type' => 'pin',
                                         'pin'=> 302001,
                                         'state'=> 'Rajasthan',
                                         'city'=>'Jaipur',
                                         'country' => 'India')));
        
        $mahakal = $ujjain->addChild(new Destination(array('label' => 'Mahakal',
                                         'type' => 'pin',
                                         'pin'=> 456001,
                                         'state'=> 'MadhyaPradesh',
                                         'city'=>'Ujjain',
                                         'country' => 'India')));
        
        $rajwada = $indore->addChild(new Destination(array('label' => 'Rajwara',
                                         'type' => 'pin',
                                         'pin'=> 452001,
                                         'state'=> 'MadhyaPradesh',
                                         'city'=>'Indore',
                                         'country' => 'India')));
        
	}

}
