<?php

class Util
{
    
    /**
     * Function for uploading attachment
     * take $file,$destination_file,$prefix,$dir as argument. 
     * 
     * @return upload file path.
     */
    
    public static function upload_file($file,$destination_file,$prefix=null,$dir=null)
    {   
        // define empty path.
        $path = '';
        
        // check if $file exists.
        if($file)
        {
            // check dir is not empty.
            if(!empty($dir))
            {   
                //define $dir path into $path.
                $path = $dir;
            }
            else
            {
               //define path.
               $path = 'upload'.DIRECTORY_SEPARATOR.date('Y').DIRECTORY_SEPARATOR.date('m').DIRECTORY_SEPARATOR.date('d');
            }
            
            //check $prefix is not empty.
            if(!empty($prefix))
            {
                //define path with prefix.
               $path = $prefix.DIRECTORY_SEPARATOR.$path;
            }
            
            // Upload file in a public path folder
            $upload_success = $file->move(public_path().DIRECTORY_SEPARATOR.$path,$destination_file);
            $upload_success = str_replace(public_path().DIRECTORY_SEPARATOR,'',$upload_success);
            
            // return upload file path.
            return str_replace('\\', '/', $upload_success);
        }
    }
    
    /**
     * function for generating unique file name based on timestamp.
     * @param $ext fileextension.
     * 
     * @return string filename.
     */
    
    public static function rand_filename($ext=null)
    {
        if($ext)
        {
            return time().'-'.'1'.rand(0,999999999).'.'.$ext;
        }
        else
        {
            return time().'-'.'1'.rand(0,999999999);
        }
    }
    
    /**
     * Create unique slug based on the page title
     * @param string $str string for which slug would be generated
     * @param  string $model_name model for which slug would be generated
     * @param string $slug_field Column name for slug.
     * 
     * 
     * @return string
     */
    
    public static function uniqueSlug($str,$model_name,$slug_field='slug')
    {        
        $slug = Str::slug($str);

        $slug_incrementor = 1;
        
        // If slug already exixts
        if( $model_name::whereRaw("$slug_field = ?", array(Str::slug($str)))->count())
        {
           /* Loop through slug incrementor to check if the combination of
            * slug and slug incrementor is unique
            * 
            * i.e
            * 
            * If existing-title-slug exists then we check if 
            *   existing-title-slug-1, existing-title-slug-2 etc. are unique.
            * First unique combination of slug and slug incrementor will be returned.
            */
            
            while ($model_name::whereRaw("$slug_field = ?", array($slug))->count())
            {
                $slug = Str::slug($str).'-'.$slug_incrementor;
                $slug_incrementor++;
            }
            
            return $slug;
        }  
            
        return $slug;       
    }
    
    public static function show_slideshow($slideshow_title)
    {
        $slideshow = SlideShow::where('title','=',$slideshow_title)->first();
        
        if($slideshow)
        {
            $slides = $slideshow->slides()->getResults();

            // slide show html.

            if($slides->count())
            {
                $slideshow_html = '<ul class="bxslider">';

                foreach($slides as $slide)
                {
                    $slideshow_html = $slideshow_html . '<li><a href="'.$slide->url.'">'.
                                        '<img src="'.$slide->main_image.'"/></a>'.
                                        '</li>';
                }

                $slideshow_html = $slideshow_html . '</ul>';

                return $slideshow_html;    
            }
        }
        
            return 'SlideShow '.$slideshow_title. ' is emepty';
    }
  
    public static function url_to_page($title)
    {
        $page = Page::where('title',$title)->first();
       
        if($page)
        {
            return URL::to("/".$page->slug);
        }
        else
        {
            return URL::to("/");
        }

    }
    
}