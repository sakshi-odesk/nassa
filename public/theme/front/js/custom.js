 (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TXGHBZ');



(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-48724371-1', 'auto');
ga('send', 'pageview');

window.onload = function(){
    new JsDatePick({
        useMode:2,
        target:"inputField",
        dateFormat:"%d-%M-%Y"
    });

    new JsDatePick({
        useMode:2,
        target:"inputField1",
        dateFormat:"%d-%M-%Y"
    });
};

function getDomestic()
{
    window.location = '/domestic-packages/'+$('#cat').val(); 
}

function getInternational()
{
    window.location = '/international-packages/'+$('#cat').val(); 
}

function getHotel()
{
    window.location = '/hotels/'+$('#cat').val(); 
}

$(document).ready(function(e){
$('.bxslider').bxSlider({
  mode: 'horizontal',
  captions: false,
  auto: true
});

});


function dom()
{
document.getElementById('tabs').style.background="url(/theme/front/images/tip1.png) no-repeat";
document.getElementById('dd').style.display='inherit';
document.getElementById('ii').style.display='none';
document.getElementById('hh').style.display='none';

}

function inter()
{
document.getElementById('tabs').style.background="url(/theme/front/images/tip2.png) no-repeat";
document.getElementById('dd').style.display='none';
document.getElementById('ii').style.display='inherit';
document.getElementById('hh').style.display='none';
}


function hot()
{
document.getElementById('tabs').style.background="url(/theme/front/images/tip3.png) no-repeat";
document.getElementById('dd').style.display='none';
document.getElementById('ii').style.display='none';
document.getElementById('hh').style.display='inherit';
}

widget.init("b7c6bf51e1005c21f2e806602d8d2b0d","5405b289e4b04b06e7b53bac");
widget.load();

