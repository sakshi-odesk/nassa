$(document).ready(function () { 
    
    if ($('#input_hidden').val() == 'text') 
    {
        $('#input_text').show();
        $('#input_text').attr('name','value');
        $('#input_textarea').hide();
        $('#input_file').hide();
     }

    if ($('#input_hidden').val() == 'textarea') {
        $('#input_textarea').show();
        $('#input_textarea').attr('name','value');
        $('#input_text').hide();
        $('#input_file').hide();
     }

    if ($('#input_hidden').val() == 'file') {
        $('#input_file').show();
        $('#input_file').attr('name','value');
        $('#input_text').hide();
        $('#input_textarea').hide();
     }
    
    $('#type').bind('change keyup', function () {
        
        if ($(this).val() == 'text') {
            $('#input_text').show();
            $('#input_text').attr('name','value');
            $('#input_textarea').hide();
            $('#input_file').hide();
        }
        
        if ($(this).val() == 'textarea') {           
            $('#input_textarea').show();
            $('#input_textarea').attr('name','value');
            $('#input_text').hide();
            $('#input_file').hide();
        }

        if ($(this).val() == 'file') {
            $('#input_file').show();
            $('#input_file').attr('name','value');
            $('#input_text').hide();
            $('#input_textarea').hide();
        }
    });
  
  
//  function package_validate()
//  {
//    $("#package_type_id").rules("add",{required:false});
//  }
  
 var $product_validator = $("#package_form").validate({

      errorPlacement: function (error, element) {
          error.appendTo(element.closest('.form-group'));
      },

  });
  
  $('#submit_package').click(function () {
      var $valid = $("#package_form").valid();
      if (!$valid) {
          $product_validator.focusInvalid();
          return false;
      }
    
    });
  
  
    
});